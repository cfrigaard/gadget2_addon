const string outputformat_density="[delta]=dimless [rho]=" +  UNIT_DENSITY;
typedef pair<size_t,size_t> t_gridparameters;
typedef map<const particle_data_2*,bool> t_searched;

_vol VolSphere(const _len& r){return 4*_pi*r*r*r/3;}

void Densitymap(const data_info& inf
		,const vector<particle_data_2>& v
		,const size_t N
		,const _bbox_len bbox
		,const bool calcrho
		,const t_gridparameters& gridparameters
		,const bool searchinradius
		,const bool cic
		,const _len r_sis
		,const string& gadgetfile)
{
	if( !(N >2 && bbox()>_len(1)) ) throw_("bad parameters");

	const bool sis=r_sis>_len(0);
	const _len delta_r=bbox()/(N);
	const _per_len 	delta_k = 2*_pi/bbox();
	const _mass masstot = inf.header1.masstotal();
	const _vol       V=bbox()*bbox()*bbox();
	const _vol delta_V=delta_r*delta_r*delta_r;
	const _density rho_mean=masstot/V;
	const range<_len> r_rho(_len(0),bbox(),N);
	const _len  l=delta_r; //r_rho.approxstep();
	const _len lhalf=delta_r/2;
	const _len maxdistfromcenter=Sqrt<_len2,_len>(3*lhalf*lhalf);

	const size_t gridsize=gridparameters.first;
	const size_t steps   =gridparameters.second;

	Mpi mpi;
	if (mpi.isMaster()){
		cout << "% Calculating density map\n";
		cout << "%   parameters:  N=" << N << " bbox=" << bbox << "  gridsize=" << gridsize << " steps=" << steps;
		if (searchinradius) cout << " searchinradius=true";
		if (cic) cout << " cic=true";
		if (sis) cout << " r_sis=" << r_sis << " " << UNIT_LENGTH;
		cout << "\n%   Simulation data:\n";
		cout << "%     datasize=" << inf.NumPart << "\n";
		cout << "%     delta r =" << delta_r << " " << UNIT_LENGTH << "\n";
		cout << "%     delta k =" << delta_k << " " << UNIT_INV_LENGTH << "\n";
		cout << "%     masstot =" << masstot << " " << UNIT_MASS << "\n";
		cout << "%     V       =" << V       << " " << UNIT_VOL << "\n";
		cout << "%     delta V =" << delta_V << " " << UNIT_VOL << "\n";
		cout << "%     rho_mean=" << rho_mean<< " " << UNIT_DENSITY << endl;
	}

	array3d<_density> rho(N,N,N,mpi.isMaster()); // only alloc for master
	#ifndef NDEBUG
		if (mpi.isMaster()) rho=_density(-3);
	#endif
	assert( rho.isSquare() && gridsize>0 );

	_mass masssum(0);
	size_t i=0;
	timer tim;

	if (cic){
		if (mpi.Size()>1) {
			warn_("cic/sis algorithm does not use multiprocessor mode yet, slaves returning...");
			if (!mpi.isMaster()) return;
		}

		rho=_density(0);
		for(vector<particle_data_2>::const_iterator itt=v.begin();itt!=v.end();++itt,++i){
			const t_idx i0=r_rho.toidx(itt->pos());
			const array3d<flt> w=r_rho.weightfactors(itt->pos());
			assert( w.size()==8 );
			const _mass m=itt->mass();
			_mass mt(0);

			for(size_t k=0;k<2;++k)
			for(size_t j=0;j<2;++j)
			for(size_t i=0;i<2;++i) {
				t_idx i2=(i0+t_idx(i,j,k)).periodic(r_rho.size());
				const _mass mw=m * w(i,j,k);
				rho[i2] += mw / delta_V;
				masssum += mw;
				mt += mw;
			}
			if (!_proximy(mt,m,1E-6)) warn_("mismatch in submass calculation, mt="
			     + tostring(mt) + " m=" + tostring(m) );
			tim.ToEta(i++,v.size(),cerr);
		}

		const _mass t=rho.sum()*delta_V;
		if (!_proximy(t,masssum,1E-6)){
			warn_("mismatch in cic masses, t=" + tostring(t) + " masssum=" + tostring(masssum));
		}
	}
	else{
		const size_t rhoN=rho.size();
		const size_t lower=mpi.Lower(rhoN);
		const size_t upper=mpi.Upper(rhoN);

		mpi << "%   creating search grid..." << mpiendl;
		searchgrid s(v,_len(0),bbox(),gridsize,true);

		mpi << "%   finding smoothing lenghts..." << mpiendl;
		FindSmoothingLengths(v,s,33,mpi.isMaster(),gadgetfile,mpi.isMaster());
		if (searchinradius) {
			mpi <<"%   adding smoothlength data to searchcube..." << mpiendl;
			s.Addsmoothinglenghts();
		}

		mpi << "%   finding density..." << mpiendl;

		t_searched searched;
		vector<_mass> masses;
		array3d<_density>::iterator itt1(rho);
		for(itt1=lower;itt1!=upper;++itt1)
		{
			const t_idx   idx=itt1.toidx();
			const _point_len p=r_rho.totype(idx);
			_mass massbox(0);

			if (searchinradius){
				const searchgrid::t_map_data m=s.find_withinradius(p+lhalf,maxdistfromcenter,true);
				searched.clear();

				for(searchgrid::t_map_data::const_iterator itt2=m.begin();itt2!=m.end();++itt2){
					const particle_data_2* d=itt2->second.first;
					t_searched::const_iterator itt3=searched.find(d);
					if (itt3==searched.end()){
						massbox += FindMassInBox(p,l,*d,bbox,steps,true);
						searched[d]=true;
					}
				}
			} else{
				for(vector<particle_data_2>::const_iterator itt2=v.begin();itt2!=v.end();++itt2){
					if (sis) massbox += FindMassInSphere(p,r_sis,*itt2,bbox,steps,false,true);
					else     massbox += FindMassInBox   (p,l,*itt2,bbox,steps,false);
				}
			}
			masses.push_back(massbox);
			if(mpi.isMaster()) tim.ToEta(i++,upper-lower,cerr);
		}

		// collect data from nodes
		// if (mpi.Size()>1) mpi << "%  node=" << tostring(mpi) << " is done..." << mpiendl;

		assert( masses.size()==upper-lower );
		timer tim2;
		mpi.CollectVector(masses,rhoN);
		if (mpi.isMaster()){
			if (mpi.Size()>1) mpi << "%  master collection done, collection time="
			                      << timer::ToHMS(tim2.elapsed()) << mpiendl;
			assert( rho.size()==masses.size() );
			const _vol v=sis ? VolSphere(r_sis) : delta_V;
			for(size_t i=0;i<rhoN;++i) {
				assert( rho[i]<-_density(-2) );
				rho[i]=masses[i]/v;
				masssum += masses[i];
			}
		} else return;
	}

	if(!calcrho){
		for(array3d<_density>::iterator itt=rho.begin();itt!=rho.end();++itt){
			const flt delta=*itt/rho_mean-1;
			*itt=_density(delta); // fake units, dimless really
		}
	}
	cout << "% Density map: array(x,y,z) of " << (calcrho ? "rho " : "delta=rho/rho0-1 ")
	      << outputformat_density << "\n";
	cout << rho;

	if (masssum>1.01*masstot) warn_(string("integrated mass greater that total mass,  masssum=")
	     + masssum  + " masstot=" + masstot);
	if( !_proximy(masssum,masstot,1E-5) ) warn_(string("summed and total mass mismatch, masssum=")
	     + masssum  + " masstot=" + masstot);
}

int Usage_Density(const args& a)
{
	cerr << "Usage: " << a[0] << " <inputfile> <N> [-f <filter>] [-cic] [-r] "
	                  << "[-s <grid> <step>] [-sis <radius>] [-sr]\n";
	cerr << "  " << Version() <<  " " << Config() <<  " \n";
	cerr << "  N: density resolution\n";
	cerr << "Options:\n";
	cerr << "  -f <int>: 1=gas, 2=halo, 4=disk, 8=bulge, 16=stars, 32=bndry\n";
	cerr << "  -cic: use Cloud-In-Cell algorithm, implies no use of -sr and -sis\n";
	cerr << "  -r: calculate rho, not delta=rho/mean(rho)-1\n";
	cerr << "  -s <int> <int>: search grid size and integration steps, default=20 20\n";
	cerr << "  -sis <float " << UNIT_LENGTH << ">: use Sphere-In-Sphere algorithm of radius r,"
	     << " implies no use of -sr and -cic\n";
	cerr << "  -sr: use seach in radius algorithm\n";
	cerr << "Output\n  to stdout: 3D ascii table of density\n";
	cerr << "  units: " << outputformat_density << "\n";
	return -1;
}

int main_density(int argc,char** argv)
{
	try{
		args a(argc,argv,true,true);
		const bool cic=a.parseopt("-cic");
		const int  filter =a.parseval<int>("-f",0);
		const bool calcrho=a.parseopt("-r");
		const t_gridparameters gridparameters=a.parseval<size_t,size_t>("-s",20,20);
		const bool searchinradius=a.parseopt("-sr");
		const _len r_sis=a.parseunit<_len>("-sis",_len(-1));

 		if(a.size()!=3) return Usage_Density(a);
		const size_t N =a.Totype<int> (2);
		if (gridparameters.first<=0 || gridparameters.second<=0)
		   throw_("bad step parameter, must be greater than zero");
		if (cic && r_sis>_len(0)) throw_("cannot specify -cic and -sis at same time");
		if ((cic || r_sis>_len(0)) && searchinradius)
		   warn_("-cic/sis implies no use of -sr, option -sr ignored");

		const data_info inf=load_snapshot(a[1].c_str(),1,true,true);
		const data_info inf2=FilterParticles(inf,filter);
		const _bbox_len bbox=inf2.header1.bbox();
		Densitymap(inf2,ToVectorData(inf2),N,bbox,calcrho,gridparameters,searchinradius,cic,r_sis,filter==0 ? a[1] : "");

		return 0;
	}
	CATCH_ALL;
	return -1;
}
