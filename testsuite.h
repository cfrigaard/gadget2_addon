#ifndef __TESTSUITE_H__
#define __TESTSUITE_H__

#define test(expr) test(expr,#expr,__FILE__, __LINE__)
#define TEST_EXCEPTION(a) if(test.Exceptions()){bool b=false; try{a;} catch(...) {b=true;} test.operator()(b==true,#a,__FILE__,__LINE__);}

#define Proximy(expr,x0)                _proximy(expr,x0,#expr,__FILE__, __LINE__,1E-12,true)
#define Proximyeps(expr,x0,eps)         _proximy(expr,x0,#expr,__FILE__, __LINE__,eps  ,true)
#define Proximyeps_noprint(expr,x0,eps) _proximy(expr,x0,#expr,__FILE__, __LINE__,eps  ,false)

class Testsuite
{
  private:
    int m_testcases;
    int m_failed;
	int m_verboselevel;
	int m_precision;

    bool m_enabled;
    bool m_exceptions;
    bool m_verbose;

	vector<bool> m_bools;

	// test streams
    ostream& m_cout;
    ostream& m_cerr;

	// temporaries
    string m_header;
    string m_description;
    string m_sub;
    string m_subsub;

    double m_time;
    double m_start;

  public:
    Testsuite(ostream& s_cout,ostream& s_cerr,const int precision=6)
		: m_testcases(0),  m_failed(0),        m_verboselevel(0),  m_precision(precision)
		, m_enabled(true), m_exceptions(true), m_verbose(false)
    	,m_cout(s_cout),   m_cerr(s_cerr) ,    m_time(-1), m_start(Time())
	{
		m_cout.precision(m_precision);
		m_cerr.precision(m_precision);
	}

    void Enable 	()       {m_enabled=true;}
    void Disable	() 	     {m_enabled=false;}
    void Reset  	() 		 {m_testcases=m_failed=0;}
    bool Verbose	() const {return m_verbose;}
 	bool Exceptions	() const {return m_exceptions;}
	int  Precision	() const {return m_precision;}

	void Setverbose		(const bool b) {m_verbose=b;}
    void Setexceptions	(const bool b) {m_exceptions=b;}
	void SetVerboseLevel(const int l)  {m_verboselevel=l;}
    void SetPrecision	(const int p)  {assert(p>0); m_precision=p;}

	bool Bool   (const int idx) 			 const {return m_bools.size()<static_cast<unsigned int>(idx)+1 ? false : m_bools[idx];} // static_cast due to warn in VC.net
    void Setbool(const int idx,const bool v) 	   {if (m_bools.size()<static_cast<unsigned int>(idx)+1) m_bools.resize(idx+1); m_bools[idx]=v;} // -

    void Header(const string& header,const string& description="")
    {
		if (m_enabled){
			const double t=Time(m_time);
			if (m_time> 0 && t>0.2) m_cout << "  Time: " << t << " [s]\n";
			if (header!="")         m_cout <<  header << " " << description << "\n";
		}
		m_time=Time();
		m_header=header;
		m_description=description;
		m_sub=m_subsub="";
    }

    void Sub(const string& sub)
    {
		m_sub=sub;
		m_subsub="";
		if(m_verboselevel>1) m_cout << "  Sub: " << sub << "\n";
    }

    void Subsub(const string& subsub)
    {
		m_subsub=subsub;
		if(m_verboselevel>2) m_cout << "    Subsub: " << subsub << "\n";
    }

    void operator()(const bool expr,const char* test,const char* subcase,const char* file,const int line)
    {
		Subsub(subcase);
		operator()(expr,test,file,line);
    }

    void operator()(const bool expr,const char* test,const char* file,const int line)
    {
		if (!m_enabled) return;
		++m_testcases;
		if(m_verboselevel>3) m_cout << "      case: " << test << "\n";

		if (!expr){
			++m_failed;
			const string cse= "[Main." + m_sub +  (m_subsub!="" ? "." : "" ) + m_subsub + "]";
			m_cout << file << ":" << line << ": warning: test failed: " << test << " " + cse + "\n";
			m_cerr << file << ":" << line << ": warning: test failed: " << test << " " + cse + "\n";
		}
    }

    template<class T>
    Testsuite& operator<<(const T& t)
    {
		if (m_enabled) m_cout << t;
		return *this;
    }

    ostream& cout() {return m_cout;}
    ostream& cerr() {return m_cerr;}

    int GetTestcases() const {return m_testcases;}
    int GetFailed   () const {return m_failed;}

    static double Time(const double start=0.0)
    {
		struct timeval tv;
		gettimeofday(&tv,NULL);
		return (1.0 * tv.tv_sec + 1E-6 * tv.tv_usec)-start;
    }

    void Summary() const
    {
		const double t=Time(m_start);
		m_cout << "Test summary:";
		m_cout << "\n  Time elapsed     : " << t << " [sec]";
		m_cout << "\n  Testcases        : " << GetTestcases();
		m_cout << "\n  Failed           : " << GetFailed();
		if (GetFailed()!=0) m_cerr << "** Test failed: " << GetFailed() << " **\n";
    }

	void System(const string& cmd)
	{
		int n=system(cmd.c_str());
		this->operator()(n==0,("system " +cmd).c_str(),__FILE__,__LINE__);
	}

	void DiffFile(const string& f1,const string& f2,const bool genref=false)
    {
		//const bool refexist=FileExist(f1);
		//AssertFileExist(f2));
		ifstream s(f1.c_str());
		const bool refexist=(!s)==false;

		const string cmd="diff -dw " + f1 + " " + f2;
		const int n=refexist ? system(cmd.c_str()) : -1;
		if (n!=0){
			if (genref) {
				m_cout << "Generating reference '" + f1 << "' from file '" + f2 + "'\n";
				System("cp " + f2 + " " + f1 );
			}
			else this->operator()(n==0,cmd.c_str(),__FILE__,__LINE__);
		}
    }
};

#endif // __TESTSUITE_H__
