int Usage_Gsort(const args& a)
{
	cerr << "Usage: " << a[0] << " <file>\n";
	cerr << "  " << Version() <<  " " << Config() <<  " \n";
	cerr << "  file: snapshot file to sort by id\n";
	return -1;
}

int main_gsort(int argc,char** argv)
{
	try{
		args a(argc,argv,false,false);
 		if(a.size()!=2) return Usage_Gsort(a);

		vector<particle_data_2> v;
		const data_info inf=load_snapshot(a[1].c_str(),1,false,false);
		reordering(inf,false);




		return 0;
	}
	CATCH_ALL;
	return -1;
}