#ifndef __UNITS_H__
#define __UNITS_H__

#ifdef USE_UNITS
	#define QUAN_DISALLOW_IMPLICIT_UNIT_CONVERSIONS
	#include <quan/components/of_named_quantity.hpp>
	// NOTE: if this include fails, because you do not have quan installed:
	//       disable units changing three lines above to: #ifdef USE_UNITS

#define QUAN_STRUCT_BASE(_QUAN_NAME,_QUAN_NAME_,_QUAN_TYPE,EXTENT,PREFIX_OFFSET,LENGTH,TIME,MASS,TEMPERATURE,CURRENT,SUBSTANCE,INTENCITY)\
	namespace meta { \
		namespace components { \
			\
			struct of_##_QUAN_NAME { \
				static const char* abstract_quantity_name() {return #_QUAN_NAME;}\
				template<typename CharType> static const CharType* unprefixed_symbol();\
				\
				enum{\
					extent = EXTENT,\
					prefix_offset = PREFIX_OFFSET\
				};\
				\
				typedef meta::dimension< 	\
					meta::rational<LENGTH>, \
					meta::rational<TIME>,  	\
					meta::rational<MASS>,	\
					meta::rational<TEMPERATURE>,\
					meta::rational<CURRENT>,	\
					meta::rational<SUBSTANCE>,  \
					meta::rational<INTENCITY> 	\
				> dimension;\
				\
				typedef meta::abstract_quantity<dimension,of_##_QUAN_NAME> abstract_quantity;\
				typedef of_##_QUAN_NAME type;\
			};\
			\
			template<> inline const char* of_##_QUAN_NAME::unprefixed_symbol<char>() {return #_QUAN_TYPE;} \
		} \
	}\


#define QUAN_STRUCT(_QUAN_NAME,_QUAN_NAME_,_QUAN_TYPE,EXTENT,PREFIX_OFFSET,LENGTH,TIME,MASS,TEMPERATURE,CURRENT,SUBSTANCE,INTENCITY)\
	namespace meta { \
		namespace components { \
			\
			struct of_##_QUAN_NAME { \
				static const char* abstract_quantity_name() {return #_QUAN_NAME;}\
				template<typename CharType> static const CharType* unprefixed_symbol();\
				\
				enum{\
					extent = EXTENT,\
					prefix_offset = PREFIX_OFFSET\
				};\
				\
				typedef meta::dimension< 	\
					meta::rational<LENGTH>, \
					meta::rational<TIME>,  	\
					meta::rational<MASS>,	\
					meta::rational<TEMPERATURE>,\
					meta::rational<CURRENT>,	\
					meta::rational<SUBSTANCE>,  \
					meta::rational<INTENCITY> 	\
				> dimension;\
				\
				typedef meta::abstract_quantity<dimension,of_##_QUAN_NAME> abstract_quantity;\
				typedef of_##_QUAN_NAME type;\
			};\
			\
			template<> inline const char* of_##_QUAN_NAME::unprefixed_symbol<char>() {return #_QUAN_TYPE;} \
		} \
	}\
	\
    template<typename Value_type>\
    struct _QUAN_NAME_: meta::components::of_##_QUAN_NAME {\
		typedef fixed_quantity<quan::meta::unit<abstract_quantity,typename meta::si_unit::none>,Value_type> _QUAN_TYPE;\
    };\
	\
	struct _QUAN_NAME : _QUAN_NAME_<default_value_type> {};\
	\
	typedef _QUAN_NAME::_QUAN_TYPE  _##_QUAN_TYPE;\
	\
	inline ostream& operator<<(ostream& s,const _QUAN_NAME::_QUAN_TYPE& x)\
	{\
		return s << x.numeric_value() << " " << _QUAN_NAME::unprefixed_symbol<char>();\
	}\
	inline istream& operator>>(istream& s,_QUAN_NAME::_QUAN_TYPE& x)\
	{\
		default_value_type v;\
		string unit;\
		const string sym=_QUAN_NAME::unprefixed_symbol<char>();\
		s >> v >> unit;\
		if (unit!=sym) throw_("bad unit in operator>>, expceted <" + sym + "> unit, got <" + unit + ">");\
		x=_QUAN_NAME::_QUAN_TYPE(v);\
		return s;\
	}\
	inline string operator+ (const string& s,const _QUAN_NAME::_QUAN_TYPE& x) {return s+tostring(x);}


#define QUAN_CONVERSION_FACTOR(_QUAN_NAME,_QUAN_NAME_,_QUAN_TYPE,_QUAN_CONV_NAME,f1,f2,f3)\
	namespace meta {\
		namespace components {\
				struct non_standard_conversion_##_QUAN_NAME {\
		    		typedef meta::conversion_factor<meta::rational<f1>,meta::rational<f2,f3>,boost::mpl::int_<0> > _QUAN_CONV_NAME;\
			};\
		}\
	}\
	template<typename Value_type>\
    struct _QUAN_NAME_: meta::components::of_##_QUAN_NAME {\
		typedef fixed_quantity<quan::meta::unit<abstract_quantity,typename meta::si_unit::none>,Value_type> _QUAN_TYPE;\
		typedef fixed_quantity<quan::meta::unit<abstract_quantity,meta::components::non_standard_conversion_##_QUAN_NAME::_QUAN_CONV_NAME>,Value_type> _QUAN_CONV_NAME;\
    };\
	struct _QUAN_NAME : _QUAN_NAME_<default_value_type> {};\
	\
	typedef _QUAN_NAME::_QUAN_TYPE  _##_QUAN_TYPE;\
	\
	inline ostream& operator<<(ostream& s,const _QUAN_NAME::_QUAN_TYPE& x)\
	{\
		return s << x.numeric_value() << " " << _QUAN_NAME::unprefixed_symbol<char>();\
	}\
	inline istream& operator>>(istream& s,_QUAN_NAME::_QUAN_TYPE& x)\
	{\
		default_value_type v;\
		string unit;\
		const string sym=_QUAN_NAME::unprefixed_symbol<char>();\
		s >> v >> unit;\
		if (unit!=sym) throw_("bad unit in operator>>, expceted <" + sym + "> unit, got <" + unit + ">");\
		x=_QUAN_NAME::_QUAN_TYPE(v);\
		return s;\
	}\
	inline string operator+ (const string& s,const _QUAN_NAME::_QUAN_TYPE& x) {return s+tostring(x);}

#define QUAN_IO_OPERATORS(_QUAN_NAME,_QUAN_NAME_,_QUAN_TYPE)\
	inline ostream& operator<<(ostream& s,const _QUAN_NAME::_QUAN_TYPE& x)\
	{\
		return s << x.numeric_value() << " " << _QUAN_NAME::unprefixed_symbol<char>();\
	}\
	inline istream& operator>>(istream& s,_QUAN_NAME::_QUAN_TYPE& x)\
	{\
		double v;\
		string unit;\
		const string sym=_QUAN_NAME::unprefixed_symbol<char>();\
		s >> v >> unit;\
		if (unit!=sym) throw_("bad unit in operator>>, expceted <" + sym + "> unit, got <" + unit + ">");\
		x=_QUAN_NAME::_QUAN_TYPE(v);\
		return s;\
	}\
	inline string operator+ (const string& s,const _QUAN_NAME::_QUAN_TYPE& x) {return s+tostring(x);}

	typedef flt default_value_type;

	namespace quan {
		// Note: base unit INTENCITY emulate factors of h
		//                                                          base units:  l, t, m, , , ,  h
		QUAN_STRUCT(length,length_,len                                 ,  1,0,   1, 0, 0,0,0,0, -1);
		QUAN_STRUCT(area,area_,len2								 	   ,  2,0,   2, 0, 0,0,0,0, -2);
		QUAN_STRUCT(volume,volume_,len3                                ,  3,0,   3, 0, 0,0,0,0, -3);
		QUAN_STRUCT(length32,length32_,len32                           ,3/2,0, 3/2, 0, 0,0,0,0, -3/2);

		QUAN_STRUCT(reciprocal_length, reciprocal_length_ ,per_len     , -1,0,  -1, 0, 0,0,0,0,  1);
		QUAN_STRUCT(reciprocal_length2,reciprocal_length2_,per_len2    , -2,0,  -2, 0, 0,0,0,0,  2);
		QUAN_STRUCT(reciprocal_volume ,reciprocal_volume_ ,per_len3    , -3,0,  -3, 0, 0,0,0,0,  3);
		QUAN_STRUCT(reciprocal_length32,reciprocal_length32_,per_len32,-3/2,0,-3/2, 0, 0,0,0,0,  3/2);

		QUAN_STRUCT(mass_unit,mass_unit_,mass                      	   ,  1,0,   0, 0, 1,0,0,0, -1);
		QUAN_STRUCT(mass_unit2,mass_unit_2,mass2                   	   ,  1,0,   0, 0, 2,0,0,0, -2);
		QUAN_STRUCT(reciprocal_mass,reciprocal_mass_,per_mass          ,  1,0,   0, 0,-1,0,0,0,  1);

		QUAN_STRUCT(time_unit,time_unit_,time                     	   ,  1,0,   0, 1, 0,0,0,0, -1);
		QUAN_STRUCT(reciprocal_time,reciprocal_time_,per_time      	   ,  1,0,   0,-1, 0,0,0,0,  1);

		QUAN_STRUCT(length_per_sec,length_per_sec_,vel           	   ,  1,0,   1,-1, 0,0,0,0,  0);
		QUAN_STRUCT(acceleration,acceleration_,acc               	   ,  1,0,   1,-2, 0,0,0,0,  1);
		QUAN_STRUCT(force_unit,force_unit_,force                  	   ,  1,0,   1,-2, 1,0,0,0,  0);
		QUAN_STRUCT(density_unit,density_unit_,density         	       ,  1,0,  -3, 0, 1,0,0,0,  2);
		QUAN_STRUCT(potential_unit,potential_unit_,potential       	   ,  1,0,   2,-2, 0,0,0,0,  0);

		QUAN_STRUCT(gravitational,gravitational_,gravitational_const   ,  1,0,   3,-2,-1,0,0,0,  0);

		QUAN_STRUCT(hubblefactor,hubblefactor_,h               	          ,0,0,0,0,0,0,0,0, 1);
		QUAN_STRUCT(reciprocal_hubblefactor,reciprocal_hubblefactor_,per_h,0,0,0,0,0,0,0,0,-1);
    } //quan

	using namespace quan;

namespace GadgetUnitSystem {

	template<class T> inline flt tonum(const T& x){return x.numeric_value();}

}

#else // not USE_UNITS

namespace GadgetUnitSystem {

	typedef flt _len;
	typedef flt _len2; // alias area
	typedef flt _len3; // alias vol
	typedef flt _len32; // len^(3/2)
	typedef flt _per_len;
	typedef flt _per_len2; // alias per area
	typedef flt _per_len3; // alias per vol
	typedef flt _per_len32; // 1/(len^(3/2)
	typedef flt _mass;
	typedef flt _mass2;
	typedef flt _per_mass;

	typedef flt _time;
	typedef flt _per_time;

	typedef flt _vel;
	typedef flt _acc;
	typedef flt _force;
	typedef flt _density;
	typedef flt _potential;

	typedef flt _gravitational_const;

	typedef flt _h;
	typedef flt _per_h;

	#define tonum(x) (x)
}
#endif // USE_UNITS

namespace GadgetUnitSystem
{
	template<class T> class Units {
		private:
		enum { LENGTH=0, MASS=1, TIME=2, UNIT_BASES=3};

		vector<T> m_si;
		vector<string> m_n;
		_gravitational_const m_G;
		bool m_h;

		void CheckConsistency() const
		{
			if (m_si.size()!=UNIT_BASES) throw_("bad unit, needs three base units");
			if (m_si.size()!=m_n.size()) throw_("bad unit, name vector not equal to si vector");
			for(unsigned int i=0;i<UNIT_BASES;++i){
				if (m_si[i]<=0)       throw_("bad unit, cannot be less that zero");
				if (m_n[i].size()==0) throw_("bad unit name, cannot be empty");
			}
			if (m_G<=_gravitational_const(0)) throw("bad gravitational constant, cannot be less that zero");
			if (!_proximy(m_G,CalcG(m_si),1E-4)) warn_(string("mismatch in G and calculated G, G=") + m_G + " CalcG=" + CalcG(m_si));
		}

		static vector<T>  DefaultValues()
		{
			vector<T> d;
			d.push_back(Constants::_kpc);
			d.push_back(Constants::_Msun*1E10);
			d.push_back(1/(10*1*100*1000/(1000*Constants::_kpc))); // =(10*h*100*kms/(Mpc/h))) 3.0857E16);
			return d;
		}
		static vector<string> DefaultNames ()
		{
			vector<string> d;
			d.push_back("kpc/h");
			d.push_back("msun1E10/h");
			d.push_back("inv10H0/h");
			return d;
		}

		static _gravitational_const CalcG(const vector<T>& si) {assert(si.size()>=UNIT_BASES); return _gravitational_const(Constants::_G/(Pow3(si[LENGTH])/(Pow2(si[TIME])*si[MASS])));}

		public:

		Units()
		: m_si(DefaultValues()), m_n(DefaultNames()), m_G(CalcG(m_si)), m_h(true)
		{CheckConsistency();}

		Units(const vector<T>& si,const vector<string>& n,const bool use_h=true)
		: m_si(si), m_n(n), m_G(CalcG(m_si)), m_h(use_h)
		{CheckConsistency();}

		const _per_time          H0   () const {return _per_time(0.1);}                       // timeunit * 100 kms Mpc^-1 = 0.1
		const _density           rho0 () const {return H0()*H0()/(8*Constants::_pi*G()/3);}  // 2.77458477E-8 h^2
		const _gravitational_const& G () const {return m_G;}
		bool        uses_factors_of_h () const {return m_h;}
		int                      size () const {return UNIT_BASES;}

		const string  name          (const unsigned int type) const {assert(type<UNIT_BASES); return m_n [type];}
		const T&      si_equivalent (const unsigned int type) const {assert(type<UNIT_BASES); return m_si[type];}
		const string  si_name       (const unsigned int type) const
		{
			assert(type<UNIT_BASES);
			string t;
			switch(type) {
				case LENGTH : t="m";  break;
				case MASS   : t="kg"; break;
				case TIME   : t="s";  break;
				default: throw_("type out of range");
			}
			return t + (uses_factors_of_h() ? "/h" : "");
		}

		string unit_length     () const {return m_n[LENGTH];}
		string unit_time       () const {return m_n[TIME];}
		string unit_mass       () const {return m_n[MASS];}
		string unit_frequency  () const	{return uses_factors_of_h() ? "h/" + replace(unit_time()  ,"/h","") : "1/" + unit_time();}
		string unit_inv_length () const	{return uses_factors_of_h() ? "h/" + replace(unit_length(),"/h","") : "1/" + unit_length();}
		string unit_vol        () const {return "(" + unit_length() +")^3";}
		string unit_density    () const
		{
			string t=unit_mass() + "/" + unit_vol();
			if (!uses_factors_of_h()) return t;
			else return "h^2 " + replace(t,"/h","");
		}
		string unit_gravitational_const () const
		{
			string t=unit_vol() + "/(" + unit_mass() + " (" + unit_time() +")^2)";
			if (!uses_factors_of_h()) return t;
			else return replace(t,"/h","");
		}

		friend ostream& operator<<(ostream& s,const Units<T>& x)
		{
			Configfile c;
	 		c.Set("uses_h",x.uses_factors_of_h());
			#ifdef USE_UNITS
				c.Set("G",x.G());
			#else
				c.Set("G",tostring(x.G()) + " gravitational_const");
			#endif
			for(unsigned int i=0;i<UNIT_BASES;++i){
				c.Set("basename"+tostring(i),x.name(i));
				c.Set("basesiconversion"+tostring(i),tostring(x.si_equivalent(i)) + " " + x.si_name(i) );
			}
			return s << c;
		}

		friend istream& operator>>(istream& s,Units<T>& x)
		{
			Configfile c(s);
			x.m_h=c.Get<bool>("uses_h");
			x.m_G=c.Get<_gravitational_const>("G",true);
			for(unsigned int i=0;i<UNIT_BASES;++i){
				x.m_n[i] =c.Get<string>("basename"+tostring(i));
				x.m_si[i]=c.Get<flt>   ("basesiconversion"+tostring(i));
			}
			x.CheckConsistency();
			x.m_G=CalcG(x.m_si); // recalc G
			return s;
		}
	};

	// 	Param file
	//
	// 	% System of units
	//
	// 	UnitLength_in_cm         3.085678e21        ;  1.0 kpc
	// 	UnitMass_in_g            1.989e43           ;  1.0e10 solar masses
	// 	UnitVelocity_in_cm_per_s 1e5                ;  1 km/sec
	// 	GravityConstantInternal  0
	//
	// 	Gadget output
	//
	// 	Hubble (internal units) = 0.1
	// 	G (internal units) = 43007.1
	// 	UnitMass_in_g = 1.989e+43 			= 1E10 _Msun
	// 	UnitTime_in_s = 3.08568e+16 		= 1/(10 H0)
	// 	UnitVelocity_in_cm_per_s = 100000   = 1000 _m _s^-1
	// 	UnitDensity_in_cgs = 6.76991e-22	= 1E10 _Msun/_kpc^3 -> _gm _cm^-3
	// 	UnitEnergy_in_cgs = 1.989e+53 		= 1E10 _Msun * (_km s^{-1})^2 -> 1.989E53 h^{-1} ergs
	//
	// Gadget units:       Expr.                           SI value                Alt. cosmounits value
	//   lenght     g_l  =                               = 3.0857E19   h^{-1} m  = h^{-1} _kpc
	//   mass       g_m  =                               = 1.9890E40   h^{-1} kg = h^{-1} _msun * 1E10
	//   time       g_t  = (10 100 h kms Mpc^{-1})^{-1}  = 3.0857E16   h^{-1} s  = 9.7781 h^{-1} Gyr
	//   Hubble     H0   = 1/(g_t)                       = 3.24084E-18 h s^{-1]  = 0.1 g_t = 0.102279 h Gyr^{-1}
    //   velocity   g_vel= l/t                           = 1000        m s^{-1}  = 3.2408E-17 kpc s^{-1} = 1.0227 kpc Gyr^{-1}
	//   grav.const G    =                               = 6.6742E-11  m^3 kg^{-1} s^{-2} = 43021.3425 h^{-3} _kpc^3 (10 H0)^2 (h^{-1} msun 1E10)^{-1}, all factors of h disapear
	//   force      F    = g_m * g_l / g_t^2             = 6.445900776552E26 kg m s^{-2} (Newton)
	//   energy     E    = g_m g_l^2 g_t^-2              = 1.9889974216389e46 h^{-1} J           = 1.1157E67 h^{-1} GeV
	//   crit.dens  rho0 = H0^2 / (8 pi G/3)             = 1.8784E-26 h^2 kg m^{-3} = 2.7746E-8 h^2 _msun * 1E10  _kpc^{-3}
	//
	// Note 1: term gyr is slightly misleading since time is (10 * 100 h kpc Mpc^{-1})^{-1} = h^{-1} 0.9778132389935
	// Note 2: slight discrepancy btw G in Texas calc=6.6742E-11 m^3/(kg s^2) and Constants::G=6.67259E-11 m^3 s^-2 kg^-1
	//
	//const flt Gadget_l=3.085678e19; // [h^{-1} m]
	//const flt Gadget_M=1.989e40;    // [h^{-1} kg]
	//const flt Gadget_t=3.08568e+16; // [h^{-1} s]
	//const flt Gadget_G=Constants::_G / (Gadget_l^3/(Gadget_M*Gadget_t^2)); // = 43021.342500804 h^{-3} kpc^3 (h^{-1} 1E10 Msun)^[-1} (10 H0)^{-2}

    // global var for storing unitsystem
	static Units<flt> g_units;

	static const flt _pi=Constants::_pi;
	static const flt _e =Constants::_e;

	// for printing with no units
	#define UNIT_LENGTH      g_units.unit_length()
	#define UNIT_VOL         g_units.unit_vol()
	#define UNIT_INV_LENGTH  g_units.unit_inv_length()
	#define UNIT_MASS        g_units.unit_mass()
	#define UNIT_TIME        g_units.unit_time()
	#define UNIT_FREQUENCY   g_units.unit_frequency()
	#define UNIT_DENSITY     g_units.unit_density()

	#define _H0   g_units.H0()
	#define _rho0 g_units.rho0()
	#define _G    g_units.G()

	// non-scalars
	typedef Triple<_len> _point_len;
	typedef Triple<_vel> _vector_vel;

	// aliases
	typedef _len2  _area;
	typedef _len3 _vol;
	typedef _per_len2 _per_area;
	typedef _per_len3 _per_vol;

} // GadgetUnitSystem namespace

#endif // __UNITS_H__
