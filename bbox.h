template<class T,class R>
class BBox
{
	public:
	typedef T type_T;
	typedef R type_R;

	private:
	const T m_bbox;

	public:
	BBox(const T& bbox) : m_bbox(bbox) {}
	const T operator()() const {return m_bbox;}

	void SubPeriodic_optimized(Triple<T>& p,const Triple<T>& q) const
	{
		assert( IsInsideBBox(p) && IsInsideBBox(q) && m_bbox>T(0));
		for (size_t i=0;i<3;++i) {
			const T x0=p[i]-q[i];
			const bool sign=x0>=T(0);
			const T x1=sign ?  x0 : -x0;
			const T x2=m_bbox-x1;
			p[i] = x1<x2 ? x0 : (sign ? -x2 : x2);
 		}
	}

	Triple<T> SubPeriodic(const Triple<T>& p,const Triple<T>& q) const
	{
		Triple<T> s=p;
		SubPeriodic_optimized(s,q);
		return s;
	}

	R DistPeriodic2(const Triple<T>& p,const Triple<T>& q) const
	{
		assert( IsInsideBBox(p) && IsInsideBBox(q) && m_bbox>T(0));
		R l2(0);
		for(int i=0;i<3;++i){
 			T s(fabs((p[i]-q[i])/T(1)));
			const T s2=m_bbox-s;
			if (s2<s) s=s2;
			l2 += s*s;
		}
		assert( l2>=R(0) && l2<=3*m_bbox*m_bbox/4  );
		return l2;
	}

	T DistPeriodic(const Triple<T>& p,const Triple<T>& q) const
	{
		return Sqrt<R,T>(DistPeriodic2(p,q));
	}

	bool IsInsideBBox(const Triple<T>& p) const
	{
		for(size_t i=0;i<3;++i)	if (p[i]<T(0) || p[i]>=m_bbox) return false;
		return true;
	}

	void WrapToInsideBox(T& p,const bool dofloattruncation=false) const
	{
		assert( m_bbox>T(0) && numeric_limits<T>::is_integer==false && numeric_limits<T>::is_exact==false );
		if (dofloattruncation) p=T(static_cast<float>(p/T(1)));

		if      (p<T(0))    {p = m_bbox-T(fmod(-p/T(1),m_bbox/T(1)));}
		else if (p==m_bbox) {p = T(0);}
		else if (p> m_bbox) {p = T(fmod(p/T(1),m_bbox/T(1)));}
		assert( p>=T(0) && p<m_bbox );
		assert( IsInsideBBox(p) );
	}

	void WrapToInsideBox(Triple<T>& p,const bool dofloattruncation=false) const {for(int i=0;i<3;++i) WrapToInsideBox(p[i],dofloattruncation);}

	bool isPointInBox(const Triple<T>& p,const T& l,const Triple<T>& q) const
	{
		assert( IsInsideBBox(p) && IsInsideBBox(q) && m_bbox>T(0) );
		for(size_t i=0;i<3;++i){
			assert( p[i]+l<=m_bbox );
			if (q[i]<p[i] || q[i]>=p[i]+l) return false;
		}
		return true;
	}

	flt FractionBoxInBox(const Triple<T>& p,const T& l1,const Triple<T>& q,const T& l2) const
	{
		if (l2>l1) throw_("l2 greater than l1 in FractionBoxInBox()");

		assert(m_bbox>T(0));
		assert(l1>T(0) && l2>T(0) && l1>=l2);
		assert(IsInsideBBox(p));
		assert(IsInsideBBox(p+l1-T(1E-9)));
		// XXX will not work yet, assert(IsInsideBBox(q) );
		// and q+x is not wrapped properly below!

		Triple<T> x;
		for(size_t i=0;i<3;++i){
			if      (q[i]+l2<=p[i])    {return 0;} // no overlap
			else if (p[i]+l1< q[i])    {return 0;} // no overlap
			else if (q[i]   < p[i])    {assert(q[i]+l2>p[i]);  x[i]=q[i]+l2-p[i];}
			else if (q[i]+l2<=p[i]+l1) {assert(q[i]>=p[i]);    x[i]=l2;}
			else                       {assert(p[i]+l1>=q[i]); x[i]=p[i]+l1-q[i];}
			assert( x[i]>=T(0) && x[i]<=l2 );
		}
		if (l2<=T(0)) throw_("bad l2 in FractionBoxInBox(), less or equal zero");
		const flt y=(x[0]*x[1]*x[2])/(l2*l2*l2);
		assert( y>=0 && y<=1 );
		return y;
	}

	// Test miss
	int isParticleInsideBox(const Triple<T>& p,const T& l,const Triple<T>& dp,const T& h,const bool assumeinsidebox) const
	{
		assert(m_bbox>T(0));
		assert( l>T(0) && h>T(0) );
		assert(IsInsideBBox(p) && IsInsideBBox(dp) );
		assert(IsInsideBBox(p+(l-T(1E-9))));

		// p=box lower corner, l=boxsize, h=smoothing, dp=sph datapoint, h=sph smoothinglenght
		// return values: 0=not reachable, 1=partial reachable, 2=full reachable=inside box

		// XXX will not work yet, assert(IsInsideBBox(q) );
		// and p+l and q+x is not wrapped properly below!

		bool iscompletlyinside=true;
		for(size_t i=0;i<3;++i){
			assert( h+l<=m_bbox/2 );
			if      (!(p[i]<=dp[i]-h && p[i]+l>dp[i]+h)) iscompletlyinside=false;
			else if (dp[i]<p[i]-h || dp[i]>=p[i]+h+l) return 0;
		}
		if (iscompletlyinside) return 2;
		else if (assumeinsidebox) return 1;

		const T lhalf=l/2;
		const T lhalfplush=lhalf+h;
		if (DistPeriodic2(p+lhalf,dp) > 1.01*3*lhalfplush*lhalfplush) return 0;
		else return 1;
	}

	friend ostream& operator<<(ostream& s,const BBox<T,R>& x){return s << x() << " " << UNIT_LENGTH;}
};

typedef BBox<_len,_area> _bbox_len;
