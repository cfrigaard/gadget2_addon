pair<vector<particle_data_2>,vector<char> > GenerateParticleData(const Gadgetdatainfo& G,const _bbox_len& bbox,const bool lineardata)
{
	const size_t n=G.ParticlesTotal();
	vector<particle_data_2> x;
	vector<char> xt;

	vector<size_t> ntypes(6);
	for(size_t i=0;i<6;++i) ntypes[i]=G.n[i];
	int typ=0;

	const size_t m=static_cast<size_t>(Pow(static_cast<flt>(n),1.0/3)+.5);
	assert( m>2 );
	const range<_len> r(_len(0),bbox(),m);
	assert( r.rmax()==bbox() );

	if(!lineardata){
		x.reserve(n);
		xt.reserve(n);

		for(size_t i=0;i<n;++i){
			assert( typ>=0 && typ<6 );
			while( ntypes[typ]==0) {++typ; assert(typ<6);}

			_point_len rnd(Random()*r.rmax(),Random()*r.rmax(),Random()*r.rmax());
			assert( bbox.IsInsideBBox(rnd) && G.m[typ]>_mass(0) );
			x.push_back(particle_data_2(typ,rnd,G.m[typ]));
			xt.push_back(typ);

			assert(ntypes[typ]>=1  && x.size()<=n);
			--ntypes[typ];
		}
	} else {
		for(size_t i=0;i<6;++i) if (G.n[i]!=0 && G.n[i]!=G.ParticlesMax()) throw_(string("asymetric data, population of box may be odd, type=") + i + " n[i]=" + G.n[i]);
		const size_t n=Pow3(r.size()); // new size
		x.reserve(n);
		xt.reserve(n);

		for(size_t k=0;k<r.size();++k)
		for(size_t j=0;j<r.size();++j)
		for(size_t i=0;i<r.size();++i){
			assert( typ>=0 && typ<6 );
			while(ntypes[typ]==0) if (++typ>=6) typ=0;

			assert( typ>=0 && typ<6 );
			const _point_len p=r.totype(t_idx(i,j,k));
			assert( _bbox_len(r.rmax()).IsInsideBBox(p) && G.m[typ]>_mass(0) );
			x.push_back(particle_data_2(typ,p,G.m[typ]));
			xt.push_back(typ);

			assert(ntypes[typ]>=1 && x.size()<=n);
			if (++typ>=6) typ=0;
		}
		SortParticletypes(x,xt);
	}
	return make_pair(x,xt);
}

void Gen_data(const string& filename,Gadgetdatainfo& G,const _bbox_len& bbox,const UniverseParameters& u,const bool lineardata)
{
	cout << "% Gendata: filname=" << filename << "\n";
	cout << "%     G=" << G << "\n";
	cout << "%     bbox=" << bbox << " " << u << (lineardata ? " lineardata" : "" ) << "\n";

	size_t particlesTot=G.ParticlesTotal();
	if (particlesTot==0) throw_("no particles specified, please add some");

	const pair<vector<particle_data_2>,vector<char> > p=GenerateParticleData(G,bbox,lineardata);
	const vector<particle_data_2> x=p.first;
	const vector<char>           xt=p.second;

	if (lineardata){
		// adjust to fit with new sizes
		particlesTot=x.size();
		const Gadgetdatainfo G2=CountParticletypes(xt);
		for(int i=0;i<6;++i) G.n[i]=G2.n[i];
		cout << "%     adjusted G=" << G2 << "\n";
	}
	assert( x.size()==G.ParticlesTotal() && particlesTot==x.size() );

	// create data structure
	io_header_1 h=u.getheader();
	const _mass calc_mass=u.Om0*_rho0*bbox()*bbox()*bbox();

	for(size_t i=0;i<6;++i) {
		const size_t n=G.n[i];
		h.npart[i]=h.npartTotal[i]=n;
		h.mass[i]=0;

		if (n>0){
			const flt mfrac=static_cast<flt>(particlesTot)/x.size()/h.npart[i];
			h.mass[i]=mfrac*calc_mass/_mass(1);
		}
 	}

	assert( !h.hasindividualmasses() );
	const flt calc_Omega=h.CalcOmega();
	const _mass calc_mass2=h.masstotal();

	if (!_proximy(u.Om0,calc_Omega,1E-6)) warn_(string("mismatch in Omega matter calculation, Om0=") + u.Om0 + " calculated Om0=" + calc_Omega);
	if (!_proximy(calc_mass,calc_mass2,1E-6)) warn_(string("mismatch in mass calculation, mass=") + tostring(calc_mass) + " calculated mass=" + tostring(calc_mass2));

	SaveSnapshot(h,x,filename,true);
}

int Usage_Gendata(const args& a)
{
	cerr << "Usage: " << a[0] << " <snapshotfile> <boxsize> [-g <type> <N> <mass>] [-h <hubble>] [-l] [-Om <OmegaMatter>] [-Ol <OmegaLambda>] [-z <redshift>]  \n";
	cerr << "  " << Version() <<  " " << Config() <<  " \n";
	cerr << "  snapshotfile: gadget snapshotfile format 1 to save to\n";
	cerr << "  boxsize <float " << UNIT_LENGTH << ">: bounding box\n";
	cerr << "Options:\n";
	cerr << "  -g <int> <int> <float " + UNIT_MASS + ">: add particle type, number and mass\n";
	cerr << "  -h <float>:  hubble constant, default h=0.72\n";
	cerr << "  -l: generate data on a grid, not random distributed data\n";
	cerr << "  -Om <float>: Omega matter, default Om=.3\n";
	cerr << "  -Ol <float>: Omega lambda, default Ol=.7\n";
	cerr << "  -z <float>: redshift, default=0\n";
	return -1;
}

int main_gendata(int argc,char** argv)
{
	try{
		args a(argc,argv,true,false);
		const flt z=a.parseval<flt>("-z",0.0);
		const flt h=a.parseval<flt>("-h",0.72);
		const flt Om=a.parseval<flt>("-Om",.30);
		const flt Ol=a.parseval<flt>("-Ol",.70);
		const bool lineardata=a.parseopt("-l");

		Gadgetdatainfo G;
		while(a.hasopt("-g")){
			const size_t n=a.getoptindex("-g");
			const int typ=a.Totype<int>(n+1);
			if (typ<0 || typ>=6) throw_("bad gadget type");
			G.n[typ]=a.Totype<int>(n+2);
			G.m[typ]=a.Tounit<_mass>(n+3);
			a.remove(n,5);
		}

		if (a.size()!=4) return Usage_Gendata(a);
		const _bbox_len bbox=a.Tounit<_len>(2);

		Gen_data(a[1],G,bbox,UniverseParameters(z,h,Om,Ol,bbox),lineardata);
		return 0;
	}
	CATCH_ALL;
	return -1;
}
