#ifndef __STRUCTS_H__
#define __STRUCTS_H__

struct io_header_1
{
	int      npart[6];
	double   mass[6];
	double   time;
	double   redshift;
	int      flag_sfr;
	int      flag_feedback;
	int      npartTotal[6];
	int      flag_cooling;
	int      num_files;
	double   BoxSize;
	double   Omega0;
	double   OmegaLambda;
	double   HubbleParam;
	char     fill[256- 6*4- 6*8- 2*8- 2*4- 6*4- 2*4 - 4*8];  // fills to 256 bytes

	bool CheckConsistency() const;

	io_header_1()
	{
		if(sizeof(io_header_1)!=256) throw_("sizeof io_header_1 not equal 256 bytes");
		memset(this,0,sizeof(io_header_1));
		num_files=1;
	}

	io_header_1(const string& file)
	{
		if(sizeof(io_header_1)!=256) throw_("sizeof io_header_1 not equal 256 bytes");

	 	AssertFileExists(file);
		ifstream s(file.c_str());

		int dummy;
		s.read(reinterpret_cast<char*>(&dummy),sizeof(dummy));
		if  (dummy!=256) throw_(string("bad header in gadget file, expected 256 got=") + dummy);
		s.read(reinterpret_cast<char*>(this),sizeof(io_header_1));
		assert( CheckConsistency() );
		s.read(reinterpret_cast<char*>(&dummy),sizeof(dummy));
		if  (dummy!=256) throw_(string("bad header in gadget file, expected 256 got=") + dummy);
	}

	size_t particlestotal() const
	{
		size_t n=0;
		for(int k=0;k<6;++k){
			assert( npart[k]==npartTotal[k] );
			n += npart[k];
		}
		return n;
	}

	bool hasindividualmasses() const
	{
		for(int k=0; k<6; k++) {
			assert( npart[k]==npartTotal[k] );
			if(mass[k]==0 && npart[k]!=0) return true;
		}
		return false;
	}

	_mass masstotal() const
	{
		if (hasindividualmasses()) throw_( "Cannot handle individual masses yet");
		double masstot=0;
		for(int k=0; k<6; k++) {
			assert( npart[k]==npartTotal[k] );
			assert( mass[k]!=0 || npart[k]==0 );
			masstot += npart[k] * mass[k];
		}
		return _mass(masstot);
	}

	_bbox_len bbox          () const {return _len(BoxSize);}
	flt       CalcOmega     () const {return masstotal() / (bbox()()*bbox()()*bbox()()) / _rho0;}
	_mass CalcMassFromOmega () const {return      Omega0 *  bbox()()*bbox()()*bbox()()  * _rho0;}
	void      CheckOmega    () const {if(fabs(CalcOmega() - Omega0) > 1.0e-3) warn_( string("Omega mismatch, calculated Omega=") + CalcOmega() + " specified Omega=" + Omega0);}

	int gettype(const size_t n) const
	{
		size_t t(0);
		for(int i=0;i<6;++i){
			t += npart[i];
			if (n<t) return i;
		}
		throw Exception("particle number out of range",__FILE__,__LINE__); // avoid compiler warn
	}

	friend ostream& operator<<(ostream& s,const io_header_1& x)
	{

		return s << make_pair(x.npart,6) << " " << make_pair(x.mass,6) << " " << x.time << " "	<< x.redshift<< " "	<< x.flag_sfr << " " << x.flag_feedback<< " " << make_pair(x.npartTotal,6) << " " << x.flag_cooling<< " " << x.num_files<< " " << x.BoxSize << " " << x.Omega0<< " " << x.OmegaLambda << " " << x.HubbleParam << "\n";
	}

	friend istream& operator>>(istream& s,io_header_1& x)
	{
		return s >> make_pair(x.npart,6) >> make_pair(x.mass,6) >> x.time >> x.redshift >> x.flag_sfr >> x.flag_feedback >> make_pair(x.npartTotal,6) >> x.flag_cooling >> x.num_files >> x.BoxSize >> x.Omega0 >> x.OmegaLambda >> x.HubbleParam;
	}
};

struct particle_data
{
 	float  Pos[3];
 	float  Vel[3];
 	float  Mass;
  	int    Type;
  	float  Rho, U, Temp, Ne;

	friend inline std::ostream& operator<<(std::ostream& s,const particle_data& x)
	{
		return s << make_pair(x.Pos,3) << " " << make_pair(x.Vel,3) << " " << x.Mass << " " << x.Type<< " " << x.Rho<< " " << x.U << " " << x.Temp << " " << x.Ne << "\n";
	}
};

struct data_info
{
 	io_header_1    header1;
	particle_data* P;
	int*           Id;
	int            NumPart;

	data_info() : P(0), Id(0), NumPart(0) {}

	void erasedata()
	{
		delete[] P;
		delete[] Id;
		P=0;
		Id=0;
	}
};


class particle_data_1
{
	private:
	Triple<float> m_p;
	float m_h;

	public:
	particle_data_1()                       : m_p(-1)   , m_h(-1) {}
	particle_data_1(const particle_data& d) : m_p(d.Pos), m_h(-1) {}
	particle_data_1(const _point_len& q)    : m_p(q[0]/_len(1),q[1]/_len(1),q[2]/_len(1)), m_h(-1) {}

	_point_len  pos () const {assert(m_p[0]>=0);  return _point_len(_len(m_p[0]),_len(m_p[1]),_len(m_p[2]));}
	_len        h   () const {assert(m_h>0);      return _len(m_h);}

	void setpos (const _point_len&  pos) {for(int i=0;i<3;++i) {m_p[i]=static_cast<float>(tonum(pos[i])); assert(m_p[i]>=0);}}
	void seth   (const _len&          h) {assert(h>_len(0)); m_h=h/_len(1);}

	friend ostream& operator<<(ostream& s,const particle_data_1& x)
	{
		return s << "particle_data_1: {pos=" << x.pos() << "}";
	}
};

class particle_data_2 : public particle_data_1
{
	private:
	Triple<float> m_v;
	float m_m;

	public:
	particle_data_2() :  m_v(0.0), m_m(-1) {}

	particle_data_2(const particle_data& d,const double& mass)
	: particle_data_1(d), m_v(d.Vel), m_m(mass)
	{
		assert(m_m>0);
		if (!(d.Mass==0 || d.Mass==m_m)) warn_(string("strange masses in particle_data_2 ctor: d.Mass=") + d.Mass + " m_m=" + m_m);
	}

	particle_data_2(const char t,const _point_len& q,const _mass& mass)
	: particle_data_1(q), m_v(0.0), m_m(mass/_mass(1))
	{
 		assert(m_m>0);
	}

	_mass       mass    () const {assert(m_m>0);               return _mass(m_m);}
	_vector_vel vel     () const {                             return _vector_vel(_vel(m_v[0]),_vel(m_v[1]),_vel(m_v[2]));}

	void setvel (const _vector_vel& vel) {for(int i=0;i<3;++i) {m_v[i]=static_cast<float>(tonum(vel[i]));}}
	void setmass(const _mass   mass) {m_m=mass/_mass(1); assert(m_m>0);}

	friend ostream& operator<<(ostream& s,const particle_data_2& x)
	{
		return s << "particle_data_2: {pos=" << x.pos() << " mass=" << x.mass() << "}";
	}
};

class Cosmo
{
	private:
	const flt m_Om,m_Ol,m_Or,m_Ok,m_h,m_sigma8;

	class FriedmannCosmicTimeIntegral
	{
		private:
		const Cosmo& m_c;

		public:
		FriedmannCosmicTimeIntegral(const Cosmo& c) : m_c(c) {}

		flt operator()(const flt aprime) const
		{
			assert( aprime>=0 );
			if (aprime==0) return 0; // this is limit of a->0
			return 1.0/m_c.E(aprime);
		}

		flt Integrate(const flt a0,const flt a1) const
		{
			assert( a0<a1 );
			const flt H0_t = ::Integrate<flt,flt,flt,FriedmannCosmicTimeIntegral>(*this,a0,a1,1,1);
			return H0_t;
		}
	};

	public:
	Cosmo(const flt Om=0.3,const flt Ol=0.7,const flt Or=8.4E-5,const flt h=0.72,const flt sigma8=0.83)
	: m_Om(Om), m_Ol(Ol), m_Or(Or), m_Ok(1-m_Om-m_Ol-m_Or), m_h(h), m_sigma8(sigma8)
	{
		assert( m_Om>=0 && m_Om<=1 );
		assert( m_Ol>=0 && m_Ol<=1 );
		assert( m_Or>=0 && m_Or<=1 );
		//assert( m_Ok>=0 );
		assert( m_h>0 && m_h<=1 );
		assert( m_sigma8>0 && m_sigma8<2 );
	}

	const flt& Om    () const {return m_Om;}
	const flt& Ol    () const {return m_Ol;}
	const flt& Or    () const {return m_Or;}
	const flt& Ok    () const {return m_Ok;}
	const flt& h     () const {return m_h;}
	const flt& sigma8() const {return m_sigma8;}

	flt E(const flt a) const {return Sqrt(m_Or/(a*a*a*a) + m_Om/(a*a*a) + m_Ol +  m_Ok/(a*a));}

	flt operator()(const flt aprime) const
	{
		if (aprime==0) return 0; // this is limit of a->0
		return 1./Pow3(aprime*E(aprime));
	}

	flt GrowthFunction(const flt a) const
	{
		const flt integral = Integrate<flt,flt,flt,Cosmo>(*this,0.,a,1,1);
		return 5.*Om()/2. * E(a) * integral;
	}

	flt CosmicTimeIntegral(const flt a0,const flt a1) const {return FriedmannCosmicTimeIntegral(*this).Integrate(a0,a1);}

	friend ostream& operator<<(ostream& s,const Cosmo& x) {return s << "Cosmo: {Om="<< x.Om() << " Ol=" << x.Ol() << " Or=" << x.Or() << " Ok=" << x.Ok() << " h=" << x.h() << " sigma8=" << x.sigma8() << "}";}
};

struct UniverseParameters
{
	const flt z0,h0,Om0,Ol0;
	const _bbox_len bbox;

	void CheckConsistency()
	{
		if (h0<0 )            throw_(string("bad parameter, h0, must be greater than zero, h0=") + h0);
		if (Om0<0)            throw_(string("bad parameter, OmegaMatter must be greater than zero, Om0=") + Om0);
		if (Ol0<0)            throw_(string("bad parameter, OmegaLambda must be greater than zero, Ol0=") + Ol0);
		if (h0>1.2 || h0<0.4) warn_ (string("suspicious h parameter, h0=") + h0);
		if (1+z0<0)           throw_(string("1+redshift is zero, z0=") + z0);
		if (bbox()<_len(0))   throw_(string("bounding box size less than zero, bbox=") + tostring(bbox));
	}

	static void CheckConsistency(const flt h0,const flt Om0,const flt Ol0,const flt z0,const _bbox_len& bbox)
	{
		UniverseParameters(h0,Om0,Ol0,z0,bbox).CheckConsistency();
	}

	UniverseParameters(const io_header_1& h)
	: z0(h.redshift), h0(h.HubbleParam), Om0(h.Omega0), Ol0(h.OmegaLambda), bbox(h.bbox())
	{
		CheckConsistency();
	}

	UniverseParameters(const flt& z,const flt& h,const flt& Om,const flt& Ol,const _bbox_len& b)
	: z0(z), h0(h), Om0(Om), Ol0(Ol), bbox(b)
	{
		CheckConsistency();
	}

	void operator=(const UniverseParameters& rhs)
	{
		memcpy(this,&rhs,sizeof(rhs)); // a little hacky, but easy way to cpy const structures
	}

	Cosmo getcosmo() const {return Cosmo(Om0,Ol0,0,h0);}
	static flt z2a (const flt z) {assert(z!=-1); return 1/(1+z);}
	static flt a2z (const flt a) {assert(a>=0);  return 1/a-1;}

	io_header_1 getheader() const
	{
		io_header_1 h;
		h.time         =1/(1+z0);
		h.redshift     =z0;
		h.flag_sfr     =0;
		h.flag_feedback=0;
		h.flag_cooling =0;
		h.num_files    =1;
		h.BoxSize      =bbox()/_len(1);
		h.Omega0       =Om0;
		h.OmegaLambda  =Ol0;
		h.HubbleParam  =h0;
		return h;
	}

	friend ostream& operator<<(ostream& s,const UniverseParameters& x)
	{
		return s << x.z0 << " " << x.h0 << " " << x.Om0 << " " << x.Ol0 << " " << x.bbox;
	}
};

struct Gadgetdatainfo
{
	size_t n[6];
	_mass m[6];
	const _bbox_len  bbox;
	const flt z0,h0,Om0,Ol0;
	io_header_1 h;

	Gadgetdatainfo()
	: bbox(_len(0)), z0(0), h0(0), Om0(0), Ol0(0)
	{
		for(size_t i=0;i<6;++i) {
			n[i]=0;
			m[i]=_mass(0);
		}
	}

	Gadgetdatainfo(const io_header_1& he)
	: bbox(_len(he.BoxSize)), z0(1.0/(1-he.redshift)), h0(he.HubbleParam)
	, Om0(he.Omega0), Ol0(he.OmegaLambda), h(he)
	{
		UniverseParameters::CheckConsistency(z0,h0,Om0,Ol0,bbox);
		if (he.num_files!=0 && he.num_files!=1) throw_("can only handle non-splitted snapshotfiles file (num_files==0/1)");

		for(size_t i=0;i<6;++i) {
			if (he.npart[i]!=he.npartTotal[i] ) throw_("npart and npartTotal mismatch");
			n[i]=he.npart[i];
			m[i]=_mass(he.mass[i]);
		}
	}

	void operator=(const Gadgetdatainfo& rhs)
	{
		memcpy(this,&rhs,sizeof(rhs)); // a little hacky, but easy way to cpy const structures
	}

	size_t ParticlesTotal() const
	{
		size_t t=0;
		for(int i=0;i<6;++i) t+=n[i];
		return t;
	}

	size_t ParticlesMax() const
	{
		size_t m=n[0];
		for(size_t i=1;i<6;++i) m=max(n[i],m);
		return m;
	}

	friend ostream& operator<<(ostream& s,const Gadgetdatainfo& G)
	{
		return s << "Gadgetdatainfo: n={" << make_pair(G.n,6) << "} m={" << make_pair(G.m,6) << "}";
	}
};

inline bool io_header_1::CheckConsistency() const
{
	if(num_files>1) throw_("num_files!=1, cannot handle multiple files yet");
	if(hasindividualmasses()) throw_("cannot handle individual masses yet");
	for(int k=0; k<6; k++) if (npart[k]!=npartTotal[k]) throw_("mismatch in npart and npartTotal");

	UniverseParameters(*this);
	CheckOmega();
	return true;
}

#endif // __STRUCTS_H__
