inline _len Mpc2kpc_inv_h(const flt Mpc_nounits,const _h& hx){return _len(Mpc_nounits*1000*hx/_h(1));} // kpc is really kpc/h, so cheat on h factor

class GraficHeader
{
	private:
	unsigned int np1;   // grid size
	unsigned int np2;   // -
	unsigned int np3;   // -
	float dx;           // grid spacing in comoving Mpc,
	float x1o;          // grid offsets in comoving Mpc
	float x2o;          // -
	float x3o;          // -
	float astart;       // the starting expansion factor (with a=1 today)
	float omegam;       // omegam,omegav,h0 are self-explanatory (H0 has units of km/s/Mpc)
	float omegav;       // -
	float H0;           // [H0 grafic]=km(s^-1) Mpc^-1

	void CheckParameters() const
	{
		if (dx<=0)                 throw_("bad dx, less that or equal zero, dx=" + tostring(dx));
		if (astart<0)              throw_("bad astart, less that zero, a=" + tostring(astart));
		if (astart>=2)             warn_ ("high astart value, a=" + tostring(astart));
		if (omegam<=0)             throw_("bad Omega matter value, less that or equal zero, Om=" + tostring(omegam));
		if (omegam>=1)             warn_ ("suspicious Omega matter value, Om=" + tostring(omegam));
		if (omegav<=0)             throw_("bad Omega v value, less that or equal zero, Ov=" + tostring(omegav));
		if (omegav>=1)             warn_ ("suspicious Omega v value, Ov=" + tostring(omegav));
		if (geth()<_h(0.1) || geth()>_h(200)) throw_("strange h value, h=" + tostring(geth()));
		if (geth()<_h(0.3) || geth()>_h(2))   warn_ ("suspicious h value, h=" + tostring(geth()) );
		if (np1!=np2 || np2!=np3) throw_("cannot handle non-square data");
		if (x1o!=x2o || x2o!=x3o) throw_("cannot handle non-constant offset");
	}

	void Clear() {memset(this,0,sizeof(GraficHeader));}

	class FortranCode {
		private:
		const flt omegam,omegav,H0;

		flt dladt(const flt a) const
		{
			// c  Evaluate dln(a)/dtau for FLRW cosmology.
			// c  Omegam := Omega today (a=1) in matter.
			// c  Omegav := Omega today (a=1) in vacuum energy.
			const flt eta=Sqrt(omegam/a+omegav*a*a+1.0-omegam-omegav);
			// c  N.B. eta=a*H/H0, dladt = da/(H0*dtau) where tau is conformal time!
			return a*eta;
		}

		flt ddplus(const flt a) const
		{
			if (a==0.0) return 0.0;
			const flt eta=Sqrt(omegam/a+omegav*a*a+1.0-omegam-omegav);
			return 2.5/(eta*eta*eta);
		}

		flt dplus(const flt a) const
		{
			// c  Evaluate D+(a) (linear growth factor) for FLRW cosmology.
			// c  Omegam := Omega today (a=1) in matter.
			// c  Omegav := Omega today (a=1) in vacuum energy.
			const flt eta=Sqrt(omegam/a+omegav*a*a+1.0-omegam-omegav);
			return eta/a*Integrate<flt,flt,flt,FortranCode>(*this,0.0,a,1,1);
		}

		flt fomega(const flt a) const
		{
			// c  Evaluate f := dlog[D+]/dlog[a] (logarithmic linear growth rate) for
			// c  lambda+matter-dominated cosmology.
			// c  Omega0 := Omega today (a=1) in matter only.  Omega_lambda = 1 - Omega0.
			if (omegam==1.0 && omegav==0.0) return 1.0;
			const flt omegak=1.0-omegam-omegav;
			const flt eta=Sqrt(omegam/a+omegav*a*a+omegak);
			return (2.5/dplus(a)-1.5*omegam/a-omegak)/(eta*eta);
		}

		public:
		FortranCode(const GraficHeader& g)
		: omegam(g.getOm()), omegav(g.getOl()), H0(g.geth()/_h(1)*100) {}

		flt operator()(const flt a) const {return ddplus(a);}
		flt Vfact(const flt& astart) const {return fomega(astart)*H0*dladt(astart)/astart;}
	};

	public:
	GraficHeader() {Clear();}
	GraficHeader(ifstream& s)
	{
		// Fortran format
		// 	open(11,file='ic_deltab',form='unformatted')
		// 	rewind 11
		// 	write(11) np1,np2,np3,dx,x1o,x2o,x3o,astart,omegam,omegav,h0
		// 	do i3=1,np3
		// 	  write(11) ((deltab(i1,i2,i3),i1=1,np1),i2=1,np2)
		// 	end do
		// 	close(11)

		// real code: grafic1.f, line 258
		//   write(11) np1,np2,np3,dx,x1o+xoff,x2o+xoff,x3o+xoff,astart,omegam,omegav,h0
		// 	  do i3=1,np3
		// 	    write(11) ((f(i1,i2,i3),i1=1,np1),i2=1,np2)
		// 	  end do
		Clear();
		assert( sizeof(unsigned int)==4 && sizeof(float)==4 ); // fit with fortran types
		Readtyp<unsigned int>(s); // 4 byte Fortran header start

		np1=Readtyp<unsigned int>(s);
		np2=Readtyp<unsigned int>(s);
		np3=Readtyp<unsigned int>(s);
		dx =Readtyp<float>(s);
		x1o=Readtyp<float>(s);
		x2o=Readtyp<float>(s);
		x3o=Readtyp<float>(s);
		astart=Readtyp<float>(s);
		omegam=Readtyp<float>(s);
		omegav=Readtyp<float>(s);
		H0=Readtyp<float>(s);

		Readtyp<unsigned int>(s); // 4 byte Fortran header end
		CheckParameters();
	};

	io_header_1 GenerateGadgetHeader(const _bbox_len bbox) const
	{
		io_header_1 h;
		h.time=astart;
		h.redshift=1./astart-1;
		h.BoxSize=bbox()/_len(1);
		h.Omega0=omegam; // Omega0==Omega matter
		h.OmegaLambda=omegav;
		h.HubbleParam=geth()/_h(1);
		return h;
	}

	size_t         gridsize() const {return static_cast<size_t>(np1)*np2*np3;}
	Triple<size_t> sizet   () const {return Triple<size_t>(np1,np2,np3);}

	_h   geth  () const {return _h(H0/100);}
	flt  geta  () const {return astart;}
	_len getdx () const {return Mpc2kpc_inv_h( dx,geth());}
	_len getx1o() const {return Mpc2kpc_inv_h(x1o,geth());}
	_len getx2o() const {return Mpc2kpc_inv_h(x2o,geth());}
	_len getx3o() const {return Mpc2kpc_inv_h(x3o,geth());}
	flt  Vfact () const {return FortranCode(*this).Vfact(astart);}
	flt  getOm () const {return omegam;}
	flt  getOl () const {return omegav;}
	void seta  (const flt& a) {assert(a>=0); astart=a;} // for testing only

	bool operator==(const GraficHeader& rhs) const
	{
		// note: does not check on x1/2/3o: x1o!=rhs.x1o || x2o!=rhs.x2o || x3o!=rhs.x3o
		if (np1!=rhs.np1 || np2!=rhs.np2 || np3!=rhs.np3 || dx!=rhs.dx || astart!=rhs.astart || omegam!=rhs.omegam || omegav!=rhs.omegav || H0!=rhs.H0) return false;
		else return true;
	}
	bool operator!=(const GraficHeader& rhs) const {return !this->operator==(rhs);}

	friend ostream& operator<<(ostream& s,const GraficHeader& x)
	{
		s << "GraficHeader: { np=" << x.sizet() << " dx=" << x.dx << " x=" << Triple<float>(x.x1o,x.x2o,x.x3o);
        s << " astart=" << x.astart << " Om=" << x.omegam << " Ov=" << x.omegav << " H0=" << x.H0 << "}";
        return s;
	}
};
