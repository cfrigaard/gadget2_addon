#include "inc.h"
void TestNbody();

int main_nbodysolve(int argc,char** argv);
int main(int argc,char** argv){return main_nbodysolve(argc,argv);}

// m ; rm out2.g_* ; nbodysolve Test/Inputs/my2_snapshot_005 out2.g -f 32 -z 1 -g 20 -c 2 -n 10 -s 0.01 kpc -d
// m ; rm out2.g_* ; nbodysolve /mnt/share/Ana/Dat/ic_a_01.g out2.g  -z 15 -g 20 -c 2 -n 200 -s 1000 kpc -d
// m ; rm out2.g_* ; nbodysolve Test/Inputs/my2_snapshot_005 out2.g -d -p >out.txt

//struct cuda_particle {
//	float x,y,z,m;
//	float vx,vy,vz,t;
//};

// void TestPipe();
// #include <sys/stat.h>
// class Pipe
// {
// 	private:
// 	FILE* m_fp;
// 	const bool m_serv;
//
// 	static FILE* Init(const bool server,const string& pipe)
// 	{
// 		umask(0);
// 		const int s=mknod(pipe.c_str(), S_IFIFO|0666, 0);
// 		if (s!=0) throw_("Could not create gview pipe <" + pipe + ">" );
//
// 		FILE* fp=0;
// 		if(server) fp = fopen(pipe.c_str(), "w");
// 		else       fp = fopen(pipe.c_str(), "r");
//
// 		if( fp==NULL ) throw_("Cound not open pipe file <" + pipe + ">");
// 		assert( fp );
// 		cerr << "\n% pipe creat";
// 		return fp;
// 	}
//
// 	public:
// 	Pipe(const bool server,const string pipe="/tmp/fifo_gview") : m_fp(Init(server,pipe)), m_serv(server) {}
// 	~Pipe(){close();}
// 	void close() {if (m_fp!=0) {fclose(m_fp); m_fp=0;}}
//
// 	void write(const string& s)
// 	{
// 		if(!m_serv) throw_("cannot write, not a server pipe");
// 		if (s.size()>255) throw_("string to big");
// 		fputs(s.c_str(),m_fp);
// 		cerr << "\n% pip write <" << s << ">";
//
// 	}
//
// 	string read()
// 	{
// 		if(m_serv) throw_("cannot read, not a client pipe");
// 		char readbuf[256];
// 		fgets(readbuf,256,m_fp);
// 		cerr << "\n% pip read <" << readbuf << ">";
// 		return readbuf;
// 	}
//
// 	void operator<<(const string& s){write(s);}
// 	void operator>>(string& s)      {s=read();}
// };
//
// void TestPipe()
// {
// 	ofstream os("testsuite.txt");
// 	Testsuite t(os,cerr);
// 	Testsuite& test=t; // alias
//
// 	test.Setverbose(false);
// 	test.SetVerboseLevel(3);
// 	test.Setexceptions(false);
// 	test.Setbool(0,false);
//
// 	test.Sub("Pipe class");
// 	Pipe p(true);
// 	p << "hello pipe";
// 	p.close();
//
// 	Pipe q(false);
// 	string s;
// 	p >> s;
// 	test( s=="hello pipe");
// }

template<class T>
inline T trunc(const T& y,const unsigned int decimals)
{
	const flt x=y/T(1); // dimensionless
	if (fabs(x)<1E-6) return y; // cannot truncate

	flt t=1;
	while (t*fabs(x)<0) t*=10;

	flt n=1;
	for(unsigned int i=0;i<decimals;++i) n*=10;
	long long m=static_cast<long long>(x*n*t+.5);
	return static_cast<T>(m)/(n*t);
}

inline pair<_point_len,_mass> CalcMassAndMeanPos(const searchgrid::t_vec_data& v)
{
	if (v.size()==0) return make_pair(_point_len(_len(-1)),_mass(-1));

	// mean mass
	_mass m(0);
	for(size_t i=0;i<v.size();++i) m += v[i]->mass();
	assert( m>_mass(0) );

	// mean position, weighted by mass
	_point_len p=v[0]->pos()*(v[0]->mass()/m);
	#ifndef NDEBUG
		_point_len q0=v[0]->pos();
		_point_len q1=q0;
	#endif
	for(size_t i=1;i<v.size();++i) {
		const flt massfactor=v[i]->mass()/m;
		p += v[i]->pos()*massfactor;
		#ifndef NDEBUG
			q0.min(v[i]->pos());
			q1.max(v[i]->pos());
		#endif
	}

	#ifndef NDEBUG
		for(unsigned int i=0;i<3;++i) assert( q0[i]<=p[i] && p[i]<=q1[i] );
	#endif
	return make_pair(p,m);
}

inline Triple<_force> CalcForce(const _point_len& p1,const _point_len& p2,const _mass& m1,const _mass& m2,const _bbox_len& bbox,const _len& h1,const _len& h2,const bool reversegravity)
{
	assert( bbox.IsInsideBBox(p1) && bbox.IsInsideBBox(p2) && h1>_len(0) && h2>_len(0));
	assert( m1>_mass(0) && m2>_mass(0) );

	// Plummer potential
	const _point_len s=bbox.SubPeriodic(p1,p2);
	const _len2 r2=s.length2<_len2>() + (h1+h2)*(h1+h2);
	const _len   r=Sqrt<_len2,_len>(r2);
	const _vol  r3=r*r*r;

	assert( r>h1 && r>h2 && r2>=_len2(0) && r2<=3*bbox()*bbox() );
	if (r3<=_vol(0)) throw_("zero distance, infinite force in CalcForce()");

	Triple<_force> f;
	for(unsigned int i=0;i<3;++i) f[i] = m1*m2*_G*s[i]/r3;

	if (reversegravity) f=-f;
	return f;
}

/*
void Kick(const flt& a,vector<particle_data_2>& v,const vector<Triple<_force> >& F,const _time& dt)
{
	assert( v.size()==F.size() && a>0 );
	for(size_t i=0;i<v.size();++i){
		assert( v[i].mass()>_mass(0) );
		const _per_mass inv_m_a(1.0/(a*v[i].mass()));
		const Triple<_force>& f=F[i];

		_vector_vel vel=v[i].vel();
		#ifndef NDEBUG
			const _vector_vel w=vel;
		#endif

		for(unsigned int t=0;t<3;++t) vel[t] += f[t]*dt*inv_m_a;
		v[i].setvel(vel);

		#ifndef NDEBUG
		static bool warn_kick=false;
		if (warn_kick==false)
	    for(unsigned int t=0;t<3;++t) {
			if (f[t]!=_force(0) && v[i].vel()[t]==w[t]) {
				warn_("loosing precision in Kick calculation, delaying futher warnings");
				warn_kick=true;
			}
	    }
	    #endif
	}
}

void Drift(const flt& a,vector<particle_data_2>& v,const vector<Triple<_force> >& F,const _time& dt,const _bbox_len& bbox)
{
	assert( v.size()==F.size() && a>0 );
	const _time factor=dt/(a*a);
	for(size_t i=0;i<v.size();++i){
		const _vector_vel vel=v[i].vel();
		_point_len p=v[i].pos();
		#ifndef NDEBUG
			const _point_len q=p;
		#endif

	    for(unsigned int t=0;t<3;++t) p[t] += vel[t]*factor;
		bbox.WrapToInsideBox(p);
		assert( bbox.IsInsideBBox(p) );
		v[i].setpos(p);

		// avoid trucation errors from double to float
		p=v[i].pos();
		bbox.WrapToInsideBox(p);
		v[i].setpos(p);

		#ifndef NDEBUG
		static bool warn_drift=false;
		if (warn_drift==false)
	    for(unsigned int t=0;t<3;++t) {
			if (vel[t]!=_vel(0) && v[i].pos()[t]==q[t]) {
				warn_("loosing precision in Drift calculation, delaying futher warnings");
				warn_drift=true;
			}
	    }
	    #endif
		assert( bbox.IsInsideBBox(v[i].pos()) );
	}
}
*/

void Kick(const flt& a,vector<particle_data_2>& v,const vector<Triple<_force> >& F,const _time& dt)
{
	assert( v.size()==F.size() && a>0 );
	for(size_t i=0;i<v.size();++i){
		assert( v[i].mass()>_mass(0) );
		const _per_mass inv_m(1.0/v[i].mass());
		const Triple<_force> f=F[i]/(a*a);

		const _point_len r=v[i].pos();
		_vector_vel u=v[i].vel();

		for(unsigned int t=0;t<3;++t) u[t] += f[t]*dt*inv_m;

		v[i].setvel(u);
	}
}

void Drift(const flt& a,vector<particle_data_2>& v,const vector<Triple<_force> >& F,const _time& dt,const _bbox_len& bbox)
{
	assert( v.size()==F.size() && a>0 );
	const _time factor=dt/(a*a);

	for(size_t i=0;i<v.size();++i){
		_point_len r=v[i].pos();
		const _vector_vel u=v[i].vel();

	    for(unsigned int t=0;t<3;++t) r[t] += u[t]*factor;

		bbox.WrapToInsideBox(r,true);
		assert( bbox.IsInsideBBox(r) );
		v[i].setpos(r);

		assert( bbox.IsInsideBBox(v[i].pos()) );
	}
}

void CalcMultipole(searchgrid& s
				  ,vector<particle_data_2>& v
                  ,vector<Triple<_force> >& F
				  ,vector<pair<_point_len,_mass> >& cubemasses
				  ,const _bbox_len& bbox,const size_t& gridsize
				  ,const size_t& neightborcells
				  ,const bool reversegravity,const flt& a)
{
	assert( F.size()==v.size() );
	fill(F.begin(),F.end(),_force(0));

	// multipole approx
	const range<_len>& r=s.getrange();
	if( neightborcells>=2*r.size() ) throw_("bad size of grid, neightborcells to large");

	cubemasses.resize(s.sizen());
	for(size_t n=0;n<s.sizen();++n) cubemasses[n]=CalcMassAndMeanPos(s[n]);

	for(size_t j=0;j<v.size();++j){
		const particle_data_2* phome=&v[j];

		const _point_len p=phome->pos();
		const _mass m=phome->mass();
		const _len      h=phome->h();
		assert( bbox.IsInsideBBox(p) && m>_mass(0) );

		const t_idx hidx=r.toidx(p);
		const Triple<int> home(hidx[0],hidx[1],hidx[2]);

		const t_idx idx1=(home-neightborcells).periodic(r.size());
		const t_idx idx2=(home+neightborcells).periodic(r.size());
		size_t homecount=0;

		// cerr << "** home=" << home << " idx1=" << idx1 << " idx2=" << idx2 << endl;
		for(size_t n=0;n<s.sizen();++n){
			const searchgrid::t_vec_data& v=s[n];
			const t_idx idx=t_idx::toidx(n,r.size());

			if (v.size()>0){
				//cerr << "** b=" << b << " n=" << n << " home=" << home << " idx=" << idx << endl;
				Triple<_force> f(_force(0));

				if (idx.isinrange(idx1,idx2)){
					for(size_t i=0;i<v.size();++i){
						const particle_data_2* pv=v[i];
						if(phome!=pv) f -= CalcForce(pv->pos(),p,pv->mass(),m,bbox,pv->h(),h,reversegravity);
						else ++homecount;
					}
				} else {
					// cerr << "** out of range home=" << home << " idx1=" << idx1 << " idx2=" << idx2 << " idx=" << idx << endl;
					assert( n<cubemasses.size() );
 					const pair<_point_len,_mass>& pr=cubemasses[n];
					if (pr.second>_mass(0))	{
					    assert( bbox.IsInsideBBox(pr.first) );
						f=CalcForce(pr.first,p,pr.second,m,bbox,_len(0),h,reversegravity);
					}
				}

				assert( j<F.size() );
				F[j] +=  f;
			}
		}
		assert( homecount==1 ); // hit ourself once only
	}
}

void CalcDirect( vector<particle_data_2>& v
                ,vector<Triple<_force> >& F
				,const _bbox_len& bbox
				,const bool reversegravity
				,const flt& a
				,const bool quiet=false)
{
	assert( F.size()==v.size() );
	fill(F.begin(),F.end(),_force(0));

	timer tim;
	size_t t=0;
	const size_t N=v.size()*v.size()/2;

	for(size_t j=0;j<v.size();++j){
		const _point_len p=v[j].pos();
		const _mass m=v[j].mass();
		const _len      h=v[j].h();

		for(size_t i=j+1;i<v.size();++i,++t){
			const particle_data_2& q=v[i];
			const Triple<_force> f=CalcForce(q.pos(),p,q.mass(),m,bbox,q.h(),h,reversegravity);
			F[i] -=  f;
			F[j] +=  f;
		}

		if(!quiet) tim.ToEta(t,N,cerr);
	}
}

void NBodySolve(const io_header_1& h,const string& filename,vector<particle_data_2>& v,const UniverseParameters& u,const size_t steps,const size_t gridsize,const _len softning,const bool noapprox,const bool reversegravity,const int neightborcells,const bool noexpansion,const bool pipe,const _time tbeg,const _time tend)
{
	const flt z=h.redshift;
	if (z<u.z0) throw_(string("begin and end redshift mismatch, z=") + z + " z0=" + u.z0);
	if (u.z0<0) warn_(string("suspicious end redshift, z0=") + u.z0 );
	if (neightborcells<1) throw_("neightborcells must be greater that zero");

	flt a=1,da=-1,a_end=-1;
	_time t,dt,t_end;
	io_header_1 h2=h;
	const Cosmo c=u.getcosmo();

	if (!noexpansion) {
		a=UniverseParameters::z2a(z);
		a_end=UniverseParameters::z2a(u.z0);
		da=(a_end-a)/steps;

		if (a<da) throw_(string("bad scalefactor values, a must be >= da, a=") + a + " da=" + da);
		if (softning>_len(u.bbox()/2) || (softning<=_len(0.1) && softning>_len(0)) ) warn_(string("suspicious softning, s=") + softning + " " + UNIT_LENGTH );

		t_end=c.CosmicTimeIntegral(0,a_end)/_H0;
		t=c.CosmicTimeIntegral(0,a   )/_H0;
		dt=c.CosmicTimeIntegral(0,a-da)/_H0;
	} else {
		t=tbeg;
		t_end=tend;
		dt=(t_end-t)/steps;
	}

//test algo
/*a=1;
a_end=10;
dt=_time(0.5);*/
// v.resize(10000);
// for(size_t i=0;i<v.size();++i){
// 	_point_len p=(v[i].pos()+u.bbox()/2);
// 	_vector_vel v(_vel(0));
// 	u.bbox.WrapToInsideBox(p);
// 	v[i].setpos(p);
// 	v[i].setvel(v);
// }
// for(unsigned int i=0;i<6;++i) h2.npart[i]=h2.npartTotal[i]=0;
// h2.npart[0]=h2.npartTotal[0]=v.size();

// v.resize(2);
//
// v[0].setpos(u.bbox()/4.03);
// v[0].setvel(_vector_vel(0.0));
// v[1].setpos(u.bbox()/4);
// v[1].setvel(_vector_vel(0.0));
// for(unsigned int i=0;i<6;++i) h2.npart[i]=h2.npartTotal[i]=0;
// h2.npart[5]=h2.npartTotal[5]=v.size();

	Mpi mpi;
	if (mpi.isMaster()) {
		cerr << "% NbodySolve: z=" << z << " z0=" << u.z0 << " steps=" << steps << " gridsize=" << gridsize;
		cerr << (softning>=_len(0) ? " softning=" + tostring(softning) +  " " +  UNIT_LENGTH : "");
		cerr << (noapprox ? " noapprox" : " neightborcells=" + tostring(neightborcells) );
		cerr << (reversegravity ? " reversegravity " : "") << (noexpansion ? " noexpansion " : "") << endl;
		cerr << "%   u=" << u << endl;
		cerr << "%   a={ " << a << " ; " << a_end << " }  da=" << da << endl;
		cerr << "%   z={ " << z << " ; " << u.z0 << " }" << endl;
		cerr << "%   t={ " << t << " " << UNIT_TIME << " ; " << t_end << " " << UNIT_TIME << " }" <<  "  dt=" << dt << " " << UNIT_TIME << endl;
		cerr << "%   tau={ " << t*_H0 << " ; " << t_end* _H0 << " }" << endl;
		cerr << "%   datasize=" << v.size() << endl;
		cerr << "%   units: G=" << _G << " H0=" << _H0 << endl;
	}

	assert( t_end>t && a_end>a && dt>_time(0) && da>0 );
	vector<Triple<_force> > F(v.size());
	vector<pair<_point_len,_mass> > cubemasses;

	if (softning>=_len(0)) for(size_t i=0;i<v.size();++i) v[i].seth(softning);

	timer tim;
	for(size_t k=0;k<steps;++k){
		cerr << "% k=" << k << "/" << steps;
		cerr << "\n%    t=" << fwidth(trunc(t,4),1,5) << " " << UNIT_TIME << "\n%   dt=" << fwidth(trunc(dt,6),1,7) << " " << UNIT_TIME;
		if (!noexpansion) {
			const flt z=UniverseParameters::a2z(a);
			const flt E=c.E(a);
			const _per_time Ht=_H0*E;

			cerr << "\n%    z=" << fwidth(trunc(z,2),2,4) << "\n%    a=" << fwidth(trunc(a,4),1,5);
			cerr << "\n%   E(t)=" << fwidth(trunc(E,3),2,4) << "\n%   H(t)=" << fwidth(trunc(Ht,3),2,4) << " " << UNIT_FREQUENCY << "\n";
		}

		if (!noapprox){
			searchgrid s(v,_len(0),u.bbox(),gridsize,true);
			if (softning<_len(0)) FindSmoothingLengths(v,s,33,mpi.isMaster());
			CalcMultipole(s,v,F,cubemasses,u.bbox,gridsize,neightborcells,reversegravity,a);
		} else {
			if (softning<_len(0)){
				searchgrid s(v,_len(0),u.bbox(),gridsize,true);
				FindSmoothingLengths(v,s,33,mpi.isMaster());
			}
			CalcDirect(v,F,u.bbox,reversegravity,a);
		}

		// choice KDK, see V.Springel article
 		Kick (a,v,F,dt/2);
		Drift(a,v,F,dt,u.bbox);
 		Kick (a,v,F,dt/2);

		// update time steps, a or t based
		if (!noexpansion){
			a += da;
			const flt H0_t = c.CosmicTimeIntegral(0,a);
			const _time last_t=t;
			t = H0_t / _H0;
			dt = t-last_t;
		} else{
			assert( a==1 );
			t = k*dt;
		}

		assert( t>_time(0) && dt>_time(0) && a>0 );

		// dump snapshot
		h2.redshift=z;
		h2.time=a;
		SaveSnapshot(h2,v,filename + "_" + suffix(k) ,true);

		if (mpi.isMaster()){
			if(pipe){
				cout << "\nPIPE_DATA_BEG:\n{";
				cout << "\n  a = " << a;
				cout << "\n  bbox = " << u.bbox();
				cout << "\n  N = " << v.size();
				for(vector<particle_data_2>::const_iterator itt=v.begin();itt!=v.end();++itt){
					const particle_data_2& p=*itt;
					cout << "\n  " << p.pos() << " " << p.h();
				}
				cout << "\n} PIPE_DATA_END";
			}
			tim.ToEta(k,steps,cerr);
		}
	}

	// Final dump
	SaveSnapshot(h2,v,filename,true);
}

int Usage_Nbodysolve(const args& a)
{
	cerr << "Usage: " << a[0] << " <snapshotfile in> <snapshotfile out>	[-c <cells>] [-d] [-f <filter] [-g <gridsize>] [-h <hubble>] [-Om <OmegaMatter>] [-Ol <OmegaLambda>] [-r] [-s <softninglength>] [-static] [-tbeg <time>] [-tend <time>] [-z <redshift>] \n";
	cerr << "  " << Version() <<  " " << Config() <<  " \n";
	cerr << "  snapshotfile in:  gadget snapshotfile format 1 to load from\n";
	cerr << "  snapshotfile out: gadget snapshotfile format 1 to save to\n";
	cerr << "Options:\n";
	cerr << "  -c <int>: neightbor cells to open in non-direct mode, default=1\n";
	cerr << "  -d: direct calculation (slow O(n^2))\n";
	cerr << "  -f <int>: 1=gas, 2=halo, 4=disk, 8=bulge, 16=stars, 32=bndry\n";
	cerr << "  -g <int>: gridsize, default n=10\n";
	cerr << "  -n <int>: stepsize in integration, default n=100\n";
	cerr << "  -h <float>:  hubble constant, default h=read from file\n";
	cerr << "  -Om <float>: omega matter, default Om=read from file\n";
	cerr << "  -Ol <float>: omega lambda, default Ol=read from file\n";
	cerr << "  -p: pipe sim data to stdout, to be used for realtime givew'ing\n";
	cerr << "  -r: reverse sign of gravity\n";
	cerr << "  -s <float "+ UNIT_LENGTH  + ">: softning factor in kpc/h, default=calc smoothing lengths (negative value)\n";
	cerr << "  -static: no universe background expansion\n";
	cerr << "  -tbeg <float>: time start, instead of file z or in static universe, default=0\n";
	cerr << "  -tend <float>: time end  , instead of file z or in static universe\n";
	cerr << "  -z <float>: redshift simulation end, default z=0\n";
	return -1;
}

int main_nbodysolve(int argc,char** argv)
{
	try{
		args a(argc,argv,true,false);
TestNbody(); // XXX

		const int neightborcells=a.parseval<int>("-c",1);
		const bool noapprox=a.parseopt("-d");
		const int filter=a.parseval<int>("-f",0);
 		const size_t gridsize=a.parseval<size_t>("-g",10);
		const size_t steps=a.parseval<size_t>("-n",100);
		const flt hub=a.parseval<flt>("-h",-1);
		const flt Om=a.parseval<flt>("-Om",-1);
		const flt Ol=a.parseval<flt>("-Ol",-1);
		const bool reversegravity=a.parseopt("-r");
		const bool pipe=a.parseopt("-p");
		const _len softning=a.parseunit<_len>("-s",_len(-1));
		const bool noexpansion=a.parseopt("-static");
		const _time tbeg=a.parseunit<_time>("-tbeg",_time(0));
		const _time tend=a.parseunit<_time>("-tend",_time(-1));
		const flt z=a.parseval<flt>("-z",0.0);

		if (a.size()!=3) return Usage_Nbodysolve(a);
		if (noapprox && (neightborcells!=1 || gridsize!=10)) warn_("direct calc option -d normaliy implies no use of -c or -g");
		if (tend>_time(0) && tend<=tbeg) throw_("time end must be greater than time begin");

 		const data_info inf=load_snapshot(a[1].c_str(),true,false);
 		vector<particle_data_2> v;
 		const io_header_1 h=FilterParticles(inf,filter,v);

if (pipe) { // XXX test
	v.resize(2000);
	io_header_1& h2=const_cast<io_header_1&>(h);
	for(unsigned int i=0;i<6;++i) h2.npart[i]=h2.npartTotal[i]=0;
	h2.npart[0]=h2.npartTotal[0]=v.size();
}
		const UniverseParameters u(z,hub>0? hub : h.HubbleParam,Om>0 ? Om : h.Omega0,Ol>0 ? Ol : h.OmegaLambda,h.bbox());

		NBodySolve(h,a[2],v,u,steps,gridsize,softning,noapprox,reversegravity,neightborcells,noexpansion,pipe,tbeg,tend);
		return 0;
	}
	CATCH_ALL;
	return -1;
}

pair<io_header_1,vector<particle_data_2> > SolarSystem()
{
	vector<particle_data_2> v;
	v.resize(2);

	const _point_len  p_sun(1./1E10);
	const _vector_vel v_sun(_vel(0));

	v[0].setpos(p_sun);
	v[0].setvel(v_sun);

	const _point_len  p_earth(1.495979E11/(1000*3.085678E16));
	const _vector_vel v_earth(_vel(0));

	io_header_1 h;

	for(unsigned int i=0;i<6;++i) h.npart[i]=h.npartTotal[i]=0;
	h.npart[1]=h.npartTotal[1]=v.size();

	return make_pair(h,v);
}

#include "testsuite.h" // XXX
void TestNbody()
{
	ofstream os("testsuite.txt");
	Testsuite t(os,cerr);
	Testsuite& test=t; // alias

	test.Setverbose(false);
	test.SetVerboseLevel(3);
	test.Setexceptions(false);
	test.Setbool(0,false);

	test.Sub("Cosmo class");
	const Cosmo c;

	const _time t1=c.CosmicTimeIntegral(0,1)/_H0;
	const _time t2=c.CosmicTimeIntegral(0,0.5)/_H0;
	const _time t3=c.CosmicTimeIntegral(0,0.1)/_H0;

	test( c.Om()==0.3 );
	test( c.Ol()==0.7 );
	test( Proximy(t1, 5.32544 ) );
	test( Proximy(t2, 1.21524 ) );
	test( Proximy(t3, 0.0230285 ) );

	test.Sub("CalcForce");
	_bbox_len bbox(50);
	flt a=1;

	_point_len p1(10,10,10);
	_point_len p2(30,20,5);
	_mass m1(4);
	_mass m2(2);
	_len h1(1E-142);
	_len h2(1E-142);

	Triple<_force> f1=CalcForce(p1,p2,m1,m2,bbox,h1,h2,true);
	Triple<_force> f2=CalcForce(p1,p2,m1,m2,bbox,h1,h2,false);
	Triple<_force> f3=CalcForce(p2,p1,m2,m1,bbox,h2,h1,true);

	test( Proximyeps(f1[0],572.222,1E-6));
	test( Proximyeps(f1[1],286.111,1E-6));
	test( Proximyeps(f1[2],-143.055,1E-5));

	for(size_t i=0;i<3;++i) {
		test( Proximy(f1[i],-f2[i]) );
		test( Proximy(f1[i],-f3[i]) );
	}

	test.Subsub("smoothing lengths");
	h1=_len(2);
	h2=_len(8);
	f1=CalcForce(p1,p2,m1,m2,bbox,h1,h2,true);
	f2=CalcForce(p1,p2,m1,m2,bbox,h1,h2,false);

	test( Proximyeps(f1[0],440.538,1E-7));
	test( Proximyeps(f1[1],220.269,1E-7));
	test( Proximyeps(f1[2],-110.134,1E-5));

	for(size_t i=0;i<3;++i) {
		test( Proximy(f1[i],-f2[i]) );
	}

	test.Sub("Kick/drift operators");

	a=1;
	_time dt(1);

	vector<particle_data_2> v;
	vector<Triple<_force> > F;

	for(size_t i=0;i<10;++i) {
		_point_len q(i,i,i);
		particle_data_2 p(5,q,_mass(4));
		p.seth(1);

		assert( bbox.IsInsideBBox(q) );
		assert( p.h()>_len(0) );
		assert( p.mass()>_mass(0) );

		v.push_back(p);
	}
	F.resize(v.size());

	for(size_t i=0;i<100;++i){
		CalcDirect(v,F,bbox,false,1);
		Kick (a,v,F,dt/2);
		Drift(a,v,F,dt,bbox);
		Kick (a,v,F,dt/2);
	}

	//cerr << v;
}

