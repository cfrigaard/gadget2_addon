#include "inc.h"
#include "convert.h"
#include "density.h"
#include "gdiff.h"
#include "gendata.h"
//#include "genic.h" XXX only for Genic code
#include "ghead.h"
#include "gfilter.h"
#include "grafic2gadget.h"
#include "massinsphere.h"
#include "nbodysolve.h"
#include "power.h"
#include "toascii.h"
#include "twopointcorr.h"

// global and class statics
static char* g_extra_newhandlermem=new char [1024*16]; // leak some mem, ok only used in stressed mem situations

void newhandler()
{
	if (g_extra_newhandlermem!=0) {
		delete[] g_extra_newhandlermem;
		g_extra_newhandlermem=0;
	}
	try{throw_("out of memory, newhandler called, throwing bad_alloc()");}
	catch(...){}
	throw bad_alloc();
}

void Initialize(const bool staticrandomseed,const int nicelevel,const bool mpienabled,const string& unitsfile,const bool asymetricmpi,const int outputprecision)
{
	set_new_handler(newhandler);
	if (outputprecision<0){
		cout.precision(cout.precision()-1);
		cerr.precision(cerr.precision()-1);
	}else{
		cout.precision(outputprecision);
		cerr.precision(outputprecision);
	}

	if (nicelevel!=0)     SetNiceLevel(nicelevel);
	if (staticrandomseed) Random(0); // seed(0)
	if (mpienabled){
		#ifndef USE_MPI
			warn_("binaries build without MPI support");
		#else
			if (staticrandomseed && Mpi::PeekRank()>1) warn_("static randomseed on multiple machines may introduce redundant data, (option -testsuite causes calls to Random(0)");
		#endif
	} else {
		if (Mpi::PeekRank()>1) throw_("binary has no parallel support yet");
        if (asymetricmpi) warn_("function lacks MPI support");
	}
	#ifdef USE_UNITS
		warn_("warning: units are enabled in binary, -DUSE_UNITS should only be enabled static compiler check");
	#endif
	if (unitsfile.size()>0){
		AssertFileExists(unitsfile);
		ifstream s(unitsfile.c_str());
		s >> g_units;
	}
}
