#ifndef __SEARCH_H__
#define __SEARCH_H__

const _len min_range_warn(1E-3);
const _len min_datarange_warn(1E20);
const flt fudge(1.01); // factor for ellim float uncertainties

class searchgrid
{
	public:
	typedef particle_data_2 t_data;
	typedef pair<const t_data*,t_idx> t_pair_dataidx;
	typedef multimap<_len2,t_pair_dataidx> t_map_data;
	typedef vector<const t_data*> t_vec_data;
	typedef vector<const t_vec_data*> t_vec_vec_data;

	private:
	array3d<t_vec_data> m_s;
	mutable vector<bool> m_searched_periodic,m_searched_particles;
	size_t m_N;
 	#ifndef NDEBUG
		mutable array3d<int> m_s2;
	#endif
	const range<_len> m_r;
	const _bbox_len m_bbox;
	const bool m_periodic;
	const vector<t_data>& m_orgdata;

	static pair<_len,_len> Minmax(const pair<Triple<_len>,Triple<_len> >& minmax)
	{
		_len pmin=minmax.first[0];
		_len pmax=minmax.second[0];
		for(size_t i=1;i<3;++i) {
			pmin=min(pmin,minmax.first[i]);
			pmax=max(pmax,minmax.second[i]);
		}
		return make_pair(pmin,pmax);
	}

	static const range<_len> Initrange(const vector<t_data>& a,const int grids)
	{
		pair<_len,_len> minmax_d=Minmax(::Minmax(a));
		range<_len> r(minmax_d.first,minmax_d.second,grids,true);
		assert( r.isinrange(minmax_d.first) );
		assert( r.isinrange(minmax_d.second) );
		return r;
	}

	void Init(const int grids)
	{
		assert( sizeof(t_vec_data)<=8*sizeof(int) ); // no pointers, allow some mem overhead
		if (Abs((m_r.rmax()-m_r.rmin())/_len(1))<min_range_warn/_len(1)) warn_("range of search data very small, r=" + tostring(m_r));

		const size_t sz=m_r.size();
		m_s.resize(sz,sz,sz);
		if (m_periodic) m_searched_periodic.resize(Pow3(sz));
		#ifndef NDEBUG
			m_s2.resize(sz,sz,sz);
		#endif
	}

    _len Mindisttowalls(const t_idx& idx,const Triple<_len>& p) const
	{
		const Triple<_len> w=m_r.totype(idx);
		const _len s=m_r.approxstep();
		_len minwall=10*s;
		for(int i=0;i<3;++i){
			assert( w[i]<=p[i]);
			_len d=p[i]-w[i];
			if (d<minwall) minwall=d;
			assert( w[i]+s>=p[i]);
			d=(w[i]+s)-p[i];
			if (d<minwall) minwall=d;
		}
		assert( FltValidity(minwall/_len(1)) && minwall>=_len(0) );
		return minwall;
	}

	inline _len2 Dist2(_point_len& p,const _point_len& q) const
	{
	 	return m_periodic ? m_bbox.DistPeriodic2(p,q) : p.dist2<_len2>(q);
	}

	size_t CalcExtraShells(const t_idx& idx,const _point_len& p,const _len2 distmin2,const flt& fudge) const
	{
		assert(m_r.approxstep()>_len(0));
		assert(FltValidity(distmin2/_len2(1)));
		const _len dwall=Mindisttowalls(idx,p);
 		const _len dist=Sqrt<_len2,_len>(distmin2)-dwall;
		const size_t s=static_cast<size_t>(dist/m_r.approxstep()*fudge)+1;
		return m_r.size()<s ? m_r.size() : s;
	}

	void MapNewNearest(const t_idx& idx,const _len2& d2,_len2& distmin2,const t_data*& f,t_idx& f_idx,t_map_data& nclosest,const size_t& N,const t_data* newnearest) const
	{
		assert( FltValidity(d2/_len2(1)) );

		#ifndef NDEBUG
		int unique=0;
		for(t_map_data::const_iterator itt=nclosest.begin();itt!=nclosest.end();++itt) {
			if(itt->second.first==f) ++unique;
		}
		assert( unique<=1 );
		#endif

		if(nclosest.size()==N) nclosest.erase(--nclosest.end());
		nclosest.insert(make_pair(d2,make_pair(newnearest,idx)));

		const t_map_data::const_iterator itt=--nclosest.end();
		distmin2=itt->first;
		f=itt->second.first;
		f_idx=itt->second.second;
	}

	const t_data* Findclosest_incube(const t_idx& idx,const _point_len& p,_len2& distmin2,const t_data* f,t_idx& f_idx,t_map_data& nclosest,const size_t N) const
	{
		assert( FltValidity(distmin2/_len2(1)));
		assert( idx[0]<m_s.sizex() && idx[1]<m_s.sizey() && idx[2]<m_s.sizez() );
		assert( (nclosest.size()==0 && f==0) || (nclosest.size()>0 && nclosest.size()<=N && f!=0) );
		assert( (nclosest.size()==0 && f==0) ||  ( f==(--nclosest.end())->second.first && f_idx==(--nclosest.end())->second.second ) );

		const t_vec_data& v=m_s[idx];
  		#ifndef NDEBUG
			assert( m_s2[idx]++==0);
		#endif
		if (m_periodic){
			if (m_searched_particles.size()>0){
				size_t nn=0;
				for(size_t m=0;m<v.size();++m){
					assert( v[m]>=&m_orgdata[0] && v[m]<&m_orgdata[0]+m_orgdata.size()*sizeof(t_vec_data) );
					const size_t particleindex=v[m]-&m_orgdata[0];
					assert( particleindex<m_searched_particles.size() );
					if ( m_searched_particles[particleindex]==false ){
						m_searched_particles[particleindex]=true;
						_point_len q=v[m]->pos();
						const _len h=v[m]->h();
						assert( h>_len(0) );
						const _len2 h2=h*h;
					    const _len2 d2=m_bbox.DistPeriodic2(q,p);
						if (d2<distmin2+h2 || nclosest.size()<N ) MapNewNearest(idx,d2,distmin2,f,f_idx,nclosest,N,v[m]);
						++nn;
					}
				}
			} else {
				for(size_t m=0;m<v.size();++m){
					_point_len q=v[m]->pos();
					const _len2 d2=m_bbox.DistPeriodic2(q,p);
					if (d2<distmin2 || nclosest.size()<N ) MapNewNearest(idx,d2,distmin2,f,f_idx,nclosest,N,v[m]);
				}
			}
		} else {
			for(size_t m=0;m<v.size();++m){
				const _len2 d2=v[m]->pos().dist2<_len2>(p);
				if (d2<distmin2 || nclosest.size()<N ) MapNewNearest(idx,d2,distmin2,f,f_idx,nclosest,N,v[m]);
			}
		}
		assert( f!=0 || v.size()==0);
		assert( (nclosest.size()==0 && f==0) || (nclosest.size()>0 && nclosest.size()<=N && f!=0) );
		assert( (nclosest.size()==0 && f==0) ||  ( f==(--nclosest.end())->second.first && f_idx==(--nclosest.end())->second.second ) );
		assert( nclosest.size()>=v.size() || nclosest.size()==N );
		return f;
	}

	const t_idx FixPeriodic(const Triple<int>& idx) const
	{
		const int sz=static_cast<int>(m_r.size());
		t_idx idx_fixed_periodic;

		for(size_t i=0;i<3;++i){
			if      (idx[i]<0)    idx_fixed_periodic[i]=sz+idx[i];
			else if (idx[i]>=sz)  idx_fixed_periodic[i]=idx[i]-sz;
			else                  idx_fixed_periodic[i]=idx[i];
			assert( idx_fixed_periodic[i]<m_r.size() );
		}
		return idx_fixed_periodic;
	}

	const t_data* Findclosest_incube_periodic(const Triple<int>& idx,const _point_len& p,_len2& distmin2,const t_data* f,t_idx& f_idx,t_map_data& nclosest,const size_t N,const size_t shell) const
	{
		const int sz=static_cast<int>(m_r.size());
		assert( m_periodic && shell<m_r.size() );

		const t_idx idx_fixed_periodic=FixPeriodic(idx);
		const size_t sidx=idx_fixed_periodic[0]+idx_fixed_periodic[1]*sz+idx_fixed_periodic[2]*sz*sz;
		assert( m_s.sizex()==m_r.size() && sidx<m_searched_periodic.size() );
		//assert( m_searched_periodic[sidx]==m_s2[idx_fixed_periodic] && m_searched_periodic[sidx]!=0 );

		if(m_searched_periodic[sidx]) return f;
		m_searched_periodic[sidx]=true;

		return Findclosest_incube(idx_fixed_periodic,p,distmin2,f,f_idx,nclosest,N);
	}

	size_t Addsaturate(const size_t x,const size_t y) const
	{
		assert(y>0);
		const size_t z=x+y+1;
		return z<m_r.size() ? z : m_r.size();
	}

	size_t Subsaturate(const size_t x,const size_t y) const
	{
		assert(y>0);
		return x>y ? x-y : 0;
	}

	const t_data* Findclosest_subshell_periodic(const t_idx& idx,const _point_len& p,_len2& distmin2,const t_data* f,t_idx& f_idx,const size_t shell,t_map_data& nclosest,const size_t N) const
	{
		assert( m_periodic );
		assert(shell>0 && shell<m_r.size());

		Triple<int> imin,imax;
		for(size_t i=0;i<3;++i){
			imin[i]=idx[i]-shell;
			imax[i]=idx[i]+shell+1;
		}

		for(int k=imin[2];k<imax[2];++k)
		for(int j=imin[1];j<imax[1];++j){
			f=Findclosest_incube_periodic(Triple<int>(imin[0],j,k),p,distmin2,f,f_idx,nclosest,N,shell);
		}
		for(int k=imin[2];k<imax[2];++k)
		for(int j=imin[1];j<imax[1];++j){
			f=Findclosest_incube_periodic(Triple<int>(imax[0]-1,j,k),p,distmin2,f,f_idx,nclosest,N,shell);
		}

		for(int k=imin[2];k<imax[2];++k)
		for(int i=imin[0]+1;i<imax[0]-1;++i){
			f=Findclosest_incube_periodic(Triple<int>(i,imin[1],k),p,distmin2,f,f_idx,nclosest,N,shell);
		}
		for(int k=imin[2];k<imax[2];++k)
		for(int i=imin[0]+1;i<imax[0]-1;++i){
			f=Findclosest_incube_periodic(Triple<int>(i,imax[1]-1,k),p,distmin2,f,f_idx,nclosest,N,shell);
		}

		for(int j=imin[1]+1;j<imax[1]-1;++j)
		for(int i=imin[0]+1;i<imax[0]-1;++i){
			f=Findclosest_incube_periodic(Triple<int>(i,j,imin[2]),p,distmin2,f,f_idx,nclosest,N,shell);
		}
		for(int j=imin[1]+1;j<imax[1]-1;++j)
		for(int i=imin[0]+1;i<imax[0]-1;++i){
			f=Findclosest_incube_periodic(Triple<int>(i,j,imax[2]-1),p,distmin2,f,f_idx,nclosest,N,shell);
		}

		return f;
	}

	const t_data* Findclosest_subshell(const t_idx& idx,const _point_len& p,_len2& distmin2,const t_data* f,t_idx& f_idx,const size_t shell,t_map_data& nclosest,const size_t N) const
	{
		if (m_periodic) return Findclosest_subshell_periodic(idx,p,distmin2,f,f_idx,shell,nclosest,N);

		const size_t sz=m_r.size();
		if (shell>=sz+1) return f;
		assert(shell>0 && shell<sz+1);

		t_idx imin,imax;
		for(size_t i=0;i<3;++i){
			imin[i]=Subsaturate(idx[i],shell);
			imax[i]=Addsaturate(idx[i],shell);
		}

		assert( imax[0]>1 && imax[1]>1 && imax[2]>1 );

	 	if (idx[0]>=shell)
		for(size_t k=imin[2];k<imax[2];++k)
		for(size_t j=imin[1];j<imax[1];++j){
			f=Findclosest_incube(t_idx(imin[0],j,k),p,distmin2,f,f_idx,nclosest,N);
		}
 		if (idx[0]+shell<sz)
		for(size_t k=imin[2];k<imax[2];++k)
		for(size_t j=imin[1];j<imax[1];++j){
			f=Findclosest_incube(t_idx(imax[0]-1,j,k),p,distmin2,f,f_idx,nclosest,N);
		}

		const size_t imin_hull=idx[0]+1>shell  ? imin[0]+1 : imin[0];
		const size_t imax_hull=idx[0]+shell<sz ? imax[0]-1 : imax[0];

 	 	if (idx[1]>=shell)
		for(size_t k=imin[2];k<imax[2];++k)
		for(size_t i=imin_hull;i<imax_hull;++i){
			f=Findclosest_incube(t_idx(i,imin[1],k),p,distmin2,f,f_idx,nclosest,N);
		}
		if (idx[1]+shell<sz)
		for(size_t k=imin[2];k<imax[2];++k)
		for(size_t i=imin_hull;i<imax_hull;++i){
			f=Findclosest_incube(t_idx(i,imax[1]-1,k),p,distmin2,f,f_idx,nclosest,N);
		}

		const size_t jmin_hull=idx[1]+1>shell  ? imin[1]+1 : imin[1];
		const size_t jmax_hull=idx[1]+shell<sz ? imax[1]-1 : imax[1];

		if (idx[2]>=shell)
		for(size_t j=jmin_hull;j<jmax_hull;++j)
		for(size_t i=imin_hull;i<imax_hull;++i){
			f=Findclosest_incube(t_idx(i,j,imin[2]),p,distmin2,f,f_idx,nclosest,N);
		}
		if (idx[2]+shell<sz)
		for(size_t j=jmin_hull;j<jmax_hull;++j)
		for(size_t i=imin_hull;i<imax_hull;++i){
			f=Findclosest_incube(t_idx(i,j,imax[2]-1),p,distmin2,f,f_idx,nclosest,N);
		}

		return f;
	}

	void Addpoint (const t_data& a)
	{
		const Triple<_len> p=a.pos();
		assert( !m_periodic || m_bbox.IsInsideBBox(p) );
		for(int i=0;i<3;++i){
			const _len& d=p[i];
			CheckFltValidity(d/_len(1));
			if (d<-min_datarange_warn || d>min_datarange_warn) warn_(string("suspicious point in data, point less that ") + -min_datarange_warn + " or greater that" + min_datarange_warn + ", p=" + d);
		}
		m_s[m_r.toidx(p)].push_back(&a);
		++m_N;
	}
	void Addpoints(const vector<t_data>& a) {for(size_t n=0;n<a.size();++n) Addpoint(a[n]);}

	public:
	searchgrid(const vector<t_data>& a,const int grids,const _len bbox=_len(-1))
		: m_N(0), m_r(Initrange(a,grids)), m_bbox(bbox), m_periodic(bbox>_len(0)), m_orgdata(a)
		{
			Init(grids);
		    Addpoints(a);
		}

	searchgrid(const vector<t_data>& a,const _len rmin,const _len rmax,const int grids,const bool periodic)
		: m_N(0), m_r(rmin,rmax,grids,true), m_bbox(rmax), m_periodic(periodic), m_orgdata(a)
		{
			Init(grids);
			Addpoints(a);
		}

	const t_vec_data&  	operator  [] (const size_t& idx) const {return m_s[idx];}
	const t_vec_data&  	operator  [] (const t_idx& idx ) const {return m_s[idx];}
	const range<_len>& 	getrange  () const {return m_r;}
	const t_idx        	sizet     () const {return m_s.sizet();}
	size_t       		sizen     () const {return m_s.size();}
	size_t       		datasize  () const {return m_N;}
	bool         		periodic  () const {return m_periodic;}
	const _point_len    	gridsize  () const
	{
		const _len b=bbox();
		const t_idx s=sizet();
 		return _point_len(b/s[0],b/s[1],b/s[2]);
	}
	_len bbox () const
	{
		if(!m_periodic) throw_("not a periodic search grid");
		assert(m_bbox()>_len(0));
		return m_bbox();
	}
	bool hasPoint (const t_vec_data& v,const t_data& d) const {return find(v.begin(),v.end(),&d)!=v.end();}
	bool hasPoint (const t_data& a)                     const {const t_vec_data& v=m_s[m_r.toidx(a.pos())]; return hasPoint(v,a);}

	/*
	void Addsmoothinglenghts()
	{
		if(m_searched_particles.size()!=0) throw_("Addsmoothinglenghts has already been called");
		m_searched_particles.resize(m_orgdata.size());

		size_t n=0;
		timer tim;
		for(vector<t_data>::const_iterator itt=m_orgdata.begin();itt!=m_orgdata.end();++itt){
			const t_data& d=*itt;
			const Triple<_len> p=d.pos();

 			const _len dwall=Mindisttowalls(m_r.toidx(p),p);
			const _len h(d.temp());
			assert( h>_len(0) );
 			if (dwall<h){
				const _len  f=h+Sqrt(3.0)*m_r.approxstep()/2;
				const _len2 f2=f*f;
				const t_idx home=m_r.toidx(p);

				const Triple<_len> p0=WrapToInsideBox(p-h,m_bbox);
				const Triple<_len> p1=WrapToInsideBox(p+h,m_bbox);
				const Triplerange<size_t> t(m_r.toidx(p0),(m_r.toidx(p1)+1).periodic(m_r.size()),m_r.size());

				for(Triplerange<size_t>::const_iterator itt=t.begin();itt!=t.end();++itt){
					const t_idx i=*itt;
					if (i!=home){
						_point_len q=m_r.totype(i);
						q += m_r.approxstep()/2;
						// XXX Just look at circumscriped circle to box, could be optimized to yield distance to box walls
						if (true || DistPeriodic2<_len,_len2>(q,p,m_bbox)<=f2){
							m_s[i].push_back(&d);
						}
					}
				}
				//	cout << n << "  p=" << p << "  h=" << h << "  t=" << t << "  a=" << nn << endl;
			}
			tim.ToEta(n++,m_orgdata.size(),cerr);
		}
	}
	*/

	void Addsmoothinglenghts()
	{
		if(m_searched_particles.size()!=0) throw_("Addsmoothinglenghts has already been called");
		m_searched_particles.resize(m_orgdata.size());
		const _len l=getrange().approxstep();
		const flt f=1.0;

		size_t n=0;
		timer tim;
		for(vector<t_data>::const_iterator itt=m_orgdata.begin();itt!=m_orgdata.end();++itt){
			const t_data& d=*itt;
			const _point_len dp=d.pos();
			assert( m_bbox.IsInsideBBox(dp) );
			const _len h=d.h();
		 	const t_idx home=m_r.toidx(dp);
 			const _len dwall=Mindisttowalls(home,dp);

 			if (h<dwall) {
				assert( hasPoint(m_s[home],d) );
			} else {
				_point_len p0=dp-f*h;
				_point_len p1=dp+f*h;
				m_bbox.WrapToInsideBox(p0);
				m_bbox.WrapToInsideBox(p1);
				const Triplerange<size_t> t(m_r.toidx(p0),(m_r.toidx(p1)+1).periodic(m_r.size()),m_r.size());

				for(Triplerange<size_t>::const_iterator itt=t.begin();itt!=t.end();++itt){
					const t_idx i=*itt;
 					if (i!=home){
						// is this potentially unsafe? whatif truncations does not yield p0+l = p1?? So that bbox.isParticleInsideBox2(m_r.totype(i),(m_r.totype(i))).periodic(m_r.size()),dp,h,true);
						const int b=m_bbox.isParticleInsideBox(m_r.totype(i),f*l,dp,f*h,true);
						// happens but should not: assert(b!=2);
						if (b>0)  {
							assert( !hasPoint(m_s[i],d) );
							m_s[i].push_back(&d);
						}
					}
				}
			}
			tim.ToEta(n++,m_orgdata.size(),cerr);
		}
	}

	t_map_data findclosest(const _point_len& p,size_t N) const
	{
		assert( !m_periodic || m_bbox.IsInsideBBox(p) );
		N=min(N,m_N);
		if (m_N<1) return findclosest_nonoptimized(p,N);
		if (m_periodic) fill(m_searched_periodic.begin(),m_searched_periodic.end(),false);
		fill(m_searched_particles.begin(),m_searched_particles.end(),false);

		#ifndef NDEBUG
			m_s2=0;
		#endif
		assert( m_s.size()>0 && N>0);
		_len2 distmin2(numeric_limits<flt>::max());
		t_map_data nclosest;

		const t_data* f=0;
		t_idx f_idx(numeric_limits<size_t>::max());
		const t_idx idx=m_r.toidx(p);

		f=Findclosest_incube(idx,p,distmin2,f,f_idx,nclosest,N);

		if(f!=0 && nclosest.size()==N){
 			const _len dwall=Mindisttowalls(idx,p);
			if (distmin2>=dwall*dwall*fudge){
				assert( FltValidity(distmin2/_len2(1)) );
				const size_t extra_shell=(Sqrt<_len2,_len>(distmin2)-dwall)*fudge>m_r.approxstep();
				const size_t optimized_maxshells=min(1+extra_shell,m_r.size());

				size_t shell=1;
				while(shell<=optimized_maxshells){
					f=Findclosest_subshell(idx,p,distmin2,f,f_idx,shell++,nclosest,N);
				}
			}
		} else {
			// if none found, proceede to more shells
			size_t shell=1;
			while((f==0 || nclosest.size()<N) && shell<m_r.size()) {
				//cout << "shell=" << shell << endl;
				f=Findclosest_subshell(idx,p,distmin2,f,f_idx,shell++,nclosest,N);
			}
			size_t extra_shells=CalcExtraShells(idx,p,distmin2,fudge);
			while(shell<=extra_shells) {
				//cout << "shell=" << shell << " extrashells=" << extra_shells << endl;
				const t_data* f_old=f;
				f=Findclosest_subshell(idx,p,distmin2,f,f_idx,shell++,nclosest,N);
				if (f!=f_old) extra_shells=CalcExtraShells(idx,p,distmin2,fudge);
			}
		}

		assert( f!=0 && distmin2>=_len2(0) );
		assert( f_idx==m_r.toidx(f->pos()) );
		assert( nclosest.size()==N );
		return nclosest;
	}

	t_map_data findclosest_nonoptimized(const _point_len& p,size_t N) const
	{
		assert( !m_periodic || m_bbox.IsInsideBBox(p) );
		assert( m_s.size()>0 && N>0);
		assert( m_r.isinrange(p));
		N=min(N,m_N);

		t_map_data nclosest;
		for(size_t n=0;n<m_s.size();++n){
			const t_vec_data& v=m_s[n];
			for(size_t m=0;m<v.size();++m){
				_point_len q=v[m]->pos();
				const _len2 d2=Dist2(q,p);
				if (nclosest.size()<N  || d2<(--nclosest.end())->first) {
					if(nclosest.size()>=N) nclosest.erase(--nclosest.end());
					nclosest.insert(make_pair(d2,make_pair(v[m],t_idx(numeric_limits<size_t>::max()))));
				}
			}
		}
		assert( nclosest.size()==N || (nclosest.size()<N && nclosest.size()==m_N) );
		for(t_map_data::iterator itt=nclosest.begin();itt!=nclosest.end();++itt){
			itt->second.second=m_r.toidx(itt->second.first->pos());
		}
		return nclosest;
	}

	const t_data& findclosest(const _point_len& p) const
	{
		t_map_data nclosest=findclosest(p,1);
		assert( nclosest.size()==1 );
		return *(nclosest.begin()->second.first);
	}

	const t_data& findclosest_nonoptimized(const _point_len& p) const
	{
		t_map_data nclosest=findclosest_nonoptimized(p,1);
		assert( nclosest.size()==1 );
		return *(nclosest.begin()->second.first);
	}

	t_map_data find_withinradius(const _point_len& p,const _len& r,const bool checkdist=true) const
	{
		assert( !m_periodic || m_bbox.IsInsideBBox(p) );
		assert( m_s.size()>0);
		assert( m_r.isinrange(p));
		assert( r>_len(0) );
		if (m_periodic) fill(m_searched_periodic .begin(),m_searched_periodic .end(),false);
		fill(m_searched_particles.begin(),m_searched_particles.end(),false);

		#ifndef NDEBUG
			m_s2=0;
		#endif

		_len2 distmin2(numeric_limits<flt>::max());
		t_map_data nclosest;

		const t_data* f=0;
		t_idx f_idx(numeric_limits<size_t>::max());
		const t_idx idx=m_r.toidx(p);

		f=Findclosest_incube(idx,p,distmin2,f,f_idx,nclosest,m_N);
		assert( m_r.size()>1 );
		const size_t maxshells=min(static_cast<size_t>(r/m_r.approxstep()*fudge+1),m_r.size()-1);
		size_t shell=1;
		while(shell<=maxshells){
			f=Findclosest_subshell(idx,p,distmin2,f,f_idx,shell++,nclosest,m_N);
		}

		if (checkdist){
			const _len2 r2=r*r;
			for(t_map_data::iterator itt=nclosest.begin();itt!=nclosest.end();++itt){
				if (itt->first>r2) nclosest.erase(itt);
			}
		}

		return nclosest;
	}

	friend ostream& operator<<(ostream& s,const searchgrid& x)
	{
		s << "searchgrid: {" << x.getrange() << " gridsize=" << x.sizet() << " datasize=" << x.datasize();
		if (x.periodic()) s << " periodic=" << x.periodic() << " bbox="  << x.bbox();
		return s<< "}";
	}
};

inline ostream& operator<<(ostream& s,const searchgrid::t_map_data& x)
{
	size_t i=0;
	s << "t_map_data: size=" << x.size() << " {";
	if(x.size()>0) s << "\n";
	for(searchgrid::t_map_data::const_iterator itt=x.begin();itt!=x.end();++itt,++i) {
		s << "[" << fwidth(i,3,0) << "]: dist2=" << itt->first;
		s << " point={" << itt->second.first << "=" << itt->second.first->pos() << ";idx=" << itt->second.second << "}\n";
	}
	return s << "}";
}

#endif // __SEARCH_H__
