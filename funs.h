#ifndef __FUNS_H__
#define __FUNS_H__

#define Unimplemented throw_("Function unimplemented")
#define Dontgethere   throw_("Dontgethere")

#ifndef NDEBUG
	#define ON_DEBUG(a) a
#else
	#define ON_DEBUG(a)
#endif

#ifdef USE_FFTW
	extern int posix_memalign(void **__memptr, size_t __alignment, size_t __size);
#endif

// Small template funs
template<class T,class R> bool isinmap (const map<T,R>& m,const T& t) {return m.size()>=2 && m.begin()->first<=t && t<(--m.end())->first;}
template<class T> const size_t getsteps(const T& r0,const T& r1,const T& rstep,const bool logarithmic) {size_t N=0; for(T r=r0;r<r1;logarithmic ? r*=rstep/T(1) : r+=rstep) ++N; return N;}

inline std::string Version(){return string("VERSION: ") + VERSION + "." + VERSION_REV;}

inline std::string Config()
{
	std::string s;
	#ifdef NDEBUG
		s+="NDEBUG";
	#else
		#ifdef PROFILE
			s+="PROFILE";
		#else
			s+="DEBUG";
		#endif
	#endif
	#ifdef USE_UNITS
		s+= " USE_UNITS";
	#endif
	#ifdef USE_FLOAT_CHECKS
		s+= " USE_FLOAT_CHECKS";
	#endif
	#ifdef USE_FFTW_IC
		s+= " USE_FFTW_IC";
	#endif
	#ifdef USE_FFTW
		s+= " USE_FFTW";
	#endif
	#ifdef USE_MPI
		s+= " USE_MPI";
	#endif
	if      (sizeof(void*)==4) s+=" 32BIT";
	else if (sizeof(void*)==8) s+=" 64BIT";
	else                       s+=" XXBIT";

    const long one= 1;
    const int big=!(*(reinterpret_cast<const char *>(&one)));
	if (big) s+=" BIGENDIAN";
	else     s+=" LITENDIAN";

	return s;
}

inline string FormatCompilerMsg(const string& file,const int line)
{
	#ifdef WIN32
		return file + ":" + line + ":";
	#else
		return file + "(" + line + ")";
	#endif
}

#ifdef WIN32
	extern "C"{
		#ifdef _AFXDLL
			__declspec(dllimport) void* __stdcall GetCurrentThread();
			__declspec(dllimport) void* __stdcall GetCurrentThreadId();
			__declspec(dllimport) int   __stdcall SetThreadPriority(void* hThread,int nPriority);
		#else
			void* __stdcall GetCurrentThread();
			void* __stdcall GetCurrentThreadId();
			int __stdcall SetThreadPriority(void* hThread,int nPriority);
		#endif
	}
#else
	int nice(int inc);
#endif

inline void SetNiceLevel(const int level)
{
	#ifdef WIN32
		SetThreadPriority(GetCurrentThread(),(-level));
	#else
		const int n=nice(level);
		if (n<0) throw_("Could not set nice level");
	#endif
}

inline size_t GetThreadId()
{
	#ifdef WIN32
		assert( sizeof(size_t)==sizeof(void*) );
		return reinterpret_cast<size_t>(GetCurrentThreadId());
	#else
		// may be replaced by return 0; if phtread not found!
		assert( sizeof(size_t)==sizeof(pthread_t) );
		const pthread_t p=pthread_self();
		size_t q=0;
		memcpy(&q,&p,sizeof(q));
		return q;
	#endif
}

inline string System(const string& cmd,const bool throwexception=true,const bool captureoutput=false)
{
	if (!captureoutput){
		const int n=system(cmd.c_str());
		if (n!=0 && throwexception) throw_(string("system command failed with code=") + n + " cmd=<" + cmd + ">");
		return "";
	} else {
		static size_t n=0;
		const string file("/tmp/tempfile." + tostring(GetThreadId()) + "."  + tostring(n++) + ".txt" ); // , or some random number, or process id
		ifstream s1(file.c_str());
		if(s1) {
			s1.close();
			System(("rm " + file).c_str(),true,false);
		}
		System(cmd + " > " + file,throwexception,false);

		string t;
		char buff[16*1024];
		ifstream s2(file.c_str());
		while(s2) {
			s2.getline(buff,16*1024);
			if (s2) t += buff;
		}
		System(("rm " + file).c_str(),true,false);
		return t;
	}
}

class timer
{
	private:
	double  m_t,m_cpu_t;
	double* m_addt;
	mutable double m_last_t,m_last_eta;

	static double gettime()
	{
		struct timeval tv;
		gettimeofday(&tv,NULL);
		return tv.tv_sec +  static_cast<double>(tv.tv_usec)/1000000;
	}

	static double getcputime()
	{
		static const double f=1.0/CLOCKS_PER_SEC;
		return f*clock();
	}

	template<class T>
	static double Remaining(const double t,const T& n,const T& N)
	{
		if (n>=N || N<=T(0)) throw_("value out of range in timer::Remaining, n>=N or N<=0, n=" + tostring(n) + " N=" + tostring(N));
		const double p=static_cast<double>(n/T(1)+1)/(N/T(1));
		const double p2=p>0 ? t/p : 0;
		return p2>t ? p2-t : 0;
	}

	public:
	timer()          : m_t(gettime()), m_cpu_t(getcputime()), m_addt(0),  m_last_t(-1), m_last_eta(-1) {}
	timer(double& t) : m_t(gettime()), m_cpu_t(getcputime()), m_addt(&t), m_last_t(-1), m_last_eta(-1) {}
	~timer() {if (m_addt!=0) (*m_addt) += elapsed();}

	void   reset  ()       {m_t=gettime(); m_cpu_t=getcputime(); m_last_t=-1; m_last_eta=-1;}
	double elapsed() const {return gettime()-m_t;}
	double cputime() const {return getcputime()-m_cpu_t;}

	static string ToHMS(const double& t)
	{
		assert( t>=0 );
		const unsigned int it=static_cast<unsigned int>(t+.5);
		const unsigned int hours=it/(60*60);
		const unsigned int mins=(it-hours*60*60)/(60);
		const unsigned int secs=(it-hours*60*60-mins*60);
		assert( secs<60 && mins<60);
		return tostring(hours) + ":" + (mins<10 ? "0": "") + tostring(mins) + ":" +  (secs<10 ? "0": "") + tostring(secs);
	}

	template<class T> static inline int Topercent(const T x,const T N,const int decimals=-1)
	{
		assert(x<N && N!=T(0));
		float pf=static_cast<float>(x/T(1))/(N/T(1));
		if (decimals>0) pf=static_cast<int>(pf*decimals)/(1.0*decimals);
		return static_cast<int>(pf*100+.5);
	}

	template<class T> void ToEta(const T& n,const T& N,ostream& s,const double timeprintsteps=30,const bool verboseprint=false) const
	{
		if (n>=N) return;
		assert( n<N );

		const double t=gettime()-m_t;
		if (n==T(0)) {
			m_last_t=t;
			return;
		}

		const double e=(t-m_last_t);
		if (e>timeprintsteps) {
			const double f=timeprintsteps*60;
			const double r=Remaining(t,n,N);
			if (m_last_eta<0 || r<f || (r>f && e>f) /*|| (m_last_eta>0 && r>1.2*m_last_eta) */ ){
				time_t tm;
				time(&tm);
				const string systime=ctime(&tm);
				if (m_last_eta<0) s << "Current system time: " << systime;
				tm += static_cast<time_t>(r);
				const string eta=ctime(&tm);
				const bool extraday=(eta.substr(0,3)!=systime.substr(0,3));
				const string eday=extraday ? " "+eta.substr(0,3)+" " : "";
				s << "Time [h:m:s]=" << ToHMS(t) <<  ", R=" << ToHMS(r) << ", ETA=" << eday << eta.substr(11,8);
				if(verboseprint) {
					const string t=", n/N=" + tostring(n) + "/" +  tostring(N) + "=" +  tostring(Topercent(n,N,10)) + "\%,";
					s << t;
					for(size_t i=t.size();i<42;++i) s << " ";
					s << " CPU=" << ToHMS(cputime());
				}
				s << endl;

				m_last_t=t;
				m_last_eta=r;
			}
		}
	}
	friend ostream& operator<<(ostream& s,const timer x)
	{
		return s << "Time [h:m:s]= " << x.ToHMS(x.elapsed()) << " CPU=" << x.ToHMS(x.cputime());
	}
};

/*
int SwapEndian(void *data,const size_t size) {

	short xs;
	long xl;

	switch (size){
		case 2:
			xs = *(short *)data;
			*(short *)data = ( ((xs & 0x0000ff00) >> 8) | ((xs & 0x000000ff) << 8) );
			break;
		case 4:
			xl = *(long *)data;
			*(long *)data = ( ((xl & 0xff000000) >> 24) | ((xl & 0x00ff0000) >> 8) |
					((xl & 0x0000ff00) << 8) | ((xl & 0x000000ff) << 24) );
			break;
			default: break;
	}

	return 0;
}
*/

#endif // __FUNS_H__
