void unit_conversion(data_info& inf)
{
	// physical constants in cgs units
	//const double GRAVITY   = 6.672e-8;
	const double BOLTZMANN = 1.3806e-16;
	const double PROTONMASS = 1.6726e-24;

	// internal unit system of the code
	//  code length unit in cm/h
	const double UnitLength_in_cm= 3.085678e21;
	const double UnitMass_in_g= 1.989e43;	 //  code mass unit in g/h
	const double UnitVelocity_in_cm_per_s= 1.0e5;

	const double UnitTime_in_s= UnitLength_in_cm / UnitVelocity_in_cm_per_s;
	//const double UnitDensity_in_cgs= UnitMass_in_g/ Pow3(UnitLength_in_cm);
	//const double UnitPressure_in_cgs= UnitMass_in_g/ UnitLength_in_cm/ Pow2(UnitTime_in_s);
	const double UnitEnergy_in_cgs= UnitMass_in_g * Pow2(UnitLength_in_cm) / Pow2(UnitTime_in_s);

	//const double G=GRAVITY/ Pow3(UnitLength_in_cm) * UnitMass_in_g * Pow2(UnitTime_in_s);
	const double Xh= 0.76;					 // mass fraction of hydrogen
	//const double HubbleParam= 0.65;

	for(int i=1; i<=inf.NumPart; i++)
	{
		if(inf.P[i].Type==0)		 // gas particle
		{
			const double MeanWeight= 4.0/(3*Xh+1+4*Xh*inf.P[i].Ne) * PROTONMASS;

			// convert internal energy to cgs units
			const double u    = inf.P[i].U * UnitEnergy_in_cgs/ UnitMass_in_g;
			const double gamma= 5.0/3;

			// get temperature in Kelvin
			inf.P[i].Temp= MeanWeight/BOLTZMANN * (gamma-1) * u;
		}
	}
}

int Parseargs(const int argc,char** argv,string& o,string& m,int& f,bool& icmode,bool& reordering,bool& unit_conversion,bool& noheader,int& outputprecision,float& poseps,float& veleps)
{
	for(int i=2;i<argc;++i){
		const string arg(argv[i]);
		const string val =arg.size()>=2 ? arg.substr(2,arg.size()) : "";
		const string arg2=arg.size()>=2 ? arg.substr(0,2)  : "";
		//cerr << "\nParsing arg: " << arg << " (" << arg2 << ") =  " << val << "\n";

		if (arg2=="-o"){
			o=val;
			//cerr << string("\n  Output: " + val);
		}
		else if (arg2=="-f"){
			if      (val=="all")  f=-1;
			else if (val=="gas")  f=1;
			else if (val=="halo") f=2;
			else if (val=="disk") f=3;
			else if (val=="bulge")f=4;
			else if (val=="stars")f=5;
			else if (val=="bndry") f=6;
			else {cerr << "\n## Error, unknown option: " << arg << "\n"; return -1;}
		}
		else if (arg2=="-m"){
			if (val=="ascii")       m="ascii";
			else if (val=="float")  m="float";
			else if (val=="double") m="double";
			else {cerr << "\n## Error, unknown option: " << arg << "\n"; return -1;}
		}
		else if (arg2=="-i"){
			icmode=true;
		}
		else if (arg2=="-r"){
			reordering=true;
		}
		else if (arg2=="-i"){
			unit_conversion=true;
		}
		else if (arg2=="-n"){
			noheader=true;
		}
		else if (arg2=="-x"){
			outputprecision=totype<int>(val);
			cout.precision(outputprecision);
			cerr.precision(outputprecision);
		}
		else if (arg2=="-p"){
			poseps=totype<float>(val);
		}
		else if (arg2=="-v"){
			veleps=totype<float>(val);
		}
		else {
			cerr << "\n## Unknown option:" << arg << "\n";
			return -1;
		}
	}
	return 0;
}

int Usage_Toascii(int argc, char **argv)
{
	cerr << "\nUsage: " << argv[0] << " <snapshotfile> [-head] [-oOutputfile] [-mOutputmode] [-fFilter] [-i] [-pPrecision]\n";
	cerr << "  " << Version() << " " << Config() << "\n";
	cerr << "  -head       : show header info only\n";
	cerr << "  -oOutputfile: filname (-o only for std out in ascii mode)\n";
	cerr << "  -mOutputmode: float | double | ascii\n";
	cerr << "  -fFilter    : all | gas | halo | disk | bulge | stars | bndry\n";
	cerr << "  -n          : no output of info header in ascii mode\n";
	cerr << "  -i          : treat file as inital condition file (i.e. without sph U,Rho, etc,)\n";
	cerr << "  -r          : reordering\n";
	cerr << "  -c          : unit conversion\n";
	cerr << "  -xPrecision : set output ascii precision to Precision digits, implies scientific format\n";
	cerr << "  -pPrecision : set particle positions precision, zero below Precision\n";
	cerr << "  -vPrecision : set particle posocity precision, zero below Precision\n";
	cerr << "  default   : -oOut.txt -mascii -fall -n\n";
	argc++; // avoid warn
	return -1;
}

template<class T>
void Writebinary(const string& file,const data_info& inf,const int f)
{
	int n=0;
	for(int i=1;i<=inf.NumPart;++i) {if (f==-1 || f==inf.P[i].Type) ++n;}
	vector<T> x;
	x.resize(3*n);

	for(int i=1,j=0;i<=inf.NumPart;++i) {
		if (f==-1 || f==inf.P[i].Type) {
			x[j]  =inf.P[i].Pos[0];
			x[j+1]=inf.P[i].Pos[1];
			x[j+2]=inf.P[i].Pos[2];
			j+=3;
		}
	}
	//Writebin("",file,x);
	ofstream s(file.c_str());
	if (!s) throw_( "could not open file <" + file + "> for writing");
	Writebin(s,x,false);
}

float Truncdigits(const float x,const float eps)
{
	if (eps<0) return x;
	else {
		const float fx=fabs(x);
		if (fx<eps) return 0;
		const int sign=x<0 ? -1 : 1;
		const float y=static_cast<int>(fx/eps+.5)*eps;
		return y*sign;
	}
}

int main_toascii(int argc, char **argv)
{
	try{
		args a(argc,argv,false,false);
		if(argc<2) return Usage_Toascii(argc,argv);
		if (!FileExists(argv[1]))
		{
			cerr << "\n## Error: file <" << argv[1] << "> does not exist...aborting!\n";
			return -1;
		}

		string arg_o="out.txt";
		string arg_m="ascii";
		int    arg_f=-1;
		bool arg_icmode=false;
		bool arg_reordering=false;
		bool arg_unit_conversion=false;
		bool arg_noheader=true;
		int arg_outputprecision=-1;
		float arg_poseps=-1;
		float arg_veleps=-1;

		if (Parseargs(argc,argv,arg_o,arg_m,arg_f,arg_icmode,arg_reordering,arg_unit_conversion,arg_noheader,arg_outputprecision,arg_poseps,arg_veleps)!=0) return Usage_Toascii(argc,argv);
		assert(arg_f>-2);
		data_info inf=load_snapshot(argv[1],1,arg_icmode);
		if (arg_reordering) 	 reordering(inf);	     // call this routine only if your ID's are set properly
		if (arg_unit_conversion) unit_conversion(inf);	 // optional stuff

		ofstream os(arg_o.c_str());
		ostream* po=(arg_o=="") ? &cout : &os;
		ostream& o=*po; // avoid VC error
		if (arg_m=="ascii"){

			o << "% Creator: " << argv[0] << "\n";
			if (!arg_noheader) o << "%   " << Version() << "  config: " << Config() << "  file: " << argv[1] << "  header: " << inf.header1 ;
			o << "%   particle data : pos[3] pos[3] Mass Type(int) Rho U Temp Ne\n";
			if (arg_outputprecision>=0) {
				o << scientific;
				o.precision(arg_outputprecision);
			}

			for(int i=1;i<=inf.NumPart;++i) {
				const particle_data& p=inf.P[i];
				if (arg_f==-1 || arg_f==p.Type) {
					// o << p;
					o << Truncdigits(p.Pos[0],arg_poseps) << " " << Truncdigits(p.Pos[1],arg_poseps) << " " << Truncdigits(p.Pos[2],arg_poseps) << " ";
					o << Truncdigits(p.Vel[0],arg_veleps) << " " << Truncdigits(p.Vel[1],arg_veleps) << " " << Truncdigits(p.Vel[2],arg_veleps) << " ";
					o << p.Mass << " " << p.Type<< " " << p.Rho<< " " << p.U << " " << p.Temp << " " << p.Ne << "\n";
				}
			}
			return 0;
		}
		else if(arg_m=="float") {Writebinary<float>(arg_o,inf,arg_f);return 0;}
		else if(arg_m=="double"){Writebinary<double>(arg_o,inf,arg_f);return 0;}
		else Dontgethere;
	}
	CATCH_ALL;
	return -1;
}

/*

int Usage_Toascii(const args& a)
{
	cerr << "\nUsage: " << argv[0] << " <snapshotfile> [-f <filter>] [-h] [-n] [-o <outputfile>] [-r]\n";
	cerr << "  " << Version() << " " << Config() <<  " \n";
	cerr << "Options:\n";
	cerr << "  -f <int>: 1=gas, 2=halo, 4=disk, 8=bulge, 16=stars, 32=bndry\n";
	cerr << "  -h: show header info only\n";
	cerr << "  -n: no output of info header mode\n";
	cerr << "  -o <file>: outputfile, if not stdout\n";
	cerr << "  -r: reordering\n";
	argc++; // avoid warn
	return -1;
}

int main_toascii(int argc, char **argv)
{
	try{
		args a(argc,argv,false);

		const int  filter =a.parseval<int>("-f",0);
		const bool headeronly=!a.parseopt("-h")
		const bool noheader=!a.parseopt("-n")
		const string& arg_o=a.parsearg<string>("-o","");
		const bool reordering=a.parseopt("-r");

		Initialize(a.parseopt("-testsuite"),false,false);
 		if(a.size()!=2) return Usage_Density(a);

		const data_info inf=load_snapshot(a[1].c_str(),1,true,true);
		if (reordering) reordering(inf);  // call this routine only if your ID's are set properly

		ofstream os(arg_o.c_str());
		ostream* po=(arg_o=="") ? &cout : &os;

		o << "% Creator: " << argv[0] << "\n";
		if (!noheader) o << "%   " << Version() << "  config: " << Config() << "  file: " << argv[1] << "  header: " << inf.header1 ;
		o << "%   particle data : pos[3] pos[3] Mass Type(int) Rho U Temp Ne\n";

		for(int i=1;i<=inf.NumPart;++i) {
			if (arg_f==-1 || arg_f==inf.P[i].Type) o << inf.P[i];
		}
		return 0;
	}
	CATCH_ALL;
	return -1;
}
*/
