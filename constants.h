#ifndef __CONSTANTS_H__
#define __CONSTANTS_H__

namespace Constants{
  const flt _pi    =3.1415926535898;      // (pi) dimensionless
  const flt _e     =2.718281828459;       // (e) dimensionless
  const flt _sqrtpi=sqrt(_pi);            // (square root of pi) dimensionless
  const flt _c     =299792458.0;          // (speed of light) m s^-1
  const flt _k     =1.3806505E-23;        // (planck constant) J K^-1
  const flt _h     =6.6260693E-34;        // (h constant) kg m^-2 s^-1
  const flt _hbar  =1.0545716823645E-34;  // (hbar=h/2pi) kg m^-2 s^-1
  const flt _G     =6.6742E-11;           // (gravitation constant) m^3 s^-2 kg^-1
  const flt _epsilon0=8.8541878176204E-12;// (permittivity of vacuum) F m^-1
  const flt _my    =0.9994556794412;      // (reduced mass of proton+electron) m_e
  const flt _rdb   =10967758.042074;      // (Rydberg constant, proton+electron) m
  const flt _q     =1.60217653E-19;       // (electron charge) _coul
  const flt _pc    =3.085678E16;          // (parsec) m
  const flt _kpc   =1000*_pc;             // (kilo parsec) m
  const flt _Mpc   =1000*1000*_pc;        // (Mega parsec) m
  const flt _AU    =1.495979E11;          // (astronomical unit) m
  const flt _yr    =31556925.9747;        // (year) s
  const flt _H0    =2.2685451949296E-18;  // (Hubble constant) Hz
  const flt _t0    =1.0/_H0;              // (cosmic time now) s

  const flt _Rsun  =6.96E8;               // (radius sun) m
  const flt _Msun  =1.989E30;             // (mass sun) kg
  const flt _Mearth=5.974E24;
  const flt _Mjup  =1.90E27;

  const flt _eV    =1.60217733E-19;     // (electron volt) J
  const flt _me    =9.1093897E-31;      // (mass of electron)kg
  const flt _mp    =1.6726231E-27;      // (mass of proton) kg
  const flt _mn    =1.6749286E-27;      // (mass of neutron) kg
  const flt _mH1   =1.6735E-27;         // (mass of Hydrogen 1, HI, one electron) kg
  const flt _mHe42 =6.6465E-27;         // (mass of Helium 42, HeI,  four electrons) kg

  const flt _wLya  =121.5684488556E-9;  // (Lyman alpha wavelength) m
  const flt _vLya  =_c/_wLya;           // (Lyman alpha frequency) Hz
};

#endif // __CONSTANTS_H__
