class FilterTophat
{
	private:
	static flt W(const flt x)  {return 3./(x*x*x) * (Sin(x) - x*Cos(x));}

	public:
	flt operator()(const flt x) const
	{
		static const flt linearthrs=1E-2;
	 	static const flt slope=(W(linearthrs)-1) / (linearthrs-0);

		const flt w=(Abs(x)>linearthrs) ? W(x) : 1+slope*x;
		assert( w<=1.0 && w>=-1);
		return w*w;
	}
};

class FilterSinc
{
	private:
	static flt W(const flt x) {return Sinc(x);}

	public:
	flt operator()(const flt x) const
	{
		static const flt linearthrs=1E-4;
		static const flt slope=(W(linearthrs)-1) / (linearthrs-0);

		const flt w=(Abs(x)>linearthrs) ? W(x) : 1+slope*x;
		assert( w<=1.0 && w>=-1);
		return w;
	}
};

template<class CMB,class FILTER>
class IntegratorWithFilter
{
	private:
	const CMB& m_pk;
	const FILTER m_W;
	const _len m_R;

	public:
	IntegratorWithFilter(const CMB& pk,const _len R) : m_pk(pk), m_W(FILTER()), m_R(R)  {assert(R>_len(0));}

	_len operator()(const _per_len k) const {
		const flt w=m_W(k*m_R);
		const _vol pk = m_pk(k);
		return k*k * pk * w;
	}

	flt Integrate(const _per_len kthreshold=_per_len(-1)) const
	{
		const flt norm=1./(2*_pi*_pi);
		const flt allowed_error=4.e-7;

		// as in convolve_pk, choose first region of integration as that, which has 16 oscillations. Thus k*R = 32*pi.
		const _per_len integ_range = 32.*_pi/(m_R);
		flt answer = 1.; // dummy value to force while loop
		flt total = 0;
		int izone = 0;
		while (Abs(answer)>allowed_error*Abs(total) || izone*integ_range<kthreshold) {
			answer = ::Integrate<_len,_per_len,flt,IntegratorWithFilter<CMB,FILTER> >(*this,izone*integ_range,(izone+1)*integ_range,1,1);
			total += answer;
			++izone;
		}
		return norm*total;
	}
};


class PkCmb
{
	public:
	typedef map<flt,flt> t_cmb;

	private:
	t_cmb m_kp;
	vector<flt> m_vk,m_vp;

	static t_cmb ReadCMBFastData(const string& filename,const int column)
	{
		AssertFileNotEmpty(filename);
		ifstream s(filename.c_str());

		/*
		if (!cmbfastmode) // read ic-mode data, k,Log10(power)
		{
			while(true){
				flt ktemp,pow;
				s >> ktemp >> pow;
				if (!s) return kp;
				if (ktemp<0) throw_("bad k parameter, less that zero");
				if (pow>10)  throw_("bad power specturm, is it in the 'k Log10(power)' form?'");
				kp[ktemp]=pow;
			}
		}*/

		// CMB doc
		// If requested the code will calculate and output matter transfer functions.
		// The output can be for specified redshifts, which is particularly useful in
		// models with massive neutrinos. If there are no massive neutrinos the
		// output file gives wavector k/h, CDM, baryons, photons and massless
		// neutrinos; the wavevector is in units of Mpc/h where h stands for the
		// Hubble constant in units of 100 km/sec/Mpc. A sixth column for massive
		// neutrinos is added if necessary. Note that these are "raw" transfer
		// functions, their normalization reflects our choice of the initial
		// conditions. This is explained in more detail in DOC/README_NORMALIZATION.
		// The output is done by subroutine outtransf in
		t_cmb kp;
		const flt sndx=.97;
		const flt cmbfast_tfnorm=1E-10 * 25 * 2 * _pi*_pi;

		if (column<1 || column>4) throw_("bad column, must be in range [1;4], 1=cdm, 2=baryons, 3=photons, 4=neutrinos");
		flt klast=-1;

		while(s){
			flt ktemp,cdm,baryons,photons,neutrinos;
			s >> ktemp;
			if (!s) break;
			s >> cdm >> baryons >> photons >> neutrinos;
			if (!s) throw_("bad read, missing column");

			if (ktemp<0) throw_("bad data point, k less than zero");
			if (klast>0 && ktemp<klast) throw_("bad data point k[n] < k[n-1]");
			klast=ktemp;

			flt ttemp;
			switch(column){
				case 1 : ttemp=cdm; break;
				case 2 : ttemp=baryons; break;
				case 3 : ttemp=photons; break;
				case 4 : ttemp=neutrinos; break;
				default : throw("bad column");
			}

			ttemp *= cmbfast_tfnorm; // normalize T(k->0) = 1.. not really necessary
			//kp[_per_len(ktemp*1000)]=_vol(Pow(ktemp,sndx)*ttemp*ttemp);
			kp[ktemp]=Log10(Pow(ktemp,sndx)*ttemp*ttemp);
		}
		return kp;
	}

	static int bsrch(int narr,const flt *arr,flt val)
	{
		// taken from Joe  --- does a binary search for val in the array pointed by arr. array should be in ascending order
		int low,high,mid;

		low=0;
		high=narr-1;

		//printf("in bsrch searching for %f %d\n",val,narr);
		while((high-low)>1){
			mid=(low+high)/2;
			if(*(arr+mid)>val)high=mid;
			if(*(arr+mid)<=val)low=mid;
		}
		//printf("end bsrch located %f at %d\n",val,high);
		return (high);
	}

	static flt power_interp(flt kk,const vector<flt>& kt,const vector<flt>& lp)
	{
		int i0,i1;
		//extern int nk;
		//extern flt *ktable, *logptable;

		const flt* ktable   =reinterpret_cast<const flt*>(&kt[0]);
		const flt* logptable=reinterpret_cast<const flt*>(&lp[0]);
		int nk=kt.size();

		flt slope,power;

		// linearly interpolate in log space
		if (kk == 0) { // handle k=0 differently
			if (ktable[0] == 0.) power = Pow(10.,logptable[0]); else power = 0.;
		} else {
			i1 = bsrch(nk,ktable,kk);
			i0 = i1-1;
			if ((ktable[0] == 0.) && (i0 == 0)) { // handle kk in first bin when k[0]=0.
			slope = (logptable[i1] - logptable[i0]) / (ktable[i1] - ktable[i0]);
			power = logptable[i0] + slope * (kk - ktable[i0]);
			} else {
				slope = (logptable[i1] - logptable[i0]) / Log10(ktable[i1]/ktable[i0]);
				power = logptable[i0] + slope * Log10(kk/ktable[i0]);
			}
			power = Pow(10.,power);
		}

		return power;
	}

	void AssignMap2Vectors()
	{
		m_vk.resize(m_kp.size());
		m_vp.resize(m_kp.size());
		size_t i=0;
		for(t_cmb::const_iterator itt=m_kp.begin();itt!=m_kp.end();++itt,++i) {
			m_vk[i]=itt->first;
			m_vp[i]=itt->second;
		}
	}

	public:
	PkCmb(const string& cmbfilename,const int column)
	: m_kp(ReadCMBFastData(cmbfilename,column)) {AssignMap2Vectors();}

	PkCmb(const string& filename)
	{
		AssertFileNotEmpty(filename);
		ifstream s(filename.c_str());
		s >> *this;
	}

	size_t   size() const {return m_kp.size();}
	_per_len kmin() const {assert(m_kp.size()>0); return _per_len( m_kp.begin() ->first*1E-3);}
	_per_len kmax() const {assert(m_kp.size()>0); return _per_len((--m_kp.end())->first*1E-3);}

	_vol operator()(const _per_len k) const
	{
		// XXX, Non internal units, per-hand conversion from/to len/Mpc
		// internal units in map <Mpc h^-1.Mpc^3 ???>
		const flt k_per_Mpc=k/_per_len(1E-3);
		// if(m_vk.size()==0 || k_per_Mpc<m_vk[0] || k_per_Mpc>m_vk[m_vk.size()-1]) warn_(string("k value out of bounds, k=") << k << " kmin=" + _per_len(m_vk[0]*1E3) + " kmax=" + _per_len(m_vk[size()-1]*1E3));
		const flt power_Mpc3=power_interp(k_per_Mpc,m_vk,m_vp);
		return _vol(power_Mpc3*1E9);
	}

	void Normalize(const flt sigma8_given,const flt sigma8_unnorm)
	{
		const flt pnorm = Pow2(sigma8_given/sigma8_unnorm);
		const flt logpnorm = Log10(pnorm);
		assert( m_kp.size()==m_vp.size() );
		size_t i=0;
		for(t_cmb::iterator itt=m_kp.begin();itt!=m_kp.end();++itt,++i) itt->second += logpnorm;
		AssignMap2Vectors();
		assert( m_kp.size()==m_vp.size() );
	}

	flt SigmaR(const _len r) const {return Sqrt( IntegratorWithFilter<PkCmb,FilterTophat>(*this,_len(r)).Integrate() );}
	flt Sigma8()             const {return SigmaR(_len(8*1000));}

	friend ostream& operator<<(ostream& s,const PkCmb& x)
	{
		s << "%  Pk: size=" << x.m_kp.size() << "\n";
		s << "%     Ouput:  r [" + UNIT_LENGTH  + "]  k [" + UNIT_INV_LENGTH + "]  power [" + UNIT_VOL  + "]  power (internal log10 repr.) [dimless]\n";
		for(t_cmb::const_iterator itt=x.m_kp.begin();itt!=x.m_kp.end();++itt){
			const _per_len k(itt->first*1E-3);
			const flt      y=itt->second;
			const _vol    p=x(k);
			const _len     r=2*_pi/k;
			s << fwidth(r/_len(1),8,16) << " " << fwidth(k/_per_len(1),1,16) << " " << fwidth(p/_vol(1),1,16) << " " << fwidth(y,2,10) << "\n";
		}
		return s;
	}

	friend istream& operator>>(istream& s,PkCmb& x)
	{
		string t=Readline(s);
		while(t.size()>0 && t[0]=='%' && t.substr(0,12)!="%  Pk: size=" ) t=Readline(s);
		t=Readline(s);
		if (!s || t.substr(0,12)!="%     Ouput:") throw_("bad pk format, head in wrong format");

		flt r,k,p,y;
		x.m_kp.clear();
		while(s){
			s >> r >> k >> p >> y;
			if (s) x.m_kp[k*1000]=y;
		}
		x.AssignMap2Vectors();

		return s;
	}
};

class PkAnalytic
{
	private:
	const flt m_OmegaAll,m_h,m_A;

	public:
	PkAnalytic(const flt OmegaAll,const flt h,const flt A=1E6)
	: m_OmegaAll(OmegaAll), m_h(h), m_A(A)
	{
		if( m_OmegaAll>2 || m_OmegaAll<0) throw_("bad Omega value");
		if (m_h<0 || m_h>1)   throw_("bad h value");
		if (m_A>1E10 || m_A<0) throw_("bad A value");
	}

	_vol operator()(const _per_len k) const
	{
		const flt A=1E6; // XXX, hard coded, but what actual value ???
		const flt T=1;

		const flt l=1.0/(m_OmegaAll*m_h*m_h /(T*T) );
		const flt a=1.7 * l;
		const flt b=9.0 * Pow(l,3./2);
		const flt c=1.0 * l*l ;
		const flt v=1;
		const flt kdimless=k/_per_len(1);
		const flt arg=Pow(1+ Pow(a*kdimless + b*Pow(kdimless,3./2) + c*kdimless*kdimless,v),2/v);

		return _vol(A)*_len(1)*k/arg;
	}

	friend ostream& operator<<(ostream& s,const PkAnalytic& x) {return s << "PkAnalytic: {Omega=" << x.m_OmegaAll << " h=" << x.m_h << " A=" << x.m_A << "}";}
};


class PkAnalytic2
{
	private:
	const flt m_Omegam,m_h,m_n;

	static flt Q(const _per_len k,const flt Omegam,const flt h) {return k*_len(1000)/(Omegam*h*h) * Pow2(2.728/2.7);}
	static flt L(const flt q) {return Ln(_e+184*q);}
	static flt C(const flt q) {return 14.4 + 325/(1+60.5*Pow(q,1.11));}
	static flt T(const _per_len k,const flt Omegam,const flt h)
	{
		assert( Omegam<=1 && h<1 && h>0);
		const flt q=Q(k,Omegam,h);
		return L(q)/(L(q)+C(q)*q*q);
	}
	static flt Delta2k(const _per_len k,const flt Omegam,const flt h,const flt n)
	{
		assert( Omegam<=1 && h<1 && h>0);
		const flt deltaH=1.94E-5 * Pow(Omegam,-0.785-0.05*Ln(Omegam)) * Exp(-0.95*(n-1)-0.169*Pow2(n-1));
		const flt Tk=T(k,Omegam,h);
		const flt arg=k/_H0 * (_len(1000)/_time(1));
		return deltaH*deltaH * Pow(arg,3.+n) * Tk*Tk;
	}

	public:
	PkAnalytic2(const flt Omegam,const flt h,const flt n)
	: m_Omegam(Omegam), m_h(h), m_n(n)
	{
		if( m_Omegam>2 || m_Omegam<0) throw_("bad Omega value");
		if (m_h<0 || m_h>1)   throw_("bad h value");
		if (m_n>4 || m_n<4) throw_("bad n value");
	}

	_vol operator()(const _per_len k) const {return 2*_pi*_pi*Delta2k(k,m_Omegam,m_h,m_n)/(k*k*k);}
	friend ostream& operator<<(ostream& s,const PkAnalytic2& x) {return s << "PkAnalytic2: {Omega=" << x.m_Omegam << " h=" << x.m_h << " n=" << x.m_n << "}";}
};

