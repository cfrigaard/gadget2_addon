int Usage_Ghead(const args& a)
{
	cerr << "Usage: " << a[0] << " <inputfile> {[-p <n> [-id]] | [-u]}\n";
	cerr << "  " << Version() <<  " " << Config() <<  " \n";
	cerr << "  inputfile: snapshot file\n";
	cerr << "Options:\n";
	cerr << "  -p <int>: print information for particle n (n range=[0;N-1])\n";
	cerr << "  -id: lookup particle m from id n (id range=[0;N-1])\n";
	cerr << "  -u: show unitsystem\n";
	return -1;
}

int main_ghead(int argc,char** argv)
{
	try{
		args a(argc,argv,false,false);

		const int  lookupn =a.parseval<int>("-p",-1);
		const bool lookupid=a.parseopt("-id");
		const bool units   =a.parseopt("-u");

 		if(a.size()!=2) return Usage_Ghead(a);
		const string& file=a[1];

		if(lookupn==-1){
			const io_header_1 h(file);

			const flt       calc_Omega=h.hasindividualmasses() ? -1 : h.CalcOmega();
			const _mass calc_mass =h.hasindividualmasses() ? _mass(-1) : h.masstotal();

			cout << "Header for file <" + file + ">";
			cout << "\n  Particles  : " << make_pair(h.npart,6);
			cout << "\n  Masses     : " << make_pair(h.mass,6);
			cout << "\n  Time       : " << h.time;
			cout << "\n  Redshift   : " << h.redshift;
			cout << "\n  Totals     : " << make_pair(h.npartTotal,6);
			cout << "\n  Num files  : " << h.num_files;
			cout << "\n  Boxsize    : " << h.BoxSize;
			cout << "\n  Omega0     : " << h.Omega0;
			if (calc_Omega>0) cout << " (calculated Omega0: " << calc_Omega << " calculated mass:" << calc_mass << ")";
			cout << "\n  OmegaLamda : " << h.OmegaLambda;
			cout << "\n  HubbleParam: " << h.HubbleParam;
			cout << "\n  Flags(sfr,feedback,cool): " << h.flag_sfr << " " << h.flag_feedback << " " << h.flag_cooling;
			cout << "\n  Header: " << tostring(h);
			if (units) {
				cout << "Unitsystem:";
				for(int i=0;i<g_units.size();++i) cout << "\n  1 " << g_units.name(i) << " = " << g_units.si_equivalent(i) << " " << g_units.si_name(i);
				cout << "\n  G = "   << _G    << " " << g_units.unit_gravitational_const();
				cout << "\n  H0 = "  << _H0   << " " << g_units.unit_frequency();
				cout << "\n  rho0 = "<< _rho0 << " " << g_units.unit_density() << "\n";
			}
		} else {
			const pair<particle_data,pair<size_t,size_t> > p1=PeekParticle(file,lookupn,lookupid);
			const particle_data_2 p2(p1.first,p1.first.Mass);
			cout << "Particle: n=" << p1.second.first << "  id=" << p1.second.second << "  type=" << static_cast<int>(p1.first.Type) << "  mass=" << p2.mass() << "\n";
			cout << "   pos=(";
			for(int i=0;i<3;++i) cout << fwidth(p2.pos()[i],8,6) << (i==2 ? "" : ";");
			cout << ")\n   vel=(";
			for(int i=0;i<3;++i) cout << fwidth(p2.vel()[i],8,6) << (i==2 ? "" : ";");
			cout << ")" << endl;
		}
		return 0;
	}
	CATCH_ALL;
	return -1;
}
