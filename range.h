#ifndef __RANGE_H__
#define __RANGE_H__

template<class T>
class range
{
	public:
		typedef map<T,size_t> t_map_fi;
		typedef vector<T>     t_vector_if;
		typedef	typename t_map_fi::const_iterator const_iterator;

	private:
		t_vector_if m_if;
		t_map_fi m_fi;
		T m_max;
		bool m_includeupperbound;

		void Init()
		{
			assert( m_if.size()>1 );
			for(size_t i=0;i<m_if.size();++i) m_fi[m_if[i]]=i;
			assert( m_fi.size()==m_if.size() );

			if (size()==0 ) throw_("bad bin size in class range");
			assert( toidx(rmin())==0 );
			assert( !upperboundincluded() || toidx(rmax())==size()-1 );
			assert(  upperboundincluded() || toidx(rmax()-approxstep()*1E-6)==size()-1 );
			assert( (upperboundincluded() && isinrange(rmax())) || (!upperboundincluded() && !isinrange(rmax())) );
			assert(  isinrange(rmax()-approxstep()*1E-6) );
			assert( !isinrange(rmax()+approxstep()*1E-6) );
		}

		static t_vector_if Init(const T& xmin,const T& xmax,const int size)
		{
			assert( size>1 );
			const T step=(xmax-xmin)/(size);
			const T eps=T(1E-9);

			if (size<=1)       throw_("bad values in range class (0), size<1");
			if (xmax<=xmin)    throw_("bad values in range class (1). max<=min");
			if (xmax-xmin<eps) warn_ ("bad values in range class (2). max-min is pathetic small");
			if (step<eps) 	   warn_ ("bad values in range class (3). step is pathetic small");
			if (size>1E4) 	   warn_ ("bad values in range class (4). bin size is very large");

			assert( xmax>xmin );
			assert( step>T(0) );

			t_vector_if m(size);
			T x(0);
			for(size_t i=0;i<static_cast<size_t>(size);++i){
				x=step*i+xmin;
				assert( x<xmax );
				m[i]=x;
			}
			assert( x<xmax );
			if( Abs((x+step)/xmax-1) > eps/T(1) ) warn_(string("bad upper limit in range class, max=") + xmax + " range max=" + x+step);

			assert( static_cast<size_t>(size)==m.size() );
			assert( m.front()==xmin );
			assert( m.back() <xmax );
			return m;
		}

	public:
		range(const T& xmin,const T& xmax,const int& size,const bool includeupperbound=false)
		:  m_if(Init(xmin,xmax,size)),m_max(xmax), m_includeupperbound(includeupperbound)
		{Init();}

		range(const T& xmin,const T& xmax,const size_t& size,const bool includeupperbound=false)
		:  m_if(Init(xmin,xmax,size)),m_max(xmax), m_includeupperbound(includeupperbound)
		{Init();}

		range(const T& xmin,const T& xmax,const T& stepsize,const bool includeupperbound=false)
		:  m_if(Init(xmin,xmax,static_cast<int>((xmax-xmin)/stepsize))),m_max(xmax), m_includeupperbound(includeupperbound)
		{assert(stepsize>0); Init();}

		const T&  rmin	 	 () const {assert(size()>1); return m_if.front();}
		const T&  rmax 	 	 () const {return m_max;}
		size_t    size		 () const {return m_if.size();}
		const T   approxstep () const {return (rmax()-rmin())/size();}
		bool      upperboundincluded() const {return m_includeupperbound;}

		bool      isinrange	(const T& x) 	  const {return x>=rmin() && (m_includeupperbound ? x<=rmax() : x<rmax());}
		bool      isinidx	(const size_t& i) const {return i<size();}
		bool      isinrange (const Triple<T>& x) const {return isinrange(x[0]) && isinrange(x[1]) && isinrange(x[2]);}

		T 		  totype(const size_t& idx)  const {assert(isinidx(idx)); return m_if[idx];}
		Triple<T> totype(const t_idx& idx)   const {return Triple<T>(totype(idx[0]),totype(idx[1]),totype(idx[2]));}
		t_idx     toidx (const Triple<T>& p) const {return t_idx(toidx(p[0]),toidx(p[1]),toidx(p[2]));}

		size_t toidx (const T& x) const
		{
			assert( isinrange(x) );
			const_iterator itt=m_fi.upper_bound(x);
			if( itt==m_fi.begin() ) return 0;
			--itt;
			assert( itt!=m_fi.end() && itt->second<size() );
			return itt->second;
		}

		array3d<flt> weightfactors(const Triple<T>& p) const
		{
			const t_idx i0=toidx(p);
			const t_idx i1=i0+1;

			const Triple<T> p0=totype(i0);
			Triple<T> p1;
			for(size_t i=0;i<3;++i) {
				p1[i]=isinidx(i1[i]) ? totype(i1[i]) : rmax();
				assert( i0[i]>=0 && i0[i]+1==i1[i] && i1[i]<=size() );
				assert( p0[i]<p1[i] && p1[i]<=rmax() );
				assert( p0[i]<=p[i] && p[i]<=p1[i] );
			}

			flt v=1;
			flt vx0[3],vx1[3];
			for(size_t i=0;i<3;++i) {
				v     *= (p1[i]-p0[i])/T(1);
				vx0[i] = ( p[i]-p0[i])/T(1);
				vx1[i] = (p1[i]- p[i])/T(1);
			}
			assert( v>0 );

			array3d<flt> w(2,2,2);
			w(0,0,0) = vx1[0]*vx1[1]*vx1[2];
			w(1,0,0) = vx0[0]*vx1[1]*vx1[2];
			w(0,1,0) = vx1[0]*vx0[1]*vx1[2];
			w(1,1,0) = vx0[0]*vx0[1]*vx1[2];
			w(0,0,1) = vx1[0]*vx1[1]*vx0[2];
			w(1,0,1) = vx0[0]*vx1[1]*vx0[2];
			w(0,1,1) = vx1[0]*vx0[1]*vx0[2];
			w(1,1,1) = vx0[0]*vx0[1]*vx0[2];

			for(size_t i=0;i<w.size();++i) {
				assert(w[i]<=v);
				w[i]=w[i]/v;
				assert( w[i]>=0 && w[i]<=1 );
			}
			assert( w.sum()>1-1E-6 && w.sum()<1+1E-6);
			return w;
		}

		template<class R>
		R weightfactors(const Triple<T>& p,const array3d<R>& a) const
		{
			if(!a.isSquare() || a.sizex()!=size()) throw_("array3d data does no fit with range, non-square or of different discrete size");
			const array3d<flt> w=weightfactors(p);
			const t_idx baseidx=toidx(p);

		    R r(0);
			for(size_t k=0;k<2;++k)
			for(size_t j=0;j<2;++j)
			for(size_t i=0;i<2;++i) {
				t_idx idx=t_idx(i,j,k)+baseidx;
				for(size_t n=0;n<3;++n) if (!isinidx(idx[n])) idx[n]=0;
				r += w(i,j,k) * a[idx];
			}
			return r;
		}

		const t_map_fi&       getmap   () const {return m_fi;}
		const t_vector_if&    getvector() const {return m_if;}
		const const_iterator  begin    () const {return getmap().begin();}
		const const_iterator  end      () const {return getmap().end();}

		friend ostream& operator<<(ostream& s,const range& x)
		{
			return s << "range:{[min=" << x.rmin() << " ; max=" << x.rmax() << (x.upperboundincluded() ? "]" : "[") << " size=" << x.size() << " approxstep=" << x.approxstep() << "}";
		}
};

#endif // __RANGE_H__
