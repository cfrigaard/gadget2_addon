int Usage_Gdiff(const args& a)
{
	cerr << "Usage: " << a[0] << " <file1> <file2> [-e | -d] [-eps <epsilon>]\n";
	cerr << "  " << Version() <<  " " << Config() <<  " \n";
	cerr << "  file1/2: snapshot file\n";
	cerr << "Options:\n";
	cerr << "  -d: do a direct per-partile diff\n";
	cerr << "  -e: calculate potential and kinetic energy\n";
	cerr << "  -r: reorder particles\n";
	cerr << "  -eps <float>: diffence epsilon\n";
	return -1;
}

double Diff(const double x,const double x0)
{
	//if (fabs(x0)<fabs(x)) return _proximy_base(x0,x,expr,file,line,epsilon,print);
	//const  d= x0==T(0) ? Abs(x)/T(1) : (x==T(0) ? Abs(x0)/T(1) : Abs((x-x0)/x0));
	//const bool b=d<epsilon;
	if (x==x0) return 0;
	const double d=0.5*(x+x0);
	if (d==0) throw_("Diff encountered div by zero");
	return (x-x0)/d;
}

bool isparticleordered(const data_info& inf)
{
	if (!inf.Id) throw_("Id's array empty");
	for(int i=0;i<inf.NumPart;++i){
		if (inf.Id[i]!=i) return false;
	}
	return true;
}

int main_gdiff(int argc,char** argv)
{
	try{
		args a(argc,argv,false,false);
		const bool energy=a.parseopt("-e");
		const bool diff=a.parseopt("-d");
		const bool reorder=a.parseopt("-r");
		const double eps=a.parseval<double>("-eps",1E-3);

		if (energy && diff) throw_("-e and -d cannot be uses together");
		if (eps<=0) throw_("eps cannot be smaller than or equal zero");

 		if(a.size()!=3) return Usage_Gdiff(a);

		vector<particle_data_2> v1,v2;

		data_info inf1=load_snapshot(a[1].c_str(),1,false,false);
		if (reorder) reordering(inf1,true);
		else if (!isparticleordered(inf1)) throw_("particles not ordered");

		FilterParticles(inf1,0,v1);

		data_info inf2=load_snapshot(a[2].c_str(),1,false,false);
		if (reorder) reordering(inf2,true);
		else if (!isparticleordered(inf2)) throw_("particles not ordered");

		FilterParticles(inf2,0,v2);

		if (v1.size()!=v2.size()) throw_("snapshotfiles must be of the same size");

		_len pdist=0;
		_vel tdist=0;
		_len2 pdist2=0;
		_vel  tdist2=0; // really _vel^2
		long double e_kin1=0,e_kin2=0;
		unsigned long ndiff=0;
		static bool report=true;
		const size_t N=20; // report this number of diffs

		for (size_t i=0;i<v1.size();++i){
			const _point_len p1=v1[i].pos();
			const _point_len p2=v2[i].pos();

			const _len d=p1.dist<_len>(p2);
			pdist += d;
			pdist2+= d*d;

			const _vector_vel vv1=v1[i].vel();
			const _vector_vel vv2=v2[i].vel();

			const _vel v=vv1.dist<_vel>(vv2);
			tdist  += v;
			tdist2 += v*v;

			e_kin1 += v1[i].mass()*vv1.length2<_vel>();
			e_kin2 += v2[i].mass()*vv2.length2<_vel>();

			if (diff){
				for(size_t j=0;j<3;++j){
					const double d=Diff(p1[j],p2[j]);
					if (fabs(d)>eps){
						++ndiff;
						if (ndiff<N)      cout << "% diff(" << fwidth(i,4,0) << "," << j << "): abs(" << fwidth(d,3,11) << ") > " << eps << "   p1=" << fwidth(p1[j],6,4) << "  p2=" << fwidth(p2[j],6,4) << "\n";
						else if (report) {cout << "% more than " << N << " differences found...delaying further diff output\n"; report=false;}
					}
				}
			}
		}

		if (ndiff>0){
			cerr << "% found " << ndiff << " difference that exceeds eps=" << eps << "\n";
		}

		if (!energy){
			cout << "% absolute difference: pos [" << UNIT_LENGTH << "] velocity " << UNIT_LENGTH << "/" << UNIT_TIME << "]\n";
			cout << pdist << "\t" << tdist << "\n";
			cout << "% RMS difference:  pos [" << UNIT_LENGTH << "]  velocity " <<  UNIT_LENGTH << "/" << UNIT_TIME << "]\n";
			cout << Sqrt(pdist2/v1.size()) << "\t" << Sqrt(tdist2/v1.size()) << "\n";
			return 0;
		}

		e_kin1 *= 0.5;
		e_kin2 *= 0.5;

		long double e_pot1=0,e_pot2=0;
		timer tim;

		for (size_t i=0;i<v1.size();++i){
			tim.ToEta(i,v1.size(),cerr);
			for (size_t j=0;j<v1.size();++j){
				if (i<j) {
					const double d1=v1[i].pos().dist<_len>(v1[j].pos());
					const double d2=v2[i].pos().dist<_len>(v2[j].pos());

					// Note: does not handle div by zero yet,
					//       needs addition of smoothing lenght, d += smooth
					e_pot1 -= _G*v1[i].mass()*v1[j].mass()/d1;
					e_pot2 -= _G*v2[i].mass()*v2[j].mass()/d2;
				}
			}
		}

		const double e1=e_kin1+e_pot1,e2=e_kin2+e_pot2;

		cout << "% absolute difference:  pos [" << UNIT_LENGTH << "]  velocity " <<  UNIT_LENGTH << "/" << UNIT_TIME << "]\n";
		cout << pdist << "\t" << tdist << "\n";
		cout << "% RMS difference:  pos [" << UNIT_LENGTH << "]  velocity " <<  UNIT_LENGTH << "/" << UNIT_TIME << "]\n";
		cout << Sqrt(pdist2/v1.size()) << "\t" << Sqrt(tdist2/v1.size()) << "\n";
		cout << "% energy unit: (" << UNIT_MASS + ") * (" + UNIT_LENGTH + ")^2 / (" + UNIT_TIME + ")^2 \n"; // not defined yet in my unit list
		cout << "% total energy:  e_kin(1) e_pot(1) e_kin(2) e_pot(2)\n";
		cout << e_kin1 << "\t" << e_pot1 << "\t" << e_kin2 << "\t" << e_pot2 << "\n";
		cout << "% total energy:  e(1) e(2)\n";
		cout << e1  << "\t" << e2 << "\n";
		cout << "% energy difference:  e(2)-e(1)  (e(2)-e(1))/e(1) [dimless]\n";
		cout << e2-e1 << "\t" << (e2-e1)/e1 << "\n";

		return 0;
	}
	CATCH_ALL;
	return -1;
}