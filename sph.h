inline flt W(const flt rh)
{
	assert( rh>=0 );
	if(rh<0.5)     {const flt t=rh*rh;  return 1.+6*t*(rh-1);} // original 1.-6*rh*rh+6*rh*rh*rh;
	else if (rh<1) {const flt t=1.0-rh; return 2.*t*t*t;}      // original 2.*Pow3(1.-rh);
	else           return 0.0;
}

template<class T,class R> inline R W_prefator(const T& h) {assert( h>T(0) ); return 8./(Constants::_pi*h*h*h);}
template<class T,class R> inline R W         (const T& r,const T& h) {assert( r>=T(0));return W_prefator<T,R>(h)*W(r/h);}

inline pair<_len,_len> FindSmoothingLengths(const vector<particle_data_2>& v,const searchgrid& s,const size_t N_nearest
                   ,const bool printeta=true,const string& gadgetfile="",const bool ismaster=true)
{
	if(s.sizen()<2*N_nearest) throw_(string("to few datapoints for estimating smoothing lengths, datapoints=")
	                            + s.sizen() + " N_nearest=" + N_nearest);

	const size_t N=v.size();
	const _bbox_len bbox(s.bbox());
	_len hmax(0),hmean(0);
	_len2 hsigma2(0);

	// XXX disable in public version,
	const bool load_precondition=false; //N_nearest==33 && gadgetfile.size()>0 && FileExists(gadgetfile);

	const string smoothingfile=gadgetfile + ".sml";
	vector<_len> stemp;
	bool smoothingfilefromdisk=false;

	if (load_precondition){
		if (!ismaster){
			// slave waits for master to write smoothingfile
			int n=0;
			while(++n<100 && (!FileExists(smoothingfile) || !isFileNewer(smoothingfile,gadgetfile))) sleep(10);
		}

		// read smoothinglenght for N_nearest==33 from disk, Note: will fail for different use of filters
		if (FileExists(smoothingfile)){
			if (isFileNewer(smoothingfile,gadgetfile)){
				if (printeta) cout << "%   Reading .sml file=<"<< smoothingfile << ">" << endl;
				ifstream is(smoothingfile.c_str());
				Readbin(is,stemp);
				if (stemp.size()!=v.size()) stemp.resize(0); // ignore data
				else smoothingfilefromdisk=true;
			}
		}
	}
	assert( stemp.size()==0 || stemp.size()==v.size() );
	assert( ismaster || stemp.size()==v.size() );
	assert( !smoothingfilefromdisk || stemp.size()==v.size() );

	timer tim;
	for(size_t i=0;i<N;++i){
		const particle_data_2& d=v[i];
		const _point_len p=d.pos();

		_len h;
		if(i>0 && smoothingfilefromdisk){
			assert( i<stemp.size() );
			h=stemp[i];
		}
		else{
			const searchgrid::t_map_data m=s.findclosest(p,N_nearest);
			assert( m.size()==N_nearest ||  m.size()==v.size() );
			h=Sqrt<_len2,_len>((--m.end())->first) / 2;
			if (i==0 && smoothingfilefromdisk && h!=stemp[i]){
				smoothingfilefromdisk=false;
				stemp.resize(0);

				warn_(string("smoothingfile h not equal calculated h, reverting to inline") +
				 " calculation, h=" + tostring(h) + " h(file)=" + tostring(stemp[i]) );
			}

			if (!smoothingfilefromdisk){
				assert( stemp.size()==i );
				stemp.push_back(h);
			}
		}

		if (2*h>bbox()) throw_(string("smoothinglenght greater than bbox/2, h=")
		                 + h + " bbox=" + bbox());
		hmax=max(hmax,h);
		hmean   +=h;
		hsigma2 +=h*h;

		particle_data_2& d2=const_cast<particle_data_2&>(d);
		d2.seth(h);
		CheckFltValidity(h/_len(1));

		if (printeta) tim.ToEta(i,N,cerr);
	}

	hmean /= N;
	hsigma2 /= (N-1);
	hsigma2 -= hmean*hmean;
	const _len sigma=Sqrt<_len2,_len>(hsigma2);
	CheckFltValidity(hmean/_len(1));
	CheckFltValidity(hmax/_len(1));

	// write smoothinglenght for N_nearest==33 to disk, master only
	if (ismaster && !smoothingfilefromdisk && load_precondition && (!FileExists(smoothingfile)
	      || !isFileNewer(smoothingfile,gadgetfile))){
		if (printeta) cout << "%   Writing .sml file=<"<< smoothingfile << ">" << endl;
		assert( stemp.size()==v.size() );
		ofstream os((smoothingfile+".tmp").c_str());
		Writebin(os,stemp,true);
		System("mv " + smoothingfile+".tmp " + smoothingfile );
	}

	if (printeta) cout << "%   Smoothing length mean=" << hmean
	                   << " sigma=" << sigma << " max=" << hmax << endl;
	return make_pair(hmean,hmax);
}

inline _mass FindMassInBox(const _point_len& p,const _len& l,const particle_data_2& d,
                    const _bbox_len& bbox,const size_t steps=20,const bool assumeinsidebox=false)
{
	const _len h=d.h();
	const _point_len dp=d.pos();
	const _per_len inv_h=1.0/h;

	const int b=bbox.isParticleInsideBox(p,l,dp,h,assumeinsidebox);
	if      (b==0) return _mass(0);
	else if (b==2) return d.mass();

	const bool rhmode=l/h<1.0;
	const _len stepsize=rhmode ? l/steps : 2*h/steps;
	_point_len p0=rhmode ? p : dp-h;
	p0 += stepsize/2;
	bbox.WrapToInsideBox(p0);

	if( stepsize>=0.2*h ) warn_("bad stepsize");
	assert( bbox.IsInsideBBox(p0) );
	assert( bbox.IsInsideBBox(dp) );

	long double t(0);
	_point_len q;
	for(size_t k=0;k<steps;++k){
		q[2]=p0[2]+stepsize*k;
		bbox.WrapToInsideBox(q[2]);
		for(size_t j=0;j<steps;++j){
			q[1]=p0[1]+stepsize*j;
			bbox.WrapToInsideBox(q[1]);
			for(size_t i=0;i<steps;++i){
				q[0]=p0[0]+stepsize*i;
				bbox.WrapToInsideBox(q[0]);
				assert( bbox.IsInsideBBox(q) );
				if (rhmode) t += W(bbox.DistPeriodic(q,dp)*inv_h);
				else{
					const flt x=bbox.FractionBoxInBox(p,l,q-stepsize/2,stepsize);
					if (CheckFltValidity(x)>0) t += x*W(bbox.DistPeriodic(q,dp)*inv_h);
				}
			}
		}
	}
	const _vol dV=stepsize*stepsize*stepsize;
	const flt massfrac = W_prefator<_len,_per_vol>(h)*dV*t;

	if( CheckFltValidity(massfrac)>1.1 ) throw_(string("bad mass fraction, greater than 1.1,") +
	     " try to increse steps, massfrac=" + massfrac + " h=" + h + " p=" + tostring(p) + " l="
	      + l + " d=" + tostring(d));
	return massfrac*d.mass();
}

inline flt analytic_sphereinsphere_sub(const flt& x)
{
	if (!(x>=-1 && x<=1)) throw_("x value out of range in analytic_sphereinsphere()");
	else if (x==-1) return 1.0; // end point
	else if (x== 1) return 0.0; // end point

	flt y=numeric_limits<flt>::quiet_NaN();
	try{
		// shape parameters
		const flt z1=1.8;
		const flt z2=1.8;
		const flt dx=0.2;

		if (x>-dx && x<dx){
			// weight two modes
			const flt fx2=(x+dx)/(2*dx);
			const flt fx1=1-fx2;
			assert( fx1>=0 && fx1<=1 );
			const flt y1=  (Cos(Pow( x+1-.02,z1)*1.8)+1)/2;
			const flt y2=1-(Cos(Pow(-x+1-.12,z2)*1.8)+1)/2;
			y=(y1*fx1+y2*fx2);
		}
		else if (x<0) y=  (Cos(Pow( x+1-.02,z1)*1.8)+1)/2;
		else          y=1-(Cos(Pow(-x+1-.12,z2)*1.8)+1)/2;
	}
	catch(const Exception& e){
		cerr << "** caught at x=" << x << " e=" << e << endl;
	}

	if (FltValidity(y)==false){
		if      (x<-.8) y=1;
		else if (x> .8) y=0;
		else     throw_("bad numerical value");
	}

	assert(y==y && y>=0 && y<=1 );
	return CheckFltValidity(y);
}

inline flt analytic_sphereinsphere(const flt& x,const bool init=false)
{
	static const flt bx=0.85;
	static const flt y1=analytic_sphereinsphere_sub(-bx);
	static const flt y2=analytic_sphereinsphere_sub( bx);

	if      (x<-bx) return Interpolate(-1.0,bx,1.0, y1,x);
	else if (x> bx) return Interpolate(  bx,1.0,y2,0.0,x);
	else            return analytic_sphereinsphere_sub(x);
}

inline _mass FindMassInSphere(const _point_len& p,const _len& r,const particle_data_2& d,
                       const _bbox_len& bbox,const size_t steps=20,const bool numberdensity=false,const bool analytic=false)
{
	const _len&      h=d.h();
	const _point_len dp=d.pos();
	const _len l=bbox.DistPeriodic(p,dp);

	if      (l-h>r)  return _mass(0);
	else if (h<r && l+h<=r) return d.mass();
	else if (numberdensity) return l>r ? _mass(0) : d.mass(); // fast, numberdensity mode, will not integrate mass

	if (analytic && h<r){
		assert( h>_len(0) && r>_len(0) && h<r && l>=_len(0) && (r+h)<bbox()/2);
		const flt f=analytic_sphereinsphere(CheckFltValidity((l-r)/h));
		return d.mass()*f;
	}

	const _len2 r2=r*r;
	const flt rh=r/h;
	const _per_len inv_h=1.0/h;
	const bool rhmode=rh<1;
	const _len stepsize=rhmode ? 2*r/steps : 2*h/steps;
	_point_len  p0= rhmode ? p-r : dp-h;
	p0 += stepsize/2;
    bbox.WrapToInsideBox(p0);

	if( stepsize>=0.2*h ) warn_("bad stepsize");
	assert( bbox.IsInsideBBox(p0) );

	long double t(0);
	_point_len q;
	for(size_t k=0;k<steps;++k){
		q[2]=p0[2]+stepsize*k;
		bbox.WrapToInsideBox(q[2]);
		for(size_t j=0;j<steps;++j){
			q[1]=p0[1]+stepsize*j;
			bbox.WrapToInsideBox(q[1]);
			for(size_t i=0;i<steps;++i){
				q[0]=p0[0]+stepsize*i;
				bbox.WrapToInsideBox(q[0]);
				assert( bbox.IsInsideBBox(q) );
				assert( bbox.IsInsideBBox(p) );
				if(bbox.DistPeriodic2(q,p)<r2){
					const _len l=bbox.DistPeriodic(q,dp);
					t += W(l*inv_h);
				}
			}
		}
	}

	const _vol dV=stepsize*stepsize*stepsize;
	const flt massfrac = W_prefator<_len,_per_vol>(h)*dV*t;

	if( CheckFltValidity(massfrac)>1.1 ) throw_(string("bad mass fraction, greater than 1.1, try to")
			+ " increse steps, massfrac=" + massfrac + " h=" + h + " p=" + tostring(p) + " r=" + r + " d=" + tostring(d));
	return massfrac*d.mass();
}

class Wprecalc
{
	private:

	// holds data for delayed (on-demand) integration
	class integrate_delayed_vars
	{
		private:
		mutable _mass m; // mutable hack to preserve const attributes
		_point_len p;
		_len h,r;

		public:
		integrate_delayed_vars()
		  : m(_mass(-1)), p(_len(-1)), h(_len(-1)), r(_len(-1)) {}
		integrate_delayed_vars(const _point_len& px,const _len& hx,const _len& rx)
		  : m(_mass(-1)), p(px), h(hx), r(rx) {}
		integrate_delayed_vars(const _mass& mm)
		  : m(_mass(mm)), p(_len(-1)), h(_len(-1)), r(_len(-1)) {assert(m>=_mass(0));}

		static _mass Integrate(const _point_len& p,const _len& h,const _len& r,const _bbox_len& bbox,
		                       const size_t& integrationsteps,const bool analytic)
		{
			assert( h>_len(0) && r>_len(0) && h<r && (r+h)<bbox()/2 );
			const _point_len q(_len(0));
			particle_data_2 d(0,q,_mass(1));
			d.seth(h);
			const _mass m=min(FindMassInSphere(p,r,d,bbox,integrationsteps,false,analytic),d.mass());
			assert( m>=_mass(0) && m<=d.mass() ); // normalized mass btw 0 and 1
			return m;
		}

		bool hasbeenintegrated() const {return m>=_mass(0);}

		_mass normalizedmass(const _bbox_len& bbox,const size_t& integrationsteps,const bool analytic) const
		{
			if (!hasbeenintegrated()){
				assert(m==_mass(-1));
				m=Integrate(p,h,r,bbox,integrationsteps,analytic);
			}
			assert( m>=_mass(0) && m<=_mass(1) );
			return m;
		}
	};

	// Map layout:
	//  t_map_data:   sphere overlap x range to integrated mass
	//    range: variable x=(l-r)/h (where l is the distace)
	//    distance intervals:  l = {r-h,r+h} => [-1;1], monotonic decreasing
	//      x=-1 fully inside,
	//      x<1 && x>-1 partial within,
	//      x=0 partial within and sphere on center of particle,
	//      x=1 fully outside
	//  t_map_lookup: discrete mapping from hr_lower and hr_upper to t_map_data
	//    range: [0;1],
	//      above 1: do direct slow calculation
	//  linear interpolate btw. lookupmap (hr), and data (x)
	typedef map<flt,integrate_delayed_vars> t_map_mass;
	typedef map<size_t,t_map_mass> t_map_lookup;

	t_map_lookup m_l;
	const range<flt> m_r;             // range values for lookup map,
	                                  // note: map lookup is made logarithmic (Ln/Exp)
	const int m_integrationres;       // step btw hr modes in lookup map
	const size_t m_integrationsteps;  // integration delta
	const _bbox_len m_bbox;
	const bool m_analytic;

	bool m_dbg;
	size_t m_tot,m_hits,m_misses,m_strange,m_rh1,m_rh2;

	flt GetKey(const _len& h,const _len& r,const _len& l,const bool integrationmode) const
	{
		assert( h>_len(0) && r>_len(0) && h<r && l>=_len(0) && (r+h)<m_bbox()/2);
		const flt x=CheckFltValidity((l-r)/h);
		assert( (integrationmode && -1.2<=x && x<=1.2) || (-1<=x && x<=1) ); // allow some slack
		return x;
	}

	const pair<size_t,size_t> GetHRindexes(const flt& hr) const
	{
		assert( hr>0 && hr<1 );
		if (!m_r.isinrange(hr)) throw_(string("hr value out of range, hr=") + hr + " range=" + tostring(m_r));
		const size_t i1=m_r.toidx(hr);
		if (i1+1>=m_r.size()) throw_("index out of range");
		return make_pair(i1,i1+1);
	}

	const pair<flt,flt> GetHRvalues(const flt& hr) const
	{
		const pair<size_t,size_t> i=GetHRindexes(hr);
		const pair<flt,flt> pr=make_pair(m_r.totype(i.first),m_r.totype(i.second));
		CheckFltValidity( pr.first );
		CheckFltValidity( pr.second );
		assert( pr.first < pr.second && pr.first<=hr && hr<=pr.second);
		assert( pr.first>0 && pr.first<1 && pr.second>0 && pr.second<1 );
		return pr;
	}

	t_map_mass IntegrateHR(const flt& hr,const _len& r)
	{
		assert( hr>0 && hr<1 );
		const _len h=hr*r;
		const _point_len q(_len(0));

		if (m_dbg) cout << "\t** IntegrateHR:  hr=" << hr << " h=" << h << " r=" << r << " res="
		               << m_integrationres <<  " integrationsteps=" << m_integrationsteps <<endl;

		const _len step=h/m_integrationres;
		t_map_mass mx;

		assert( h<=m_integrationres*(step+_len(1)) );
		for(int i=-m_integrationres-1;i<=m_integrationres+1;++i){
			const _len x=i*step+r;
		    _point_len p(q[0]+x,q[1],q[2]);
		    m_bbox.WrapToInsideBox(p);

			const _len l=m_bbox.DistPeriodic(p,q);
			const flt key=GetKey(h,r,l,true);

			mx[key]=integrate_delayed_vars(p,h,r);
		}
		assert( mx.size()>4 );
		return mx;
	}

	_mass Interpolate(const t_map_mass& mm,const flt& x)
	{
		if (!isinmap(mm,x)) throw_("value out of range in Interpolate (0)");

		// iterator lower_bound(const key_type& k): Finds the first element whose key is not less than k.
		// iterator upper_bound(const key_type& k): Finds the first element whose key greater than k.
		t_map_mass::const_iterator itt=mm.upper_bound(x);
		if (itt==mm.end()  ) throw_("out of bounds in Interpolate (1)");
		if (itt==mm.begin()) throw_("out of bounds in Interpolate (2)");

		const flt x2=itt->first;
		const _mass m2=itt->second.normalizedmass(m_bbox,m_integrationsteps,m_analytic);
		--itt;
		const flt x1=itt->first;
		const _mass m1=itt->second.normalizedmass(m_bbox,m_integrationsteps,m_analytic);

		assert( x1<=x && x<x2 );
		return Math::Interpolate(x1,x2,m1,m2,x);
	}

	public:
	Wprecalc(const int integrationres,const size_t integrationsteps,const size_t lookupres,const _bbox_len& bbox,
	         const bool analytic)
	:  m_r(1E-12,1,1.0/lookupres,true), m_integrationres(integrationres), m_integrationsteps(integrationsteps)
	, m_bbox(bbox), m_analytic(analytic), m_dbg(false)
	{
		assert(m_integrationres>1 &&  lookupres>1);
		if (m_integrationres<4)      throw_("too few steps in integration lookup");
		if (m_integrationres<10)     warn_ ("very few steps in integration lookup");
		if (m_integrationsteps<4)    throw_("too low integration resolution");
		if (m_integrationsteps<10)   warn_ ("very limited integration resolution");
		if (m_r.size()<4)            throw_("lookup resolution too low");
		if (lookupres<10)            warn_ ("lookup resolution low");
		m_tot=m_hits=m_misses=m_strange=m_rh1=m_rh2=0;
	}

	void ReportStatistics(const string& filename) const
	{
		if (m_tot==0) return;
		const size_t x=m_l.size();
		const size_t y=x==0 ? 0 : m_l.begin()->second.size();
		array3d<flt> a(x,y,1),b(x,y,1);
		vector<flt> xx;
		vector<flt> yy;

		size_t N=0,m=0;
		size_t i=0;
		for(t_map_lookup::const_iterator itt1=m_l.begin();itt1!=m_l.end();++itt1,++m){
			xx.push_back(m_r.totype(itt1->first));
			const t_map_mass& mm=itt1->second;
			size_t n=0;
			for(t_map_mass::const_iterator itt2=mm.begin();itt2!=mm.end();++itt2,++N,++n){
				_mass ms(-1);
				if (itt2->second.hasbeenintegrated()) {
					++i;
					ms=itt2->second.normalizedmass(m_bbox,m_integrationsteps,m_analytic);
					//cerr << itt->first << " " << m << " ";
				}
				a(m,n,0)=ms/_mass(1);
				b(m,n,0)=itt2->second.normalizedmass(m_bbox,m_integrationsteps,m_analytic)/_mass(1);
				if (m==0) yy.push_back(itt2->first);
			}
		}

		cerr << "Wprecalc DTOR: tot=" << m_tot << " hits=" << m_hits << " miss=" << m_misses << " m_strange="
		     << m_strange << " rh1=" << m_rh1 << " rh2=" << m_rh2;
		cerr << " N=" << N << " i=" << i << " populationfraction=" << 1.0*i/N << endl;

		a.Writeascii(filename + "_a.a3d");
		b.Writeascii(filename + "_b.a3d");
		assert( xx.size()==yy.size() && xx.size()*yy.size()==a.size() );
		Writeascii(filename + "_x.txt",xx);
		Writeascii(filename + "_y.txt",yy);
	}

	_mass Lookup(const _point_len& p,const _len& r,const particle_data_2& d,const bool numberdensity,
	             const bool dbg=false)
	{
		const bool old_dbg=m_dbg;
		m_dbg=dbg;

		const _len h=d.h();
		const _len l=m_bbox.DistPeriodic(p,d.pos());
		if (h<=_len(0) || r<=_len(0)) throw_("bad h or r parameters, cannot be zero");

		if (m_dbg) cout << "** Lookup:  l=" << l << " h=" << h << " r=" << r << " hr=" << h/r << " rh=" << r/h << endl;

		const bool hrmode=h<r;
		const flt hr=CheckFltValidity(h/r);

		if      (l-h>r)            {if (m_dbg) cout << "\t%% return 0" << endl; m_dbg=old_dbg; return _mass(0);}
		else if (hrmode && l+h<=r) {if (m_dbg) cout << "\t%% return 1" << endl; m_dbg=old_dbg; return d.mass();}
		// fast, numberdensity mode, will not integrate mass
		else if (numberdensity)    {m_dbg=old_dbg; return l>r ? _mass(0) : d.mass();}

		if (!hrmode) {
			++m_rh1;
			//warn_(string("cannot handle hr ratios above 1.0, h=") + h + " r=" + r);
			return FindMassInSphere(p,r,d,m_bbox,m_integrationsteps,numberdensity,m_analytic);
		}
		else if (!m_r.isinrange(hr) || m_r.toidx(hr)>=m_r.size()-2 || m_r.toidx(hr)<=1) {
			++m_rh2;
			//warn_(string("borderline case, h=") + h + " r=" + r);
			return FindMassInSphere(p,r,d,m_bbox,m_integrationsteps,numberdensity,m_analytic);
		}

		const flt x=GetKey(h,r,l,false);
		if      (x>= 1.0) {if (m_dbg) cout << "\t$$ return 0" << endl; m_dbg=old_dbg; return _mass(0);}
		else if (x<=-1.0) {if (m_dbg) cout << "\t$$ return 1" << endl; m_dbg=old_dbg; return d.mass();}

		assert( hr>0 && hr<1 && -1<x && x<1 );
		const pair<size_t,size_t> hrsteps_idx=GetHRindexes(hr);
		const pair<flt,flt>       hrsteps_flt=GetHRvalues (hr);

		const size_t i1=hrsteps_idx.first;
		const size_t i2=hrsteps_idx.second;
		const flt    hr1=hrsteps_flt.first;
		const flt    hr2=hrsteps_flt.second;

		++m_tot;
		if (m_dbg) cout << "\tx=" << x << " hr1=" << hr1 << " hr2=" << hr2 << " i1=" << i1 << " i2=" << i2 << endl;

		assert( hr1>0 && hr1<1 && hr2>0 && hr2<1 );
		assert( i1<i2 && hr1<hr2 && hr1<=hr && hr<=hr2 );

		if (m_l.find(i1)==m_l.end()) {
			++m_misses;
			if (m_dbg) cout << "\tnot found: hr=" << hr << " hr1=" << hr1 << " i1=" << i1 << endl;
			m_l[i1]=IntegrateHR(hr1,r);
		} else ++m_hits;
		if (m_l.find(i2)==m_l.end()) {
			++m_misses;
			if (m_dbg) cout << "\tnot found: hr=" << hr << " hr2=" << hr2 << " i2=" << i2 << endl;
			m_l[i2]=IntegrateHR(hr2,r);
		} else ++m_hits;

		assert( m_l.find(i1)!=m_l.end() && m_l.find(i2)!=m_l.end() );
		const t_map_mass& mm1=m_l[i1];
		const t_map_mass& mm2=m_l[i2];

		const _mass m1=Interpolate(mm1,x);
		const _mass m2=Interpolate(mm2,x);

		const flt f=CheckFltValidity(min(Math::Interpolate(hr1,hr2,m1,m2,hr)/_mass(1),1.0));
		if (m_dbg) cout << "\tf=" << f << "  m=" << f*d.mass() << " hr=" << hr << " m1=" << m1/_mass(1)*d.mass()
		                << " m2=" << m2/_mass(1)*d.mass() << endl;
		assert( f>=0 && f<=1 );

		m_dbg=old_dbg;
		return f*d.mass();
	}
};
