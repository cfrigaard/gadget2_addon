const flt default_sigma8=.84;

int Usage_Convert(const args& a)
{
	cerr << "Usage: " << a[0] <<  " <mode> [mode parameters]\n";
	cerr << "  " << Version() <<  " " << Config() <<  " \n";
	cerr << "  mode: pk2xi, pk2sigma, xi2pk, xi2sigma, sigma2pk, tf2pk\n\n";

	cerr << "    pk2xi/sigma <powerfile> <kmin> <kmax> <kstep> [-l ]\n";
	cerr << "      powerfile: powerspectum file, from tf2pk\n";
	cerr << "      kmin   <float " + UNIT_INV_LENGTH + ">: min k for integration\n";
	cerr << "      kmax   <float " + UNIT_INV_LENGTH + ">: max k for integration\n";
	cerr << "      kmstep <float " + UNIT_INV_LENGTH + ">: step size\n";
	cerr << "      Options:\n";
	cerr << "        -l: step logarithmic\n\n";

	cerr << "    xi2pk <xi file> <bbox>\n";
	cerr << "        xi file format: column 1=r [" + UNIT_LENGTH + "], column 4=xi(r) [dimless]\n\n";

	cerr << "    tf2pk <file> [-8 <sigma8>] [-c <column>]\n";
	cerr << "        -8 <float>: specify sigma to use, default=" << default_sigma8 << "\n";
	cerr << "        -c <int>: column to pick from cmb file,\n";
	cerr << "                  1=cdm, 2=baryons, 3=photons, 4=neutrinos, default=1\n";
	return -1;
}

int Pk2Xi(args& a,const bool sigma2mode)
{
	const bool logscale=a.parseopt("-l");

	if(a.size()!=9) return Usage_Convert(a);
	const string file=a.Totype<string>(2);
	const _per_len kmin=a.Tounit<_per_len>(3);
	const _per_len kmax=a.Tounit<_per_len>(5);
	const _per_len kstep=a.Tounit<_per_len>(7);
	const _len Rmax =2*_pi/kmin;
	const _len Rmin =2*_pi/kmax;
	const _len Rstep=2*_pi/kstep;

	if (Rmin>Rmax) throw_("kmax<kmin, please swap ranges");
	if (logscale && Rstep<_len(1)) throw_("bad step parameter for logarithmic scale");

	const PkCmb pk(file);
	const _per_len kthreshold(0.001);
	const flt sigma8_unnorm=pk.Sigma8();

	cout << (sigma2mode ? "% Pk2Sigma,\n" : "% Pk2Xi,\n");
	cout << "%   parameters: file=" << file << " logscale=" << logscale << "\n";
	cout << "%     kmin="    << kmin      << " " << UNIT_INV_LENGTH << " kmax=" << kmax      << " " << UNIT_INV_LENGTH << " kstep=" << kstep << " " << UNIT_INV_LENGTH << "\n";
	cout << "%     Rmin="    << Rmin      << " " << UNIT_LENGTH     << " Rmax=" << Rmax      << " " << UNIT_LENGTH     << " Rstep=" << Rstep << " " << UNIT_LENGTH << "\n";
	cout << "%     Pk kmin=" << pk.kmin() << " " << UNIT_INV_LENGTH << " kmax=" << pk.kmax() << " " << UNIT_INV_LENGTH << " found sigma8=" << sigma8_unnorm << "\n";
	cout << "%   Output:  r [" << UNIT_LENGTH << "]  k [" << UNIT_INV_LENGTH << "]  m(r) [" << UNIT_MASS <<"]  ";
	cout << (sigma2mode ? "sigma2 [dimless]\n" : "Xi(r) [dimless]\n");

	timer tim;
	for(_len R=Rmin;R<=Rmax;(logscale ? R*=Rstep/_len(1) : R+=Rstep)){
		const _per_len k=2*_pi/R;
		flt f;
		if (sigma2mode){
			const IntegratorWithFilter<PkCmb,FilterTophat> intgr2(pk,R);
			f=Sqrt(intgr2.Integrate(kthreshold));
		}
		else {
			const IntegratorWithFilter<PkCmb,FilterSinc> intgr2(pk,R);
			f=intgr2.Integrate(kthreshold);
		}

		const _vol V=4*_pi*R*R*R/3;
		const _mass m=_rho0*V;

		cout << fwidth(R/_len(1),8,12) <<  " " << fwidth(k/_per_len(1),4,12) << " " << fwidth(m/_mass(1E-10),8,12) << " " <<  fwidth(f,4,12) <<  endl;
		tim.ToEta(R-Rmin,Rmax-Rmin,cerr);
	}

	return 0;
}

int Xi2Pk(args& a)
{
	const size_t column=a.parseval<int>("-c",4);
	const string tempfile1="out_temp_xi.txt";
	const string tempfile2="out_temp_pk.txt";

	if(a.size()<=2) return Usage_Convert(a);
	const string file=a.Totype<string>(2);
	const _len bbox=a.Tounit<_len>(3);

	cout << "% Xi2pk,\n";
	cout << "%   parameters: file=" << file << "  bbox=" << bbox << " " << UNIT_LENGTH << "  column=" << column << "\n";

	AssertFileNotEmpty(file);
	const array3d<flt> x=array3d<flt>::Readmatlab(file);
	const t_idx sz=x.sizet();

	if (column>sz[0]) throw_("bad column parameter");
	const vector<_len> r=totype<_len,flt>(x.slice(t_idx(0,0,0),t_idx(1,sz[1],1)));
	const vector<flt> xi=x.slice(t_idx(column-1,0,0),t_idx(column,sz[1],1));
	assert( xi.size()==r.size() );

	System("rm " + tempfile1 + " " + tempfile2 + " 2>/dev/null",false);
	ofstream s(tempfile1.c_str());
	for(size_t i=0;i<r.size();++i) s << r[i]/_len(1000) << " " << xi[i] << "\n";
	s.close();
	AssertFileNotEmpty(tempfile1);

	System(string("convolve_pk ") + bbox/_len(1000) + " " + tempfile1 + " " + tempfile2);
	cout << "%   Output: ?? power []\n";
	System("cat " + tempfile2);
	System("rm " + tempfile1 + " " + tempfile2 + " 2>/dev/null",false);

	return 0;
}


int Tf2Pk(args& a)
{
	const int column=a.parseval<int>("-c",1);
	const flt sigma8=a.parseval<flt>("-8",default_sigma8);

	if(a.size()<=1) return Usage_Convert(a);
	const string file=a.Totype<string>(2);

	if (column<1 || column>4) throw_("bad column, must be in range [1;4], 1=cdm, 2=baryons, 3=photons, 4=neutrinos");

	AssertFileNotEmpty(file);
	PkCmb pk(file,column);

	if(sigma8>0){
		const flt sigma8_unnorm=pk.Sigma8();
		pk.Normalize(sigma8,sigma8_unnorm);
		const flt sigma8_norm=pk.Sigma8();
	 	if (!_proximy(sigma8_norm,sigma8,1E-2)) throw_(string("sigma8 and found norm differ, sigma8=") + sigma8 + " found sigma8=" + sigma8_norm);
		if (!_proximy(sigma8_norm,sigma8,1E-6)) warn_(string("sigma8 and found norm differ, sigma8=") + sigma8 + " found sigma8=" + sigma8_norm);
	}

	cout << "% Tf2Pk,\n%   parameters:  file=<" << file << ">  column=" << column << "  sigma8=" << sigma8 << "\n";
	cout << pk;

	return 0;
}

int main_convert(int argc,char** argv)
{
	try{
		args a(argc,argv,true,false);

 		if(a.size()<=2) return Usage_Convert(a);
		const string mode=a.Totype<string>(1);

		if      (mode=="pk2xi")    return Pk2Xi(a,false);
		else if (mode=="pk2sigma") return Pk2Xi(a,true);
		else if (mode=="xi2pk")    return Xi2Pk(a);
		else if (mode=="tf2pk")    return Tf2Pk(a);
		else return Usage_Convert(a);
	}
	CATCH_ALL;
	return -1;
}
