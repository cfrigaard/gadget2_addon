#ifndef __BINS_H__
#define __BINS_H__

template<class T,class R>
class bins
{
	public:
		typedef pair<R,long long> t_mp; // key value sum, count
		typedef pair<T,t_mp> t2_mp;     // range value sum, keys
		typedef pair<T,t2_mp> t3_mp;    // range val, keys
		typedef vector<t3_mp> t_vector_mp;
		typedef	typename t_vector_mp::const_iterator const_iterator;

	private:
		t_vector_mp m_b;
		size_t m_outofbounds;
		const range<T> m_r;
		const bool m_allowoutofbounds;

	public:
	bins(const T xmin,const T xmax,const size_t xbins,const bool allowoutofbounds=false)
	: m_outofbounds(0), m_r(xmin,xmax,xbins,true), m_allowoutofbounds(allowoutofbounds)
	{
		for(size_t i=0;i<m_r.size();++i) {
			m_b.push_back(t3_mp(m_r.totype(i),t2_mp(T(0),t_mp(R(0),0))));
		}
	}

	t_mp Count(const bool countoutofbounds=false) const
	{
		t_mp v(R(0),0);
		for(const_iterator itt=m_b.begin();itt!=m_b.end();++itt) {
			v.first  +=itt->second.second.first;
			v.second +=itt->second.second.second;
		}
		if (countoutofbounds) v.second += m_outofbounds;
		return v;
	}

	const t2_mp& Find(const T& d) const
	{
		if(!m_r.isinrange(d)) throw_("out of bound value in bins, val=" + tostring(d) + " range=" + tostring(m_r));
		const size_t idx=m_r.toidx(d);
		return m_b[idx].first;
	}

	T Mean(const T& d) const
	{
		const t2_mp& m=Find(d);
		if (m.second.second==0) return T(0);
		else return m.first/static_cast<T>(m.second.second);
	}

	void AddBinCount(const T& d,const R& count)
	{
		const bool inrange=m_r.isinrange(d);
		if(!inrange){
			if(!m_allowoutofbounds) throw_("out of bound value in bins, val=" + tostring(d) + " range=" + tostring(m_r));
			++m_outofbounds;
		}else{
			const size_t idx=m_r.toidx(d);
			t2_mp& t2=m_b[idx].second;
			t2.first += d;
			t_mp& t=t2.second;
			t.first += count;
			++t.second;
		}
	}

	size_t          size    () const {return m_b.size();}
	const_iterator  begin   () const {return m_b.begin();}
	const_iterator  end     () const {return m_b.end();}
	const range<T>& getrange() const {return m_r;}

	friend ostream& operator<<(ostream& s,const bins<T,R>& x)
	{
		s << "bins: " << x.getrange() << "{\n";
 		for(typename bins<T,R>::const_iterator itt=x.begin();itt!=x.end();++itt){
			const t2_mp t2=*itt;
			const t_mp  t =t2.second;
			cout << "  " << t2.first << " " << t.first << " " << t.second << "\n";
		}
		return s << "}";
	}
};

#endif // __BINS_H__
