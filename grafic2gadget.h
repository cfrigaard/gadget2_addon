void grafic2gadget(const string& filedelta,const string& filevel,const string& fileout,const _bbox_len& bbox_set,const int types,const bool compatiple,string pushd)
{
	cout << "% Grafic2gadget fileconversion" << endl;
	cout << "%   parameters: file delta=<" << filedelta << "> vel=<" << filevel << " output=<" << fileout << ">  type=" << types;
	if (bbox_set()>_len(0)) cout << " bbox=" << bbox_set;
	if (pushd.size()>0) cout << " pushd=" << pushd;
	if (compatiple) cout << " compatiple";
	cout << endl;

	if (types<0 || types>2) throw_("types out of range, must be in [0;2]");
	if (pushd.size()>1 && pushd[pushd.size()-1]!='/') pushd+='/';

	const string dfile=pushd + filedelta;
	AssertFileNotEmpty(dfile);
	ifstream s(dfile.c_str());
	cout << "%   reading file <" << dfile << ">..." << endl;

	const GraficHeader h1(s);
	const Triple<size_t> sz=h1.sizet();

	const size_t bbox_int=static_cast<size_t>((h1.getdx()/_len(1)*sz[0]+.5)/1000)*1000;
	const _bbox_len bbox(bbox_set()<=_len(0) ? _len(bbox_int) : bbox_set() );

	const _mass masstotal   =h1.getOm()*_rho0*bbox()*bbox()*bbox();
	const _mass massparticle=masstotal/h1.gridsize()/(types==2 ? 2 : 1);

	cout << "%      " << h1 << endl;
	cout << "%      masstotal=" << masstotal << "  massparticle=" << massparticle << endl;
	if (bbox_set()<=_len(0)) cout << "%   Guessing bbox=" << bbox << endl;

	if (!(sz[0]==sz[1] && sz[1]==sz[2]) ) throw_("cannot handle non-square data"); // has to be square
	if (massparticle<=_mass(0)) throw_("bad mass");
	if(bbox()<=_len(0)) throw_("bad bounding box");

	// density data not used for now
	// 	cout << "%  reading file <" << filedelta << "> gridsize=" << h1.gridsize() << "..." << endl;
	// 	const array3d<float> delta=ReadFortran<float>(s,sz);
	// 	if (FileSize(filedelta)!=s.tellg()) throw_("trucated file");
	// 	s.close();
	// 	array<_mass> m(h.np1,h.np2,h.np3);
	// 	size_t n=0;
	// 	for(size_t k=0;k<h.np3;++k){
	// 		for(size_t j=0;j<h.np2;++j){
	// 			for(size_t i=0;i<h.np1;++i){
	// 				assert( n<delta.size() );
	// 				const flt d=delta[n++];
	// 				m(i,j,k)=m_mean*(d+1)
	// 			}
	// 		}
	// 	}

	array3d<Triple<float> > velb(sz);
	array3d<Triple<float> > velc(sz);
	GraficHeader h2,h3;

	// types: 0 => halo only
	//        1 => dm only
	//        2 => dm and halo
	assert( types>=0 && types<3 );
	const int i0= (types==0 || types==2) ? 0 : 3;
	const int i1= (types==1 || types==2) ? 6 : 3;

	for(int i=i0;i<i1;++i){
		const size_t j=i%3;
		assert( j>=0 && j<3);
		const string file=pushd + filevel+ (i<3? 'b' : 'c') + static_cast<char>('x'+ j);
		cout << "%   reading file <" << file << ">..." << endl;

		AssertFileNotEmpty(file);
		ifstream s(file.c_str());
		const GraficHeader h(s);
		cout << "%     " << h << endl;
		if (h1!=h) throw_("mismatch in GraficHeader structure for different files");

		if (i==0)      h2=h;
		else if (i==3) h3=h;

		const array3d<float> w=array3d<float>::ReadFortran(s,sz);

		array3d<Triple<float> >& vel=i<3 ? velb : velc;
		assert(w.sizet()==vel.sizet() );
		for(size_t n=0;n<vel.size();++n) vel[n][j]=w[n];

		if (static_cast<long long>(FileSize(file))!=s.tellg()) throw_("trucated file");
	}

	const _h h=h1.geth();

	// -c option: dx and offset compatiple with:
	// const _len dx=bbox()/sz[0];
	// const _len dx1=dx0;
	// const _len dx2=dx0;
	// const _len offset1=0;
	// const _len offset2=0;

	const _len dx0=bbox()/sz[0];
	const _len dx1=compatiple ? dx0 : h2.getdx();
	const _len dx2=compatiple ? dx0 : h3.getdx();
	const _len offset1=compatiple ? _len(0.5*dx0) : h2.getx1o(); //
	const _len offset2=compatiple ? _len(0)       : h3.getx1o(); // == 0.5*(dx0-dx1);

	assert( dx1!=dx2 || offset1!=offset2 );
	//assert( types==0  || compatiple || _proximy(offset2,0.5*(dx0-dx1),1E-7) );

	const flt vfact=h1.Vfact();
	assert( types==1 || h2.Vfact()==vfact );
	assert( types==0 || h3.Vfact()==vfact );
	const flt vv=1.0/Sqrt(h1.geta()); // velocity from physical to comoving

	// c  velocity (proper km/s) =  Displacement (comoving Mpc at astart) * vfact.
	// c  vfact = dln(D+)/dtau where tau=conformal time.
	// c  These functions (fomega, dladt) are in time.f, so link them in.

	io_header_1 gh=h1.GenerateGadgetHeader(bbox);
	vector<particle_data_2> v;

	for(size_t type=(i0==0 ? 1 : 5);type<(i1==6 ? 6 : 2);type+=4) {
		assert( type==1 || type==5 );
		array3d<Triple<float> >& vel=type==1 ? velb    : velc;
		const _len&               dx=type==1 ? dx1     : dx2;
		const _len&           offset=type==1 ? offset1 : offset2;

		size_t count=0;
		cout << "%  generating data: type=" << type << " h=" << h << " dx=" << dx << " bbox=dx*N=" << dx*sz[0] <<  " offset=" << offset << endl;

		for(size_t k=0;k<sz[2];++k){
			// read(11) ((velx(i1,i2,i3),i1=1,np1),i2=1,np2)
			// c  Unperturbed grid position.
			const _len z0=k*dx+offset;
			for(size_t j=0;j<sz[1];++j){
				const _len y0=j*dx+offset;
				for(size_t i=0;i<sz[0];++i,++count){
					//c  For the y-component, replace x0 with y0.
					// x(i1,i2,i3)=x0+velx(i1,i2,i3)/vfact
					const _len x0=i*dx+offset;
					const Triple<float>& vfloat=vel(i,j,k);

					_point_len p(x0,y0,z0);
					assert( bbox.IsInsideBBox(p) );

					_vector_vel w;
					for(int t=0;t<3;++t) {
						p[t] += Mpc2kpc_inv_h(vfloat[t]/vfact,h); // position from Mpc to kpc/h
					    w[t]  = _vel(vfloat[t]*vv); // velocity from physical to comoving
					}

					bbox.WrapToInsideBox(p,true); // trucate to float
					v.push_back(particle_data_2(type,p,massparticle));
					v.back().setvel(w);
				}
			}
		}
		assert( type==1 || type==5 );
		gh.npart[type]=gh.npartTotal[type]=count;
		gh.mass [type]=tonum(massparticle); // not handling individual masses in multiple levels
	}

	assert( gh.particlestotal()==v.size() );
	cout << "%  saving snapshotfile: <" << pushd << fileout << ">" << endl;
	SaveSnapshot(gh,v,pushd + fileout,true);
}

int Usage_Grafic2gadget(const args& a)
{
	cerr << "Usage: " << a[0] << " [-b <bbox>] [-c] [-cd <dir>] [-d deltafile] [-o gadgetfile] [-v velocityfile] [-t <types>]\n";
	cerr << "  " << Version() <<  " " << Config() <<  " \n";
	cerr << "Options:\n";
	cerr << "   -b <float " + UNIT_LENGTH + ">: specify bounding box size\n";
	cerr << "   -c: compability mode with trenti IC_multilevel_assembler code (does not set dx and offset according to ic files)\n";
	cerr << "   -cd <string>: pushd to dir before executing\n";
	cerr << "   -d <file>: grafic over-density file, default=ic_deltab\n";
	cerr << "   -o <file>: gadget2 output file name, default=ic_gadget.g\n";
	cerr << "   -v <file>: grafic velocity file-pattern, looks for baryon velocities <vel>b{x/y/z}, and CDM file <vel>c{x/y/z}, default=ic_vel\n";
	cerr << "   -t <int>: type of particles, 0=halo only, 1=dm only, 2=halo and dm, default=2\n";
	return -1;
}

int main_grafic2gadget(int argc,char** argv)
{
	try{
		args a(argc,argv,true,false);

		const _bbox_len bbox=a.parseunit<_len>("-b",_len(-1));
		const bool compatiple=a.parseopt("-c");
		const string pushd=a.parseval<string>("-cd","");
		const string filedelta=a.parseval<string>("-d","ic_deltab");
		const string filevel=a.parseval<string>("-v","ic_vel");
		const string fileout=a.parseval<string>("-o","ic_gadget.g");
		const int types=a.parseval<int>("-t",2);

		if(a.size()!=1) return Usage_Grafic2gadget(a);
		if (compatiple && types!=1) warn_("-c normaly implies -t 1");

		grafic2gadget(filedelta,filevel,fileout,bbox,types,compatiple,pushd);
		return 0;
	}
	CATCH_ALL;
	return -1;
}
