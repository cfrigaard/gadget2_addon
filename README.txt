This is an addon module to Gadget-2

See Makefile and manual page for more information (run man man/density.1 etc.).

The published software package, called the Gadget Add-on library,
contains a series of methods to process and analyze cosmological data.

The main back-end cosmological N-body simulator used was Gadget-2, and input/output
is mainly focused on handling Gadget2 format data in the form of snapshot files.
Gadget-2 can be obtained from http://www.mpa-garching.mpg.de/gadget/, thanks Volker!.

Also thanks to Edwin Sirko for the public available IC generator
[http://www.astro.princeton.edu/~esirko/ic/] and Brad Krane for an IC to snapshot
converter [http://www.science.uwaterloo.ca/~bjkrane/software.htm]. Browsing through
Edwin'c code save me a lot development time, and i snatched some nice integration
algorithm there!

The code is in C++, with compilation and test running under a number of different
architectures. A 'modern' C++ compiler has to be used, preferable GCC version
3.4.5 or 4.x or later, currently these systems are supported

\begin{itemize}
    \item GCC 4.1.2 under mandrake or ubuntu linux,
      [ gcc version 4.1.2 20070302 (prerelease) (4.1.2-1mdv2007.1) ]
    \item GCC 3.4.5 under redhat linux 64 bit,
	  [ gcc version 3.4.5 20051201 (Red Hat 3.4.5-2) ]
    \item GCC 4.0.1 under Mac Darwin
      [ gcc version 4.0.1 (Apple Computer, Inc. build 5363) ]
\end{itemize}

\begin{itemize}
	\item{Basic config:} no dependencies
	\item{Optional configs:}\\
		* only with USE_UNITS option: boost (v 1.33.1), quan (v 0.2.0),\\
		* only with USE_FFTW  option: fftw3 (v 3.1.2), fftw++-(1.02)\\
		* only with USE_MPI   option: mpich (v 2-1.0.4)\\
\end{itemize}

Add to lib path:
\begin{itemize}
    \item declare -x LD_LIBRARY_PATH=.:/usr/local/lib
    \item or add /usr/local/lib to /etc/ld.so.conf as superuser
\end{itemize}

Testing;
\begin{itemize}
    \item Untar test library (found on frigaard.homelinux.org)
    \item Add soft link to test directory in gadget_addon directory, or copy it to the directory directly
    \item run the testsuite binary
\end{itemize}

Short module description:

\begin{itemize}
    \item{ density     :} calculates density on a regular grid, to be used for generating a powerspectrum,
                          direct integration (correct but slow) and CIC algorithms supported. Newly added speedup mode: SIS (sphere-in-sphere) mode.
    \item{ massinsphere:} find the mass and massvariance in a sphere,
                          use -8 option for massvariance in 8 h^-1 Mpc sphere.
    \item{ nbodysolve:}   experimental nbody solver, using direct gravitational computations
                          of complexity O(n^2), or an approximate multipole calculation.
                          Not reference tested, and still numerically unstable.
    \item{ power       :} calculates the powerspectrum from the overdensities found by the 'density' module.
    \item{ twopointcorr:} find the twopoint correlation function, slow right now.
    \item{ toascii     :} converts a snapshotfile into ascii, for quick debugging/preview.
    \item{ gendata     :} generates synthetic data (particles on a grid or random).
    \item{ genic       :} ic generator, not working yet.
    \item{ gfilter     :} filtrates a snapshotfile on ids and/or particle types,
                          generates smoothinglengths ascii file.
    \item{ ghead       :} dumps the header information for a snapshotfile, for debug/preview.
    \item{ grafic2gadget:} grafic to gadget format converter, one-level assembler only.
                          Reference tested against Trenti's Grafic to gadget conversion tool.
    \item{ convert     :} various conversion algorithms, not reference tested yet.
    \item{ testsuite   :} test module, relies on the testdata down loadable separable. May not
                          work on 64 bit architectures.
\end{itemize}

Status:
\begin{itemize}
    \item{density      :} working, MPI enabled, algorithm(s). Reference tested.
    \item{massinsphere :} working, MPI enabled, algorithm(s). Reference tested.
    \item{nbodysolve   :} partial working, not MPI enabled, algorithm(s) not reference tested.
    \item{power        :} working, not MPI enabled. Reference tested.
    \item{twopointcorr :} working, MPI enabled, not reference tested.
    \item{toascii      :} working, not MPI enabled.
    \item{gendata      :} working, not MPI enabled.
    \item{genic        :} preliminary only.
    \item{gfilter      :} working, not MPI enabled,
    \item{ghead        :} working, not MPI enabled
    \item{grafic2gadget:} working, not MPI enabled. Reference tested.
    \item{convert      :} working, not MPI enabled, algorithm(s) not reference tested.
    \item{testsuite    :} working, two modes:  1) normal non-MPI mode, 2) MPI testing mode for examining mpi calls.
\end{itemize}

Version:    0.7.215
Date:       Martch 23, 2008
Site:       http://frigaard.homelinux.org/pub/gadget_addon-0.7.tgz
            http://frigaard.homelinux.org/pub/gadget_addon-0.7-testfiles.tgz

New in version 0.7.215: pre-exam release, new normalization in powerspectrum, various optimizations, production release.

The code is copyrighted 2008 by Carsten Frigaard.
All rights placed in public domain under GNU licence, 2008

© 2008 Carsten Frigaard. Permission to use, copy, modify, and distribute this software
and its documentation for any purpose and without fee is hereby granted, provided that
the above copyright notice appear in all copies and that both that copyright notice
and this permission notice appear in supporting documentation.
