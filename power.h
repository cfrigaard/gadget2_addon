const string outputformat_power=
 "k [" + UNIT_INV_LENGTH
 + "]  k_binmean [" + UNIT_INV_LENGTH
 + "]  bincount [dimless]"+  "  P(k) [" + UNIT_VOL
 + "]  P_mean(k) [" + UNIT_VOL
 + "]  Delta^2(k)=P(k)*k^3/(2*pi^2) [dimless]"
 + "]  Delta_mean^2(k)=P_mean(k)*k_mean^3/(2*pi^2) [dimless"
 + "]  Power_unormalized(bin) [dimless"
 + "] delta(P_mean(k)) ["  + UNIT_VOL + "]";

typedef bins<_per_len,flt> k_bins;

void Power(const string& filename,const _bbox_len& bbox,const size_t kbins)
{
	array3d<flt> d=array3d<flt>::Readascii(filename); // [d]=density, dimless for delta
	const flt dmin=d[d.FindMin()];
	if (!dmin<0 || dmin<-1.0) warn_("suspicious data, is it really in dimless delta rho format?");
	const array3d<cmplx> c=totype<cmplx>(d);
	d.clear();

	if(bbox()<_len(1)) throw_("bad bbox step parameter");
	if(c.size()<1)     throw_("to small datasize");
	if(!c.isSquare())  throw_("cannot handle non-square size data yet");

	// Matlab: freq = (1:n/2)/(n/2)*nyquist;
	const flt nyquist=0.5;
	const size_t N=c.sizex();
	const size_t n_nyquist=static_cast<size_t>(N*nyquist+.5)-1;
	if (N%2!=0) warn_(string("N not modulus of two, N=") + N + " nyquist rate="
	                   + n_nyquist + ", try density with even grid parameter");
	if (N/2<n_nyquist) throw_("N/2 and nyquist rate mismatch");

	const _vol V=bbox()*bbox()*bbox();
	const _per_len k_delta=2*_pi/bbox();
	const _per_len k_len_min=k_delta; // k_delta*idx2klen(t_idx(0,0,0),N);
	const _per_len k_len_max=k_delta*idx2klen(t_idx(n_nyquist,n_nyquist,n_nyquist),N);

	const _vol normalization=V; // no /Pow3(2*_pi) factor -> normalize without 2pi factors!

	cout << "% Fourier analysis,"
		 << "\n%    bbox=" << bbox << " " << UNIT_LENGTH  << "  kbins=" << kbins
		 << "  V=" << V << " " << UNIT_VOL << "  k_delta=" << k_delta << " " << UNIT_INV_LENGTH
		 << "\n%    k_len_min=" << k_delta << " " << UNIT_INV_LENGTH
		 << "  k_len_max=" << k_len_max << " " << UNIT_INV_LENGTH
		 << "\n%    datasize=" << c.sizex() << "  n_nyquist=" << n_nyquist
		 << "  normalization=" << normalization << " " << UNIT_VOL << "\n";

	// Find DC value and Parseval
	long double totalsum=0,parseval_rspace=0;
	for(size_t n=0;n<c.size();++n) {
		const flt x=c[n].real();
		totalsum += x;
		parseval_rspace += x*x;
	}

	// Do the Fourier transformation
	array3d<cmplx> F=FFt3(c);// [F]=[delta]=dimless
	assert( F.sizex()==c.sizex() && F.isSquare() );
	if (!F[0].imag()==0) warn_("DC part has imaginary component, DC=" + tostring(F[0]) );
	if (!_proximy(static_cast<flt>(totalsum),F[0].real(),1E-6))
	   warn_(string("DC part of Fourier not equal sum, totalsum=") + totalsum + " calc DC=" + F[0].real());

	long double parseval_kspace=0;
 	for(size_t n=0;n<F.size();++n) {
  		F[n] /= F.size(); // normalize to 1/N^3, why=definition of Fourier
		parseval_kspace += (F[n]*Conj(F[n])).real();
	}
	parseval_kspace *= c.size();
	if (!_proximy(parseval_rspace,parseval_kspace,1E-6))
	  warn_(string("Parseval theorem not fulfilled, parseval(r-space)=") + parseval_rspace
	        + " parseval(k-space)=" + parseval_kspace );

	long long sum=0;
	k_bins k_d2abs(k_len_min,k_len_max,kbins,true);

	for(size_t k=0;k<N;++k){
		for(size_t j=0;j<N;++j){
			for(size_t i=0;i<N;++i){
				const flt idx=idx2klen(t_idx(i,j,k),N);
				const _per_len k_len=k_delta*idx;
				if(k_len<k_len_max){
					const cmplx& x=F(i,j,k); // [F]=[delta]=dimless
					const flt x_abs=Abs(x);	 // [x]=dimless
					const flt x_pow=x_abs*x_abs;

					k_d2abs.AddBinCount(k_len,x_pow);
					++sum;
				}
			}
		}
	}

	if( sum!=k_d2abs.Count(true).second)
		warn_(string("bad sums in Fourier analysis (map mismatch) sum=")
		+ tostring(sum) + " expected sum=" + tostring(k_d2abs.Count().second));

	int i=0;
	cout << "% counts=[ ";
	for(k_bins::const_iterator itt=k_d2abs.begin();itt!=k_d2abs.end();++itt,++i) {
		cout << itt->second.second.second << " ";
		if (i%16==0) cout << "\n%  ";
	}
	cout << "]\n";

	cout << "% Power analysis: " << outputformat_power << "\n";

	const int fpre=3,fpost=10;
	assert( k_d2abs.getrange().size()>2 );
	const _per_len k_per_bin=k_d2abs.getrange().approxstep(); // assume non-logarithmic bins
	assert( _proximy(k_d2abs.getrange().totype(1)-k_d2abs.getrange().totype(0)
	  ,k_d2abs.getrange().totype(2)-k_d2abs.getrange().totype(1),1E-9) );
	assert( _proximy(k_per_bin,k_d2abs.getrange().totype(2)-k_d2abs.getrange().totype(1),1E-9) );

	for(k_bins::const_iterator itt=k_d2abs.begin();itt!=k_d2abs.end();++itt) {
		const long long c=itt->second.second.second;

		if(c>0) {
			const flt       N=static_cast<flt>(c);
			const _per_len  k=itt->first;
			const _per_len  kmean=itt->second.first/N;
			const flt           p=itt->second.second.first;
			assert( kmean>=k && kmean<k+k_per_bin && p>=0 );

			const _vol      pk=p*normalization/N;
			const flt       pk_per_kbin2_k   =pk*k*k*k/(2*Pow2(_pi));
			const flt       pk_per_kbin2_mean=pk*kmean*kmean*kmean/(2*Pow2(_pi));

			const _len   delta_l=2*_pi/k_per_bin;
			const _vol   delta_V=delta_l*delta_l*delta_l;
			const _per_vol     n=N/delta_V; // XXX number density, is it N_total/V or N/delta_V, or ???
			const _vol error_bin=Sqrt(2/(4*_pi*kmean*kmean*k_per_bin*V)) * (pk+1/n);

			cout << fwidth(k/_per_len(1)     ,fpre,fpost) << " "; // bin k begin (end) value
			cout << fwidth(kmean/_per_len(1) ,fpre,fpost) << " "; // mean k bin value (k(bin)<=kmean<k(bin+1)
			cout << fwidth(c                 ,6   ,0    ) << " "; // count in bin
			cout << fwidth(pk/_vol(1)        ,fpre,fpost) << " "; // power mean in bin
			cout << fwidth(pk_per_kbin2_k    ,fpre,fpost) << " "; // power per k
			cout << fwidth(pk_per_kbin2_mean ,fpre,fpost) << " "; // power per k mean
			cout << fwidth(p                 ,fpre,fpost) << " "; // total unnormalized power in bin
			cout << fwidth(error_bin/_len(3) ,fpre,fpost) << endl;// error estimate
		}
	}
}

int Usage_Power(const args& a)
{
	cerr << "Usage: " << a[0] << " <input density file> <boxsize> [-b <kbins>] \n";
	cerr << "  " << Version() <<  " " << Config() <<  " \n";
	cerr << "  input density file: 3D ascii file of delta density (a3d)\n";
	cerr << "  boxsize <float " + UNIT_LENGTH  + ">: boxsize with " << UNIT_LENGTH << " unit\n";
	cerr << "Options:\n";
	cerr << "  -b <int>: number of k-bins, default=40\n";
	cout << "Output: to stdout\n   " << outputformat_power << "\n";
	return -1;
}

int main_power(int argc,char** argv)
{
	try{
		args a(argc,argv,true,false);
		const size_t kbins=a.parseval<size_t>("-b",40);

		if(a.size()!=4) return Usage_Power(a);
		const _bbox_len bbox=a.Tounit<_len>(2);

		Power(a[1],bbox,kbins);
		return 0;
	}
	CATCH_ALL;
	return -1;
}
