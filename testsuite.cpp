#include "inc.h"
#include "testsuite.h"

// for test of Greens
void Gen_data(const string& filename,Gadgetdatainfo& G,const _bbox_len& bbox,const UniverseParameters& u,const bool lineardata){}
//#include "genic2.h"  XXX only for Genic code

const string testdatapath_root  ="Test/";
const string testdatapath_inputs=testdatapath_root + "Inputs/";
const string testdatapath_refs  =testdatapath_root + "Refs/";

#define Inputs _DO_NOT_USE_DIRECT_PATHS
#define Refs   _DO_NOT_USE_DIRECT_PATHS

void TestBasicAssumptions(Testsuite& test)
{
	test.Header("BasicAssumptions");

	test(sizeof(char)==1 );
	test(sizeof(short)==2 );

    if (sizeof(size_t)==4){
        // 32 bit system
		test(sizeof(size_t)==4 );
		test(sizeof(long)==4);
		test(sizeof(unsigned long)==4);
		test(sizeof(long double)==12);
		test(sizeof(void*)==4 );
	} else {
        // 64 bit system
		test(sizeof(size_t)==8);
		test(sizeof(long)==8);
		test(sizeof(unsigned long)==8);
		test(sizeof(long double)==16);
		test(sizeof(void*)==8 );
	}

	test(sizeof(int)==4 );
	test(sizeof(long long)==8 );
	test(sizeof(unsigned int)==4 );
	test(sizeof(unsigned long long)==8 );
	test(sizeof(float)==4 );
	test(sizeof(double)==8 );

	test(sizeof(struct particle_data)==48  );
	test(sizeof(struct io_header_1)==256  );
	test(sizeof(pointf)==3*sizeof(float) );
	test(sizeof(pointd)==3*sizeof(double) );

	test(sizeof(flt)==sizeof(double) );

    test(sizeof(_len)==sizeof(flt));
	test(sizeof(_point_len)==sizeof(Triple<flt>));

	test(sizeof(particle_data_1)==4*4 );
	test(sizeof(particle_data_2)==8*4 );

	// test alignment
	vector<particle_data_1> x1;
	x1.resize(13);
	vector<particle_data_2> x2;
	x2.resize(13);
	test( reinterpret_cast<const char*>(&x1[0])+4*4==reinterpret_cast<const char*>(&x1[1]) );
	test( reinterpret_cast<const char*>(&x2[0])+8*4==reinterpret_cast<const char*>(&x2[1]) );
	test( reinterpret_cast<const char*>(&x1.back())-reinterpret_cast<const char*>(&x1.front())==12*4*4 );
	test( reinterpret_cast<const char*>(&x2.back())-reinterpret_cast<const char*>(&x2.front())==12*8*4 );
}

void TestStringops(Testsuite& test)
{
	test.Header("Stringops");

	test(tostring("hello")=="hello");
	test(tostring(int(-42))=="-42");
	test(tostring(float(-42))=="-42");

	string s= string("hello ") + "world";
	test( s == "hello world");
	s= string("hello ") + int(42);
	test( s == "hello 42");
	s= string("hello ") + float(42);
	test( s == "hello 42");

	test.Sub("Serialization");
	test.Subsub("simple types");

	int a=1;
	float b=2.2;
	double c=3.3;
	char d='d';
	cmplx e(-1,42.1);
	pointd pp(1,2.2,-42.1);

	s=tostring(a) + " " + b +  " " + c +  " " + d + " " + tostring(e) + " " + tostring(pp);
	istringstream is(s);
	a=0;b=0;c=0;d=0,e=0,pp=pointd(0,0,0);
	is >> a >> b >> c >> d >> e >> pp;
	test( a==1 && Proximyeps(b,2.2,1E-7) &&  Proximy(c,3.3) && d=='d');
	test( Proximy(e,cmplx(-1,42.1)) );
	test( pointd(1,2.2,-42.1)==pp  );

	test.Subsub("stl types");

	vector<size_t> aa,bb,cc;
	for(size_t i=0;i<7;++i) aa.push_back(i);
	s=tostring(aa);

	bb=totype<vector<size_t> >(s);
	test( aa==bb );

	istringstream is2(s);
	is2 >> cc;
	test( aa==cc );

	vector<pointd> dd,ee,ff;
	for(size_t i=0;i<7;++i) dd.push_back(pointd(i,1.102*i*i,1.3*i));
	s=tostring(dd);

	ee=totype<vector<pointd> >(s);
	test( Proximyeps(dd,ee,1E-15) );

	istringstream is3(s);
	is3 >> ff;
	test( Proximyeps(dd,ff,1E-15) );

	list<int> gg,hh;
	for(int i=0;i<9;++i) gg.push_back(i);
	istringstream is4(tostring(gg));
	is4 >> hh;

	test( gg.size()==hh.size() );
	for(int i=0;i<9;++i) {
		test(gg.front()==hh.front());
		gg.pop_front();
		hh.pop_front();
	}

	map<int,bool> ii,jj;
	for(int i=0;i<9;++i) ii[i]=i%2==0;
	istringstream is5(tostring(ii));
	is5 >> jj;

	test( ii.size()==jj.size() );
	for(int i=0;i<9;++i) {
		test( ii[i]==jj[i]);
	}

	test.Subsub("compound stl types");
	vector<vector<pointd> > kk,ll;
	for(int i=0;i<9;++i) {
		kk.push_back(vector<pointd>());
		for(int j=0;j<3;++j) {
			kk[i].push_back(j);
		}
	}
	s=tostring(kk);

	istringstream is6(s);
	is6 >> ll;
	test( ll.size()==9 );
	for(int i=0;i<9;++i) {
		test( ll[i].size()==3 );
		for(int j=0;j<3;++j) {
			test( ll[i][j]==kk[i][j] );
		}
	}

	test.Subsub("tostring  / totype compunds");

	vector<long long> v;
	vector<float> w;
	map<long long,double> m;
	map<long long,bool> n;
	for(size_t i=0;i<13;++i) {m[2*i]=i*i; n[4*i]=i%2; v.push_back(2*i); w.push_back(3*i);}

	string s0=tostring(v);
	string s1=tostring(totype<vector<long long> >(s0));
	test( s0==s1 );

	s0=tostring(w);
	s1=tostring(totype<vector<float> >(s0));
	test( s0==s1 );

	s0=tostring(m);
	s1=tostring(totype<map<long long,double> >(s0));
	test( s0==s1 );

	s0=tostring(n);
	s1=tostring(totype<map<long long,bool> >(s0));
	test( s0==s1 );

	test.Subsub("String replace, removerems, strip");

	test( replace("abc def","b","")=="ac def" );
	test( replace("abc def","b","x")=="axc def" );
	test( replace("abc def","def","hij")=="abc hij" );
	test( replace("abc defx","def","hij")=="abc hijx" );
	test( replace("abc defx","a","hij")=="hijbc defx" );
	test( replace("","a","hij")=="" );
	test( replace("","","hij")=="" );
	test( replace("a","","")=="a" );
	test( replace("a","a","")=="" );

	test( removerems("  % test %","%")=="  ");
	test( removerems("test test","%")=="test test");
	test( removerems("test %","%")=="test ");
	test( removerems("%","%")=="");
	test( removerems("","%")=="");
	test( strip("  strip   ")=="strip");
	test( strip("     ")=="");

}

void TestPoint(Testsuite& test)
{
	test.Header("Point");
	pointd p(0,-1,3);
	pointd q(-1.2,1.1,17.2);

	test(p[0]==0);
	test(p[1]==-1);
	test(p[2]==3);
	test(Proximy(q[0],-1.2) );
	test(Proximy(q[1],1.1));
	test(Proximy(q[2],17.2));
	test(Proximyeps(q.dist2<flt>(p),207.49,1E-15));

	pointd s=p+q;
	test(Proximy(s[0],0-1.2));
	test(Proximyeps(s[1],-1.+1.1,1E-6));
	test(Proximy(s[2],20.2));
	s=p-q;
	test(Proximy(s[0],0+1.2));
	test(Proximy(s[1],-1.-1.1));
	test(Proximyeps(s[2],3.-17.2,1E-6));
	s=p*q;
	test(Proximy(s[0],0*1.2));
	test(Proximy(s[1],-1.*1.1));
	test(Proximyeps(s[2],3.*17.2,1E-3));
	s=p/13;
	test(s[0]==0/13);
	test(Proximy(s[1],-1.0/13));
	test(Proximy(s[2],3.0/13));

	const pointd a(0,0,0);
	const pointd aa(1,0,0);
	const pointd b(1,1,1);
	const pointd c(1,12.7,-6);
	const pointd d(-3,1.3,_pi);

	test( a.dot(a)== 0 );
	test( a.dot(b)== 0 );
	test( aa.dot(aa)== 1 );
	test( b.dot(b)== 3 );

	test( Proximyeps(c.dot(d),-5.339555921538803,1E-15));
	test( b.projection(aa)==pointd(1,0,0));
	test( a.lengthfromline(aa,pointd(1,0,0))==0);
	test( a.lengthfromline(aa,pointd(1,1,0))==1);
	test( Proximyeps(b.lengthfromline(c,d),4.468479316383891,1E-6));

	// 	container x;
	// 	for(int i=0;i<10;++i){
	// 	  x.push_back(i);
	// 	  x.push_back(2*i);
	// 	}
	// 	container y=StrongReduced2column(x);
	// 	test( y.size()==2*2 );
	// 	test( y[0]==0 );
	// 	test( y[1]==0 );
	// 	test( y[2]==9 );
	// 	test( y[3]==18 );
	//
	// 	x[5*2+1]=17;
	// 	y=StrongReduced2column(x);
	// 	for(size_t i=0;i<y.size();i+=2){
	// 	  cout << "\n" << y[i] << " " << y[i+1];
	// 	}
	// 	test( y.size()==5*2 );
	// 	test( y[0]==0 );
	// 	test( y[1]==0 );
	// 	test( y[2]==4 );
	// 	test( y[3]==8 );
	// 	test( y[4]==5 );
	// 	test( y[5]==17 );
	// 	test( y[6]==6 );
	// 	test( y[7]==12 );
	// 	test( y[8]==9 );
	// 	test( y[9]==18 );
	//
	// 	x.clear();
	// 	for(int i=0;i<10;++i){
	// 	  x.push_back(i);
	// 	  x.push_back(_pi/1000);
	// 	}
	// 	y=StrongReduced2column(x);
	// 	test( y.size()==2*2 );
	// 	x[7*2+1]=2.1;
	// 	x[4*2+1]=-1.2;
	// 	y=StrongReduced2column(x);
	// 	test( y.size()==7*2 );
	//
	// 	x.clear();
	// 	for(int i=0;i<100;++i){
	// 	  x.push_back(i);
	// 	  x.push_back(sin((flt)i/100));
	// 	}
	// 	y=StrongReduced2column(x,1E-2);
	// 	test( y.size()==4*2);
	//
	// 	x.clear();
	// 	for(int i=0;i<1000;++i){
	// 	  x.push_back(i);
	// 	  x.push_back(sin((flt)i/20.1));
	// 	}
	// 	y=StrongReduced2column(x,1E-2);
	// 	test(y.size()==145*2);
	// 	Writematlab("test","d:/testx.m",x,2,true);
	// 	Writematlab("test","d:/testy.m",y,2,true);
	//
	// 	Line l(1,200,0.1);
	// 	x.resize(100);
	// 	for(int i=0;i<100;++i) x[i]=1;
	// 	for(int i=40;i<60;++i) x[i]=0;
	// 	y=l.ReducedSimple(x);
	//
	// 	test(y.size()==6*2);
	// 	test(y[1]==1);
	// 	test(y[3]==1);
	// 	test(y[5]==0);
	// 	test(y[7]==0);
	// 	test(y[9]==1);
	// 	test(y[11]==1);

	test.Sub("Triple extrafuns");

	test.Subsub("Triple periodic");

	Triple<int> t0(0,1,2);
	Triple<int> t1(1,3,1293);
	Triple<int> t2(-1,-2,-8);
	Triple<int> t3(-1,-3,-1293);

	size_t sz=5;

	Triple<size_t> t=t0.periodic(sz);
	test( t0[0]==static_cast<int>(t[0]) && t0[1]==static_cast<int>(t[1]) && t0[2]==static_cast<int>(t[2]) );
	sz=2;
	t=t0.periodic(sz);
	test( t[0]==0 && t[1]==1 && t[2]==0 );

	t=t1.periodic(sz);
	test( t[0]==1 && t[1]==1 && t[2]==1 );
	sz=3;
	t=t1.periodic(sz);
	test( t[0]==1 && t[1]==0 && t[2]==0 );
	sz=4;
	t=t1.periodic(sz);
	test( t[0]==1 && t[1]==3 && t[2]==1 );
	sz=10;
	t=t1.periodic(sz);
	test( t[0]==1 && t[1]==3 && t[2]==3 );
	sz=1293;
	t=t1.periodic(sz);
	test( t[0]==1 && t[1]==3 && t[2]==0 );
	sz=1294;
	t=t1.periodic(sz);
	test( t[0]==1 && t[1]==3 && t[2]==1293 );

	sz=10;
	t=t2.periodic(sz);
	test( t[0]==9 && t[1]==8 && t[2]==2 ); // 10-1=9, 10-2=8... 10-8=2
	sz=5;
	t=t2.periodic(sz);
	test( t[0]==4 && t[1]==3 && t[2]==2 );
	sz=4;
	t=t2.periodic(sz);
	test( t[0]==3 && t[1]==2 && t[2]==0 );
	sz=3;
	t=t2.periodic(sz);
	test( t[0]==2 && t[1]==1 && t[2]==1 );
	sz=2;
	t=t2.periodic(sz);
	test( t[0]==1 && t[1]==0 && t[2]==0 );

	sz=2;
	t=t3.periodic(sz);
	test( t[0]==sz-1 && t[1]==sz-1 && t[2]==sz-1 );
	sz=3;
	t=t3.periodic(sz);
	test( t[0]==sz-1 && t[1]==0 && t[2]==0 );
	sz=4;
	t=t3.periodic(sz);
	test( t[0]==sz-1 && t[1]==sz-3 && t[2]==sz-1 );
	sz=10;
	t=t3.periodic(sz);
	test( t[0]==sz-1 && t[1]==sz-3 && t[2]==sz-3 );
	sz=1293;
	t=t3.periodic(sz);
	test( t[0]==sz-1 && t[1]==sz-3 && t[2]==0 );
	sz=1294;
	t=t3.periodic(sz);
	test( t[0]==sz-1 && t[1]==sz-3 && t[2]==sz-1293 );

	{
		test.Subsub("Defect: Check of error in periodic");
		const t_idx i0(1,2,3);

		for(size_t k=0;k<2;++k)
		for(size_t j=0;j<2;++j)
		for(size_t i=0;i<2;++i) {
			t_idx i1=i0;
			i1[0] += i;
			i1[1] += j;
			i1[2] += k;

			const t_idx i2=(i0+t_idx(i,j,k));
			test(i1==i2);

			if (i1[0]>=10) i1[0]=0;
			if (i1[1]>=10) i1[1]=0;
			if (i1[2]>=10) i1[2]=0;
			const t_idx i3=i2.periodic(size_t(10));

			test(i1==i3);
		}
	}


	{
		test.Subsub("Triple streams");
		Triple<flt> a(1,2,-288),a1;
		Triple<int> b(1,2,4),b1;
		Triple<cmplx> c(cmplx(1,2),cmplx(2,-13),cmplx(0,-2)),c1;
		_point_len p(_len(1),_len(-42),_len(2)),p1;

		string s=tostring(a) + " " + tostring(b) + " " + tostring(c) + " "+ tostring(p);
		#ifndef USE_UNITS
			istringstream is(s);
			is >> a1 >> b1 >> c1 >> p1; /// XXX, produces odd <kpc;-42> tag for units,
			test( a1==a );
			test( b1==b );
			test( c1==c );
			test( p1==p );
		#endif
	}
}

#ifdef _SSE

inline int Intify( float val )
{
  int rc;
  asm( "cvttss2si	%1, %0"
       : "=r" ( rc )
       : "m" ( val ) );
  return( rc );
}

float ScalarProduct( float* a1, float* a2, int n )
{
  float ans[4] __attribute__ ((aligned(16)));
  register int i;
  if( n >= 8 )
  {
    __asm__ __volatile__(
        "xorps      %%xmm0, %%xmm0"
        :
        :
        :  "xmm0" );
    for( i = 0; i < ( n >> 3 ); ++i )
    {
      __asm__ __volatile__(
            "movups     (%0), %%xmm1\n\t"
            "movups     16(%0), %%xmm2\n\t"
            "movups     (%1), %%xmm3\n\t"
            "movups     16(%1), %%xmm4\n\t"
            "add        $32,%0\n\t"
            "add        $32,%1\n\t"
            "mulps      %%xmm3, %%xmm1\n\t"
            "mulps      %%xmm4, %%xmm2\n\t"
            "addps      %%xmm2, %%xmm1\n\t"
            "addps      %%xmm1, %%xmm0"
            : "+r" ( a1 ), "+r" ( a2 )
            :
            :  "xmm0", "xmm1", "xmm2", "xmm3", "xmm4" );
    }
    __asm__ __volatile__(
        "movaps     %%xmm0, %0"
        :  "=m" ( ans )
        :
        :  "xmm0", "memory" );
    n -= i << 3;
    ans[0] += ans[1] + ans[2] + ans[3];
  }
  else
    ans[0] = 0.0;
  for( i = 0; i < n; ++i )
    ans[0] += a1[i] * a2[i];
  return( ans[0] );
}

void TestDistanceOptimization(Testsuite& test)
{
	test.Sub("DistanceOptimization");
	const size_t N=10000000;

	test.Subsub("Intify");
	long long n1=0,n2=0;
	timer t1;
	for(size_t i=0;i<N;++i){
		float x=i;
		n1 += static_cast<int>(x);
	}
	const double tt1=t1.elapsed();

	timer t2;
	for(size_t i=0;i<N;++i){
		float x=i;
		n2 += Intify(x);
	}
	const double tt2=t2.elapsed();
	test( n1==n2 );
	cout << "\ntt1=" << tt1 << "\ntt2=" << tt2;

	test.Subsub("ScalarProduct");
	float a[10],b[10];
	for(int i=0;i<8;++i) a[i]=b[i]=i*i; a[8]=b[8]=0;

	double ans1=0,ans2=0;
	timer t3;
	for(size_t i=0;i<N;++i){
		for(int j=0; j<8;++j) ans1 += a[j]*b[j];
	}
	const double tt3=t3.elapsed();

	for(int i=0;i<8;++i) a[i]=b[i]=i*i; a[8]=b[8]=0;
	timer t4;
	for(size_t i=0;i<N;++i){
		ans2 += ScalarProduct(a,b,8);
	}
	const double tt4=t4.elapsed();
	test( ans1==ans2 );

	cout << "\ntt3=" << tt3 << "\ntt4=" << tt4 << endl;
}
#else
	void TestDistanceOptimization(Testsuite& test) {} // empty test
#endif // NO_SSE

_len      F1(const _len x){return 12*x;}
_per_len  F2(const _len x){return 1.0/(x+_len(2));}
flt       F3(const flt  x){assert(x!=0); return 1.0/x;}

void TestMath(Testsuite& test)
{
	test.Header("Math");

	test(Proximyeps_noprint(1.,0.,1E-12)==false);
	test(Proximyeps_noprint(0.,1.,1E-12)==false);
	test(Proximyeps_noprint(1.,1.1,1E-12)==false);
	test(Proximyeps_noprint(1.,1.+1E-11,1E-12)==false);

	test(Proximy(1.,1)==true);
	test(Proximyeps(1.,1+1E-13/2,1E-13)==true);

	typedef vector<flt> container;
	container c;
	for(int i=0;i<100;++i) {c.push_back(i*_pi); c.push_back(1000*i*_pi); c.push_back(-1000*i*_pi);}
	c[3]= -500*_pi;
	c[6]= 700*_pi;

	test(Proximy(Max(c,1,3),700*_pi));
	test(Proximy(Min(c,1,3),-500*_pi));
	test(Max(c,2,3)>100*_pi);
	test(Min(c,2,3)>=0);
	test(Max(c,3,3)<=0);
	test(Min(c,3,3)<=100*_pi);
	test( Pow2(int(0))==0);
	test( Pow2(0.0)==0.0);
	test( Pow3(int(0))==0);
	test( Pow3(0.0)==0.0);
	test( Pow15(0.0)==0.0);

	test( Pow2(10)==100);
	test( Proximy(Pow2(10.),100.));
	test( Pow3(10)==1000);
	test( Proximy(Pow3(10.),1000.));
	test( Proximyeps(Pow15(10.),Pow(10.,1.5),1E-15) );

	test( Proximy(Pow(10.,0),1));
	test( Proximy(Pow(10.,0.),1.));

	test( Proximy(Pow(10.,2),100.));
	test( Proximy(Pow(10.,3),1000.));
	test( Proximy(Pow(10.,4),10000.));
	test( Proximy(Pow(10.,8),1E8));
	test( Proximy(Pow(10.,-2),1./100.));
	test( Proximy(Pow(10.,-3),1./1000.));
	test( Proximy(Pow(10.,-4),1./10000.));
	test( Proximy(Pow(10.,-8),1E-8));
	test( Proximyeps(Log10(2.),0.30102999566398,1E-14));
	test( Log10(1.)==0.);

	for(double i=0;i<10;++i)
	{
		test( Proximyeps(Pow(i,1.5),i*Sqrt(i),1E-15));
	}

	test( Proximyeps(Exp(1.),    2.718281828459,1E-13));
	test( Proximyeps(Exp(-1.),1./2.718281828459,1E-13));
	test( Proximyeps(Exp(0.),1,1E-13));
	test(Exp(0.)==1.0);
	test(Proximy(Exp(100.),2.6881171418161356e+043));
	TEST_EXCEPTION(Exp(10000.));

	test( Proximy( Abs(cmplx(1,2)),Sqrt(1.+2*2) ) );
	test( Conj(cmplx(1,2))==cmplx(1,-2) );

	test( Sum(0)==0 );
	test( Sum(1)==1 );
	test( Sum(2)==3 );
	test( Sum(10)==10+9+8+7+6+5+4+3+2+1 );

	test( Fact(0)==1 );
	test( Fact(1)==1 );
	test( Fact(2)==2 );
	test( Fact(10)==10*9*8*7*6*5*4*3*2*1 );

	test.Sub("Random");
	Random(0);	// seed with zero

	map<int,bool> seeds;
	for(int i=0;i<10;++i) {
		const int s=RandomSeed();
		test( seeds.find(s)==seeds.end() );
		seeds[s]=true;
	}

	#ifdef NDEBUG
		const float a1=Random();
		const float a2=Random();
		const float a3=Random();
		test( Proximyeps(a1,.3943829238415,1E-13));
		test( Proximyeps(a2,.7830992341042,1E-13));
		test( Proximyeps(a3,.7984400391579,1E-13));
	#endif

	map<flt,bool> ran;
	for(int i=0;i<1000;++i)
	{
		const float a=Random();
		test( a>=0 && a<1.0);
		test( ran.find(a)==ran.end() );
		ran[a]=true;
	}

	test.Subsub("Random repeatable");
	const int s=RandomSeed();
	vector<double> r;
	Random(s);
	for(int i=0;i<100;++i) {
		r.push_back(Random());
	}
	Random(s);
	for(int i=0;i<100;++i) {
		double x=Random();
		test( Proximy(r[i],x));
	}

	// 	test.Subsub("Gaussian and Rayleight distributions");
	// 	vector<flt> x;
	// 	for(int i=0;i<100000;++i){
	// 		const flt g0=cgasdev_ctr();
	// 		const flt g1=gaussian_distribution(0,1);
	// 		const flt r1=rayleigh_distribution(0,1);
	//
	// 		const flt r=cgasdev_ctr();
	// 		const flt i=cgasdev_ctr();
	// 		const cmplx z(r,i);
	//
	// 		const flt g2=gaussian_distribution(8,13);
	// 		const flt r2=rayleigh_distribution(8,13);
	//
	// 		x.push_back(g0);
	// 		x.push_back(g1);
	// 		x.push_back(g2);
	// 		x.push_back(r1);
	// 		x.push_back(r2);
	// 	}

	test.Sub("Mean");
	vector<pointf> x;
	vector<float> y;

	for(int i=0;i<101;++i)
	{
		x.push_back(pointf(i,3*i,2*i));
		y.push_back(i);
		y.push_back(3*i);
		y.push_back(2*i);
	}

	const pointf m1=Mean(x);
	const float  m2=Mean(y);
	test ( m1==pointf(50,150,100) );
	test ( m2==100 );

	{
		test.Subsub("MeanVar");

		vector<double> x;
		vector<float> y;

		for(int i=0;i<101;++i)
		{
			x.push_back(i);
			y.push_back(i);
		}

		double rms1,sigma21;
		const double m1=MeanVar(x,rms1,sigma21);

		float rms2,sigma22;
		const float  m2=MeanVar(y,rms2,sigma22);

		test( m1 == 50 );
		test( m2 == 50 );
		test( Proximy(rms1,3350) );
		test( Proximy(rms2,3350) );
		test( Proximy(sigma21,850 ) );
		test( Proximy(sigma22,850 ) );
		test( Proximy(rms1-m1*m1,sigma21) );
	}

	test.Sub("DistPeriodic");
	const float e=1E-6;
	pointd a(1,1,1);
	pointd b(2,2,2);
	const BBox<flt,flt> bb(10.);

	test( Proximyeps(bb.DistPeriodic(a,b),Sqrt(1.0+1+1),e) );
	b=pointd(9,9,9);
	test( Proximyeps(bb.DistPeriodic(a,b),Sqrt(2.0*2+2*2+2*2),e) );

	b=pointd(9,4,5);
	test( Proximyeps(bb.DistPeriodic(a,b),Sqrt(2.0*2+3*3+4*4),e) );

	b=pointd(9,4,7);
	test( Proximyeps(bb.DistPeriodic(a,b),Sqrt(2.0*2+3*3+4*4),e) );

	a=pointd(10,2000,14000);
	b=pointd(45000,8000,9000);
	BBox<flt,flt> bb3(50000.);
	test( Proximyeps(bb3.DistPeriodic(a,b),Sqrt(Pow2(5010.) + Pow2(6000.) + Pow2(5000.) ),1E-4) );

	a=pointd(0,0,0);
	b=pointd(0,0,50000-1E-6);
	test( Proximyeps( bb3.DistPeriodic(a,b),0,1E-5) );
	b=pointd(50000-1E-6,50000-1E-6,50000-1E-6);
	test( Proximyeps( bb3.DistPeriodic(a,b),0,1E-5) );
	b=pointd(0,49999,0);
	test( Proximy( bb3.DistPeriodic(a,b),Sqrt(1.0) ) );
	b=pointd(49999,49999,49999);
	test( Proximy( bb3.DistPeriodic(a,b),Sqrt(1.0+1+1) ) );

	test.Sub("SubPeriodic");

	_point_len ua(_len(10),_len(2000),_len(14000));
	_point_len ub(_len(45000),_len(8000),_len(9000));

	_point_len q=_bbox_len(_len(50000.)).SubPeriodic(ua,ub);
	test( Proximy(q[0]/_len(1),5010));
	test( Proximy(q[1]/_len(1),-6000));
	test( Proximy(q[2]/_len(1),5000));

	const _bbox_len bb4(_len(50000.));
	for(int i=0;i<50000;i+=15211){
		for(int j=0;j<50000;j+=4672){
			for(int k=0;k<50000;k+=19392){
				ub=_point_len(_len(i),_len(j),_len(k));
				q=bb4.SubPeriodic(ua,ub);
				const _len2 d1=q.length2<_len2>();
				const _len2 d2=bb4.DistPeriodic2(ua,ub);
				test( Proximyeps(d1/_len2(1),d2/_len2(1),1E-4)  );
			}
		}
	}

	test.Sub("Periodic distances");

	_bbox_len bb5(_len(5));
	const _len xlim(5-1E-12);
	test( xlim<_len(5) );

	_len2 d2=bb5.DistPeriodic2(_point_len(xlim),_point_len(_len(0)));
	test( Proximyeps(d2/_len2(1),1.0*3*0,1E-23) );

	d2=bb5.DistPeriodic2(_point_len(_len(1)),_point_len(xlim));
	test( Proximyeps(d2/_len2(1),1.0*1*1*3,1E-11) );

	d2=bb5.DistPeriodic2(_point_len(xlim),_point_len(_len(1)));
	test( Proximyeps(d2/_len2(1),1.0*1*1*3,1E-11) );

	d2=bb5.DistPeriodic2(_point_len(xlim),_point_len(_len(2),_len(2),_len(2)));
	test( Proximyeps(d2/_len2(1),1.0*2*2*3,1E-11) );

	d2=bb5.DistPeriodic2(_point_len(xlim),_point_len(_len(3),_len(2),_len(3)));
	test( Proximy(d2/_len2(1),1.0*2*2*3) );

	d2=bb5.DistPeriodic2(_point_len(xlim),_point_len(_len(3),_len(2.9),_len(3)));
	test( Proximy(d2/_len2(1),2.0*2*2+Pow2(5-2.9)) );

	test.Sub("Wrapped points");

	_point_len p(_len(1),_len(2),_len(3));
	q=p;
	_bbox_len(_len(10.0)).WrapToInsideBox(q);
	for(size_t i=0;i<3;++i) test( Proximy(p[i]/_len(1),q[i]/_len(1)));

	p=_point_len(_len(9),_len(8),_len(11));
	q=p;
	_bbox_len(_len(10.0)).WrapToInsideBox(q);
	test( Proximy(q[2]/_len(1),1) );
	p=_point_len(_len(9),_len(0),_len(11));
	q=p;
	_bbox_len(_len(7.0)).WrapToInsideBox(q);
	test( Proximy(q[0]/_len(1),2) );
	test( Proximy(q[1]/_len(1),0) );
	test( Proximy(q[2]/_len(1),4) );
	p=_point_len(_len(-0.1),_len(-1),_len(-100.01));
	q=p;
	_bbox_len(_len(10.0)).WrapToInsideBox(q);
	test( Proximy(q[0]/_len(1),9.9) );
	test( Proximy(q[1]/_len(1),9) );
	test( Proximyeps(q[2]/_len(1),9.99,1E-15) );

	test.Sub("tocmplx");

	array3d<double> aa(1,2,5);
	aa=1;
	array3d<cmplx>  ca=totype<cmplx>(aa);
	test( aa.size()==ca.size() );
	for(size_t i=0;i<ca.size();++i) test(cmplx(aa[i])==ca[i]);

	vector<vector<int> > vi;
	vector<vector<double> > vd;
	for(int j=0;j<5;++j)
	{
		vector<int>    vvi;
		vector<double> vvd;
		for(int i=0;i<5;++i)
		{
			vvi.push_back(i);
			vvd.push_back(i);
		}
		vi.push_back(vvi);
		vd.push_back(vvd);
	}
	#ifdef __GCC_V4__
	vector<vector<cmplx> > cvi=totype<cmplx>(vi);
	vector<vector<cmplx> > cvd=totype<cmplx>(vd);
	for(int j=0;j<5;++j)
	{
		for(int i=0;i<5;++i)
		{
			test( cvi[j][i]==cmplx(vi[j][i]) );
			test( cvd[j][i]==cmplx(vd[j][i]) );
		}
	}
	#endif

	test.Sub("cmplx");
	cmplx v(1,2);
	test( Abs(v)==Sqrt(5.0) );
	test( Conj(v)==cmplx(1,-2) );
	test( (v*Conj(v)).imag()==0.0 );
	test( (v*Conj(v)).real()==5.0 );

	test.Sub("Integration");
	_len2 f1=Integrate<_len,_len,_len2>(F1,_len(0),_len(10),1,1);
	test( Proximyeps(f1/_len2(1),600,1E-9));

	flt f2=Integrate<_per_len,_len,flt>(F2,_len(0),_len(10),1,1);
	test( Proximyeps(f2,1.7917595,1E-7));

	flt f3=Integrate<flt,flt,flt>(F3,0.1,10.0,1,1);
	test( Proximyeps(f3,4.60517,1E-7));

	{
		test.Sub("Interpolate");
		map<flt,flt> xy0,xy1,xy3;
		for(flt x=-10;x<20;x+=1) xy0[x]=x*x;
		for(flt x=-10;x<20;++x)  xy1[x]=42;

		flt y=InterpolateLinear(0.,xy0);
		test( Proximyeps( y,0,1E-6 ));
		y=InterpolateLinear(-9.9,xy0);
		test( Proximyeps( y,9.9*9.9,1E-3 ));
		y=InterpolateLinear(18.,xy0);
		test( Proximyeps( y,18.*18.,1E-6 ));

		for(flt x=-9.0;x<19;x+=0.173)  {
			y=InterpolateLinear(x,xy1);
			test( y==42 );
		}

		for(flt x=-10;x<20;x+=0.37) xy3[x]=x;
		for(flt x=-10;x<19;x+=0.1){
			y=InterpolateLinear(x,xy3);
			test( Proximyeps( x, y,1E-15 ) );
		}
	}

	TestDistanceOptimization(test);
}

void TestCompressedArraySub(Testsuite& test,const float l,const float u,float step,const double eps)
{
	test.Subsub("CompressedArraySubsub");
	assert( l<u );

	vector<float> p;
	vector<pointd> p2;
	for(float x=l;x<u;x+=0.1*step)
	{
		for(float y=l;y<u;y+=0.71*step)
		{
			for(float z=l;z<u;z+=0.17*step)
			{
				p.push_back(x);
				p.push_back(y);
				p.push_back(z);
				p2.push_back(pointd(x,y,z));
			}
		}
	}
	const float m0=Min(p,1,3);
	const float m1=Max(p,1,3);
	CompressedArray a(m0,m1);

	for(size_t i=0;i<p2.size();++i) a.push_back(p2[i]);
	test( p2.size()==a.size() );
	for(size_t i=0;i<p2.size();++i)
	{
		const pointd pa=tod(a[i]);
		const pointd pb=p2[i];
		for(int j=0;j<3;j++)
		{
			test( Proximyeps_noprint(pa[j],pb[j],eps) );
		}
	}
}

void SearchSubsearch_N(Testsuite& test,const _point_len& p,const particle_data_2& expected_p,const searchgrid& s,const size_t N,const size_t subjump)
{
	// test.Subsub("SearchSubsearch_N");
	// test( s.datasize()>20 );

	const _len2 expected_d2=p.dist2<_len2>(expected_p.pos());
	for(size_t subn=1;subn<N;subn+=subjump){
		const searchgrid::t_map_data c1=s.findclosest(p,subn);
		const searchgrid::t_map_data c2=s.findclosest_nonoptimized(p,subn);

		const _len2 d1=p.dist2<_len2>(c1.begin()->second.first->pos());
		const bool proxy3=Proximyeps( expected_d2/_len2(1),d1/_len2(1),1E-15 );
		test( &expected_p==c1.begin()->second.first || proxy3);

		const _len2 d2=p.dist2<_len2>(c2.begin()->second.first->pos());
		const bool proxy4=Proximyeps( expected_d2/_len2(1),d2/_len2(1),1E-15 );
		test( &expected_p==c2.begin()->second.first || proxy4);

		test( c1.size()<=subn && c1.size()==c2.size() );
		for(searchgrid::t_map_data::const_iterator l=c1.begin(),o=c2.begin();l!=c1.end();++l,++o)
		{
			test( l->first==o->first );
			test( l->second.first==o->second.first ||  l->first==o->first );
			test( l->second.second==o->second.second );
		}
	}

}

const particle_data_2& SearchSubsearch_1(Testsuite& test,const _point_len& p,const searchgrid& s)
{
	// test.Subsub("SearchSubsearch_1");

	const particle_data_2& c1=s.findclosest_nonoptimized(p);
	const particle_data_2& c2=s.findclosest(p);

	const _len2 d1=p.dist2<_len2>(c1.pos());
	const _len2 d2=p.dist2<_len2>(c2.pos());
	const bool proxy=Proximy( d1/_len2(1),d2/_len2(1) );
	test( &c1==&c2 || proxy);
	if (&c1!=0 && &c2!=0 ) test( c1.pos()==c2.pos() || proxy);
	if (&c1!=&c2 && !proxy)  {
		warn_( string("*** bad search: ") + tostring(s.getrange()) + " p1=" + tostring(c1.pos()) + " p2=" + tostring(c2.pos()) + " min dist=" + p.dist2<_len2>(c1.pos()) + "," + p.dist2<_len2>(c2.pos()) );
	}
	return c1;
}

void TestEtcClasses(Testsuite& test)
{
	test.Header("TestEtcClasses");

	test.Sub("File functionality 1");
	timer filetime;
	const string f="/tmp/testsuite.myfile.tmp";
	ofstream s(f.c_str());
	test( FileExists("nosuchfile")==false );
	test( FileExists(f)==true );
	AssertFileExists(f);
	test( FileSize(f)==0 );
	s << "hello";
	s.close();
	test( FileSize(f)==5 );
	AssertFileNotEmpty(f);

	test.Subsub("Read/Write - binary vector");
	vector<flt> xx;
	vector<int> ii;
	for(size_t i=0;i<120;++i) {xx.push_back(i*i); ii.push_back(i);}

	ofstream os((testdatapath_refs + "out").c_str(),ios::binary);
	Writebin(os,xx,true);
	Writebin(os,ii,true);
	Writebin(os,xx,true);
	os.close();

	ifstream is((testdatapath_refs + "out").c_str(),ios::binary);
	vector<flt> xx2,xx3;
	vector<int> ii2;
	Readbin(is,xx2);
	Readbin(is,ii2);
	Readbin(is,xx3);

	test( xx.size()==xx2.size() && xx.size()==xx3.size() );
	test( ii.size()==ii2.size() );

	if ( xx.size()==xx2.size() && xx.size()==xx3.size() && ii.size()==ii2.size() )
	for(size_t i=0;i<120;++i) {
		test( xx[i]==xx2[i] && xx[i]==xx3[i] );
		test( ii[i]==ii2[i] );
	}

	test.Subsub("Read/Write - binary map");
	map<int,int> mm;
	map<size_t,long double> nn;

	for(size_t i=0;i<120;++i) {mm[-i]=i*i+i; nn[i]=-i*i;}

	ofstream os2((testdatapath_refs + "out").c_str(),ios::binary);
	Writebin(os2,mm,true);
	Writebin(os2,nn,true);
	Writebin(os2,mm,true);
	os2.close();

	ifstream is2((testdatapath_refs + "out").c_str(),ios::binary);
	map<int,int> mm2,mm3;
	map<size_t,long double> nn2;
	Readbin(is2,mm2);
	Readbin(is2,nn2);
	Readbin(is2,mm3);

	test( mm.size()==mm2.size() && mm.size()==mm3.size() );
	test( nn.size()==nn2.size() );

	if ( mm.size()==mm2.size() && mm.size()==mm3.size() && nn.size()==nn2.size() )
	for(size_t i=0;i<120;++i) {
		test( mm[-i]==mm2[-i] && mm[-i]==mm3[-i] );
		test( nn[i]==nn2[i] );
	}

	test.Sub("class range");
	range<flt> r1(0.,10.,10);
	test( r1.toidx(0)==0 );
	test( r1.toidx(1E-300)==0 );
	test( r1.toidx(0.1)==0 );
	test( r1.toidx(0.9)==0 );
	test( r1.toidx(1.1)==1 );
	test( r1.toidx(9.9)==9 );
	test( r1.toidx(9.9999999)==9 );
	test( !r1.isinrange(10) );
	test( !r1.isinrange(11) );
	r1=range<flt>(0.,10.,10,true);
	test( r1.isinrange(10.) );
	test( !r1.isinrange(10.00001) );
	test( !r1.isinrange(11) );

	range<flt> r2(0.,10.,1.0);
	test( r1.size()==r2.size() && r1.size()==10 );
	for(flt x=0;x<10;++x) test( r1.toidx(x)==r2.toidx(x) );
	for(flt x=-40000;x<13000;x+=37413){
		for(int i=12;i<300;i+=53){
			range<flt> r3(x,Abs(x)+12.,i);
			test( r3.size()==size_t(i));
			test( r3.isinrange(x) );
			test( !r3.isinrange(Abs(x)+12) );
			test( r3.isinrange(Abs(x)+12-1E-3) );
			test( !r3.isinrange(Abs(x)+12+1E-3) );
			test( r3.isinidx(0) );
			test( r3.isinidx(i-1) );
			test( r3.rmin()==x);
			test( Proximy(r3.rmax(),Abs(x)+12 ));
			for(flt y=r3.rmin();y<r3.rmax();y+=13){
				const size_t idx=r3.toidx(y);
				const flt z=r3.totype(idx);
				test( r3.toidx(z)==idx );
				test( z<y || z==r3.rmin() );
			}
		}
	}

	test.Subsub("Pathetic ranges");
	range<flt> r(1E-9,1E9,10,true);
	test( r.size()==10 );
	test( r.toidx(1E-9)==0 );
	test( r.toidx(1E9)==9);
	test( r.isinrange(1E9));
	test( !r.isinrange(1.0001E9));
	r=range<flt>(-1E9,1E9,10,true);
	test( r.size()==10 );
	test( r.toidx(-1E9)==0 );
	test( r.toidx(1E9-1E-9)==9);
	r=range<flt> (-1E2,1E-2,10,true);
	test( r.size()==10 );
	test( r.toidx(-1E2)==0 );
	test( r.toidx(1E-2-1E-30)==9);

	test.Subsub("range weights");
	r=range<flt>(0.,10.,10);
	array3d<flt> w=r.weightfactors(Triple<flt>(0,0,0));
	test( w.sizet()==t_idx(2,2,2) );
	test( w(0,0,0)==1 );
	test( w(1,0,0)==0 );
	test( w(0,1,0)==0 );
	test( w(1,1,0)==0 );
	test( w(0,0,1)==0 );
	test( w(1,0,1)==0 );
	test( w(0,1,1)==0 );
	test( w(1,1,1)==0 );

	w=r.weightfactors(Triple<flt>(9.999,9.999,9.999));
	test( w(0,0,0)<0.01 );
	test( w(1,0,0)<0.01 );
	test( w(0,1,0)<0.01 );
	test( w(1,1,0)<0.01 );
	test( w(0,0,1)<0.01 );
	test( w(1,0,1)<0.01 );
	test( w(0,1,1)<0.01 );
	test( w(1,1,1)>0.9 );

	w=r.weightfactors(Triple<flt>(0,9.999,0));
	test( w(0,0,0)<0.01 );
	test( w(0,1,0)>0.9 );

	w=r.weightfactors(Triple<flt>(0,0,9.999));
	test( w(0,0,0)<0.01 );
	test( w(0,0,1)>0.9 );

	w=r.weightfactors(Triple<flt>(0,9.999,9.999));
	test( w(0,0,0)<0.01 );
	test( w(0,1,1)>0.9 );

	w=r.weightfactors(Triple<flt>(9.999,0,9.999));
	test( w(0,0,0)<0.01 );
	test( w(1,0,1)>0.9 );

	w=r.weightfactors(Triple<flt>(9.999,9.999,0));
	test( w(0,0,0)<0.01 );
	test( w(1,1,0)>0.9 );

	w=r.weightfactors(Triple<flt>(5.5,3.5,4.5));
	for(size_t k=0;k<2;++k)
	for(size_t j=0;j<2;++j)
	for(size_t i=0;i<2;++i) test( Proximyeps(w(i,j,k),1/8.,1E-9) );

	for(size_t x=0;x<10;++x){
		w=r.weightfactors(Triple<flt>(x+.5,x+.5,x+.5));
		test( Proximyeps(w(0,0,0),1./8.,1E-6) );
		test( Proximyeps(w(1,0,0),1./8.,1E-6) );
		test( Proximyeps(w(0,1,0),1./8.,1E-6) );
		test( Proximyeps(w(1,0,1),1./8.,1E-6) );
		test( Proximyeps(w(1,1,1),1./8.,1E-6) );

		w=r.weightfactors(Triple<flt>(x,x,x));
		test( w(0,0,0)>0.99 );
		test( w(1,1,1)<0.01 );
	}

	w=r.weightfactors(Triple<flt>(9.9,9.9,0));
	test( Proximyeps(w(0,0,0),0.01,1E-14) );
	test( Proximyeps(w(1,0,0),0.09,1E-14) );
	test( Proximyeps(w(0,1,0),0.09,1E-14) );
	test( Proximyeps(w(1,1,0),0.81,1E-15) );
	test(            w(0,0,1)==0 );
	test(            w(1,0,1)==0 );
	test(            w(0,1,1)==0 );
	test(            w(1,1,1)==0 );

	w=r.weightfactors(Triple<flt>(9.9,9.4,9.1));
	test( Proximyeps(w(0,0,0),0.054,1E-14) );
	test( Proximyeps(w(1,0,0),0.486,1E-15) );
	test( Proximyeps(w(0,1,0),0.036,1E-14) );
	test( Proximyeps(w(1,1,0),0.324,1E-14) );
	test( Proximyeps(w(0,0,1),0.006,1E-14) );
	test( Proximyeps(w(1,0,1),0.054,1E-14) );
	test( Proximyeps(w(0,1,1),0.004,1E-14) );
	test( Proximyeps(w(1,1,1),0.036,1E-14) );

	array3d<flt> t(10,10,10);
	t=1;
	flt y=r.weightfactors(Triple<flt>(9.9,9.4,9.1),t);
	test( y==1.0 );

	t(0,0,0)=12;
	y=r.weightfactors(Triple<flt>(9.9,9.4,9.1),t);
	test( Proximyeps( y, 1.396 ,1E-15) );

	y=r.weightfactors(Triple<flt>(9.9,9.7,9.6),t);
	flt y2=r.weightfactors(Triple<flt>(.1,0.3,.4),t);
	test( Proximyeps(y,y2,1E-15) );

	{
		test.Sub("class Triplerange");
		Triple<size_t> a(1,2,3),b(3,4,15);
		Triplerange<size_t> t(a,b);
		int n=0;
		Triplerange<size_t>::const_iterator itt=t.begin();
		for(itt=t.begin();itt!=t.end();++itt) ++n;
		test( n==(3-1)*(4-2)*(15-3) );

		itt=t.begin();
		test( *itt==Triple<size_t>(1,2,3));
		++itt;
		test( *itt==Triple<size_t>(2,2,3));
		++itt;
		test( *itt==Triple<size_t>(1,3,3));
		++itt;
		test( *itt==Triple<size_t>(2,3,3));

		a=Triple<size_t>(1,1,1);
		b=Triple<size_t>(2,2,2);
		t=Triplerange<size_t>(a,b,20);
		n=0;
		for(itt=t.begin();itt!=t.end();++itt) ++n;
		test( n==1 );

		t=Triplerange<size_t>(b,a,3);
		n=0;
		for(itt=t.begin();itt!=t.end();++itt) ++n;
		test( n==2*2*2 );

		t=Triplerange<size_t>(b,a,10);
		n=0;
		for(itt=t.begin();itt!=t.end();++itt){
			// cout << " n=" << n << "  i=" << *itt << "  end=" << t.end() << endl;
			++n;
		}
		test( n==9*9*9);

		a=Triple<size_t>(19,1,18);
		b=Triple<size_t>(0,2,0);
		t=Triplerange<size_t>(a,b,20);
		n=0;
		for(itt=t.begin();itt!=t.end();++itt) ++n;
		test( n==1*1*2 );
		itt=t.begin();
		++itt;
		test( *itt==Triple<size_t>(19,1,19) );
	}

	{
		test.Sub("class Configfile");
		{
			ofstream s((testdatapath_refs + "out").c_str());
			s << "% Configfile_begin\n";
			s << "% remark\n";
			s << " n = 12\n";
			s << "    % remark\n";
			s << "%%%%%% remark\n";
			s << "m=2 \n";
			s << "n13= \"hello world\"\n";
			s << "n14 =hello world\n";
			s << " o  42\n";
			s << "% Configfile_end\n";
			s << "n  112\n";
		}

		Configfile c(testdatapath_refs + "out","%");
		test( c.size()==5 );
		test( c.hasEntry("n") && c("n")[0]=="12" );
		test( c.hasEntry("m") && c("m")[0]=="2" );
		test( c.hasEntry("n13") && c("n13")[0]=="\"hello" );
		test( c.hasEntry("n14") && c("n14").size()==2 && c("n14")[0]=="hello" &&  c("n14")[1]=="world" );
		test( c.hasEntry("o") &&  c("o")[0]=="42");
		test( c[0]=="n = 12");

		test( c.Get<int>("n")==12 );
		test( c.Get<int>("m")==2 );
		test( c.Get<int>("o")==42 );

		c.Set("new","hello world 2");
		test( c("new").size()==1 && c("new")[0]=="hello world 2" );
		c.Save(testdatapath_refs + "out");

		Configfile d(testdatapath_refs + "out");
		c.Save(testdatapath_refs + "out2");
		test.DiffFile(testdatapath_refs + "out",testdatapath_refs + "out2" ,false);

		Configfile gview(testdatapath_inputs + "gview.ini");
		test( gview.Get<bool>("axis")==true );
		test( Proximy(gview.Get<flt>("subsnapstep"),0.05) );
	}

	{
		test.Sub("class bins"); /// XXX empty test for now
	}

	{
		test.Subsub("Search in periodic searchgrid");
		vector<particle_data_2> b(3);
		b[0].setpos(_point_len(_len(1),_len(1),_len(1)));
		b[1].setpos(_point_len(_len(2),_len(2),_len(2)));
		b[2].setpos(_point_len(_len(3),_len(2),_len(3)));

		const _len xlim(5-1E-12);
		test( xlim<_len(5) );
		const searchgrid s(b,_len(0),_len(5),10,true);
		const particle_data_2& f1=s.findclosest(_point_len(_len(1),_len(1),_len(0.5)));
		const particle_data_2& f1n=s.findclosest_nonoptimized(_point_len(_len(1),_len(1),_len(0.5)));

		test( &f1==&b[0]);
		test( &f1==&f1n);
		const particle_data_2& f2=s.findclosest(_point_len(_len(0),_len(0),_len(0.5)));
		const particle_data_2& f2n=s.findclosest_nonoptimized(_point_len(_len(1),_len(1),_len(0.5)));
		test( &f2==&b[0]);
		test( &f2==&f2n);
		const particle_data_2& f3=s.findclosest(_point_len(xlim));
		const particle_data_2& f3n=s.findclosest_nonoptimized(_point_len(_len(1),_len(1),_len(0.5)));
		test( &f3==&b[0]);
		test( &f3==&f3n);

		const searchgrid::t_map_data m0=s.findclosest_nonoptimized(_point_len(_len(xlim),_len(xlim),_len(4.99)),3);
		searchgrid::t_map_data::const_iterator itt=m0.begin();
		test( itt++->second.first==&b[0]);
		test( itt++->second.first==&b[2]);
		test( itt++->second.first==&b[1]);

		const searchgrid::t_map_data m1=s.findclosest(_point_len(xlim,xlim,_len(4.99)),3);
		itt=m1.begin();
		test( itt++->second.first==&b[0]);
		test( itt++->second.first==&b[2]);
		test( itt++->second.first==&b[1]);

		const searchgrid::t_map_data m2=s.findclosest(_point_len(_len(0.1),xlim,xlim),6);
		test( m2.size()==3 );
		itt=m2.begin();
		test( itt++->second.first==&b[0]);
		test( itt++->second.first==&b[1]);
		test( itt++->second.first==&b[2]);

		const _point_len p(_len(1),_len(1),_len(0.5));
		const searchgrid::t_map_data c2=s.findclosest_nonoptimized(p,s.datasize());
		{
			for(_len r=_len(0.1);r<_len(10);r*=1.1){
			const searchgrid::t_map_data c1=s.find_withinradius(p,r);
			test( c2.size()==s.datasize() );
				// cout << "  r=" << r << "  r2=" << r*r << " size=" << s.datasize() << " found=" << c1.size() << endl;
				if       (r<_len(.48)) test( c1.size()==0);
				else if  (r<_len(1.95))test( c1.size()==1);
				else if  (r<_len(3.1)) test( c1.size()==2);
				else                   test( c1.size()==3);

				searchgrid::t_map_data::const_iterator itt1=c1.begin();
				for(searchgrid::t_map_data::const_iterator itt2=c2.begin();itt2!=c2.end();++itt2){
					test( (itt1!=c1.end() && itt1->first==itt2->first) || itt2->first>r*r );
					if (itt1!=c1.end()) {
						++itt1;
						// cout << "  r2=" << itt1->first << endl;
					}
				}
			}
		}

		//cout << m0 << endl << m1 << endl << m2;
		//for(size_t i=0;i<b.size();++i) {
		//	const _point_len p(_len(5),_len(5),_len(5));
		//	const _point_len q=b[i].pos();
		//	const _len2 d2=DistPeriodic2<_len,_len2>(p,q,_len(5));
		//	cout << "\nb" << i << "=" << &b[i] << " p=" << b[i].pos() << "  d2=" << d2;
		//}
	}

	{
		test.Sub("searchgrid");
		const bool debugsearch=false; // extra verbose checking
		const size_t subjump=debugsearch ?  1 : 13;

		vector<particle_data_2> a(100);
		for(size_t n=0;n<a.size();++n){
			_point_len q;
			for(int i=0;i<3;++i) q[i]=_len(1.2*n+i);
			a[n].setpos(q);
		}

		test.Subsub("search one grid");
		searchgrid s2(a,5);

		const range<_len>& r=s2.getrange();
		_len step=r.approxstep();
		for(_len x=r.rmin();x<r.rmax();x+=2.5*step)
		for(_len y=r.rmin();y<r.rmax();y+=1.5*step)
		for(_len z=r.rmin();z<r.rmax();z+=3.5*step){
			const _point_len p(x,y,z);
			const particle_data_2& c0=SearchSubsearch_1(test,p,s2);
			SearchSubsearch_N(test,p,c0,s2,20,subjump);
		}

		test.Sub("BBox class");

		for(_len x=_len(0.1);x<_len(10000); x*=1.23){
			const _bbox_len bbox(x);
			test( bbox()==x );
			test( bbox.IsInsideBBox(_point_len(_len(0.))) );
			test( bbox.IsInsideBBox(_point_len(x-_len(1E-6))) );
			_point_len p=_point_len(x);
			test( bbox.IsInsideBBox(p)==false );
			bbox.WrapToInsideBox(p);
			test( bbox.IsInsideBBox(p) );
			test( p[0]==_len(0.) && p[1]==_len(0.) && p[2]==_len(0.));

			p=_point_len(x/2,x/3.,x);
			test( bbox.IsInsideBBox(p)==false );
			bbox.WrapToInsideBox(p);
			test( p[0]==_len(x/2) && p[1]==_len(x/3) && p[2]==_len(0.));
			test( bbox.IsInsideBBox(p) );

			test( bbox.isPointInBox(p,x/2,p));
			for(size_t i=0;i<3;++i){
				const _bbox_len bbox2(10*x);
				_point_len q=p;
				q[i] += _len(x);
				test( bbox2.isPointInBox(p,x,q)==false);
				q[i] = p[i] + x-_len(1E-9);
				test( bbox2.isPointInBox(p,x,q));
			}

			test( bbox.FractionBoxInBox(p,x/2,p,x/2)==1.0 );
			test( bbox.FractionBoxInBox(p,x/2,p,x/4)==1.0 );

			p=_point_len(_len(0.));
			for(flt y=2;y<20;++y){
				test( bbox.FractionBoxInBox(p,x/2,p,x/y)==1.0 );
				test( bbox.FractionBoxInBox(p,x/2,p,x/y/1.2)==1.0 );
				//test( bbox.FractionBoxInBox(p,x/2,p+x/y/2,x/y)==1.0 );
				test( bbox.FractionBoxInBox(p,x/2,p+x/2,x/y)==0.0 );
				test( bbox.FractionBoxInBox(p+x/8,x/4,p,x/4)==1/8. );
				test( bbox.FractionBoxInBox(p+x/4,x/4,p,x/4)==0 );
				test( Proximyeps(bbox.FractionBoxInBox(p+x/16,x/4,p,x/4),0.421875,1E-15) );
			}

		}

		const _bbox_len bbox3(_len(10));
		test( bbox3.FractionBoxInBox(_point_len(_len(0.)),_len(2),_point_len(_len(1.))                ,_len(2))==1/8. );
		test( bbox3.FractionBoxInBox(_point_len(_len(0.)),_len(2),_point_len(_len(1.),_len(0),_len(0)),_len(2))==1/2. );
		test( bbox3.FractionBoxInBox(_point_len(_len(0.)),_len(2),_point_len(_len(1.),_len(0),_len(0)),_len(1))==1. );

		const _point_len px(_len(1.));
		test( bbox3.FractionBoxInBox(px,_len(2),_point_len(_len(0.),_len(0),_len(0)),_len(2))==1/8. );
		test( bbox3.FractionBoxInBox(px,_len(2),_point_len(_len(1.),_len(0),_len(1)),_len(2))==1/2. );
		test( bbox3.FractionBoxInBox(px,_len(2),_point_len(_len(1.),_len(1),_len(0)),_len(2))==1/2. );
		test( bbox3.FractionBoxInBox(px,_len(2),_point_len(_len(1.),_len(1),_len(1)),_len(2))==1. );
		test( bbox3.FractionBoxInBox(px,_len(2),_point_len(_len(0.),_len(0),_len(1)),_len(2))==1/4. );
		test( bbox3.FractionBoxInBox(px,_len(2),_point_len(_len(0.),_len(1),_len(0)),_len(2))==1/4. );
		test( bbox3.FractionBoxInBox(px,_len(2),_point_len(_len(0.),_len(1),_len(1)),_len(2))==1/2. );

		if (!test.Verbose()) return;

		test.Subsub("search with different grid sizes");
		vector<particle_data_2> b(2);
		b[0].setpos(_point_len(_len(10),_len(20),_len(30)));
		b[1].setpos(_point_len(_len(0) ,_len(0) ,_len(0)));
		int loopmax=test.Verbose() ? 10 : 5;
		if (debugsearch) loopmax=250;
		for(int i=2;i<loopmax;++i){
			if (i>20) i+=23;
			if (i>100) i+=57;
			searchgrid s(b,i);
			const range<_len>& r=s.getrange();
			if(debugsearch) cout << "2: " << 1.*i/loopmax << " size=" << i << " r=" << r << "\n";
			const t_idx imin=r.toidx(b[1].pos());
			const t_idx imax=r.toidx(b[0].pos());
			test( imin==t_idx(0,0,0) );
			size_t z=max(max(imax[0],imax[1]),imax[2]);
			test( z==r.size()-1 );

			test( r.isinrange(b[0].pos()) );
			test( r.isinrange(b[1].pos()) );

			for(_len x=r.rmin();x<r.rmax();x+=_len(3.13))
			for(_len y=r.rmin();y<r.rmax();y+=_len(5.15))
			for(_len z=r.rmin();z<r.rmax();z+=_len(7.17)){
				const _point_len p(x,y,z);
				if(!r.isinrange(p)) warn_("## not in range, p=" + tostring(p) + " r=" + tostring(r));
				const particle_data_2& c0=SearchSubsearch_1(test,p,s);
				SearchSubsearch_N(test,p,c0,s,20,subjump);
			}
		}

 		test.Subsub("Search in several grids");
		searchgrid s1(a,2);
		searchgrid s3(a,8);
		searchgrid s4(a,10);
		searchgrid s5(a,100);
		if (!debugsearch) {
			step= step*4.+_len(3.51);
			if (!test.Verbose()) step*=4;
		}
		for(_len x=r.rmin();x<r.rmax();x+=0.5*step){
			if(debugsearch) cout << "\n3: " << x/r.rmax();
			for(_len y=r.rmin();y<r.rmax();y+=0.31*step){
				if(debugsearch) cout << "." << flush;
				for(_len z=r.rmin();z<r.rmax();z+=0.53*step){
					const _point_len p(x,y,z);
					const particle_data_2& c0=s1.findclosest_nonoptimized(p);
					const particle_data_2& c1=s1.findclosest(p);
					const particle_data_2& c2=s2.findclosest(p);
					const particle_data_2& c3=s3.findclosest(p);
					const particle_data_2& c4=s4.findclosest(p);
					const particle_data_2& c5=s5.findclosest(p);
					const flt d=(c0.pos().dist2<_len2>(p))/_len2(1);
					const flt eps=1E-17;
					test( Proximyeps(p.dist2<_len2>(c1.pos())/_len2(1),d,eps) );
					test( Proximyeps(p.dist2<_len2>(c2.pos())/_len2(1),d,eps) );
					test( Proximyeps(p.dist2<_len2>(c3.pos())/_len2(1),d,eps) );
					test( Proximyeps(p.dist2<_len2>(c4.pos())/_len2(1),d,eps) );
					test( Proximyeps(p.dist2<_len2>(c5.pos())/_len2(1),d,eps) );
					if ( !Proximyeps(p.dist2<_len2>(c1.pos())/_len2(1),d,eps)) {
						cout << "residual=" << p.dist2<_len2>(c1.pos())/_len2(d)-1.0 << " " << r << "\n";
						cout << "p0=" << c0.pos() << " p2=" << c1.pos() << " min dist="	<< p.dist2<_len2>(c0.pos()) << " " << p.dist2<_len2>(c1.pos()) << "\n";
					}

					if(debugsearch){
						SearchSubsearch_N(test,p,c0,s1,20,subjump);
						SearchSubsearch_N(test,p,c0,s2,20,subjump);
						SearchSubsearch_N(test,p,c0,s3,20,subjump);
						SearchSubsearch_N(test,p,c0,s4,20,subjump);
						SearchSubsearch_N(test,p,c0,s5,20,subjump);
					}
				}
			}
		}

		test.Subsub("Search in random search grids");
		const flt mstep=debugsearch ? 1.57 : 5.57;
		for(flt m=0.5;m<20.;m+=mstep){
			vector<particle_data_2> b(25);
			for(size_t n=0;n<b.size();++n) {
				_point_len q;
				for(int i=0;i<3;++i) q[i]=_len(Random()*m);
				b[n].setpos(q);
			}

			const searchgrid s(b,10);
			const range<_len>& r=s.getrange();
			_len step=r.approxstep() * (debugsearch ? 0.1 : 1.0);
			test( s.datasize()>20 );

			if(debugsearch)cout << "\n4: " << m/20. << " searchclass=" << s; cout.flush();

			for(_len x=r.rmin();x<r.rmax();x+=3.5*step){
				for(_len y=r.rmin();y<r.rmax();y+=3.31*step){
					if(debugsearch) cout << "." << flush;
					for(_len z=r.rmin();z<r.rmax();z+=3.53*step){
						const _point_len p(x,y,z);
						const particle_data_2& c0=SearchSubsearch_1(test,p,s);
						SearchSubsearch_N(test,p,c0,s,20,subjump);
					}
				}
			}
		}
	}

	{
		test.Subsub("Search in real data");
		const string density_file=testdatapath_inputs+"my2_snapshot_021";

		const size_t N_nearest=4;
		const size_t gridsize=20;

		const data_info inf=load_snapshot(density_file.c_str(),1,true,true);
		const data_info inf2=FilterParticles(inf,32);
		const vector<particle_data_2> v=ToVectorData(inf2);
		const searchgrid s(v,_len(0),_len(50000),gridsize,true);

		const size_t N=5;
		for(size_t k=0;k<N;++k){
			const _len z=s.getrange().totype(k);
			for(size_t j=0;j<N;++j){
				const _len y=s.getrange().totype(j);
				for(size_t i=0;i<N;++i){
					const _point_len p(s.getrange().totype(i),y,z);

					const searchgrid::t_map_data m1=s.findclosest(p,N_nearest);
					const searchgrid::t_map_data m2=s.findclosest_nonoptimized(p,N_nearest);

					test( m1.size()==m2.size());
					int i=0;
					for(searchgrid::t_map_data::const_iterator itt1=m1.begin(),itt2=m2.begin();itt1!=m1.end() && itt2!=m2.end();++itt1,++itt2,++i){
						test( Proximyeps( itt1->first/_len2(1),itt2->first/_len2(1),1E-13) );
						if (!Proximyeps( itt1->first/_len2(1),itt2->first/_len2(1),1E-13)) {
							cout << "** bad point: p=" << p << " i=" << i << " d2_1=" << itt1->first << " d2_2=" << itt2->first << endl;
						}
					}
					const particle_data_2& expected_p=s.findclosest_nonoptimized(p);
					SearchSubsearch_N(test,p,expected_p,s,N_nearest,N_nearest-1);
				}
			}
		}
	}

	test.Sub("class CompressedArray");
	TestCompressedArraySub(test,0,12.3,2.9,1E-3);
	TestCompressedArraySub(test,1,12.3,3.1,1E-3);
	TestCompressedArraySub(test,-23.7,2.3,4.3,1E-1);
	/// XXX does not work? TestCompressedArraySub(test,-3.7,0,1,1E-1);

	test.Sub("File functionality 2");

	while(filetime.elapsed()<1) {};

	const size_t f1=FileTime(f);
	ofstream ss2((f+"2").c_str());
	ss2.close();
	const size_t f2=FileTime(f+"2");
	test( f2>f1 );
	test( isFileNewer(f+"2",f)==true );
	test( isFileNewer(f,f+"2")==false );

	System("rm " + f,true);
	System("rm " + f + "2",true);
}

void TestICFormatSub(Testsuite& test, const string& out_filename,const string& in_filename,const unsigned int filesize,const vector<int>& extrafields,const vector<int>& repeats,const bool dodiff,const bool icmode=true)
{
	test.Sub("TestICFormatSub");

	assert(extrafields.size()==6);
	assert(repeats.size()==6);

	ofstream o(out_filename.c_str());

	const unsigned int M=FileSize(in_filename);
	test( M==filesize );
	data_info inf=load_snapshot(in_filename.c_str(),1,icmode);
	inf.P++;
	inf.Id++;

	Writetyp(o,sizeof(io_header_1));
	Writetyp(o,inf.header1);
	Writetyp(o,sizeof(io_header_1));

	int N=0;
	int extraTot=0;

	for(int k=0;k<6;++k)
	{
		N += inf.header1.npart[k];
		assert(static_cast<size_t>(k)<extrafields.size());
		const int subN=inf.header1.npart[k] * repeats[k] ;
		if(subN>0 && extrafields[k]>0 )
		{
			extraTot +=  (subN + 1*2) * extrafields[k] ;
		}
	}
	test( N==inf.NumPart );

	const unsigned int Nbytestot=4*(N*(3+3+1) + 2*(4) + extraTot) + 256;
	test( Nbytestot==M );

	vector<float> x;
	for(int i=0;i<N;++i) for(int j=0;j<3;++j) x.push_back(inf.P[i].Pos[j]);
	Writebin(o,x,true);			 // POS

	x.resize(0);
	for(int i=0;i<N;++i) for(int j=0;j<3;++j) x.push_back(inf.P[i].Vel[j]);
	Writebin(o,x,true);			 // VEL

	vector<unsigned int> y;
	assert( sizeof(unsigned int)==4);
	for(int i=0;i<N;++i) y.push_back(inf.Id[i]);
	Writebin(o,y,true);			 // ID
	y.resize(0);

	for(int j=0;j<6;++j)
	{
		for(int k=0;k<extrafields[j];++k)
		{
			x.resize(0);
			const int subN=inf.header1.npart[j] * repeats[j];
			if (subN>0)
			{
				for(int i=0;i<subN;++i) x.push_back(0);
				// Extra data
				Writebin(o,x,true);
			}
		}
	}

	o.close();
	const unsigned int M2=FileSize(out_filename);
	test( M2 == M );

	if (dodiff) test.DiffFile(in_filename,out_filename);
}

void TestICFormat(Testsuite& test)
{
	test.Header("TestICFormat");
	vector<int> e;				 // extra blocks
	vector<int> r;				 // repeat block
	e.resize(0);
	e.push_back(2); e.push_back(1); e.push_back(1); e.push_back(1); e.push_back(1); e.push_back(1);
	r.resize(0);
	r.push_back(1); r.push_back(1); r.push_back(1); r.push_back(1); r.push_back(1); r.push_back(1);
	TestICFormatSub(test,testdatapath_refs + "out",testdatapath_inputs+"gassphere_littleendian.dat",53296,e,r,false);

	e.resize(0);
	e.push_back(4); e.push_back(0); e.push_back(0); e.push_back(0); e.push_back(0); e.push_back(0);
	r.resize(0);
	r.push_back(1); r.push_back(1); r.push_back(1); r.push_back(1); r.push_back(1); r.push_back(1);
	TestICFormatSub(test,testdatapath_refs + "out",testdatapath_inputs + "gassphere_snapshot_005",65088,e,r,false,false);

	if (test.Verbose())
	{
		e.resize(0);
		e.push_back(2); e.push_back(1); e.push_back(0); e.push_back(0); e.push_back(0); e.push_back(0);
		r.resize(0);
		r.push_back(1); r.push_back(1); r.push_back(1); r.push_back(1); r.push_back(1); r.push_back(1);
		TestICFormatSub(test,testdatapath_refs + "out",testdatapath_inputs + "lcdm_gas_snapshot_005",2228536,e,r,false,false);
		/*		TestICFormatSub(test,testdatapath_refs + "out",testdatapath_inputs + "my_snapshot_005",2228536,e,r,false,false);
				TestICFormatSub(test,testdatapath_refs + "out",testdatapath_inputs + "my2_snapshot_005",3146040,e,r,false,false);

				e.resize(0);
				e.push_back(1);	e.push_back(0); e.push_back(1);	e.push_back(1);	e.push_back(1); e.push_back(1);
				r.resize(0);
				r.push_back(1);	r.push_back(1); r.push_back(1);	r.push_back(1);	r.push_back(1); r.push_back(1);
				TestICFormatSub(test,testdatapath_refs + "out",testdatapath_inputs + "lcdm_gas_littleendian.dat",1966376,e,r,true);

				e.resize(0);
				e.push_back(0);	e.push_back(0); e.push_back(0);	e.push_back(0);	e.push_back(1); e.push_back(1);
				r.resize(0);
				r.push_back(1);	r.push_back(1); r.push_back(1);	r.push_back(1);	r.push_back(1); r.push_back(1);
				TestICFormatSub(test,testdatapath_refs + "out",testdatapath_inputs + "galaxy_littleendian.dat",1680288,e,r,true);

				e.resize(0);
				e.push_back(0);	e.push_back(0); e.push_back(1);	e.push_back(1);	e.push_back(1); e.push_back(1);
				r.resize(0);
				r.push_back(1);	r.push_back(1); r.push_back(1);	r.push_back(1);	r.push_back(1); r.push_back(1);*/
		// this one is 8 bytes off = one tag of size 2*4
		//TestICFormatSub(test,testdatapath_refs + "out",path + cluster_littleendian.dat",8288212,e,r,false);
	}
}


void TestArray3D(Testsuite& test)
{
	test.Header("Array3D");
	array3d<int> a(1,2,3);
	a=0;

	a(0,0,2)=3;
	a(0,1,1)=2;
	a(0,1,0)=32;

	test( a(0,0,0)==0 );
	test( a(0,0,2)==3 );
	test( a(0,1,1)==2 );
	test( a(0,1,0)==32 );

	array3d<double> b(12,13,7);
	b=42;
	for(size_t i=0;i<b.sizex();++i)
		for(size_t j=0;j<b.sizey();++j)
			for(size_t k=0;k<b.sizez();++k)
			{
				test( b(i,j,k)==42 );
				b(i,j,k)=43;
			}
	for(size_t i=0;i<b.sizex();++i)
		for(size_t j=0;j<b.sizey();++j)
			for(size_t k=0;k<b.sizez();++k)
			{
				test( b(i,j,k)==43 );
			}

	b(3,2,5)=124;
	b(1,7,3)=1245;

	test.Sub("Read/Write");

	b.Writebin  (testdatapath_refs + "out");
	b.Writeascii(testdatapath_refs + "out2");
	array3d<double> c1=array3d<double>::Readbin  (testdatapath_refs + "out");
	array3d<double> c2=array3d<double>::Readascii(testdatapath_refs + "out2");

	test( c1.sizex()==b.sizex());
	test( c1.sizey()==b.sizey());
	test( c1.sizez()==b.sizez());
	test( c2.sizex()==b.sizex());
	test( c2.sizey()==b.sizey());
	test( c2.sizez()==b.sizez());

	test( c1(3,2,5)==124);
	test( c1(1,7,3)==1245);
	test( c1(0,0,0)==43);
	test( c1(1,1,1)==43);
	test( c1(11,12,6)==43);

	test( c2(3,2,5)==124);
	test( c2(1,7,3)==1245);
	test( c2(0,0,0)==43);
	test( c2(1,1,1)==43);
	test( c2(11,12,6)==43);

	a.resize(2,2,2);
	a(1,0,1)=-42;
	a.Writeascii(testdatapath_refs + "out","  ");
	b=b.Readascii (testdatapath_refs + "out");
	test( a.size()==b.size() && a(1,0,1)==b(1,0,1) );

	test.Sub("Indexing");
	a.resize(3,3,3);
	a=42;
	a(0,0,0)=0;
	a(1,0,0)=1;
	a(0,1,0)=2;
	a(0,0,1)=3;
	int sum=0;

	test( a.size()==3*3*3 );
	for(size_t i=0;i<a.size();++i)
	{
		if(i==0) test( a[i]==0 );
		else if(i==1) test( a[i]==1 );
		else if(i==3) test( a[i]==2 );
		else if(i==9) test( a[i]==3 );
		else  test( a[i]==42 );
		sum += a[i];
	}
	test( a.sum()==sum );

	array3d<double> x(3,5,2);
	x=42;
	t_idx idx(2,4,1);
	test( x.IndexOk(idx)==true );
	test( x.IndexOk(t_idx(2,1,1))==true );
	test( x.IndexOk(t_idx(3,1,1))==false );
	test( x.IndexOk(t_idx(1,5,1))==false );
	test( x.IndexOk(t_idx(1,1,2))==false );

// 	pointd p=x.ToPoint(t_idx(0,0,0),10);
// 	test( p[0]==0 );
// 	test( p[1]==0 );
// 	test( p[2]==0 );
// 	p=x.ToPoint(t_idx(0,4,0),10);
// 	test( p[0]==0 );
// 	test( p[1]==10/5*4 );
// 	test( p[2]==0 );
//
// 	idx=x.ToIndex(p,10);
// 	test( idx==t_idx(0,4,0));
//
// 	for(float bbox=0.1;bbox<1E10; bbox*=5.1){
// 		for(size_t k=0;k<x.sizez()-1;++k){
// 			for(size_t j=0;j<x.sizey()-1;++j) {
// 				for(size_t i=0;i<x.sizex()-1;++i)
// 				{
// 					p=x.ToPoint(t_idx(i,j,k),bbox);
// 					idx=x.ToIndex(p+0.001,bbox);
// 					test( idx==array3d<double>::t_idx(i,j,k) );
// 				}
// 			}
// 		}
// 	}

	x(1,2,1)= -42;
	x(1,3,1)=142;
	idx=x.FindMin();
	test( idx==t_idx(1,2,1) );
	test( -42==x[idx] );
	idx=x.FindMax();
	test( idx==t_idx(1,3,1) );
	test( 142==x[idx] );

	test.Subsub("Indexing - primary indexing");
	x=-42;
	test( x(0,0,0)==-42 );
	x[1]=2;
	test( x(1,0,0)==2 );
	test( x(0,1,0)==-42 );
	test( x(0,0,1)==-42 );

	{
		test.Sub("iterators");

		array3d<flt> f(10,10,10);
		flt x=0;
		for(array3d<flt>::iterator itt=f.begin();itt!=f.end();++itt) {
			*itt = x;
			test( *itt==x );
			++x;
		}
		test( f(0,0,0)==0 );
		test( f(1,0,0)==1 );
		test( f(2,0,0)==2 );
		test( f(0,1,0)==10 );
		test( f(0,0,1)==100 );

		x=0;
		for(array3d<flt>::const_iterator itt=f.begin();itt!=f.end();++itt) {
			test( *itt==x );
			++x;
		}

		array3d<array3d<flt> > tst(1,1,3);
		tst(0,0,0)=array3d<flt>(10,11,12);
		array3d<array3d<flt> >::const_iterator itt=tst.begin();
		test( (*itt).sizet()==t_idx(10,11,12));
		++itt;
		test( (*itt).sizet()==t_idx(0,0,0));
		++itt;
		test( (*itt).sizet()==t_idx(0,0,0));
		++itt;
		test( itt==tst.end() );
	}

	// 	// Produce erratic output
	// 	test.Subsub("indexing speed - cache performance");
	// 	const size_t N=50;
	// 	array3d<size_t> n1(N,N,N);
	//
	// 	timer t1;
	// 	for(size_t l=0;l<N;++l)
	// 	for(size_t i=0;i<N;++i)
	// 	for(size_t j=0;j<N;++j)
	// 	for(size_t k=0;k<N;++k) n1(i,j,k)=k;
	// 	const double t1e=t1.elapsed();
	//
	// 	timer t2;
	// 	for(size_t l=0;l<N;++l)
	// 	for(size_t k=0;k<N;++k)
	// 	for(size_t j=0;j<N;++j)
	// 	for(size_t i=0;i<N;++i) n1(i,j,k)=i;
	// 	const double t2e=t2.elapsed();
	//
	// 	test( t2e<=t1e );

}

void TestUnits(Testsuite& test)
{
	test.Header("Units");

	test.Sub("Unit class");
	const Units<flt> u;

	test( u.name(0)== "kpc/h" );
	test( u.si_equivalent(0)==Constants::_kpc );
	test( Proximyeps(u.G(), _gravitational_const(43021.286731797),1E-13) );

 	string s=tostring(u);
	{
		ofstream os((testdatapath_refs + "out").c_str() );
		os << s;
	}
	test.DiffFile(testdatapath_refs + "test_units_01.ref",testdatapath_refs + "out" ,test.Bool(0));
	{
		Units<flt> v;
		ifstream is((testdatapath_refs + "out").c_str() );
		is >> v;
		is.close();

		ofstream os((testdatapath_refs + "out").c_str() );
		os << tostring(v);;
	}
	test.DiffFile(testdatapath_refs + "test_units_01.ref",testdatapath_refs + "out" ,test.Bool(0));

	vector<flt> tt;
	vector<string> n;
	tt.push_back(1);
	tt.push_back(1);
	tt.push_back(1);
	n.push_back("a");
	n.push_back("b");
	n.push_back("c");

	const Units<flt> w(tt,n,false);

	test( w.name(0)== "a" );
	test( w.name(1)== "b" );
	test( w.name(2)== "c" );

	test( w.si_equivalent(0)==1 );
	test( w.si_equivalent(1)==1 );
	test( w.si_equivalent(2)==1 );
	test( w.G() == _gravitational_const(6.6742E-11) );

	s=tostring(w);
	{
		ofstream os((testdatapath_refs + "out").c_str() );
		os << s;
	}
	test.DiffFile(testdatapath_refs + "test_units_02.ref",testdatapath_refs + "out" ,test.Bool(0));
	{
		Units<flt> v;
		ifstream is((testdatapath_refs + "out").c_str() );
		is >> v;
		is.close();

		ofstream os((testdatapath_refs + "out").c_str() );
		os << tostring(v);;
	}
	test.DiffFile(testdatapath_refs + "test_units_02.ref",testdatapath_refs + "out" ,test.Bool(0));

	if (test.Verbose()){
	    const string cmd="massinsphere " + testdatapath_inputs + "my2_snapshot_021 "+ "-8 -r 2 -f 32 -testsuite >" + testdatapath_refs + "out 2>/dev/null ";
		test.System(cmd + "-units " + testdatapath_refs +  "test_units_01.ref");
		test.DiffFile(testdatapath_refs + "test_units_03.ref",testdatapath_refs + "out",test.Bool(0));

		test.System(cmd +"-units " + testdatapath_refs +  "test_units_02.ref");
		test.DiffFile(testdatapath_refs + "test_units_04.ref",testdatapath_refs + "out",test.Bool(0));
	}
}

double       phival          (double x,double y,double z) {return x*x*x*x + y*y - 3.0*z;}
const pointd expected_dphi   (double x,double y,double z) {return pointd(4.0*x*x*x,2.0*y,-3);}
double       expected_ddphi  (double x,double y,double z) {return 12.0*x*x + 2 + 0;}
double       expected_dxdxphi(double x,double y,double z) {return 12.0*x*x;}

/*
vector<flt> div_kspace(const vector<flt>& y,const flt bbox,const bool twice=true)
{
	vector<cmplx> Fy=Ft(totype<cmplx>(y));
	const flt delta_k=2*_pi/bbox;
	for(size_t i=0;i<Fy.size();++i) {
		const flt k=delta_k*idx2k(i,Fy.size());
		cmplx c=Fy[i];

		if (twice) c *= (-k*k);
		else       c = cmplx(-k*c.imag(),k*c.real());
	}
	return totype<flt>(iFt(Fy));;
}
*/

void TestNablaSub(Testsuite& test,const double delta,const double epsilon)
{
	test.Subsub("nabla");

	// test nabla^2
	// phi(x,y,z) = x^4 + y*y - 3z
	// nabla phi = [4*x^3 ; 2*y ; -3]
	// nabla nabla phi=12*x^2 + 2 + 0

	array3d<double> phi1(10,10,10),phi2(10,10,10),phi3(10,10,10),phi4(10,10,10),phi5(10,10,10),phi6(10,10,10),phi7(10,10,10);
	for(size_t k=0;k<phi1.sizez();++k)
		for(size_t j=0;j<phi1.sizey();++j)
			for(size_t i=0;i<phi1.sizex();++i)
			{
				phi1(i,j,k)=phival(i,j,k);
				phi2(i,j,k)=phival(i-delta,j,k);
				phi3(i,j,k)=phival(i+delta,j,k);
				phi4(i,j,k)=phival(i,j-delta,k);
				phi5(i,j,k)=phival(i,j+delta,k);
				phi6(i,j,k)=phival(i,j,k-delta);
				phi7(i,j,k)=phival(i,j,k+delta);
			}


	for(size_t k=0;k<phi1.sizez();++k)
	{
		for(size_t j=0;j<phi1.sizey();++j)
		{
			for(size_t i=0;i<phi1.sizex();++i)
			{
				const double dpx0=(phi3(i,j,k)-phi2(i,j,k));
				const double dpy0=(phi5(i,j,k)-phi4(i,j,k));
				const double dpz0=(phi7(i,j,k)-phi6(i,j,k));
				const pointd dphi=pointd(dpx0,dpy0,dpz0)/(2*delta);

				const double dpx1=(phi1(i,j,k)-phi2(i,j,k));
				const double dpy1=(phi1(i,j,k)-phi4(i,j,k));
				const double dpz1=(phi1(i,j,k)-phi6(i,j,k));
				const pointd p1(dpx1,dpy1,dpz1);

				const double dpx2=(phi3(i,j,k)-phi1(i,j,k));
				const double dpy2=(phi5(i,j,k)-phi1(i,j,k));
				const double dpz2=(phi7(i,j,k)-phi1(i,j,k));
				const pointd p2(dpx2,dpy2,dpz2);

				const pointd p3=(p2-p1)/(delta*delta);
				const double ddphi=p3[0]+p3[1]+p3[2];

				const pointd e_dphi = expected_dphi(i,j,k);
				const double e_ddphi= expected_ddphi(i,j,k);

				const bool t=Proximyeps_noprint(ddphi,e_ddphi,epsilon);
				test( t );
				if (!t)
				{
					cout << "delta=" << delta << " phi=" << fwidth(phi1(i,j,k)) << " ";
					cout << " ddphi=" << fwidth(ddphi) << " ";
					cout << " expected_ddphi=" << e_ddphi;
					cout << " dphi=" << dphi;
					cout << " expected_dphi=" << e_dphi;
					cout << "\n";
				}
			}
		}
	}
}

void TestSubsubHermitian(Testsuite& test,const array3d<cmplx>& X)
{
	test.Subsub("Test Hermitian and vec k => vec -k size=" + tostring(X.sizex()));
	test( X.isSquare() );
	const flt eps=1E-6;
	const size_t N=X.sizex();
	const size_t Nhalf=N/2;
	test( 2*Nhalf==N ); // must be even

	int r=0;
	// test FFT data and aliases above N/2
	for(size_t k=0;k<N;++k){
		for(size_t j=0;j<N;++j){
			for(size_t i=0;i<N;++i){
				const t_idx idx=t_idx(i,j,k);
				const cmplx c1=X[idx];
				const t_idx inv_idx=idx2inv_idx(idx,N);
				const cmplx c2=Conj(X[inv_idx]);

				const bool e=Proximyeps_noprint(c1,c2,eps);
				test( e );
				if (idx==inv_idx){
					test( fabs(c1.imag())<eps );
					++r;
				}
				if (!e) cout << "ne!" << idx  << " => " << inv_idx << " e=" << Proximyeps(c1,c2,eps) << "  c1=" << c1 << " c2=" << c2 << endl;
			}
		}
	}
	test( r==8 ); // eight 'corners' in FFT aliased data
	test( isHermitian(X,eps) );

	test.Subsub("HermitianConstraint size=" + tostring(X.sizex()) );
	if (X.sizex()>4){
		array3d<cmplx> Y=X;
		Y(1,1,2)=cmplx(-42,12);
		Y(3,1,2)=cmplx(122,-1.23);
 		test( !isHermitian(Y) );
 		HermitianConstraint(Y);
 		test( isHermitian(Y) );

		for(size_t i=0;i<Y.size();++i) Y[i]=cmplx(Sin(0.01*i),Cos(0.13*i*i));
		test( !isHermitian(Y) );
		HermitianConstraint(Y);
		test( isHermitian(Y) );

		const array3d<cmplx> y=iFFt3(Y);
		//test(!hasImaginaryparts(y,1E-3)); // XXX why does this not work?
		//for(size_t i=0;i<y.size();++i) test( Abs(y[i].imag())<1E-6 );
	}
}

void TestFourier(Testsuite& test)
{
	test.Header("Fourier");
	{
		cmplx x=Exp(cmplx(1,0));
		test( Proximyeps(x.real(),2.718281828459,1E-13) );

		x=Exp(cmplx(0,1));
		test( Proximyeps(x.real(),.540302305,1E-8) );
		test( Proximyeps(x.imag(),.841470984,1E-9) );
	}

	test.Sub("Basic Fourier funs");

	test( PowersOf2<size_t>(0)==0 );
	test( PowersOf2<size_t>(1)==1 );
	test( PowersOf2<size_t>(2)==1 );
	test( PowersOf2<size_t>(3)==2 );
	test( PowersOf2<size_t>(4)==2 );
	test( PowersOf2<size_t>(5)==3 );
	test( PowersOf2<size_t>(6)==3 );
	test( PowersOf2<size_t>(7)==3 );
	test( PowersOf2<size_t>(8)==3 );
	test( PowersOf2<size_t>(9)==4 );
	test( PowersOf2<size_t>(10)==4 );
	test( PowersOf2<size_t>(16)==4 );
	test( PowersOf2<size_t>(17)==5 );
	test( PowersOf2<size_t>(31)==5 );
	test( PowersOf2<size_t>(32)==5 );
	test( PowersOf2<size_t>(33)==6 );
	test( PowersOf2<size_t>(63)==6 );
	test( PowersOf2<size_t>(64)==6 );
	test( PowersOf2<size_t>(65)==7 );
	test( PowersOf2<size_t>(127)==7 );
	test( PowersOf2<size_t>(128)==7 );
	test( PowersOf2<size_t>(129)==8 );
	test( PowersOf2<size_t>(254)==8 );
	test( PowersOf2<size_t>(255)==8 );
	test( PowersOf2<size_t>(256)==8 );
	test( PowersOf2<size_t>(256+1)==9 );

	vector<cmplx> x(154);
	for(size_t i=0;i<x.size();++i) x[i]=cmplx(i,3*i+5);
	vector<cmplx> y=x;
	for(size_t i=0;i<y.size();++i) y[i] = y[i].real();
	const vector<cmplx> z=totype<cmplx>(totype<flt>(y));

	test( z.size()==y.size() );
	test( y.size()==x.size() );
	for(size_t i=0;i<x.size();++i) {
		const cmplx c=cmplx(i,3*i+5);
		test( x[i]==c );
		test( z[i]==c.real() );
	}

	test.Sub("1D Ft");
	for(size_t nn=2;nn<=127;nn*=2) /// XXX, does not handle non power of two yet
    {
		vector<cmplx> r,F,r2;
		for(size_t i=0;i<nn;++i) r.push_back(cmplx(i,2*i));

		const size_t N=r.size();
		F=Ft(r);
		r2=iFt(F);
		{
			const vector<cmplx>  FF = FFt(r);
			const vector<cmplx> rr2=iFFt(FF);

			flt e0=1E-4,e1=1E-9;
			if (nn>=16) e0 *= 2;
			if (nn>=32) e0 *= 3;
			for(size_t i=0;i<N;++i)	{
				test( Proximyeps_noprint(r2[i],r[i]  ,e0 ) || Abs(r2[i])<e1);
				test( Proximyeps_noprint( F[i],FF[i] ,e0 ) || Abs( F[i])<e1);
				test( Proximyeps_noprint( r[i],rr2[i],e0 ) || Abs( r[i])<e1);
				test( Proximyeps_noprint(r2[i],rr2[i],e0 ) || Abs(r2[i])<e1 || Abs(rr2[i])<e1 );
			}
		}
	}

	vector<cmplx> r,F;
	for(size_t i=0;i<10;++i) r.push_back(cmplx(i+1,0));
	F=Ft(r);

	test( Proximyeps(F[0].real(),  55,1E-5) );
	test( Proximyeps(F[1].real(),  -5,1E-13) );
	test( Proximyeps(F[9].real(),  -5,1E-12) );
	const double eps=5E-6;
	test( Proximyeps(F[1].imag(), 15.3884,eps) );
	test( Proximyeps(F[2].imag(),  6.8819,eps) );
	test( Proximyeps(F[3].imag(),  3.6327,eps) );
	test( Proximyeps(F[4].imag(),  1.6246,eps) );
	test( Proximyeps(F[5].imag(),       0,1E-12) );
	test( Proximyeps(F[9].imag(),-15.3884,eps) );
	test( Proximyeps(F[8].imag(),- 6.8819,eps));
	test( Proximyeps(F[7].imag(),- 3.6327,1E-5));
	test( Proximyeps(F[6].real(),      -5,1E-13));
	test( Proximyeps(F[6].imag(), -1.6246,eps) );
	test( Proximyeps(F[1].real(),      -5,1E-13));

	{
		vector<cmplx> x;
		for(double i=1;i<101;++i)
		{
			x.push_back( Sin(i)+ 2*Cos(3*i) - 0.5*Sin(i*i) );
		}
		const vector<cmplx> F=Ft(x);
		vector<cmplx> x2=iFt(F);

		test( Proximyeps(x[0].real(), -1.5592,1E-4) );
		test( Proximyeps(x[1].real(),  3.2080,1E-4) );

		test( Proximyeps_noprint(x2,x,3E-5) );

		test( Proximyeps(F[0],         cmplx(-2.0121,0      ),1E-4) );
		test( Proximyeps_noprint(F[1], cmplx( 0.6871,-4.3535),1E-4) );
		test( Proximyeps(F[2],         cmplx( 0.4429,-0.9941),1E-4) );
		test( F.size()==100 );
		test( Proximyeps_noprint(F[99],cmplx( 0.6871,4.3535 ),1E-4) );
	}

	test.Sub("Sunspot");

	{
		const array3d<double> a=array3d<double>::Readmatlab(testdatapath_inputs+"sunspot_dat.txt");
		test( a.sizex()==2 );
		test( a.sizey()==288 );
		test( a.sizez()==1 );

		vector<double> wolfer(288);
		double sum=0;
		for(size_t i=0;i<a.sizey();++i) {wolfer[i]=a(1,i,0); sum+=a(1,i,0);}
		test( wolfer[0]==5 );
		test( wolfer[9]==8 );
		test( wolfer[29]==73 );
		test( wolfer[141]==36.7 );

		const vector<cmplx> Y = Ft(totype<cmplx>(wolfer));
		test( Proximyeps(Y[0].real(),13949.,1E-15) );
		test( Y[0].imag()<1E-6 );
		test( Y[0].real()==sum );

		vector<cmplx> Y2(Y.size()-1);
		for(size_t i=0;i<Y2.size();++i) Y2[i]=Y[i+1];

		const int n=Y2.size();
		test( n==287 );
		const int m=n/2;
		test( m==143 );
		vector<double> power(m);

		for(size_t i=0;i<power.size();++i) power[i]=Pow2(Abs(Y2[i])); //power = abs(Y(1:n/2)).^2;
		const double nyquist = 1./2;
		vector<double> freq(m);
		// strange conv. from int/double in n/2=m=?=287/2
		for(size_t i=0;i<freq.size();++i) freq[i]=static_cast<double>(i+1)/(287./2)*nyquist; // freq = (1:n/2)/(n/2)*nyquist;

		const array3d<double> mat_Yr   =array3d<double>::Readmatlab(testdatapath_inputs+"sunspot_y_real.txt");
		const array3d<double> mat_Yi   =array3d<double>::Readmatlab(testdatapath_inputs+"sunspot_y_imag.txt");
		const array3d<double> mat_power=array3d<double>::Readmatlab(testdatapath_inputs+"sunspot_power.txt");
		const array3d<double> mat_freq =array3d<double>::Readmatlab(testdatapath_inputs+"sunspot_freq.txt");

		test( Y.size()==mat_Yr.sizey() );
		test( Y.size()==mat_Yi.sizey() );
		test( power.size()==mat_power.sizey() );
		test( freq.size() ==mat_freq.sizex() );
		test( power.size() == freq.size() && freq.size()== size_t(m) );

		const double eps=1E-2;
		for(size_t i=0;i<Y.size();++i){
			test( Proximyeps_noprint(Y[i].real(),mat_Yr[i],eps ));
			test( Proximyeps_noprint(Y[i].imag(),mat_Yi[i],eps ));
		}
		for(int i=0;i<m;++i){
			//cout << "\n" << i << "  p=" << power[i] << "  mp=" << mat_power[i];
			//cout << "\n" << i << "  f=" << freq[i] << "  mf=" << mat_freq[i];
			test( Proximyeps_noprint( power[i],mat_power[i],1E-3 ));
			test( Proximyeps_noprint( freq[i] ,mat_freq [i],1E-7 ));
		}
	}

/*
	test.Subsub("k space diff");
	vector<flt> y1,y2,y3,y4,expected_ddy1,expected_dy2,expected_ddy2,expected_dy3,expected_ddy3,expected_dy4,expected_ddy4;

	for(flt x=0;x<2*_pi*1;x+=0.1){
		flt f1=phival(x,1,1);
		flt f2=expected_ddphi(x,1,1);
 		y1.push_back(f1);
		expected_ddy1.push_back(f2);

		const flt cosx=Cos(x);
		const flt cosx2=cosx*cosx;
		const flt cosx3=cosx*cosx*cosx;

		y2.push_back( Sin(x)+Sin(2*x)+Sin(3*x) );
 		expected_dy2 .push_back( Cos(x)+2*Cos(2*x)+3*Cos(3*x) );
 		expected_ddy2.push_back(-Sin(x)-4*Sin(2*x)-9*Sin(3*x) );

		y3.push_back( x*x+Tan(x)-3*x+Cos(x/2) );
		expected_dy3 .push_back(-(cosx2*Sin(x/2)-2*((2*x-3)*cosx2))/(2*cosx2) );
		expected_ddy3.push_back(-(cosx3*Cos(x/2)-8*(cosx3+Sin(x)))/(4*cosx3) );

		y4.push_back( x*x*x );
		expected_dy4 .push_back(3*x*x );
		expected_ddy4.push_back(6*x );
	}

	vector<flt> ddy1=div_kspace(y1,1);
	vector<flt>  dy2=div_kspace(y2,1,false);
	vector<flt> ddy2=div_kspace(y2,1);
	vector<flt>  dy3=div_kspace(y3,1,false);
	vector<flt> ddy3=div_kspace(y3,1);
	vector<flt>  dy4=div_kspace(y4,1,false);
	vector<flt> ddy4=div_kspace(y4,1);
  	// no test of output

	Writeascii("ddy1.m",ddy1);
	Writeascii("dy2.m", dy2);
	Writeascii("ddy2.m",ddy2);
	Writeascii("dy3.m", dy3);
	Writeascii("ddy3.m",ddy3);
	Writeascii("dy4.m", dy4);
	Writeascii("ddy4.m",ddy4);
	Writeascii("expected_ddy1.m",expected_ddy1);
	Writeascii("expected_dy2.m" ,expected_dy2);
	Writeascii("expected_ddy2.m",expected_ddy2);
	Writeascii("expected_dy3.m" ,expected_dy3);
	Writeascii("expected_ddy3.m",expected_ddy3);
	Writeascii("expected_dy4.m" ,expected_dy4);
	Writeascii("expected_ddy4.m",expected_ddy4);
*/

	test.Sub("2D Ft");

	vector<vector<cmplx> > rr,FF,rr2;
	for(size_t j=0;j<4;++j)	{
		r.resize(0);
		for(size_t i=0;i<3;++i) {
			const cmplx c=cmplx(i+1,2*(j));
			r.push_back(c);
		}
		rr.push_back(r);
	}
	FF=Ft2(rr);

	// x=[1 2 3; 1+2i 2+2i 3+2i; 1+4i 2+4i 3+4i; 1+6i 2+6i 3+6i]
	//
	// fft(x,[],2)
	//
	// ans =
	//
	//    6.0000            -1.5000 + 0.8660i  -1.5000 - 0.8660i
	//    6.0000 + 6.0000i  -1.5000 + 0.8660i  -1.5000 - 0.8660i
	//    6.0000 +12.0000i  -1.5000 + 0.8660i  -1.5000 - 0.8660i
	//    6.0000 +18.0000i  -1.5000 + 0.8660i  -1.5000 - 0.8660i
	//
	// 	fft2([1 2 3; 1+2i 2+2i 3+2i ;  1+4i 2+4i 3+4i ;  1+6i 2+6i 3+6i])
	//
	// ans =
	//
	//   24.0000 +36.0000i  -6.0000 + 3.4641i  -6.0000 - 3.4641i
	//  -12.0000 -12.0000i        0                  0
	//         0 -12.0000i        0                  0
	//   12.0000 -12.0000i        0                  0
	//
	// fft(fft([1 2 3; 1+2i 2+2i 3+2i ;  1+4i 2+4i 3+4i ;  1+6i 2+6i 3+6i],[],2))
	//
	// ans =
	//
	//   24.0000 +36.0000i  -6.0000 + 3.4641i  -6.0000 - 3.4641i
	//  -12.0000 -12.0000i        0                  0
	//         0 -12.0000i        0                  0
	//   12.0000 -12.0000i        0                  0
	//
	// 	>> z(1,1,1)
	//
	// ans =
	//
	//   24.0000 +36.0000i
	//
	// >> z(1,1)
	//
	// ans =
	//
	//   24.0000 +36.0000i
	//
	// >> z(1,1)
	//
	// ans =
	//
	//   24.0000 +36.0000i
	//
	// >> z(2,1)
	//
	// ans =
	//
	//  -12.0000 -12.0000i
	//
	// >> z(1,2)
	//
	// ans =
	//
	//   -6.0000 + 3.4641i
	//
	vector<vector<cmplx> > t;
	vector<cmplx> v;
	v.push_back(cmplx(24.0000 , 36.0000));
	v.push_back(cmplx(-6.0000 ,  3.4641));
	v.push_back(cmplx(-6.0000 , -3.4641));
	t.push_back(v);
	v.resize(0);
	v.push_back(cmplx(-12.0000, -12.0000));
	v.push_back(cmplx(0,0));
	v.push_back(cmplx(0,0));
	t.push_back(v);
	v.resize(0);
	v.push_back(cmplx(0, -12.0000));
	v.push_back(cmplx(0,0));
	v.push_back(cmplx(0,0));
	t.push_back(v);
	v.resize(0);
	v.push_back(cmplx(12.0000, -12.0000));
	v.push_back(cmplx(0,0));
	v.push_back(cmplx(0,0));
	t.push_back(v);

	// note opp indexeing as matlab
	test( FF[0][0]==cmplx(24.0000 , 36.0000) );
	test( Proximyeps_noprint(FF[0][1],cmplx( -6.0000 ,   3.4641),1E-6) );
	test( Proximyeps_noprint(FF[1][0],cmplx(-12.0000 , -12.0000),1E-6) );

	test( FF.size()==t.size() );
	for(size_t i=0;i<FF.size();++i)	{
		test( FF[i].size()==t[i].size() );
		for(size_t j=0;j<FF[i].size();++j) {
			test( Proximyeps_noprint( t[i][j],FF[i][j],1E-5) );
		}
	}
	rr2=iFt2(FF);

	test( rr2.size()==rr.size() );
	for(size_t i=0;i<rr2.size();++i) {
		test( rr2[i].size()==rr[i].size() );
		for(size_t j=0;j<rr2[i].size();++j)	{
			test( Proximyeps_noprint( rr2[i][j],rr[i][j],1E-6) );
		}
	}

	test.Sub("2D Ft 2");

	if (test.Verbose())
	{
		vector<vector<cmplx> > x;
		for(double j=1;j<101;++j){
			vector<cmplx> xx;
			for(double i=1;i<101;++i){
				const cmplx c(Sin(i)+ 2*Cos(3*i) - 0.5*Sin(i*i) + j + Cos(3*j),0 );
				xx.push_back( c );
			}
			x.push_back(xx);
		}
		const vector<vector<cmplx> > F=Ft2(x);
		vector<vector<cmplx> > x2=iFt2(F);

		test( Proximyeps(x[0][0].real(), -1.54924 ,1E-5) );
		test( Proximyeps(x[1][0].real(),  1.40092 ,1E-6) );

		test( x.size()==x2.size() );
		for(size_t i=0;i<x.size();++i)
		{
			const vector<cmplx>& xx=x[i];
			const vector<cmplx>& x2x=x2[i];
			test( xx.size()==x2x.size() );
			for(size_t j=0;j<xx.size();++j)
			{
				test( Abs(xx[j])==0 || (i==0 && j==57) || Proximyeps_noprint( xx[j], x2x[j] , 4E-2 ) );
				test( Abs(x2x[j].imag())<1E-2 );
			}
		}

		test( Proximyeps(F[0][0], 1.0e+005*cmplx(5.0474,0),1E-5) );
		test( Proximyeps_noprint(F[1][0], 1.0e+005*cmplx(-0.0505 , 1.5910),1E-3) );
		test( Proximyeps_noprint(F[2][0], 1.0e+005*cmplx(-0.0505 , 0.7947),1E-3) );
		test( Proximyeps_noprint(F[3][0], 1.0e+005*cmplx(-0.0505 , 0.5289),1E-3) );
		test( F.size()==100 );
		/// XXX test( Proximyeps(F[99][99].real(),  1.0281e-013,3E-2 ) );
		/// XXX test( Proximyeps(F[99][99].imag(), -1.3966e-012,5E-1 ) );
	}

	test.Sub("3D Ft");

	{
		/*
		x=zeros(2,2,2); x(:,:,1)=[[1 0;12 -13]];	x(:,:,2)= [17 9;7 3]

		x(:,:,1) =

			1     0
			12   -13

		x(:,:,2) =

			17     9
			7     3

		>> fft(x,[],3)

		ans(:,:,1) =

			18     9
			19   -10

		ans(:,:,2) =

		-16    -9
			5   -16

		>> fft(fft(x,[],3),[],2)

		ans(:,:,1) =

			27     9
			9    29

		ans(:,:,2) =

		-25    -7
		-11    21

		>> fft(fft(fft(x,[],3),[],2))

		ans(:,:,1) =

			36    38
			18   -20

		ans(:,:,2) =

		-36    14
		-14   -28
		*/

		array3d<cmplx> x(2,2,2);
		x(0,0,0)=   1;
		x(1,0,0)=  12;
		x(0,1,0)=   0;
		x(1,1,0)= -13;
		x(0,0,1)=  17;
		x(1,0,1)=   7;
		x(0,1,1)=   9;
		x(1,1,1)=   3;

		const array3d<cmplx> Fx =Ft3 (x);
		const array3d<cmplx> iFx=iFt3(Fx);

		test( Proximy(Fx(0,0,0).real(), 36));
		test( Proximyeps(Fx(1,0,0).real(), 18,1E-14));
		test( Proximy(Fx(0,1,0).real(), 38));
		test( Proximyeps(Fx(1,1,0).real(),-20,1E-13));
		test( Proximyeps(Fx(0,0,1).real(),-36,1E-14));
		test( Proximyeps(Fx(1,0,1).real(),-14,1E-14));
		test( Proximyeps(Fx(0,1,1).real(), 14,1E-13));
		test( Proximyeps(Fx(1,1,1).real(),-28,1E-14));

		for(size_t  i=0;i<x.sizex();++i) {
			for(size_t  j=0;j<x.sizey();++j) {
				for(size_t  k=0;k<x.sizez();++k) {
					test( Proximyeps_noprint( iFx(i,j,k),x(i,j,k),1E-5) );
				}
			}
		}

		array3d<cmplx> v(7,4,5);
		for(size_t i=0;i<v.sizex();++i) {
			for(size_t j=0;j<v.sizey();++j)	{
				for(size_t k=0;k<v.sizez();++k) {
					//v(i,j,k)=cmplx(i*j*k,i+j-k);
					v(i,j,k)=cmplx(i*j*k,0);
				}
			}
		}

		const array3d<cmplx> Fv = Ft3(v);
		const array3d<cmplx> iFv=iFt3(Fv);

		for(size_t i=0;i<v.sizex();++i) {
			for(size_t j=0;j<v.sizey();++j) {
				for(size_t k=0;k<v.sizez();++k) {
					test( Proximyeps_noprint( iFv(i,j,k),v(i,j,k),2E-5) );
					test( Abs(iFv(i,j,k).imag())<1E-3 );
				}
			}
		}
	}

	test.Sub("more 3d");

	{
		array3d<cmplx> c(10,10,10);
		for(size_t k=0;k<c.sizez();++k)
		for(size_t j=0;j<c.sizey();++j)
		for(size_t i=0;i<c.sizex();++i) {
			double x=i;
			double y=j;
			double z=k;
			c(i,j,k)= x*x*x*x + y*y - 3.0*z;
		}
		const array3d<cmplx> fc=Ft3(c);

		/*
		>> z3(1,1,1)
		ans =
			1548300
		>> z3(2,1,1)
		ans =
		3.9319e+005 +1.0554e+006i
		>> z3(3,1,1)
		ans =
		-2.2023e+005 +6.2843e+005i
		>> z3(4,1,1)
		ans =
		-3.4919e+005 +3.4662e+005i
		>> z3(1,2,1)
		ans =
		2.3607e+002 +1.5388e+004i
		>> z3(1,3,1)
		ans =
		-3.5528e+003 +6.8819e+003i
		>> z3(1,4,1)
		ans =
		-4.2361e+003 +3.6327e+003i
		>> z3(1,1,2)
		ans =
		1.5000e+003 -4.6165e+003i
		>> z3(1,1,3)
		ans =
		1.5000e+003 -2.0646e+003i
		>> z3(1,1,4)
		ans =
		1.5000e+003 -1.0898e+003i
		*/

		test( Proximyeps( fc(0,0,0),cmplx( 1.5483*1.0e+006,0),1E-4) );
		test( Proximyeps( fc(1,0,0).real(), 0.3932 *1.0e+006,4E-5) );
		test( Proximyeps( fc(1,0,0).imag(), 1.0554 *1.0e+006,1E-5) );

		test( Proximyeps( fc(2,0,0),cmplx(-0.2202 , 0.6284)*1.0e+006,2E-4) );
		test( Proximyeps( fc(3,0,0),cmplx(-0.3492 , 0.3466)*1.0e+006,1E-4) );

		test( Proximyeps( fc(0,1,0).real(), 2.3607e+002  , 1E-5) );
		test( Proximyeps( fc(0,1,0).imag(), 1.5388e+004  , 1E-4) );
		test( Proximyeps( fc(0,2,0).real(), -3.5528e+003 , 1E-5) );
		test( Proximyeps( fc(0,2,0).imag(),  6.8819e+003 , 1E-5) );
		test( Proximyeps( fc(0,3,0),cmplx(-4.2361e+003 , 3.6327e+003),1E-5) );

		test( Proximyeps( fc(0,0,1).real(), 1.5000e+003 ,1E-11 ) );
		test( Proximyeps( fc(0,0,1).imag(),-4.6165e+003 ,1E-5  ) );
		test( Proximyeps( fc(0,0,2).real(), 1.5000e+003 ,1E-11 ) );
		test( Proximyeps( fc(0,0,2).imag(),-2.0646e+003 ,1E-4  ) );
		test( Proximyeps( fc(0,0,3).real(), 1.5000e+003 ,1E-11 ) );
		test( Proximyeps( fc(0,0,3).imag(),-1.0898e+003 ,1E-4  ) );
	}

	test.Sub("nabla");
	TestNablaSub(test,0.1,1E-1);
	TestNablaSub(test,0.01,1E-2);
	TestNablaSub(test,0.001,1E-3);
	TestNablaSub(test,0.0001,1E-4);

#ifdef USE_FFTW
	test.Sub("FFTW");
	{
		test.Subsub("Convolve");
		unsigned int i;
		unsigned int n=32;
		unsigned int np=n/2+1;

		Complex *f=FFTWComplex(np);
		Complex *g=FFTWComplex(np);
		Complex *h=FFTWComplex(np);
		Complex *h2=FFTWComplex(np);
		Complex d[]={-5,Complex(3,1),Complex(4,-2),Complex(-3,1),Complex(0,-2),Complex(0,1),Complex(4,0),Complex(-3,-1),Complex(1,2),Complex(2,1),Complex(3,1)};

		unsigned int m=sizeof(d)/sizeof(Complex);
		convolution convolve(n,m);

		for(i=0; i < m; i++) f[i]=d[i];
		for(i=0; i < m; i++) g[i]=d[i];
		convolve.fft(h,f,g);

		for(i=0; i < m; i++) f[i]=d[i];
		for(i=0; i < m; i++) g[i]=d[i];
		convolve.direct(h2,f,g);

		for(i=0;i<m;++i) test(Proximyeps_noprint(h[i],h2[i],3E-15));

		FFTWdelete(f);
		FFTWdelete(g);
		FFTWdelete(h);
		FFTWdelete(h2);
	}
	{
		test.Subsub("Example 00 r");

		unsigned int n=4;
		unsigned int np=n/2+1;
		double *f=FFTWdouble(n);
		double *f2=FFTWdouble(n);
		Complex *g=FFTWComplex(np);

		rcfft1d Forward(n,f,g);
		crfft1d Backward(n,g,f);

		for(unsigned int i=0; i < n; i++) f[i]=i;
		Forward.fft(f,g);
		Backward.fftNormalized(g,f2);

		for(unsigned int i=0; i < n; i++) test( Proximyeps(f[i],f2[i],1E-14) );

		FFTWdelete(g);
		FFTWdelete(f);
		FFTWdelete(f2);
	}
	{
		test.Subsub("Example 02 r");

		unsigned int nx=4, ny=5;
		unsigned int nyp=ny/2+1;
		size_t align=sizeof(Complex);

		array2<double> f(nx,ny,align);
		array2<Complex> g(nx,nyp,align);

		rcfft2d Forward(f,g);
		crfft2d Backward(g,f);

		for(unsigned int i=0; i < nx; i++)
			for(unsigned int j=0; j < ny; j++)
	  			f(i,j)=i+j;

		Forward.fft(f,g);
		Backward.fftNormalized(g,f);

		for(unsigned int i=0; i < nx; i++)
			for(unsigned int j=0; j < ny; j++)
	  			test( Proximyeps(f(i,j),i+j,1E-15) );

	}
	{
		test.Subsub("Example 03");

		unsigned int nx=4, ny=5, nz=6;
		size_t align=sizeof(Complex);

		array3<Complex> f(nx,ny,nz,align);

		fft3d Forward3(-1,f);
		fft3d Backward3(1,f);

		for(unsigned int i=0; i < nx; i++)
			for(unsigned int j=0; j < ny; j++)
				for(unsigned int k=0; k < nz; k++)
					f(i,j,k)=i+j+k;

		Forward3.fft(f);
		Backward3.fftNormalized(f);

		for(unsigned int i=0; i < nx; i++)
			for(unsigned int j=0; j < ny; j++)
				for(unsigned int k=0; k < nz; k++)
					test( Proximyeps(f(i,j,k),Complex(i+j+k),1E-15) );

	}

	if (!test.Verbose()) return;

	{
		test.Subsub("FFTW 3D");

		const size_t offset=2;
		const size_t maxs=11;
		const size_t step=3;

		for(size_t k=offset;k<maxs;k+=step){
			for(size_t j=offset;j<maxs;j+=step){
				for(size_t i=offset;i<maxs;i+=step){

					array3d<cmplx>  a(i,j,k);
					array3<Complex> f(i,j,k,sizeof(Complex));
					array3<Complex> g(i,j,k,sizeof(Complex));

					for(size_t kk=0;kk<k;++kk){
						for(size_t jj=0;jj<j;++jj){
							for(size_t ii=0;ii<i;++ii){
								const double y=1.2*ii+2.2*jj-3.1*kk;
								a(ii,jj,kk)=y;
								f(ii,jj,kk)=y;
								g(ii,jj,kk)=y;
							}
						}
					}

 					const array3d<cmplx> b=Ft3(a);
					const array3d<cmplx> c=iFt3(b);

					const array3d<cmplx> d= FFt3(a);
					const array3d<cmplx> e=iFFt3(d);

					test( c.size()==a.size() );
					test( e.size()==a.size() );

					fft3d Forward3(-1,f);
					fft3d Forward3g(-1,g);
					fft3d Backward3(1,f);

					Forward3.fft(f);
					Forward3.fft(g);
					Backward3.fftNormalized(f);

					test( Proximyeps(g(0,0,0),b(0,0,0),1E-15) );
					test( Proximyeps(g(0,0,0),d(0,0,0),1E-15) );

					for(size_t kk=0;kk<k;++kk){
						for(size_t jj=0;jj<j;++jj){
							for(size_t ii=0;ii<i;++ii){
								const double y=1.2*ii+2.2*jj-3.1*kk;
								test( Abs(c(ii,jj,kk).imag())<1E-3 );
								if(Abs(y)>1E-9){
									test( (Abs(b(ii,jj,kk).real())<1E-9 || Abs(g(ii,jj,kk).real())<1E-9) || Proximyeps_noprint(b(ii,jj,kk).real(),g(ii,jj,kk).real(),1E-4  ) );
									test( (Abs(b(ii,jj,kk).imag())<1E-9 || Abs(g(ii,jj,kk).imag())<1E-9) || Proximyeps_noprint(b(ii,jj,kk).imag(),g(ii,jj,kk).imag(),1E-4  ) );

									test( Proximyeps_noprint( c(ii,jj,kk).real(),y,1E-4) );
									test( Proximyeps_noprint( e(ii,jj,kk).real(),y,1E-4) );
									test( Proximyeps_noprint( f(ii,jj,kk).real(),y,1E-4) );
								}
							}
						}
					}
				}
			}
		}
	}
#endif // USE_FFTW

	{
		test.Sub("k indexing again");
		// N=4, N%2==0
		//   idx: k=0-N-1, j=0-N-1, i=0-N-1
		//        0=DC, 1=1 pos k, 2=-2 k, 3=-1 k, kvec= {-2.-1,0,1}
		//   DC: (0,0,0), max -k (-2,-2,-2) -> no -(-k)
		//   corners: (0,0,0) (2,0,0) (0,2,0) ... (2,2,0) (2,2,2)
		// N=5, N%2==1
		//   idx: k=0-N-1, j=0-N-1, i=0-N-1
		//        0=DC, 1=1 pos k, 2=2 pos, 3=-2 k, 4=-1 k, kvec= {-2.-1,0,1,2}
		//   DC: (0,0,0), max -k=(-2,-2,-2) -> -(-k)=(2,2,2)
		//   corners: (0,0,0) only

		test.Subsub("size 4");
		test( idx2k(0,4)==0 );
		test( idx2k(1,4)==1 );
		test( idx2k(3,4)==-1 );
		test( idx2kidx(t_idx(0,0,0),4)==Triple<int>(0,0,0) );
		if (test.Verbose()) {
			// XXX, these will fail, why??
			// test( idx2k(2,4)==-2 );
			// test( idx2kidx(t_idx(1,2,3),4)==Triple<int>(1,-2,-1) );
			// cout << "** idx2kidx(t_idx(1,2,3),4)=" << idx2kidx(t_idx(1,2,3),4) <<endl;
			// gives: idx2kidx(t_idx(1,2,3),4)=(1;2;-1)
		}
		test( hasinv_idx(t_idx(0,0,0),4) );
		test( hasinv_idx(t_idx(3,1,0),4) );
		test( !hasinv_idx(t_idx(2,1,0),4));
		test( idx2inv_idx(t_idx(0,0,0),4)==t_idx(0,0,0) );
		test( idx2inv_idx(t_idx(1,3,2),4)==t_idx(3,1,2) );
		test( isidx_corner(t_idx(0,0,0),4));
		test( isidx_corner(t_idx(0,0,2),4));
		test( isidx_corner(t_idx(0,2,0),4));
		test( isidx_corner(t_idx(0,2,2),4));
		test( isidx_corner(t_idx(2,0,0),4));
		test( isidx_corner(t_idx(2,0,2),4));
		test( isidx_corner(t_idx(2,2,0),4));
		test( isidx_corner(t_idx(2,2,2),4));
		test( hasinv_idx(t_idx(0,0,0),4));
		test( !hasinv_idx(t_idx(0,0,2),4));
		test( !hasinv_idx(t_idx(0,2,0),4));
		test( !hasinv_idx(t_idx(0,2,2),4));
		test( !hasinv_idx(t_idx(2,0,0),4));
		test( !hasinv_idx(t_idx(2,0,2),4));
		test( !hasinv_idx(t_idx(2,2,0),4));
		test( !hasinv_idx(t_idx(2,2,2),4));

		test.Subsub("size 5");
		test( idx2k(0,5)==0 );
		test( idx2k(1,5)==1 );
		test( idx2k(2,5)==2 );
		test( idx2k(3,5)==-2 );
		test( idx2k(4,5)==-1 );
		test( idx2kidx(t_idx(0,0,0),5)==Triple<int>(0,0,0) );
		test( idx2kidx(t_idx(1,2,3),5)==Triple<int>(1,2,-2) );
		test( idx2kidx(t_idx(2,3,4),5)==Triple<int>(2,-2,-1) );

		test( hasinv_idx(t_idx(0,0,0),5));
		test( hasinv_idx(t_idx(4,1,0),5));
		test( hasinv_idx(t_idx(4,4,4),5));

		test( idx2inv_idx(t_idx(0,0,0),5)==t_idx(0,0,0) );
		test( idx2inv_idx(t_idx(1,4,3),5)==t_idx(4,1,2) );
		test( isidx_corner(t_idx(0,0,0),5));
		test( !isidx_corner(t_idx(0,0,2),5));
		test( !isidx_corner(t_idx(0,2,0),5));
		test( !isidx_corner(t_idx(0,2,2),5));
		test( !isidx_corner(t_idx(2,0,0),5));
		test( !isidx_corner(t_idx(2,0,2),5));
		test( !isidx_corner(t_idx(2,2,0),5));
		test( !isidx_corner(t_idx(2,2,2),5));
		test( !isidx_corner(t_idx(0,0,3),5));
		test( !isidx_corner(t_idx(0,3,0),5));
		test( !isidx_corner(t_idx(0,3,3),5));
		test( !isidx_corner(t_idx(3,0,0),5));
		test( !isidx_corner(t_idx(3,0,3),5));
		test( !isidx_corner(t_idx(3,3,0),5));
		test( !isidx_corner(t_idx(3,3,3),5));
		test( hasinv_idx(t_idx(0,0,0),5));
		test( hasinv_idx(t_idx(0,0,2),5));
		test( hasinv_idx(t_idx(0,2,0),5));
		test( hasinv_idx(t_idx(0,2,2),5));
		test( hasinv_idx(t_idx(2,0,0),5));
		test( hasinv_idx(t_idx(2,0,2),5));
		test( hasinv_idx(t_idx(2,2,0),5));
		test( hasinv_idx(t_idx(2,2,2),5));
		test( hasinv_idx(t_idx(0,0,0),5));
		test( hasinv_idx(t_idx(0,0,3),5));
		test( hasinv_idx(t_idx(0,3,0),5));
		test( hasinv_idx(t_idx(0,3,3),5));
		test( hasinv_idx(t_idx(3,0,0),5));
		test( hasinv_idx(t_idx(3,0,3),5));
		test( hasinv_idx(t_idx(3,3,0),5));
		test( hasinv_idx(t_idx(3,3,3),5));

		test.Subsub("index size looping, 0 to 32");
		for(size_t N=2;N<32;++N)
		for(size_t k=0;k<N;++k)
		for(size_t j=0;j<N;++j)
		for(size_t i=0;i<N;++i) {
			const t_idx idx(i,j,k);
			test( idx==idx2inv_idx(idx2inv_idx(idx,N),N) );
			const flt l1=idx2klen(idx,N);
			const flt l2=idx2klen(idx2inv_idx(idx,N),N);
			const Triple<_per_len> kvec1=idx2kvec(idx,N,_per_len(1));
			const _per_len l3=kvec1.length<_per_len2>();
			const Triple<_per_len> kvec2=idx2kvec(idx2inv_idx(idx,N),N,_per_len(1));
			const _per_len l4=kvec2.length<_per_len2>();

			test( Proximy(l1,l2) );
			test( Proximy(l1,l3/_per_len(1)) );
			test( Proximy(l3,l4) );
		}
	}

	{
		test.Sub("Fouier 3D - k-vectors and aliasing");

		// matlab index  (n,m,k) <=> array3d index (m,n,k)

		const array3d<flt> y=array3d<flt>::Readmatlab( testdatapath_inputs + "x_real.txt");
		test( y.sizex()==4 && y.sizey()==y.sizex()*y.sizex() && y.sizez()==1 );

		array3d<flt> x(y.sizex(),y.sizex(),y.sizex() );
		for(size_t i=0;i<x.size();++i) x[i]=y[i];
		test( Proximyeps( x(0,0,0), 0.4048 ,1E-3) );
		test( Proximyeps( x(0,1,0), 0.6271 ,1E-3) );
		test( Proximyeps( x(1,0,0), 0.5268 ,1E-3) );
		test( Proximyeps( x(1,1,0), 0.8074 ,1E-3) );

		test( Proximyeps( x(3,3,2), 0.6177 ,1E-3) );
		test( Proximyeps( x(3,3,3), 0.8459 ,1E-3) );

		test.Subsub("one more FFT iFFt");
		const array3d<cmplx> X=FFt3(totype<cmplx>(x));
		const array3d<cmplx>x2=iFFt3(X);
		for(size_t i=0;i<x.size();++i) {
			test( Proximyeps_noprint(x2[i].real(),x[i],1E-13) );
			test( fabs(x2[i].imag())<1E-12 );
		}

		test.Subsub("vec k => vec -k");
		test( idx2inv_idx(t_idx(0,0,0),4)==t_idx(0,0,0) );
		test( idx2inv_idx(t_idx(1,0,0),4)==t_idx(3,0,0) );
		test( idx2inv_idx(t_idx(2,0,0),4)==t_idx(2,0,0) );
		test( idx2inv_idx(t_idx(3,0,0),4)==t_idx(1,0,0) );

		test( idx2inv_idx(t_idx(0,1,0),4)==t_idx(0,3,0) );
		test( idx2inv_idx(t_idx(1,1,0),4)==t_idx(3,3,0) );
		test( idx2inv_idx(t_idx(2,3,0),4)==t_idx(2,1,0) );
		test( idx2inv_idx(t_idx(3,3,0),4)==t_idx(1,1,0) );

		test( idx2inv_idx(t_idx(0,0,1),4)==t_idx(0,0,3) );
		test( idx2inv_idx(t_idx(3,0,1),4)==t_idx(1,0,3) );
		test( idx2inv_idx(t_idx(3,2,1),4)==t_idx(1,2,3) );
		test( idx2inv_idx(t_idx(3,3,3),4)==t_idx(1,1,1) );

		TestSubsubHermitian(test,X);

		// (0,0,0) => dc        (0,1,0) => (0,3,0)   (0,2,0) => no       (0,3,0) => (0,1,0)
		// (1,0,0) => (3,0,0)   (1,1,0) => (3,3,0)   (1,2,0) => (3,2,0)  (1,3,0) => (3,1,0)
		// (2,0,0) => no        (2,1,0) => (2,3,0)   (2,2,0) => no       (2,3,0) => (2,1,0)
		// (3,0,0) => (1,0,0)   (3,1,0) => (1,3,0)   (3,2,0) => (2,3,0)  (3,3,0) => (1,1,0)

		// (0,0,1) => (0,0,3)   (0,1,1) => (0,3,3)   (0,2,1) => (0,2,3)  (0,3,1) => (0,1,3)
		// (1,0,1) => (3,0,3)   (1,1,1) => (3,3,3)   (1,2,1) => (3,2,3)  (1,3,1) => (3,1,3)
		// (2,0,1) => (2,0,3)   (2,1,1) => (2,3,3)   (2,2,1) => (2,2,3)  (2,3,1) => (2,1,3)
		// (3,0,1) => (1,0,3)   (3,1,1) => (1,3,3)   (3,2,1) => (1,2,3)  (3,3,1) => (1,1,3)

		// same as (x,x,0)
		// (0,0,2) => no        (0,1,2) => (0,3,2)   (0,2,2) => no       (0,3,2) => (0,1,2)
		// (1,0,2) => (3,0,2)   (1,1,2) => (3,3,2)   (1,2,2) => (3,2,2)  (1,3,2) => (3,1,2)
		// (2,0,2) => no        (2,1,2) => (2,3,2)   (2,2,2) => no       (2,3,2) => (2,1,2)
		// (3,0,2) => (1,0,2)   (3,1,2) => (1,3,2)   (3,2,2) => (2,3,2)  (3,3,2) => (1,1,2)

		// fftn(x)
		//
		// ans(:,:,1) =
		//
		//   33.6531             0.1889 + 1.4497i  -2.0312             0.1889 - 1.4497i
		//    2.4545 - 0.6460i  -2.1908 - 1.0280i   0.2751 + 0.3152i   1.0668 + 1.3903i
		//   -3.2073             0.6909 - 1.3817i  -1.8479             0.6909 + 1.3817i
		//    2.4545 + 0.6460i   1.0668 - 1.3903i   0.2751 - 0.3152i  -2.1908 + 1.0280i
		//
		//
		// ans(:,:,2) =
		//
		//    1.7778 + 0.2224i   0.2416 - 0.6671i   2.0471 - 0.4651i  -1.4429 + 1.6312i
		//   -2.5979 + 3.2231i   0.4203 + 1.7898i   0.0191 - 0.0409i   1.9427 + 2.9426i
		//   -2.4072 + 0.3698i   2.0500 - 0.4867i  -0.5229 - 1.2466i  -2.6887 - 1.3413i
		//   -0.9991 - 1.1784i  -2.5370 + 0.0340i  -0.6367 + 3.0476i  -0.7015 + 0.3937i
		//
		//
		// ans(:,:,3) =
		//
		//    1.6960             0.6945 - 0.2028i  -4.0872             0.6945 + 0.2028i
		//   -2.5380 - 0.3022i   2.7221 - 0.3443i  -1.2960 - 1.3718i   4.9066 - 0.0995i
		//   -0.9867             0.6320 + 4.4709i  -0.4217             0.6320 - 4.4709i
		//   -2.5380 + 0.3022i   4.9066 + 0.0995i  -1.2960 + 1.3718i   2.7221 + 0.3443i
		//
		//
		// ans(:,:,4) =
		//
		//    1.7778 - 0.2224i  -1.4429 - 1.6312i   2.0471 + 0.4651i   0.2416 + 0.6671i
		//   -0.9991 + 1.1784i  -0.7015 - 0.3937i  -0.6367 - 3.0476i  -2.5370 - 0.0340i
		//   -2.4072 - 0.3698i  -2.6887 + 1.3413i  -0.5229 + 1.2466i   2.0500 + 0.4867i
		//   -2.5979 - 3.2231i   1.9427 - 2.9426i   0.0191 + 0.0409i   0.4203 - 1.7898i

		// » fft(x)
		//
		//ans(:,:,1) =
		//
		// 2.2653             2.6895             2.3448             2.4266
		// 0.0194 + 0.2207i   0.1334 + 0.1543i  -0.6843 - 0.3072i  -0.3886 + 0.7955i
		// -0.6847            -0.8488            -0.8558             0.1372
		// 0.0194 - 0.2207i   0.1334 - 0.1543i  -0.6843 + 0.3072i  -0.3886 - 0.7955i
		//
		//
		// ans(:,:,2) =
		//
		// 1.9724             1.3657             2.3399             2.2000
		// -0.7750 + 0.4255i   0.6197 - 0.2853i   0.9641 - 0.3755i  -0.0717 - 0.2503i
		// 0.1175            -0.1125            -0.3542            -0.3909
		// -0.7750 - 0.4255i   0.6197 + 0.2853i   0.9641 + 0.3755i  -0.0717 + 0.2503i
		//
		//
		// ans(:,:,3) =
		//
		// 1.6094             2.1822             1.0882             3.0685
		// 0.6557 - 0.4815i   0.3167 - 0.8210i  -0.2669 + 0.0668i   0.1729 - 0.1017i
		// 0.2075            -0.1641            -0.2829             0.3946
		// 0.6557 + 0.4815i   0.3167 + 0.8210i  -0.2669 - 0.0668i   0.1729 + 0.1017i
		//
		//
		// ans(:,:,4) =
		//
		// 2.1528             1.9588             2.0382             1.9508
		// 0.5014 - 0.1568i   0.0797 - 0.1026i   0.9505 + 0.4426i   0.2277 + 0.1306i
		// -0.5587             1.4763            -0.1164            -1.1715
		// 0.5014 + 0.1568i   0.0797 + 0.1026i   0.9505 - 0.4426i   0.2277 - 0.1306i

		if (!test.Verbose()) return;

		test.Sub("Hermitian matrix and k => -k vector, NxNxN loop");
		for(size_t N=4;N<=128;N*=2){
			array3d<cmplx> z(N,N,N);
			for(size_t i=0;i<z.size();++i) z[i]=cmplx(Sin(0.1*i),0);
			const array3d<cmplx> Z=FFt3(z);
			TestSubsubHermitian(test,Z);
		}

		test.Sub("Hermitian and Greens function");
		_bbox_len bbox(_len(50000));

		for(unsigned int f=0;f<2;++f){
			for(unsigned int s=4;s<42;++s){
				if (f==0 || isPowersOf2(s)){
					test.Subsub("loop s=" + tostring(s) + " f=" + tostring(f) );

					const flt eps=s>=26 ? 1E-4 : 1E-6;

					array3d<cmplx> r2(s,s,s);
					for(size_t n=0;n<r2.size();++n) r2[n]=cgasdev_ctr();
					test(!hasImaginaryparts(r2));

					array3d<cmplx> R2=f==0 ? Ft3(r2) : FFt3(r2);
					test(hasImaginaryparts(R2));
					test(isHermitian(R2,eps,true));

					array3d<cmplx> R3=R2;
					R3(2,3,1)=42;
					R3(3,1,1)=-1042;
					// XXX fails, why?
					// test(!isHermitian(R3,eps));
					HermitianConstraint(R3);
					test(isHermitian(R3,eps,true));

					array3d<cmplx> r22=f==0 ? iFt3(R2) : iFFt3(R2);
					test(!hasImaginaryparts(r22));
					array3d<flt> rr22=totype<flt>(r22);

					// XXX only for Genic code
					// const array3d<_len_cmplx> F0=Greens_dndn(R2,0,bbox);
					// const array3d<_len_cmplx> F1=Greens_dndn(R2,1,bbox);
					// const array3d<_len_cmplx> F2=Greens_dndn(R2,2,bbox);
					//
					// test(isHermitian(F0,eps,true));
					// test(isHermitian(F1,eps,true));
					// test(isHermitian(F2,eps,true));
				}
			}
		}
	}
}

void TestSPH(Testsuite& test)
{
	test.Header("SPH");

	test.Sub("W");
	const flt hx=3;
	test(  W(0./hx)==1 );
	test(  W(1E-9/hx)>1-1E-9 );
	test(  W(1./hx)<=1.0 );
	test(  W(1./hx)>0 );
	test(  W(2./hx)>0.07 );
	test(  W(2.9/hx)>7E-5 );
	test(  W(3./hx)<=1E-5 );
	test(  W(3.0001/hx)==0 );

	test.Sub("W filter");
	_len h(3.7);
	_per_len3 w=W<_len,_per_len3>(2*h,h);
	test( w==_per_len3(0) );
	w=W<_len,_per_len3>(h+_len(1E-6),h);
	test(  w==_per_len3(0) );

	_per_len3 w1=W<_len,_per_len3>(_len(0),h);
	_per_len3 w2=W<_len,_per_len3>(_len(1E-9),h);
	test(  Proximyeps(w1/_per_len3(1),w2/_per_len3(1),1E-9) );

	flt intg(0);
	_len s(0.01);
	for(_len r=_len(0);r<h;r+=s){
		intg += W<_len,_per_len3>(r,h)*s*s*s;
	}
	test( Proximyeps(intg,7.00052E-6,1E-6) );

	const _point_len p(_len(10));
	const _point_len p0(_len(5));
	const _point_len p1(_len(15));
	const unsigned int steps=20;

	const _len stepsize=_len(10)/steps;
	const _len3 dV=stepsize*stepsize*stepsize;

	intg=0;
	for(unsigned int k=0;k<steps;++k){
		for(unsigned int j=0;j<steps;++j){
			for(unsigned int i=0;i<steps;++i){
				_point_len q(stepsize*i,stepsize*j,stepsize*k);
				q+=p0;
				const _len l=_bbox_len(_len(20)).DistPeriodic(q,p);
				const  _per_len3 w=W<_len,_per_len3>(l,h);
				intg += dV*w;
			}
		}
	}
	test( Proximyeps(intg,1.,1E-4) );

	test.Sub("bbox");
	const _bbox_len bb(_len(100));
	h=_len(2);

	test( bb.isParticleInsideBox(p0,_len(1) ,p0,h,false)==1 );
	test( bb.isParticleInsideBox(p0,_len(5),p0,h,false)==1 );
	test( bb.isParticleInsideBox(p0,_len(10),p0+_len(5),h,false)==2 );
	test( bb.isParticleInsideBox(p0,_len(4.001),p0+_len(2),h,false)==2 );
	test( bb.isParticleInsideBox(p0,_len(4) ,p0+_len(2),h,false)==1 );
	test( bb.isParticleInsideBox(p0,_len(2) ,p0+_len(1),h,false)==1 );
	test( bb.isParticleInsideBox(p0,_len(2) ,p0+_len(5),h,false)==0 );
	test( bb.isParticleInsideBox(p0,_len(2) ,p0+_len(4.5),h,false)==0 );
	test( bb.isParticleInsideBox(p0,_len(2) ,p0+_len(4.01),h,false)==1 ); // proximy only
	test( bb.isParticleInsideBox(p0,_len(2) ,p0+_len(3.99),h,false)==1 );

	test.Subsub("bbox - boundary cases");
	test( bb.isParticleInsideBox(_len(0),_len(1),_len(99),h,false)==1 );
	test( bb.isParticleInsideBox(_len(0),_len(1),_len(90),h,false)==0 );
	test( bb.isParticleInsideBox(_len(99),_len(1),_len(0),h,false)==1 );
	test( bb.isParticleInsideBox(_len(99),_len(1),_len(1),h,false)==1 );
	test( bb.isParticleInsideBox(_len(99),_len(1),_len(3),h,false)==0 );

	{
		test.Subsub("analytic fun");
		vector<flt> vx,vy;
		for(flt x=-1;x<1;x+=.01) {
			vx.push_back(x);
			vy.push_back(analytic_sphereinsphere(x));
		}
		Writeascii(testdatapath_refs + "out2" ,vx);
		Writeascii(testdatapath_refs + "out"  ,vy);

		test.DiffFile(testdatapath_refs + "test_analytic_sphereinsphere_x.ref",testdatapath_refs + "out2" ,test.Bool(0));
		test.DiffFile(testdatapath_refs + "test_analytic_sphereinsphere_y.ref",testdatapath_refs + "out"  ,test.Bool(0));
	}

	if (!test.Verbose()) return;
	{
		test.Subsub("Massintegrate - variabel distances");

		size_t n=0;
		for(flt hr=0.05;hr<1;hr+=.1,++n) {
			//if (hr*1.57>1) hr=.999999;
			const int integrationres=40;
			const size_t integrationsteps=100;

			const _bbox_len bbox(_len(50000));
			const _len r(8000);
			assert( 0<hr && hr<1);
			const _len h=hr*r;
			const _point_len q(_len(0));
			const _len step=h/integrationres;

			particle_data_2 d(0,q,_mass(1));
			d.seth(h);

			vector<flt> vx,vy1,vy2;
			for(int i=-integrationres-1;i<=integrationres+1;++i){
				const _len x=i*step+r;
				_point_len p(q[0]+x,q[1],q[2]);
				bbox.WrapToInsideBox(p);

				const _len l=bbox.DistPeriodic(p,q);
				const _mass m1=FindMassInSphere(p,r,d,bbox,integrationsteps,false,false);
				const _mass m2=FindMassInSphere(p,r,d,bbox,integrationsteps,false,true);

				test( m1<_mass(3*1E-3) || Proximyeps_noprint(m1/_mass(1),m2/_mass(1),0.55) ); // mmm, eps seems large here!

				assert( r>h );
				const flt xf=(l-r)/h;
				vx.push_back(xf);
				vy1.push_back(m1/_mass(1));
				vy2.push_back(m2/_mass(1));
			}

			Writeascii(testdatapath_refs + "out" ,vx);
			Writeascii(testdatapath_refs + "out1",vy1);
			Writeascii(testdatapath_refs + "out2",vy2);

			test.DiffFile(testdatapath_refs + "test_massintegrate_x_0"  + tostring(n) + ".ref",testdatapath_refs + "out"  , test.Bool(0) );
			test.DiffFile(testdatapath_refs + "test_massintegrate_y1_0" + tostring(n) + ".ref",testdatapath_refs + "out1" , test.Bool(0) );
			test.DiffFile(testdatapath_refs + "test_massintegrate_y2_0" + tostring(n) + ".ref",testdatapath_refs + "out2" , test.Bool(0) );
		}
	}

	if (false) /// XXX
	{
		test.Sub("Wprecalc class");
		const _bbox_len bbox(_len(50000));
		particle_data_2 d(0,_point_len(_len(0)),_mass(13));
		const _len h(1000);
		d.seth(h);

		test.Sub("loop  tests");

		for(int ires=10;ires<100;ires+=30){
			cout << "** ires=" << ires << endl;

			flt eps=1E-3;
			if      (ires<20) eps=2*1E-2;
			else if (ires>50) eps=1E-4;

			const int integrationres     =ires;
			const size_t integrationsteps=100;
			const size_t lookupres       =ires;
			Wprecalc w(integrationres,integrationsteps,lookupres,bbox,false);

			for(_len r=_len(1000);r<_len(4000);r+=_len(100)){
				cout << "\t** r=" << r << endl;

				_point_len p=d.pos()+h;
				bbox.WrapToInsideBox(p);
				_mass m1=FindMassInSphere(p,r,d,bbox,integrationsteps,false);
				_mass m2=w.Lookup(p,r,d,false);
				bool b=Proximyeps_noprint(m1/_mass(1),m2/_mass(1),eps);
				test( b );
				if (!b){
					Proximyeps(m1/_mass(1),m2/_mass(1),eps);
					cout << "\t## m1=" << m1 << " m2=" << m2 << " hr=" << h/r << " h=" << h << " r=" << r << endl;
					w.Lookup(p,r,d,false,true);
				}

				p=d.pos()+r/2;
				bbox.WrapToInsideBox(p);
				m1=FindMassInSphere(p,r,d,bbox,integrationsteps,false);
				m2=w.Lookup(p,r,d,false);
				b=Proximyeps_noprint(m1/_mass(1),m2/_mass(1),eps);
				test( b );
				if (!b){
					Proximyeps(m1/_mass(1),m2/_mass(1),eps);
					cout << "\t## m1=" << m1 << " m2=" << m2 << " hr=" << h/r << " h=" << h << " r=" << r << endl;
					w.Lookup(p,r,d,false,true);
				}
			}
		}

		flt eps=1E-2;
		const int integrationres     =20;
		const size_t integrationsteps=100;
		const size_t lookupres       =20;
		Wprecalc w(integrationres,integrationsteps,lookupres,bbox,false);

		test.Subsub("Adv loop 1");
		for(_len r=_len(1001);r<_len(4000);r+=_len(10)){
			cout << "** r=" << r << endl;
			for(_len x=-2*h;x<2*h; x+=_len(1) ){
				_point_len p=d.pos();
				p[0]+=x;
				bbox.WrapToInsideBox(p);
				const _mass m1=FindMassInSphere(p,r,d,bbox,integrationsteps,false);
				const _mass m2=w.Lookup(p,r,d,false);
				const bool b=Proximyeps_noprint(m1/_mass(1),m2/_mass(1),eps);
				test( b );
				if (!b){
					Proximyeps(m1/_mass(1),m2/_mass(1),eps);
					cout << "\t## m1=" << m1 << " m2=" << m2 << " hr=" << h/r << " h=" << h << " r=" << r << endl;
					w.Lookup(p,r,d,false,true);
				}
			}
		}

		test.Subsub("Adv loop 2");
		for(_len h=_len(10);h<_len(4000);h+=_len(10)){
			d.seth(h);
			cout << "** h=" << h << endl;
			for(_len r(h+_len(1));r<4*h;r *=1.1735 ){
				cout << "** r=" << r << endl;
				for(_len x(1);x<_len(25000);x *= 1.13 ){
					_point_len p=_point_len(d.pos()+h+r);
					bbox.WrapToInsideBox(p);
					const _mass m1=FindMassInSphere(p,r,d,bbox,integrationsteps,false);
					const _mass m2=w.Lookup(p,r,d,false);
					const bool b=Proximyeps_noprint(m1/_mass(1),m2/_mass(1),eps);
					test( b );
					if (!b){
						Proximyeps(m1/_mass(1),m2/_mass(1),eps);
						cout << "\t## m1=" << m1 << " m2=" << m2 << " hr=" << h/r << " h=" << h << " r=" << r << endl;
						w.Lookup(p,r,d,false,true);
					}
				}
			}
		}
	}

	test.Sub("mass in box");
	particle_data_2 d(5,p,_mass(3));
	d.seth(_len(2)); // smoothing len
	h=_len(2);

	_mass m0=FindMassInBox(p-h    ,2*h    ,d,_len(100));
	_mass m1=FindMassInBox(p-h/2  ,1*h    ,d,_len(100));
	_mass m2 = FindMassInBox(p     ,_len(10),d,_len(100),20);
	_mass m21 =FindMassInBox(p     ,_len(10),d,_len(100),21);
	_mass m25 =FindMassInBox(p-_len(1E-3)     ,_len(10),d,_len(100),21);

	_mass m212=FindMassInBox(p     ,_len(10),d,_len(100),22);
	_mass m213=FindMassInBox(p     ,_len(10),d,_len(100),19);
	_mass m214=FindMassInBox(p     ,_len(10),d,_len(100),40);
	_mass m22 =FindMassInBox(p     ,_len(10),d,_len(100),100);
	_mass m23 =FindMassInBox(p     ,_len(10),d,_len(100),101);

	_mass m3=FindMassInBox(p0    ,_len(10),d,_len(100));
	_mass m4=FindMassInBox(p0    ,_len(5) ,d,_len(100));
	_mass m5=FindMassInBox(p     ,_len(10),d,_len(100));
	_mass m6=FindMassInBox(p-h/4 ,h/2     ,d,_len(100));
	_mass m7=FindMassInBox(p0    ,_len(2) ,d,_len(100));

	test( Proximyeps_noprint(m0  /_mass(1),3,1E-6));
	test( Proximyeps(m1  /_mass(1),2.43241,1E-6));
	test( Proximyeps(m21 /_mass(1),3/8.,1E-5));
	test( Proximyeps(m212/_mass(1),3/8.,1E-6));
	test( Proximyeps(m213/_mass(1),3/8.,1E-5));
	test( Proximyeps(m214/_mass(1),3/8.,1E-7));
	test( Proximyeps(m22 /_mass(1),3/8.,1E-9));
	test( Proximyeps(m23 /_mass(1),3/8.,1E-9));
	test( Proximyeps(m25 /_mass(1),0.376576,1E-6));
	test( Proximyeps(m3  /_mass(1),3,1E-6));
	test( Proximyeps(m4 /_mass(1),3/8.,1E-6));
	test( Proximyeps(m5 /_mass(1),3/8.,1E-6));
	test( Proximyeps(m6 /_mass(1),0.695899,2E-6));
	test( Proximyeps(m7 /_mass(1),0,1E-12));

	for(size_t i=11;i<50;++i){
		_mass m=FindMassInBox(p,_len(10),d,_len(100),i);
		flt e=1E-5;
		if (i<20) e=1E-3;
		test( Proximyeps_noprint(m / _mass(1),3/8.,e));
		m=FindMassInBox(p-h,2*h,d,_len(100),i);
		test( Proximyeps_noprint(m / _mass(1),3,e));
	}

	test.Subsub("mass in box - on box boundary");
	const _bbox_len bbox(_len(20));
	_point_len q(_len(0.));
	d=particle_data_2(5,q,_mass(3));
	d.seth(_len(2)); // smoothing len

	m1=FindMassInBox(q                ,1*h ,d,bbox);
	m2=FindMassInBox(q                ,2*h ,d,bbox);
	m3=FindMassInBox(_point_len(_len(18)),_len(2) ,d,bbox);
	m4=FindMassInBox(_point_len(_len(19)),_len(1) ,d,bbox);

	test( Proximyeps(m1/_mass(1),3/8.,1E-6));
	test( Proximyeps(m2/_mass(1),3/8.,1E-6));
	test( Proximyeps(m3/_mass(1),3/8.,1E-6));
	test( Proximyeps(m4/_mass(1),0.302523,1E-5));

	test.Sub("mass in sphere");

	h=_len(3.7);
	d=particle_data_2(5,p,_mass(3));
	d.seth(_len(2)); // smoothing len

	const _mass r1=FindMassInSphere(p ,0.1*h   ,d,_len(100));
	const _mass r2=FindMassInSphere(p ,0.5*h   ,d,_len(100));
	const _mass r3=FindMassInSphere(p ,0.9*h   ,d,_len(100));
	const _mass r4=FindMassInSphere(p ,1.0*h   ,d,_len(100));
	const _mass r5=FindMassInSphere(p ,4*h     ,d,_len(100));
	const _mass r6=FindMassInSphere(p1,  h     ,d,_len(100));
	const _mass r7=FindMassInSphere(p1,2*h     ,d,_len(100));
	const _mass r8=FindMassInSphere(p1,4*h     ,d,_len(100));

	test( Proximyeps(r1/_mass(1),0.182916,1E-7));
	test( Proximyeps(r2/_mass(1),2.99884 ,1E-6));
	test( Proximyeps(r3/_mass(1),3       ,1E-6));
	test( Proximyeps(r4/_mass(1),3       ,1E-6));
	test( Proximyeps(r5/_mass(1),3       ,1E-6));
	test( Proximyeps(r6/_mass(1),0       ,1E-6));
	test( Proximyeps(r7/_mass(1),0.0223239,1E-5));
	test( Proximyeps(r8/_mass(1),3       ,1E-6));

	test.Subsub("Massinsphere - boundary case");

	_point_len q2=_point_len(_len(0.));
	d=particle_data_2(5,q2,_mass(3));
	d.seth(_len(10));
	h=_len(10);

	q2=_point_len(_len(99));
	const _mass r9 =FindMassInSphere(q2 ,h ,d,_len(100));
	q2=_point_len(_len(1));
	const _mass r10=FindMassInSphere(q2 ,h ,d,_len(100));

	test( r9==r10 );
	test( Proximyeps( r9/_mass(1),2.9963,1E-6));
	test( Proximyeps(r10/_mass(1),2.9963,1E-6));
}

void TestSigma(Testsuite& test)
{
	test.Header("Sigma");

	test.Sub("Pk class");
	const _len3 p1=PkAnalytic(1,1)  (_per_len(1)   );
	const _len3 p2=PkAnalytic(1,0.7)(_per_len(0.1) );
	const _len3 p3=PkAnalytic(1,0.7)(_per_len(0.01));

	test( Proximyeps(p1/_len3(1),6.2000e+003,1E-5 ));
	test( Proximyeps(p2/_len3(1),2.0321e+004,1E-5 ));
	test( Proximyeps(p3/_len3(1),8.8773e+003,1E-5 ));

	test.Sub("CMB data");
	const PkCmb pk(testdatapath_inputs + "lcdm_tf00.dat",1);
	test( pk.size()==64 );
	_len3 power=pk( _per_len(20E-5*1E-3) );
	test( Proximyeps( power/_len3(1E9),0.000252754,1E-5) );
	power=pk( _per_len(4*1E-3) );
	test( Proximyeps( power/_len3(1E9),5.28424e-07,1E-6) );

	test.Subsub("pk limits");
	power=pk( _per_len(0) );
	test( Proximy( power/_len3(1),0) );
	power=pk( _per_len(1E12) );
	test( power/_len3(1)<1E-20 );

	test.Subsub("save load");
	{
		ofstream s((testdatapath_refs+"out").c_str());
		s << pk;
	}
	test.DiffFile(testdatapath_refs + "test_pk.ref",testdatapath_refs + "out",test.Bool(0) );

	{
		const PkCmb pk2(testdatapath_refs+"out");
		test( Proximyeps(pk2.kmin()/_per_len(1),pk.kmin()/_per_len(1),1E-6) );
		test( Proximyeps(pk2.kmax()/_per_len(1),pk.kmax()/_per_len(1),1E-6) );
		for(_per_len k=pk2.kmin();k<pk2.kmax(); k+=(pk2.kmax()-pk2.kmin())/100){
			test( Proximyeps_noprint( pk2(k)/_len3(1),pk(k)/_len3(1),3E-5) );
		}
	}

	test.Sub("Filters");

	FilterTophat W;
	flt w=W(_per_len(4)*_len(0.1));
	test( Proximyeps(w,Pow2(0.984091158),1E-9 ) );
	flt w2=W(_per_len(4)/1000*_len(0.1*1000));
	test( Proximy(w,w2) );
	w=W(_per_len(4)/1000*_len(0));
	test( w==1 );
	w=W(_per_len(0)*_len(8));
	test( w==1 );
	w=W(0);
	test( w==1 );

	for(flt x=0;x<1E-3;x+=1E-6){
		w=W(_per_len(x)*_len(1));
		test( w<=1 && w>0.9999 );
		w=W(_per_len(1)*_len(x));
		test( w<=1 && w>0.9999 );

	}
	for(flt x=0;x<2;x+=0.01){
		w=W(_per_len(x)*_len(1));
		test( w<=1 && w>0.4 );
		w=W(_per_len(1)*_len(x));
		test( w<=1 && w>0.4 );
	}

	FilterSinc S;
	w=S(0);
	test( w==1 );
	w=S(1E-9);
	test( Proximyeps(w,1,1E-13) );
	w=S(1E-3);
	test( Proximyeps(w,1,1E-6) );
	w=S(1E-4);
	test( Proximyeps(w,1,1E-8) );
	w=S(1);
	test( Proximyeps(w,.84147,1E-5) );
	w=S(42);
	test( Proximyeps(w,-0.021822,1E-5) );

	test.Sub("Sigma class");

	const flt sigma8=Sqrt(IntegratorWithFilter<PkCmb,FilterTophat>(pk,_len(8*1000)).Integrate());
	test( Proximyeps(sigma8,0.000403132,1E-5) );

	PkAnalytic pk2(1.,0.7);
	IntegratorWithFilter<PkAnalytic,FilterTophat> s(pk2,_len(8*1000));
	_len sigmakR=s(_per_len(4));
	test( Proximyeps( sigmakR/_len(1),6.0315E-15,1E-6 ));// prob wrong in units, but test anyway
}

void TestICFun(Testsuite& test)
{
	test.Header("IC fun");

	ranc_ctr(42);
	flt m=0,s=0;
	const int N=100000;
	for(int i=0;i<N;++i)
	{
		const flt r=cgasdev_ctr();
		m+=r;
		s+=r*r;
	}

	m /= N;
	s /= (N-1);
	s -= m*m;

	// expected mean=0, var=1
	test( Proximyeps_noprint(m,0,1E-2) );
	test( Proximyeps_noprint(s,1,1E-2) );
	{
		test.Subsub("EnforceHermitian");
		array3d<cmplx> a1(10,10,10),a2(10,10,10),b1,b2,c1,c2;

		const size_t N=a1.size();
		for(size_t n=0;n<N;++n) {
			a1[n] = cmplx(n,n);
			a2[n] = cmplx(Random(),Random());
		}
		b1=a1;
		b2=a2;

		EnforceHermitian(b1);
		EnforceHermitian(b2);

		c1=b1;
		c2=b2;

		for(size_t k=0;k<b1.sizez();++k){
			c1.transpose_slice(k);
			c2.transpose_slice(k);
			for(size_t j=0;j<b1.sizey();++j){
				for(size_t i=0;i<b1.sizex();++i){
					const cmplx r1=b1(i,j,k);
					const cmplx s1=c1(i,j,k); // why not Conj??
					test( r1==s1 );

					const cmplx r2=b2(i,j,k);
					const cmplx s2=c2(i,j,k);// why not Conj??
					test( r2==s2 );
				}
			}
		}

		array3d<complex<_per_len> > k(10,10,10);
		k=_per_len(0);
		EnforceHermitian(k); // test units, should just compile
	}
	{
		/*
		test.Subsub("Div in k-space again");
		const size_t N=10;
		vector<cmplx> f(N),efdx(N),efdxdx(N);

		const _len dx(0.1);
		const _len bbox=dx*N;
		const _per_len dk=2*_pi/bbox;

		cout << "dx=" << dx << " bbox=" << bbox << " dk=" << dk << endl;

		for(size_t i=0;i<N;++i) {
			const flt x=i*dx/_len(1);
			f   [i]=x*x;
			efdx[i]=2*x;
			efdxdx[i]=2;
		}
		const vector<cmplx> F=Ft(f);
		vector<cmplx> FF1(F.size());
		vector<cmplx> FF2(F.size());

		for(size_t i=0;i<N;++i) {
			const _per_len   k=dk*idx2k(i,N);
			const _per_len2 k2=k*k;

			const cmplx fdx(1,k/_per_len(1));
			FF1[i]=fdx*F[i];

			const cmplx fdxdx(-k2/_per_len2(1),1);
			FF2[i]=fdxdx*F[i];
		}

		const vector<cmplx> dfdx  =iFt(FF1);
		const vector<cmplx> dfdxdx=iFt(FF2);
		const vector<flt> dffdx   =iFFt(FF1);
		const vector<flt> dffdxdx =iFFt(FF2);

		cout << "fdx/dxdx="  << dfdx << dfdxdx << endl;
		cout << "ffdx/dxdx=" << dffdx << dffdxdx << endl;
		cout << "efdx/dxdx=" << efdx << efdxdx;
		*/
	}
	{
	/*
		test.Subsub("Greens");
		const size_t N=10;
		array3d<cmplx> f(N,N,N),efdxdx(N,N,N);

		const _len dx(0.1);
		const _len bbox=dx*f.sizex();
		const _per_len dk=2*_pi/bbox;
		//const flt dx = 2*Constants::_pi/(dk*n);

		cout << "dx=" << dx << " bbox=" << bbox << " dk=" << dk << endl;

		for(size_t k=0;k<f.sizez();++k)
		for(size_t j=0;j<f.sizey();++j)
		for(size_t i=0;i<f.sizex();++i) {
			const flt x=i*dx/_len(1);
			const flt y=j*dx/_len(1);
			const flt z=k*dx/_len(1);
			f     (i,j,k)=phival(x,y,z);
			efdxdx(i,j,k)=expected_dxdxphi(x,y,z);
			cout << "  idx=" << Triple<flt>(x,y,z) << " f=" << f(i,j,k) <<  " ef=" << efdxdx(i,j,k) << endl;
		}

		array3d<cmplx> F=Ft3(f);

		for(size_t k=0;k<N;++k)
		for(size_t j=0;j<N;++j)
		for(size_t i=0;i<N;++i) {
			const flt idx=idx2klen(t_idx(i,j,k),N);
			const _per_len k_len=dk*idx;
			const _per_len2 k2=k_len*k_len

			const flt dx=cmplx(0,idx2k(i)*dk)*F(i,j,k);
			const flt dy=cmplx(0,idx2k(j)*dk)*F(i,j,k);
			const flt dz=cmplx(0,idx2k(k)*dk)*F(i,j,k);
		}
		array3d<cmplx> iF=iF3(f);

		//const array3d<complex<_len> > Gdx  =Greens_dx<flt,_len>(dk,F);
		//const array3d<complex<_len> > Gdy  =Greens_dy<flt,_len>(dk,F);
		//const array3d<complex<_len> > Gdz  =Greens_dz<flt,_len>(dk,F);
		const array3d<cmplx> Gdxdx=Greens_dxdx<flt,flt>(dk,F);
		const array3d<cmplx> Gdxdy=Greens_dxdy<flt,flt>(dk,F);
		const array3d<cmplx> Gdxdz=Greens_dxdz<flt,flt>(dk,F);
		const array3d<cmplx> Gdydy=Greens_dydy<flt,flt>(dk,F);
		const array3d<cmplx> Gdydz=Greens_dydz<flt,flt>(dk,F);
		const array3d<cmplx> Gdzdz=Greens_dzdz<flt,flt>(dk,F);

		//const array3d<cmplx> fdx=iFt3(Gdx);
		//const array3d<cmplx> fdy=iFt3(Gdy);
		//const array3d<cmplx> fdz=iFt3(Gdz);
		const array3d<cmplx> fdxdx=iFt3(Gdxdx);
		const array3d<cmplx> fdxdy=iFt3(Gdxdy);
		const array3d<cmplx> fdxdz=iFt3(Gdxdz);
		const array3d<cmplx> fdydy=iFt3(Gdydy);
		const array3d<cmplx> fdydz=iFt3(Gdydz);
		const array3d<cmplx> fdzdz=iFt3(Gdzdz);

		cout << "fdxdx=" << fdxdx << endl;
		for(size_t n=0;n<fdxdx.size();++n) cout << fdxdx[n].real()/(bbox*bbox*bbox) << endl;
		cout << efdxdx;
	*/
	}
	{
		test.Subsub("Growthfuncton");
		Cosmo c(0.27,0.73,0.);
		flt f=c.GrowthFunction(1.);
		test( Proximyeps(f,0.76001,1E-6));
		f=c.GrowthFunction(0.5);
		test( Proximyeps(f,0.473345,1E-6));
		f=c.GrowthFunction(0.1);
		test( Proximyeps(f,0.0999509,1E-7));

		f=Cosmo(1.,1.,0.).GrowthFunction(1.);
		test( Proximyeps(f,1.63525,1E-5));
		f=Cosmo(0.1,1.,0.).GrowthFunction(1.);
		test( Proximyeps(f,0.763382,1E-6));
	}
}

void TestMpi(Testsuite& test,Mpi& m)
{
	#ifdef USE_MPI
	test.Header("Mpi");
	test.Sub("Initialization");
	string s;

	if (m.Size()==1) return;
	test( m.Lower(10)<=m.Upper(10) );
	test( m.Upper(10)<=10 );

	m.Barrier();
	test.Sub("Functionality");

	test.Subsub("partitioning");
	for(size_t N=0;N>123;++N){
		test( m.Lower(0,N)==0 );
		test( m.Upper(N,N)==N );
		for(size_t i=1;i<123;++i){
			if (i<N) test(m.Lower(i+1,N)==m.Upper(i,N));
			else test(m.Lower(0,N)==N && m.Lower(i,N));
		}
	}

	test.Subsub("int and string send and receive");
	if (m.Rank()==0){
		for(size_t i=1;i<m.Size();++i) m.SendInt(i,i);
	}
	else{
		const size_t n=m.RecvInt();
		test( n==m.Rank() );
	}
	m.Barrier();
	if (m.Rank()==0){
		for(size_t i=1;i<m.Size();++i) m.SendString("hello slave " + tostring(i),i);
	}
	else{
		s=m.RecvString();
		test( s=="hello slave " +  tostring(m.Rank()) );
	}

	m.Barrier();
	test.Subsub("twoway comm");
	if (m.Rank()==0){
		for(size_t i=1;i<m.Size();++i) m.SendInt(i,i);
		for(size_t i=1;i<m.Size();++i) {
			size_t n=m.RecvInt(i);
			test( n==-i );
		}
	}
	else{
		const size_t n=m.RecvInt();
		test( n==m.Rank() );
		m.SendInt( -n, 0);
	}

	m.Barrier();
	test.Subsub("template send and receive");
	if (m.Rank()==0){
		for(size_t i=1;i<m.Size();++i) m.Send(i,i);
	}
	else{
		size_t n;
		m.Recv(n);
		test( n==m.Rank() );
	}

	m.Barrier();
	if (m.Rank()==0){
		for(size_t i=1;i<m.Size();++i) {
			m.Send("hello_slave_" + tostring(i),i);
			m.Send(1.1*i,i);
		}
	}
	else{
		double x;
		m.Recv(s);
		m.Recv(x);
		test( s=="hello_slave_" +  tostring(m.Rank()) );
		test( Proximyeps_noprint(x,1.1*m.Rank(),1E-12) );
	}

	m.Barrier();
	test.Subsub("binary send/recv");

	const char c1='a';
	const char c2[]="hello world";
	const float x=_pi;
	if (!m.isMaster()){
		m.SendBinary(&c1,1,0);
		m.SendBinary(c2,11,0);
		m.SendBinary(reinterpret_cast<const char*>(&x),sizeof(x),0);
		m.SendBin(&x,1,0);
	} else {
		for(size_t i=1;i<m.Size();++i) {
			vector<char> cv=m.RecvBinary(i);
			test( cv.size()==1 && cv[0]==c1 );

			cv=m.RecvBinary(i);
			test( cv.size()==11 && cv[0]=='h' && cv[10]=='d' );

			float y=0;
			const vector<char> cc=m.RecvBinary(i);
			test( cc.size()==sizeof(y) );
			if (cc.size()==sizeof(y)) {
				memcpy(&y,&cc[0],cc.size());
				test( x==y );
			}

			y=0;
			m.RecvBin(&y,1,i);
			test( x==y );
		}
	}

	for(size_t N=1;N<113;N+=5){
		vector<char>   vc(N),vc2(N);
		vector<int>    vi(N),vi2(N);
		vector<float>  vf(N),vf2(N);
		vector<double> vd(N),vd2(N);

		for(size_t i=0;i<N;++i) {
			vc[i]='a' + i%10;
			vi[i]=i;
			vf[i]=i*_pi;
			vd[i]=i*_pi;
		}

		if (!m.isMaster()){
			m.SendBin(vc,0);
			m.SendBin(vi,0);
			m.SendBin(vf,0);
			m.SendBin(vd,0);
		}
		else{
			for(size_t i=1;i<m.Size();++i) {
				m.RecvBin(vc2,i);
				m.RecvBin(vi2,i);
				m.RecvBin(vf2,i);
				m.RecvBin(vd2,i);

				test( vc.size()==vc2.size() );
				test( vi.size()==vi2.size() );
				test( vf.size()==vf2.size() );
				test( vd.size()==vd2.size() );

				for(size_t i=0;i<N;++i) {
					test( vc[i]==vc2[i] );
					test( vi[i]==vi2[i] );
					test( vf[i]==vf2[i] );
					test( vd[i]==vd2[i] );
				}
			}
		}
	}

	m.Barrier();
	test.Subsub("CollectVector");
	for(size_t M=0;M<2;++M)
	for(size_t N=m.Size();N<114;++N){
		vector<float> n;
		vector<double> nn;
		vector<int>    nnn;
		const size_t lower=m.Lower(N);
		const size_t upper=m.Upper(N);
		for(size_t i=lower;i<upper;++i) {
			n .push_back(i*i);
			nn.push_back(-i*i);
			nnn.push_back(-i);
		}

		m.Barrier();
		m.CollectVector(n ,N,M==0);
		m.Barrier();
		m.CollectVector(nn,N,M==0);
		m.Barrier();
		m.CollectVector(nnn,N,M==0);
		m.Barrier();

		if (m.isMaster()){
			test( n.size()==N );
			test( nn.size()==N );
			for(size_t i=0;i<n.size();++i) {
				if(M==0){
					test( n [i]==i*i );
					test( nn[i]==-i*i );
				} else {
					test( Proximyeps_noprint(n [i],i*i ,1E-5) );
					test( Proximyeps_noprint(nn[i],-i*i,1E-5) );
				}
				test( -nnn[i]==static_cast<int>(i) );
			}
		}
	}

	m.Barrier();
	test.Subsub("CollectMap");
	for(size_t M=0;M<2;++M)
	for(size_t N=1;N<114;++N){
		map<int,float>        n;
		map<long long,double> nn;
		map<long long,long long> nnn;
		map<int ,bool>           nnnn;
		for(size_t i=0;i<N;++i) {
			n [2*i]=i;
			nn[i]=-1.0*i*i;
			nnn [i+1000*m.Rank()]=-i;
			nnnn[i+1000*m.Rank()]=i%2;
		}

		m.Barrier();
		m.CollectMap(n,M==0,2);
		m.Barrier();

		map<long long,double> nn2=nn;
		m.CollectMap(nn ,M==0,1);
		m.Barrier();
		m.CollectMap(nn2,M==0,2);
		m.Barrier();
		m.CollectMap(nnn,M==0,0);
		m.Barrier();
		m.CollectMap(nnnn,M==0,0);
		m.Barrier();

		if (m.isMaster()){
			test( n.size() ==N );
			test( nn.size()==N );
			test( nn2.size()==N );
			test( nnn.size()==N*m.Size() );
			test( nnnn.size()==N*m.Size() );
			for(size_t i=0;i<n.size();++i) {
				test( n [2*i]== i*m.Size() );
				if (M==0) test( nn[i]==-1.0*i*i );
				else test( Proximyeps_noprint(nn[i],-1.0*i*i  ,1E-5) );
				test( Proximyeps_noprint(nn2[i],-1.0*m.Size()*(i*i),1E-4) );
			}
			for(size_t j=0;j<m.Size();++j)
			for(size_t i=0;i<N;++i) {
				test( nnn .find(i+1000*j)!=nnn .end() && nnn .find(i+1000*j)->second==static_cast<int>(-i) );
				test( nnnn.find(i+1000*j)!=nnnn.end() && nnnn.find(i+1000*j)->second==i%2 );
			}
		}
	}
/*
	test.Subsub("some real scenario, classes, tags, source and destinations");
	if (m.Rank()==0){
		vector<_point_len> x,xx;
		for(size_t i=0;i<13;++i) x.push_back(i*i);
		_len bbox(42);

		// master...send data
		for(size_t i=1;i<m.Size();++i){
			m.Send(string("Mpi__tag"),i);
			m.Send(x                 ,i);
			m.Send(bbox              ,i);
		}

		for(size_t i=1;i<m.Size();++i){
			m.Recv(s,MPI_ANY_SOURCE);
			if( s!="Mpi__result") throw_("bad format in DistAllToAll2()");
			const size_t node=m.RecvNode();
			size_t n0,n1,r;
			m.Recv(r ,node);
			m.Recv(n0,node);
			m.Recv(n1,node);
			m.Recv(xx,node);

			test( r==node );
			test( n0>=m.Upper(10) );
			test( n1> m.Upper(10) );
			test( xx==x );
		}
	} else {
		string s;
		m.Recv(s,0);
		if ( s=="Mpi__tag") {
			vector<pointd> y;
			_len bbox2;
			m.Recv(y    ,0);
			m.Recv(bbox2,0);

			m.Send("Mpi__result",0);
			m.Send(m.Rank()   ,0);
			m.Send(m.Lower(10),0);
			m.Send(m.Upper(10),0);
			m.Send(y          ,0);
		}
	}

    /// XXX has no working broadcast function yet

	m.Barrier();
	test.Subsub("Broadcast");
	int n=m.Rank();
	m.BroadcastInt(n,0);
	test( n==0 );

	s="broadcast_" + tostring(m.Rank()) ;
	m.Broadcast(s,0);
	test( s=="broadcast_0");

	if(m.Rank()!=0) MpiSlaveWorker(m);
	else {
		vector<_point_len> y;
		for(size_t i=0;i<100;++i) y.push_back(_point_len(i,2*i,3*i));
		_len bbox(40000);

		vector<_len2> d2all=DistAllToAll2(y,bbox,m);
	}
*/

	#endif // USE_MPI
}

void TestGrafic2Gadget(Testsuite& test)
{
	test.Header("TestGrafic2Gadget");
	ifstream s((testdatapath_inputs + "ic_velbx").c_str());
	GraficHeader h(s);
	test( "GraficHeader: { np=(32;32;32) dx=0.5 x=(0;0;0) astart=0.0286465 Om=0.35 Ov=0.65 H0=65}" == tostring(h) );

	const flt e=1E-7;
	test( h.gridsize()==32768 );
	test( h.sizet()==Triple<size_t>(32,32,32) );
	test( Proximyeps(h.geth() ,_h(0.65),e) );
	test( Proximyeps(h.getOm(),0.35,e) );
	test( Proximyeps(h.getOl(),0.65,e) );
	test( Proximyeps(h.geta() ,0.0286465,e*10) );

	test( Proximyeps(h.getdx() ,_len(0.5*1000*tonum(h.geth())),e) );
	test( Proximyeps(h.getx1o(),_len(0),e) );
	test( Proximyeps(h.getx2o(),_len(0),e) );
	test( Proximyeps(h.getx3o(),_len(0),e) );

	ofstream os((testdatapath_refs + "out").c_str() );
	os << "% a v\n";
	const flt dx=0.01;
	for(flt a=dx;a<2;a+=dx){
		//test( Proximyeps(h.Vfact(),_len(0),e) );
		h.seta(a);
		os << a << " " << h.Vfact() << "\n";
	}
	os.close();
	test.DiffFile(testdatapath_refs + "test_grafic2gadget_01.ref",testdatapath_refs + "out",test.Bool(0));

	const string lvd=testdatapath_inputs + "grafic2gadget";
	test.System("grafic2gadget -t 2 -cd " + lvd + " 1>/dev/null");
	test.System("cp " + lvd + "/ic_gadget.g " + testdatapath_refs + "out");
	test.System("ghead " + testdatapath_refs + "out >" + testdatapath_refs + "out2"),
	test.DiffFile(testdatapath_refs + "test_grafic2gadget_02.ref",testdatapath_refs + "out2",test.Bool(0));

	test.System("grafic2gadget -c -t 1 -cd " + lvd + " 1>/dev/null");
	test.System("mv " + lvd + "/ic_gadget.g " + testdatapath_refs + "out");
	test.System("ghead " + testdatapath_refs + "out >" + testdatapath_refs + "out2"),
	test.DiffFile(testdatapath_refs + "test_grafic2gadget_03.ref",testdatapath_refs + "out2",test.Bool(0));

	test.System("toascii " + testdatapath_refs + "out -o" + testdatapath_refs + "out2"),
	test.System("sort <" + testdatapath_refs + "out2 >" + testdatapath_refs + "out3");
	test.DiffFile(testdatapath_refs + "test_grafic2gadget_04.ref",testdatapath_refs + "out3",test.Bool(0));
}

void TestGHead(Testsuite& test)
{
	test.Header("TestGHead");

	test.System("ghead " + testdatapath_inputs + "galaxy_littleendian.dat >" + testdatapath_refs + "out 2>/dev/null");
	test.DiffFile(testdatapath_refs + "test_ghead_02.ref",testdatapath_refs + "out",test.Bool(0));

	// cannot handle these files yet,
	//	test.System("ghead " + testdatapath_inputs + "cluster_littleendian.dat >" + testdatapath_refs + "out");
	//	test.DiffFile(testdatapath_refs + "test_ghead_01.ref",testdatapath_refs + "out",test.Bool(0));
	//	test.System("ghead " + testdatapath_inputs + "gassphere_littleendian.dat >" + testdatapath_refs + "out");
	//	test.DiffFile(testdatapath_refs + "test_ghead_03.ref",testdatapath_refs + "out",test.Bool(0));

	test.System("ghead " + testdatapath_inputs + "lcdm_gas_snapshot_005 >" + testdatapath_refs + "out");
	test.DiffFile(testdatapath_refs + "test_ghead_04.ref",testdatapath_refs + "out",test.Bool(0));
	test.System("ghead " + testdatapath_inputs + "my_snapshot_005 >" + testdatapath_refs + "out");
	test.DiffFile(testdatapath_refs + "test_ghead_05.ref",testdatapath_refs + "out",test.Bool(0));
	test.System("ghead " + testdatapath_inputs + "my2_snapshot_005 >" + testdatapath_refs + "out");
	test.DiffFile(testdatapath_refs + "test_ghead_06.ref",testdatapath_refs + "out",test.Bool(0));

	test.Sub("Ghead - particle info");
	const string cmd=testdatapath_inputs + "my2_snapshot_021  >>" + testdatapath_refs + "out ";
	test.System("rm " + testdatapath_refs + "out");

	for(int i=0;i<10;++i){
		test.System("ghead     -p "+ tostring(i) + " " + cmd);
		test.System("ghead -id -p "+ tostring(i) + " " + cmd);
	}
	test.System("ghead     -p 0 "     + cmd);
	test.System("ghead     -p 1 "     + cmd);
	test.System("ghead     -p 98302 " + cmd);
	test.System("ghead     -p 98303 " + cmd);
	test.System("ghead -id -p 0 "     + cmd);
	test.System("ghead -id -p 1 "     + cmd);
	test.System("ghead -id -p 98302 " + cmd);
	test.System("ghead -id -p 98303 " + cmd);

	test.DiffFile(testdatapath_refs + "test_ghead_07.ref",testdatapath_refs + "out",test.Bool(0));
}

void TestGFilter(Testsuite& test)
{
	test.Header("GFilter");

	test.Subsub("nosort");
	test.System("gfilter " + testdatapath_inputs + "my2_snapshot_005 -ns -o " + testdatapath_refs + "out 2>/dev/null");
	test.System("ghead "   + testdatapath_refs + "out >" + testdatapath_refs  + "out2  2>/dev/null");
	test.DiffFile(testdatapath_refs + "test_gfilter_01.ref",testdatapath_refs + "out2",test.Bool(0));

	if (test.Verbose()) {
		test.System("toascii "   + testdatapath_refs + "out -i -o | head -n 100 >" + testdatapath_refs+ "out2  2>/dev/null");
		test.DiffFile(testdatapath_refs + "test_gfilter_02.ref",testdatapath_refs + "out2",test.Bool(0));
	}

	test.System("gfilter " + testdatapath_inputs + "my2_snapshot_005 -o " + testdatapath_refs + "out 2>/dev/null");
	test.System("ghead "   + testdatapath_refs + "out >" + testdatapath_refs  + "out2  2>/dev/null");
	test.DiffFile(testdatapath_refs + "test_gfilter_03.ref",testdatapath_refs + "out2",test.Bool(0));

	if (test.Verbose()) {
		test.System("toascii "   + testdatapath_refs + "out -i -o | head -n 100 >" + testdatapath_refs+ "out2  2>/dev/null");
		test.DiffFile(testdatapath_refs + "test_gfilter_04.ref",testdatapath_refs + "out2",test.Bool(0));
	}

	test.System("gfilter " + testdatapath_inputs + "my2_snapshot_005 -f 3 -o " + testdatapath_refs + "out 2>/dev/null");
	test.System("ghead "   + testdatapath_refs + "out >" + testdatapath_refs  + "out2  2>/dev/null");
	test.DiffFile(testdatapath_refs + "test_gfilter_05.ref",testdatapath_refs + "out2",test.Bool(0));

	if (test.Verbose()) {
		test.System("toascii "   + testdatapath_refs + "out -i -o | head -n 100 >" + testdatapath_refs+ "out2  2>/dev/null");
		test.DiffFile(testdatapath_refs + "test_gfilter_06.ref",testdatapath_refs + "out2",test.Bool(0));
	}
}

void TestToAscii(Testsuite& test)
{
	test.Header("TestToAscii");

	test.System("toascii " + testdatapath_inputs + "lcdm_gas_snapshot_005 -o -mascii -fall | head -n 100 >" + testdatapath_refs + "out");
	test.DiffFile (testdatapath_refs + "test_toascii_01.ref",testdatapath_refs + "out",test.Bool(0));
	test.System("toascii " + testdatapath_inputs + "lcdm_gas_snapshot_005 -o -mascii -fgas | head -n 100 >" + testdatapath_refs + "out");
	test.DiffFile (testdatapath_refs + "test_toascii_02.ref",testdatapath_refs + "out",test.Bool(0));
	test.System("toascii " + testdatapath_inputs + "lcdm_gas_snapshot_005 -o" + testdatapath_refs + "out -mfloat  -fall");
	test.DiffFile (testdatapath_refs + "test_toascii_03.ref",testdatapath_refs + "out",test.Bool(0));
	test.System("toascii " + testdatapath_inputs + "lcdm_gas_snapshot_005 -o" + testdatapath_refs + "out -mdouble -fgas");
	test.DiffFile (testdatapath_refs + "test_toascii_04.ref",testdatapath_refs + "out",test.Bool(0));
	test.System("toascii " + testdatapath_inputs + "lcdm_gas_littleendian.dat -o -i -mascii  -fall | head -n 100 >" + testdatapath_refs + "out");
	test.DiffFile (testdatapath_refs + "test_toascii_05.ref",testdatapath_refs + "out",test.Bool(0));
	test.System("toascii " + testdatapath_inputs + "lcdm_gas_littleendian.dat -o" + testdatapath_refs + "out -i -mdouble -fall");
	test.DiffFile (testdatapath_refs + "test_toascii_06.ref",testdatapath_refs + "out",test.Bool(0));
	test.System("toascii " + testdatapath_inputs + "lcdm_gas_littleendian.dat -o"  +testdatapath_refs + "out -i -mdouble -fgas");
	test.DiffFile (testdatapath_refs + "test_toascii_07.ref",testdatapath_refs + "out",test.Bool(0));

	// some of these are wrong in filtering
	test.System("toascii " + testdatapath_inputs + "my2_snapshot_005 -o -mascii | head >" + testdatapath_refs + "out");
	test.DiffFile (testdatapath_refs + "test_toascii_08.ref",testdatapath_refs + "out",test.Bool(0));
	test.System("toascii " + testdatapath_inputs + "my2_snapshot_005 -o -mascii -fhalo | head >" + testdatapath_refs + "out");
	test.DiffFile (testdatapath_refs + "test_toascii_09.ref",testdatapath_refs + "out",test.Bool(0));
	test.System("toascii " + testdatapath_inputs + "my2_snapshot_005 -o -mascii -fgas | head >" + testdatapath_refs + "out");
	test.DiffFile (testdatapath_refs + "test_toascii_10.ref",testdatapath_refs + "out",test.Bool(0));
	test.System("toascii " + testdatapath_inputs + "my2_snapshot_005 -o -mascii -fbndry | head >" + testdatapath_refs + "out");
	test.DiffFile (testdatapath_refs + "test_toascii_11.ref",testdatapath_refs + "out",test.Bool(0));
}

void TestTwopointCorr(Testsuite& test)
{
	test.Header("TestTwopointCorr");

	test.Sub("01");
	test.System("gendata " + testdatapath_refs + "out 50000 kpc/h -g 0 100 1 msun1E10/h -testsuite >/dev/null");
	test.System("twopointcorr " + testdatapath_refs + "out 50  -r 1000 -testsuite 2>/dev/null >" + testdatapath_refs + "out2");
	test.DiffFile(testdatapath_refs + "test_2p_01.ref",testdatapath_refs + "out2",test.Bool(0));

	test.Subsub("Fast RR");
	test.System("twopointcorr " + testdatapath_refs + "out 50 -r 1000 -g -testsuite 2>/dev/null >" + testdatapath_refs + "out2");
	test.DiffFile(testdatapath_refs + "test_2p_01_fast.ref",testdatapath_refs + "out2",test.Bool(0));

	test.Sub("02");
	test.System("gendata " + testdatapath_refs + "out 50000 kpc/h -g 0 332 1 msun1E10/h -g 1 207 1 msun1E10/h -testsuite 2>/dev/null >/dev/null");
	test.System("twopointcorr " + testdatapath_refs + "out 50 -f 63 -r 1000 -testsuite  2>/dev/null >" + testdatapath_refs + "out2");
	test.DiffFile(testdatapath_refs + "test_2p_02.ref",testdatapath_refs + "out2",test.Bool(0));

	test.Subsub("Fast RR");
	test.System("twopointcorr " + testdatapath_refs + "out 50 -f 63 -r 1000 -testsuite -g 2>/dev/null >" + testdatapath_refs + "out2");
	test.DiffFile(testdatapath_refs + "test_2p_02_fast.ref",testdatapath_refs + "out2",test.Bool(0));

	test.Sub("03");
	test.System("gendata " + testdatapath_refs + "out 50000 kpc/h -g 0 17 1 msun1E10/h -g 1 119 1 msun1E10/h -g 5 299 1 msun1E10/h -testsuite 2>/dev/null >/dev/null");
	test.System("twopointcorr " + testdatapath_refs + "out 100 -f 63 -r 1000 -testsuite 2>/dev/null >" + testdatapath_refs + "out2");
	test.DiffFile(testdatapath_refs + "test_2p_03.ref",testdatapath_refs + "out2",test.Bool(0));

	test.Subsub("Fast RR");
	test.System("twopointcorr " + testdatapath_refs + "out 100 -f 63 -r 1000 -testsuite -g 2>/dev/null >" + testdatapath_refs + "out2");
	test.DiffFile(testdatapath_refs + "test_2p_03_fast.ref",testdatapath_refs + "out2",test.Bool(0));

	test.Sub("04");
	test.System("twopointcorr " + testdatapath_inputs + "my2_snapshot_021 40 -f 32 -r 1000 -testsuite 2>/dev/null >" + testdatapath_refs + "out2");
	test.DiffFile(testdatapath_refs + "test_2p_04.ref",testdatapath_refs + "out2",test.Bool(0));

	test.Subsub("Fast RR");
	test.System("twopointcorr " + testdatapath_inputs + "my2_snapshot_021 40 -f 32 -r 1000 -testsuite -g 2>/dev/null >" + testdatapath_refs + "out2");
	test.DiffFile(testdatapath_refs + "test_2p_04_fast.ref",testdatapath_refs + "out2",test.Bool(0));
}

void TestMassInSphere(Testsuite& test)
{
	test.Header("MassInSphere");

	test.Sub("01");
	test.System("massinsphere " + testdatapath_inputs + "my2_snapshot_021 -8 -r 10 -f 32 -testsuite >" + testdatapath_refs + "out 2>/dev/null" );
	test.DiffFile(testdatapath_refs + "test_massinsphere_01.ref",testdatapath_refs + "out",test.Bool(0));
	test.Sub("02");
	test.System("massinsphere " + testdatapath_inputs + "my2_snapshot_021 2 kpc/h 1000 kpc/h 300 kpc/h -r 10 -f 32  -testsuite >" + testdatapath_refs + "out 2>/dev/null" );
	test.DiffFile(testdatapath_refs + "test_massinsphere_02.ref",testdatapath_refs + "out",test.Bool(0));
	test.Sub("03");
	test.System("massinsphere " + testdatapath_inputs + "my2_snapshot_021 10 kpc/h 20 kpc/h 1 kpc/h -r 10 -f 32 -testsuite >" + testdatapath_refs + "out 2>/dev/null" );
	test.DiffFile(testdatapath_refs + "test_massinsphere_03.ref",testdatapath_refs + "out",test.Bool(0));
	test.Sub("04");
	test.System("massinsphere " + testdatapath_inputs + "my2_snapshot_021 10 kpc/h 20 kpc/h 1.1 kpc/h -l -r 10 -f 32 -testsuite >" + testdatapath_refs + "out 2>/dev/null" );
	test.DiffFile(testdatapath_refs + "test_massinsphere_04.ref",testdatapath_refs + "out",test.Bool(0));

	// fast mode, just case 01 and 02 (-r changed to 100), with -n option
	test.Sub("05");
	test.System("massinsphere " + testdatapath_inputs + "my2_snapshot_021 -8 -r 10 -f 32 -testsuite -n >" + testdatapath_refs + "out 2>/dev/null" );
	test.DiffFile(testdatapath_refs + "test_massinsphere_05.ref",testdatapath_refs + "out",test.Bool(0));
	test.Sub("06");
	test.System("massinsphere " + testdatapath_inputs + "my2_snapshot_021 2 kpc/h 1000 kpc/h 300 kpc/h -r 100 -f 32 -testsuite -n >" + testdatapath_refs + "out 2>/dev/null" );
	test.DiffFile(testdatapath_refs + "test_massinsphere_06.ref",testdatapath_refs + "out",test.Bool(0));
}

void TestRunCmd(Testsuite& test,const string& cmd,const string reffile,const int n)
{
	const string nn=(n<9 ? "0" : "") + tostring(n);
	test.Subsub(nn);
	test.System(cmd + " >" + testdatapath_refs + "out");
	test.DiffFile(testdatapath_refs + reffile + "_" + nn + ".ref",testdatapath_refs + "out",test.Bool(0));
}

void TestConvert(Testsuite& test)
{
	test.Header("Convert");

	test.Sub("tf2pk");
	int n=0;
	TestRunCmd(test,"convert tf2pk " + testdatapath_inputs + "lcdm_tf00.dat"                 ,"test_convert_tp2pk",++n);
	TestRunCmd(test,"convert tf2pk " + testdatapath_inputs + "tfcmb_lcdm_matter_49.dat"      ,"test_convert_tf2pk",++n);
	TestRunCmd(test,"convert tf2pk " + testdatapath_inputs + "tfcmb_lcdm_matter_00.dat"      ,"test_convert_tf2pk",++n);
	TestRunCmd(test,"convert tf2pk " + testdatapath_inputs + "tfcmb_norm_lcdm_matter_49.dat" ,"test_convert_tf2pk",++n);
	TestRunCmd(test,"convert tf2pk " + testdatapath_inputs + "tfcmb_norm_lcdm_matter_00.dat" ,"test_convert_tf2pk",++n);
	TestRunCmd(test,"convert tf2pk " + testdatapath_inputs + "tfcmb_alone_lcdm_matter_49.dat","test_convert_tf2pk",++n);
	TestRunCmd(test,"convert tf2pk " + testdatapath_inputs + "tfcmb_alone_lcdm_matter_00.dat","test_convert_tf2pk",++n);

	test.Sub("pk2xi");
	n=0;
	System         ("convert tf2pk " + testdatapath_inputs + "lcdm_tf00.dat >" + testdatapath_refs + "out2" );
	TestRunCmd(test,"convert pk2xi " + testdatapath_refs + "out2 0.0062831853 h/kpc 0.062831853 h/kpc 0.062831853 h/kpc","test_convert_pk2xi",++n);
	TestRunCmd(test,"convert pk2xi " + testdatapath_refs + "out2 0.0314159265 h/kpc 0.062831853 h/kpc 0.881232161 h/kpc","test_convert_pk2xi",++n);

	test.Sub("pk2sigma");
	n=0;
	TestRunCmd(test,"convert pk2sigma " + testdatapath_refs + "out2 0.0062831853 h/kpc 0.062831853 h/kpc 0.062831853 h/kpc","test_convert_pk2sigma",++n);
	TestRunCmd(test,"convert pk2sigma " + testdatapath_refs + "out2 0.0314159265 h/kpc 0.062831853 h/kpc 0.881232161 h/kpc","test_convert_pk2sigma",++n);

	test.Sub("xi2pk");
	n=0;
	TestRunCmd(test,"convert xi2pk " + testdatapath_refs + "test_convert_pk2xi_01.ref 50000 kpc/h","test_convert_xi2pk",++n);
	TestRunCmd(test,"convert xi2pk " + testdatapath_refs + "test_convert_pk2xi_02.ref 50000 kpc/h","test_convert_xi2pk",++n);
}

void TestPower(Testsuite& test)
{
	test.Header("Power");
	const string options="50000 kpc/h";

	test.Sub("Dummy data");
	for(int i=8;i<=64;i*=2)
	{
		test.Subsub("dummy power "+ tostring(i) );

		array3d<flt> f(i,i,i);
		flt x=0;
		for(array3d<flt>::iterator itt=f.begin();itt!=f.end();++itt) *itt = ++x;

		f.Writeascii(testdatapath_refs + "out");
		test.System("power " + testdatapath_refs + "out" +  " " + options + " >" + testdatapath_refs + "out2");
		test.DiffFile(testdatapath_refs + "test_power_dummydata.01." + i + ".ref",testdatapath_refs + "out2",test.Bool(0) );

		f=0;
		f.Writeascii(testdatapath_refs + "out");
		test.System("power " + testdatapath_refs + "out" +  " " + options + " >" + testdatapath_refs + "out2");
		test.DiffFile(testdatapath_refs + "test_power_dummydata.02." + i + ".ref",testdatapath_refs + "out2",test.Bool(0) );

		f=42;
		f.Writeascii(testdatapath_refs + "out");
		test.System("power " + testdatapath_refs + "out" +  " " + options + " >" + testdatapath_refs + "out2");
		test.DiffFile(testdatapath_refs + "test_power_dummydata.03." + i + ".ref",testdatapath_refs + "out2",test.Bool(0) );

		f(0,0,0)=1000;
		f(7,6,3)=100;
		f(i-1,i-1,i-1)=-0.9;
		f.Writeascii(testdatapath_refs + "out");
		test.System("power " + testdatapath_refs +"out" + " " + options + " >" + testdatapath_refs + "out2");
		test.DiffFile(testdatapath_refs + "test_power_dummydata.04." + i + ".ref",testdatapath_refs + "out2",test.Bool(0) );
	}

	test.Sub("Real data");
	const string power_opt="50000 kpc/h -b 40";

	for(int i=32;i<=128;i*=2) {
		test.Subsub("real power " + tostring(i) );
		test.System("density " + testdatapath_inputs + "my2_snapshot_021 " + i + " -f 32 -cic >" + testdatapath_refs +"out");
		test.System("power   " + testdatapath_refs + "out" +  " " + options + " >" + testdatapath_refs + "out2");
		test.DiffFile(testdatapath_refs + "test_power." + i + ".ref",testdatapath_refs + "out2",test.Bool(0) );
	}
}

void TestDensity(Testsuite& test)
{
	test.Header("Density");

	const string density_file=testdatapath_inputs+"my2_snapshot_021";
	const string density_opt="-f 32 2>/dev/null ";

	for(int i=3;i<=9;i+=3)
	{
		test.Subsub((string("density plain") + i).c_str() );
		const string opt=string("density ") +  density_file + " " + i + " " + density_opt;

		test.System(opt + " >" + testdatapath_refs + "out");
		test.DiffFile(string(testdatapath_refs + "test_density_plain.") + i + ".ref" , testdatapath_refs + "out",test.Bool(0) );

		test.Subsub((string("density search opt") + i).c_str() );
		test.System(opt + " -sr >" + testdatapath_refs + "out");
		test.DiffFile(string(testdatapath_refs + "test_density_plain_sr.") + i + ".ref" , testdatapath_refs + "out",test.Bool(0) );

		test.Subsub((string("density cic") + i).c_str() );
		test.System(opt + " -cic >" + testdatapath_refs + "out");
		test.DiffFile(string(testdatapath_refs + "test_density_plain_cic.") + i + ".ref" , testdatapath_refs + "out",test.Bool(0) );
	}
}

bool TestAll(const bool verbose,const bool genrefs)
{
	Mpi mpi;
	string outfile="testsuite";
	if (!mpi.isMaster()) outfile +="_slave_" + tostring(mpi.Rank());
	outfile += ".txt";

	ofstream os(outfile.c_str());
	Testsuite t(os,cerr);
	Testsuite& test=t; // alias

	test.Setverbose(verbose);
	test.SetVerboseLevel(3);
	test.Setexceptions(false);
	test.Setbool(0,genrefs);

	if (mpi.Size()>1){
		if (mpi.isMaster()) os << "** mpi testsuite nodes=" << mpi.Size() << endl;
		else {
			TestMpi(test,mpi); // slave
			return 0;
		}
	}

	test.Header("Main test " + Config(),"all tests");


	TestBasicAssumptions(test);
	TestStringops(test);
 	TestPoint(test);
 	TestMath(test);

  	if (!DirExists("Test/Refs")) {
		warn_("Directory ./Test/Refs does not exist...skipping rest of testsuite");
		return true;
  	}

  	TestEtcClasses(test);
 	TestICFormat(test);
 	TestArray3D(test);
 	TestUnits(test);
 	TestFourier(test);
 	TestSPH(test);
 	TestSigma(test);
 	TestICFun(test);
 	TestMpi(test,mpi);

	if (test.Verbose()) {
		TestGHead(test);
		TestGFilter(test);
		TestGrafic2Gadget(test);
		TestToAscii(test);
		TestTwopointCorr(test);
		TestConvert(test);
	    TestMassInSphere(test);
		TestDensity(test);
		TestPower(test);
	}

	t.Header("");
	t.Summary();
	os << "\n";
	if (test.GetFailed()>0)
	{
		os << "** Test suite failed!\n";
		return false;
	}
	else { os << "...All " << test.GetTestcases() << " tests ok"; return true;}
}

int main(int argc,char** argv)
{
	try	{
		SetNiceLevel(4);
		const bool verbose=argc>1;
		const bool genrefs=argc>2;
		const bool b=TestAll(verbose,genrefs);
		return b ? 0 : -1;
	} CATCH_ALL;
	return -1;
}
