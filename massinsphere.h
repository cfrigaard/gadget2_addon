const string outputformat_massinspheres="r [" + UNIT_LENGTH  + "] sigma(r)^2=<m(r)^2><m(r)>^2-1 [dimless] <m(r)> [" + UNIT_MASS + "] Var(m(r)) [(" + UNIT_MASS + ")^2] sigma2_errorbar [dimless]";

flt CalcErrorbar(const vector<_mass>& masses,const _mass& mean,const flt& sigma2_dimless)
{
	if (masses.size()==0) throw_("zero vector size in CalcErrorbar function");

	flt test=0;
	vector<flt> delta;
	delta.reserve(masses.size());
	for(size_t i=0;i<masses.size();++i) {
		const flt sigma2i=masses[i]*masses[i]/(mean*mean)-1;
		delta.push_back(Pow2(sigma2i));
		test += sigma2i;
	}
	assert(_proximy( test/(masses.size()),sigma2_dimless,1E-6) );

	flt deltarms,deltasigma2;
	MeanVar(delta,deltarms,deltasigma2);
	return Sqrt( deltasigma2/masses.size() );
}

void Massinsphere(const vector<particle_data_2>& v,const _len& r0,const _len& r1,const _len& rstep,const _bbox_len& bbox,const size_t& Nrandom,const t_gridparameters& gridparameters,const bool searchinradius,const bool logarithmic,const bool numberdensity,const bool integrationtables,const pair<int,int>& integrationparameters,const bool analytic,const string& gadgetfile)
{
	const size_t gridsize=gridparameters.first;
	const size_t steps   =gridparameters.second;

	Mpi mpi;
	if (mpi.isMaster()) {
		cout << "% Massinsphere: r0=" << r0 << " " << UNIT_LENGTH << " r1=" << r1 << " " << UNIT_LENGTH
		     << " rstep=" << rstep << " " << UNIT_LENGTH << " bbox=" << bbox << " Nrandom=" << Nrandom
		     << " gridsize=" << gridsize << " steps=" << steps;
		cout << (searchinradius ? " searchinradius=true" : "" )  << (logarithmic ? " logarithmic=true" : "");
		cout << (numberdensity?" numberdensity=true" : "")<<(integrationtables?" integrationtables=true":"");
		if (integrationtables) cout << " hrsteps=" << integrationparameters.first << " lookupsres="
		     << integrationparameters.second;
		if (analytic) cout << " analytic=true";
		cout << endl;
	}
	if (logarithmic && rstep<_len(0))  throw_("bad step size in logarithmic stepping, should be > 1.0");
	if (Nrandom<2) throw_("bad Nrandom value");
	if (r1>bbox()/2) throw_("upper range larger than bbox/2");

	const size_t lower=mpi.Lower(Nrandom);
	const size_t upper=mpi.Upper(Nrandom);

	// const size_t N=(upper-lower)*static_cast<size_t>(logarithmic ? /*Ln*/(r1/r0)/Ln(rstep/_len(1))+.5 :(r1-r0)/rstep+.5 );
	const size_t N=(upper-lower)*getsteps(r0,r1,rstep,logarithmic);

	mpi << "% finding smoothing lenghts..." << mpiendl;
	searchgrid s(v,_len(0),bbox(),gridsize,true);
	const pair<_len,_len> smoothdata=FindSmoothingLengths(v,s,33,mpi.isMaster(),gadgetfile,mpi.isMaster());
	if (searchinradius) {
		mpi <<"% adding smoothlength data to searchcube..." << mpiendl;
		s.Addsmoothinglenghts();
	}

	mpi << "% finding mass variance..." << mpiendl;
	if (mpi.isMaster()) cout << "%   output: " << outputformat_massinspheres << endl;

	timer tim;
	size_t n=0;
	map<const particle_data_2*,bool> searched;
	Wprecalc w(integrationparameters.first,steps,integrationparameters.second,bbox,analytic);

	for(_len r=r0;r<r1;logarithmic ? r*=rstep/_len(1) : r+=rstep) {
		vector<_mass> masses;
		masses.reserve(Nrandom);

		for(size_t i=lower;i<upper;++i) {
			const _point_len p(bbox()*Random(),bbox()*Random(),bbox()*Random());
			_mass  mtemp(0);

			if(searchinradius){
				const searchgrid::t_map_data m=s.find_withinradius(p,r,true);
				searched.clear();

				for(searchgrid::t_map_data::const_iterator itt2=m.begin();itt2!=m.end();++itt2){
					const particle_data_2* d=itt2->second.first;
					map<const particle_data_2*,bool>::const_iterator itt3=searched.find(d);
					if (itt3==searched.end()){
						if (integrationtables) mtemp += w.Lookup        (p,r,*d,numberdensity);
						else                   mtemp += FindMassInSphere(p,r,*d,bbox,steps,numberdensity,analytic);
						searched[d]=true;
					}
				}
			}
			else {
				for(vector<particle_data_2>::const_iterator itt=v.begin();itt!=v.end();++itt){
					if (integrationtables) mtemp +=         w.Lookup(p,r,*itt,numberdensity);
					else                   mtemp += FindMassInSphere(p,r,*itt,bbox,steps,numberdensity,analytic);
				}
			}

			masses.push_back(mtemp);
			if(mpi.isMaster()) tim.ToEta(n++,N,cerr);
		}
		// cerr << "% done on node=" << mpi << " " << tim << endl;

		mpi.CollectVector(masses,Nrandom);
		if (mpi.isMaster()) {
			_mass2 sigma2(0),rms(0);
			const _mass mean=MeanVar(masses,rms,sigma2);
			const flt sigma2_dimless=sigma2/(mean*mean);
			const int fpre=10,fpost=10;

			if (mean>_mass(0)) cout << " " << fwidth(r/_len(1),8,6) << " " << fwidth(sigma2_dimless,4,fpost);
			else               cout << "%" << fwidth(r/_len(1),8,6) << " " << fwidth("nan",4,fpost);
			cout << " " << fwidth(  mean/_mass(1) ,fpre,fpost);
			cout << " " << fwidth(sigma2/_mass2(1),fpre,fpost);

			if (mean<=_mass(0)) cout << " " << fwidth("nan",fpre,fpost) << endl;
			else{
				const flt errorbar=CalcErrorbar(masses,mean,sigma2_dimless);
				cout << " " << fwidth(errorbar,8,fpost) <<  endl;
			}

			tim.ToEta(r-r0,r1-r0,cerr);
			w.ReportStatistics("out"); // XXX for dbg/report only
		}
	}
}

int Usage_Massinsphere(const args& a)
{
	cerr << "Usage: " << a[0] << " <inputfile> {<rmin> <rmax> <rstep> | -8} [-a] [-f <filter>]"
	     << " [-i] [-is <xsteps> <hrsteps>] [-l] [-n] [-r <Nrandom>] [-s <grid> <step>] [-sr]\n";
	cerr << "   " << Version() <<  " " << Config() <<  " \n";
	cerr << "   inputfile: snapshot file\n";
	cerr << "   rmin,rmax,rstep, <float " << UNIT_LENGTH + ">: radius for loop parameters\n";
	cerr << "Options:\n";
	cerr << "  -8: sigma_8 mode (r=8 " << UNIT_LENGTH  << ")\n";
	cerr << "  -a: use sph sphere-in-sphere analytic lookup function\n";
	cerr << "  -f <int>: 1=gas, 2=halo, 4=disk, 8=bulge, 16=stars, 32=bndry\n";
	cerr << "  -i: use sph integration lookup tables for speedup\n";
	cerr << "  -is <int> <int>: sph lookup parameters, -is implies -i, default=50 50\n";
	cerr << "  -l: step logarithmic, rstep then implies r_next *= rstep\n";
	cerr << "  -n: do not integrate mass, use numberdensity <n(r)^2>/<n(r)>^2-1,"
	     << " instead of masses\n";
	cerr << "  -r <int>: number N random points for mean calculation, default=100\n";
	cerr << "  -s <int> <int>: search grid size and integration steps, default=20 20\n";
	cerr << "  -sr: use search in radius algorithm\n";
	cout << "Output: to stdout\n  " << outputformat_massinspheres << "\n";
	return -1;
}

int main_massinsphere(int argc,char** argv)
{
	try{
		args a(argc,argv,true,true);

		const bool sigma8=a.parseopt("-8");
		const bool analytic=a.parseopt("-a");
		const int  filter=a.parseval<int>("-f",0);
		const bool integrationtables=a.parseopt("-i") || a.hasopt("-is");
		const pair<int,int> integrationparameters=a.parseval<int,int>("-is",50,50);
		const bool logarithmic=a.parseopt("-l");
		const bool numberdensity=a.parseopt("-n");
		const int Nrandom=a.parseval<int>("-r",100);
		const t_gridparameters gridparameters=a.parseval<size_t,size_t>("-s",20,20);
		const bool searchinradius=a.parseopt("-sr");

		if (integrationparameters.first<=0 || integrationparameters.second<=0) throw_("bad integration parameters");
		if (Nrandom<0) throw_("bad number of random points");
		if (gridparameters.first<=0 || gridparameters.second<=0)
		     throw_("bad step parameter, must be greater than zero");
		if (analytic && integrationtables) warn_("specifying -a and -i(s) togheter is normally not needed");
		if (searchinradius) warn_("using -sr optimization may still produce unstable results");

 		if(a.size()!=8 && !(a.size()==2 && sigma8)) return Usage_Massinsphere(a);
		const _len    r0=sigma8 ? _len(8*1000)           : a.Tounit<_len>(2);
		const _len    r1=sigma8 ? _len(8*1000)+_len(0.5) : a.Tounit<_len>(4);
		const _len rstep=sigma8 ? _len(1)                : a.Tounit<_len>(6);

 		const data_info inf=load_snapshot(a[1].c_str(),1);
		const _bbox_len bbox=inf.header1.bbox();
 		vector<particle_data_2> v;
 		FilterParticles(inf,filter,v);
		Massinsphere(v,r0,r1,rstep,bbox,Nrandom,gridparameters,searchinradius,logarithmic,numberdensity
		             ,integrationtables,integrationparameters,analytic,filter==0 ? a[1] : "");

		return 0;
	}
	CATCH_ALL;
	return -1;
}
