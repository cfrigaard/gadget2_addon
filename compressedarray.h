#ifndef __COMPRESSED_ARRAY__
#define __COMPRESSED_ARRAY__

static const float CSCALE=60000.0;

class CompressedArray
{
private:
	typedef unsigned short int shortint;
	typedef Triple<shortint> shortpoint;
		
	vector<shortpoint> m_dat;
	const float m_min,m_max;
    const float m_scale;

	shortpoint toshort(const pointf& p) const
	{
		assert( p[0]>=m_min && p[0]<=m_max);
		assert( p[1]>=m_min && p[1]<=m_max);
		assert( p[2]>=m_min && p[2]<=m_max);
		const pointf p2=((p-m_min)/m_scale)*CSCALE;
		assert( p2[0] >=0 );
		assert( p2[1] >=0 );
		assert( p2[2] >=0 );
		assert( p2[0] <CSCALE );
		assert( p2[1] <CSCALE );
		assert( p2[2] <CSCALE );
		
		const shortint nx=static_cast<shortint>(p2[0]);
		const shortint ny=static_cast<shortint>(p2[1]);
		const shortint nz=static_cast<shortint>(p2[2]);
		return shortpoint(nx,ny,nz);
	}
	
	pointf topoint(const shortpoint& p) const
	{
		pointf n(p[0],p[1],p[2]);
		n /= CSCALE;
		n *= m_scale;
		n += m_min;
		return n; 
	}

public:
	CompressedArray(const float minval,const float maxval)
	: m_min(minval-1), m_max(maxval+1), m_scale(m_max-m_min)
	{
		assert(m_max>m_min);
		assert(m_scale>0);
		assert(sizeof(float)==4);
		assert(sizeof(shortint)==2);
		assert( 65535 == 0xFFFF);
	}

	void push_back(const pointf& p)	{m_dat.push_back(toshort(p));}
	void push_back(const pointd& p)	{m_dat.push_back(toshort(tof(p)));}
	
	const pointf operator[](const size_t n) const
	{
		assert( n<m_dat.size());
		return topoint(m_dat[n]);
	}

	void resize (const size_t n) {m_dat.resize(n);}
	void reserve(const size_t n) {m_dat.reserve(n);}
	size_t size() const {return m_dat.size();}

	void push_back(const vector<float>& x)
	{
		assert( x.size()%3 == 0);
		for(size_t i=0;i<x.size();i+=3) push_back(pointf(x[i],x[i+1],x[i+2]));
	}
};

#endif // __COMPRESSED_ARRAY__
