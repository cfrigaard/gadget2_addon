const string outputformat_twopointcorr="r [" + UNIT_LENGTH  + "] TPCF_n TPCF_DP TPCF_hew TPCF_ham TPCF_ls N <d>=sum/N [" + UNIT_LENGTH  + "]";
const string outputformat_twopointcorr_density="r [" + UNIT_LENGTH  + "] xi(r)=<delta(r)delta(r+r')> N <d>=sum/N [" + UNIT_LENGTH  + "]";

struct map_val {
	_len sum;
	flt overdensity;
	long long N;

	map_val(long long n=0) : sum(_len(0)), overdensity(0), N(n) {}
	_len mean()           const {return N==0 ? _len(0) :         sum/static_cast<flt>(N);}
	flt meanoverdensity() const {return N==0 ? 0       : overdensity/static_cast<flt>(N);}
	void operator+=(const map_val& x) {sum+=x.sum; overdensity+=x.overdensity; N+=x.N;}

	void add(const _len2& d2,const bool countdouble)
	{
		assert( N<numeric_limits<long long>::max()-2 );
		const _len d=Sqrt<_len2,_len>(d2);
		sum += d;
		++N;
		if (countdouble){
			sum += d;
			++N;
		}
	}

	friend ostream& operator<<(ostream& s,const map_val& x) {return s << x.sum << " " << x.overdensity << " " << x.N;}
	friend istream& operator>>(istream& s,map_val& x)       {return s >> x.sum >> x.overdensity >> x.N;}
};

typedef map<_len2,map_val> t_map;

t_map InitBins(const int nbins,const _bbox_len& bbox,const bool print=false)
{
	const flt n=Pow(bbox()/_len(1),1.0/nbins);
	_len v(1.0);
	t_map bins;
	while(v<=bbox()) {bins[v*v]=0; v*=n;}
	if (bins.size()<2) throw_("bin size too small");
	if ((--bins.end())->first<0.9*bbox()*bbox()) bins[bbox()*bbox()]=0;
	if (print){
		cout << "%  bins: {";
		int i=0;
		for(t_map::const_iterator itt=bins.begin();itt!=bins.end();++itt) {
			cout << " " << Sqrt<_len2,_len>(itt->first);
			if (++i>16) {cout << "\n%      "; i=0;} // avoid long lines
		}
		cout << " }\n%  no bins: " << bins.size() << endl;
	}

	bins[_len2(-1)]=0; // dummy bin for out-of-range data
	return bins;
}


inline long long Count(const t_map& bins)
{
	long long v=0;
	for(t_map::const_iterator itt=bins.begin();itt!=bins.end();++itt) {
		assert( v<numeric_limits<long long>::max()-itt->second.N );
		v+=itt->second.N;
	}
	return v;
}

inline t_map::iterator AddBinCount(t_map& bins,const _len2 d2,const bool countdouble)
{
	assert( d2>=_len2(0) );
	t_map::iterator itt=bins.lower_bound(d2);
	assert( itt!=bins.begin() );
	if (itt==bins.end()) {
		itt=bins.begin(); // dummy bins[0] - count dist out of range
		assert( itt->first==_len2(-1) && d2>_len2(0) );
	}
    assert( d2<=itt->first || itt==bins.begin() );
	itt->second.add(d2,countdouble);
	return itt;
}

inline t_map Pr(const t_map& bins,const vector<_point_len>& d1,const vector<_point_len>& d2,const _bbox_len& bbox,const bool eqdataset,const Mpi& mpi,timer& tim,const flt progress,const flt tot)
{
	const size_t N1_lower=mpi.Lower(d1.size());
	const size_t N1_upper=mpi.Upper(d1.size());
	const size_t N2=d2.size();

	assert( bins.size()>1 );
	t_map m=bins;

	mpi << "%    progress ";
	for(size_t i=N1_lower;i<N1_upper;++i){
		const _point_len& p1d=d1[i];
		if(eqdataset){
			for(size_t j=i+1;j<N2;++j){
				// will not really produce repeatable range due to loop over r
				const _len2 dd2=bbox.DistPeriodic2(p1d,d2[j]);
				AddBinCount(m,dd2,true); // count twice
			}
		}else{
			for(size_t j=0;j<N2;++j){
				const _len2 dd2=bbox.DistPeriodic2(p1d,d2[j]);
				AddBinCount(m,dd2,false);
			}
		}
		if(mpi.isMaster()) {
			tim.ToEta(progress+static_cast<flt>(mpi.Size())*i*N2,tot,cerr);
		}
	}
	//mpi << " done " << mpiendl;
	//cerr << " done on node=" << mpi << " " << tim << endl;

	mpi.CollectMap(m,true,2); // collect map, sum data
	if (mpi.isMaster()){
		const long long N3=Count(m);
		const long long N4=static_cast<long long>(eqdataset ? (N2-1) : N2)*d1.size();
		if ( N3!=N4 ) warn_("Warning: Bad datacount in algorithm, N3=" + tostring(N3)
		  + " N4=" + tostring(N4) + " d1.size()=" + d1.size()  + " d2.size()=" + d2.size());

		assert( m.size()>2 );
		m.erase(m.begin());

		int i=0;
		cout << "%  counts: [";
		for(t_map::const_iterator itt=m.begin();itt!=m.end();++itt)  {
			cout << " " << itt->second.N;
			if (++i>16) {cout << "\n%      "; i=0;} // avoid long lines
		}
		cout << " ]" << endl;
	}
	return m;
}

flt FastRR(t_map::const_iterator itt,const t_map& map,const _bbox_len& bbox)
{
	assert( itt!=map.end() );
	_len y=Sqrt<_len2,_len>(itt->first);
	const _len3 y2=y*y*y;
	if (itt==map.begin()) {y=_len(0);}
	else                  {--itt; y=Sqrt<_len2,_len>(itt->first);}
	const _len3 y1=y*y*y;
	assert( y2>y1 );
	const _len3 v=bbox()*bbox()*bbox();
	return 4*_pi/3*(y2-y1)/v;
}

void TwoPointCorrelationOnDensity(const string& file,const int nbins,const _bbox_len& bbox)
{
	Mpi mpi;
	if (!mpi.isMaster()) return;

	assert( nbins>2 && bbox()>_len(0));
	cout << "% Calculating twopoint correlation function, overdensity mode" << endl;
	cout << "%   paramters: bbox=" << bbox << " nbins=" << nbins << endl;

	array3d<flt> d=array3d<flt>::Readascii(file);
	const flt dmin=d[d.FindMin()];
	if (!dmin<0 || dmin<-1.0) warn_("suspicious data, is it really in dimless delta rho format?");
	if (!d.isSquare()) throw_("cannot handle non-square overdensity data in twopoint calcultion");

	t_map bins=InitBins(nbins,bbox);
	range<_len> r(_len(0),bbox(),d.sizex());

	const unsigned long long tot=Sum<unsigned long long>(d.size()-1);
	unsigned long long sum=0;
	timer tim;
	for(size_t n=0;n<d.size();++n){
		const flt od1=d[n];
		const t_idx i1=d.toidx(n);
		const _point_len p=r.totype(i1);
		for(size_t m=n+1;m<d.size();++m){
			const flt od2=d[m];
			const t_idx i2=d.toidx(m);
			const _point_len q=r.totype(i2);
			const _len2 d2=bbox.DistPeriodic2(p,q);

			t_map::iterator itt=AddBinCount(bins,d2,false);
			assert( itt!=bins.end() );
			itt->second.overdensity += od1*od2;

			++sum;
		}
		tim.ToEta(sum,tot,cerr);
	}
	assert( sum==tot );
	assert( bins.size()>2 );
	bins.erase(bins.begin()); // index 0 is out-of-range bin

	cout << "%  outputformat: " << outputformat_twopointcorr_density << endl;
	for(t_map::const_iterator itt=bins.begin();itt!=bins.end();++itt){
		const long long n=itt->second.N;
		const _len d    =Sqrt<_len2,_len>(itt->first);
		const _len dmean=itt->second.mean();
		const flt odmean=itt->second.meanoverdensity();

		assert( dmean <= d );
		#ifndef NDEBUG
		if (dmean>_len(0) && itt!=bins.begin()){
			--itt;
			const _len d_prev=Sqrt<_len2,_len>(itt->first);
			assert( dmean > d_prev );
			++itt;
		}
		#endif

		const int fpre=6,fpost=10;
		cout.precision(8);
		cout << fwidth(d,6,6) << " " << fwidth(odmean,fpre,fpost) << " " << fwidth(n,8,0)  << " " << fwidth(dmean,fpre,fpost) << endl;
		sum -= n;
	}
	assert( sum==0 );
}

void TwoPointCorrelation(const data_info& inf,const int nbins,const _bbox_len& bbox,const int Nrandom,const bool fastRR)
{
	assert( nbins>2 && Nrandom>0 && bbox()>_len(0));
	cout << "% Calculating twopoint correlation function" << endl;

	vector<_point_len> d;
	for (int i=0;i<inf.NumPart;++i) {
		const particle_data& P=inf.P[i+1];
 		d.push_back(_point_len(_len(P.Pos[0]),_len(P.Pos[1]),_len(P.Pos[2])));
	}

	//vector_random<float> r(Nrandom,bbox()); // inplace-random generator, saves space
	vector<_point_len> r;
	for(int i=0;i<Nrandom;++i) r.push_back(_point_len(bbox()*Random(),bbox()*Random(),bbox()*Random()));

	const flt NDD=static_cast<flt>(d.size())*(d.size()-1);
	const flt NDR=static_cast<flt>(r.size())*(d.size());
	const flt NRR=static_cast<flt>(r.size())*(r.size()-1);

	if ( d.size()<2 || r.size()<2) throw_("too small data or random size");
	if ( NDD>numeric_limits<long long>::max() || NDR>numeric_limits<long long>::max()
	       || NRR>numeric_limits<long long>::max() ) throw_("too large data size");

	Mpi mpi;
	if(mpi.isMaster()){
		const _point_len mean=Mean(d);
		assert( mean[0]<bbox() && mean[1]<bbox() && mean[2]<bbox());

		cout << "%  parameters: (" << nbins << ";" << bbox << ") Nrandom:" << Nrandom
		     << (fastRR ? " fastRR=true" :  "") << " datasize: " << d.size() << endl;
		cout << "%  data mean: " << mean << endl;
		cout << "%  counts  NDD: " << NDD << "  NDR: " << NDR << "  NRR: " << NRR << endl;
	}

	t_map bins=InitBins(nbins,bbox,mpi.isMaster());

	bins[_len2(-1)]=0; // dummy bin for out-of-range data
	const flt tot=NDD+NDR + (fastRR ? 1 : NRR);
	flt progress=0;

	timer tim;
	const t_map aDD=Pr(bins,d,d,bbox,true,mpi,tim,progress,tot);
	progress += NDD;

	const t_map aDR=Pr(bins,r,d,bbox,false,mpi,tim,progress,tot);
	progress += NDR;

	const t_map aRR=fastRR ?  t_map() : Pr(bins,r,r,bbox,true,mpi,tim,progress,tot);

	//! For ploting RR data series only
	//!  for(t_map::const_iterator itt=aRR.begin();itt!=aRR.end();++itt)
	//!    cout << "\t" << Sqrt<_len2,_len>(itt->first)
	//!         << "  " << itt->second.N << endl;;
	//!  return;

	if (!mpi.isMaster()) return;

	cout << "%  outputformat: " << outputformat_twopointcorr << endl;
	assert( aDD.size()==aDR.size() &&  aDR.size()==bins.size()-1 && (aRR.size()==bins.size()-1 || (aRR.size()==0 && fastRR)) );
	for(t_map::const_iterator itt1=aDD.begin(),itt2=aDR.begin(),itt3=aRR.begin();itt1!=aDD.end();++itt1,++itt2){
		const flt DD=itt1->second.N/NDD;
		const flt DR=itt2->second.N/NDR;
		const flt RR=fastRR ? FastRR(itt1,aDD,bbox) : itt3->second.N/NRR;
		if (!fastRR) ++itt3;

		//! For debugging RR fast
		//! const flt RR2=FastRR(itt1,aDD,bbox);
		//! cerr << "%  d=" << fwidth(Sqrt<_len2,_len>(itt2->first),8,8)  << "  RR=" << fwidth(RR,1,12)
		//!      << " RR2=" << fwidth(RR2,1,12) << " DR=" << fwidth(DR,1,12) << endl;

		const flt f0=RR>0 ? DD/RR-1 : -1;
		const flt f1=DR>0 ? DD/DR-1 : -1;
		const flt f2=RR>0 ? (DD-DR)/RR : -1;
		const flt f3=DR>0 ? (DD*DR)/(DR*DR)-1 : -1;
		const flt f4=RR>0 ? (DD - 2*DR + RR)/RR : -1;

		const long long n=itt1->second.N;
		const _len d    =Sqrt<_len2,_len>(itt1->first);
		const _len dmean=itt1->second.mean();
		assert( dmean <= d );
		#ifndef NDEBUG
		if (dmean>_len(0) && itt1!=aDD.begin()){
			--itt1;
			const _len d_prev=Sqrt<_len2,_len>(itt1->first);
			assert( dmean > d_prev );
			++itt1;
		}
		#endif

		const int fpre=6,fpost=10;
		cout.precision(8);
		cout << fwidth(d,6,6) << " " << fwidth(f0,fpre,fpost);
		cout << " " << fwidth(f1,fpre,fpost) << " " << fwidth(f2,fpre,fpost);
		cout << " " << fwidth(f3,fpre,fpost) << " " << fwidth(f4,fpre,fpost);
		cout << " " << fwidth(n,8,0)         << " " << fwidth(dmean,fpre,fpost) << endl;
	}
}

int Usage_Twopointcorr(const args& a)
{
	cerr << "Usage: " << a[0]
	     << " <snapshotfile> <nbins>  [-d <boxsize>] [-f <filter>] [-g] [-r <Nrandom>]\n";
	cerr << "  " << Version() <<  " " << Config() <<  " \n";
	cerr << "  snapshotfile: snapshot file to be analyzed, (density file in -d mode)\n";
	cerr << "  nbins: number of bins to use\n";
	cerr << "Options:\n";
	cerr << "  -d <float " << UNIT_LENGTH << ">: calculate twopointcorrelations from"
	     << " overdensity (implies no use of -f,-g or -r)\n";
	cerr << "  -f <int>: 1=gas, 2=halo, 4=disk, 8=bulge, 16=stars, 32=bndry, default=0\n";
	cerr << "  -r <int>: use N random points for correlation generation, default=100000\n";
	cerr << "  -g: use fast RR calcultion in assuming plain geometric RR data\n";
	cerr << "Output\n  to stdout: table of r and twopointcorrelations, N and distance"
	     << " mean <d> in each bin\n";
	cerr << "             r are upper bounds of the bins (b in ]r_last;r])\n";
	cerr << "  units: " << outputformat_twopointcorr << endl;
	return -1;
}

int main_twopointcorr(int argc,char** argv)
{
	try{
		args a(argc,argv,true,true);

		const int Nrandom=a.parseval<int>("-r",100000);
		const int filter=a.parseval<int>("-f",0);
		const bool fastRR=a.parseopt("-g");
		const _len dens=a.parseunit("-d",_len(-1));

		if(Nrandom<0) throw_("Bad number of random points");
 		if(a.size()!=3) return Usage_Twopointcorr(a);
		const int nbins=a.Totype<int>(2);

		if (dens>_len(0)){
			if(Nrandom!=100000 || filter!=0 || fastRR) warn_("density mode ignores -f,-g and -r options");
			TwoPointCorrelationOnDensity(a[1],nbins,dens);
		}
		else {
			const data_info inf=load_snapshot(a[1].c_str(),1,true,true);
			const _bbox_len bbox=inf.header1.bbox();
			TwoPointCorrelation(FilterParticles(inf,filter),nbins,bbox,Nrandom,fastRR);
		}
		return 0;
	}
	CATCH_ALL;
	return -1;
}
