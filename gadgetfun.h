#ifndef __GADGETFUN_H__
#define __GADGETFUN_H__

typedef pair<particle_data*,int*> pair_data;
inline void fread_x(void* p,size_t sz,size_t n,FILE* fp) {if(n!=fread(p,sz,n,fp)) throw_("Bad read in file");}
inline int  SKIP_1(FILE* fd)	        		{int dummy=0;  fread_x(&dummy,  sizeof(dummy),  1, fd); return dummy;}
inline void SKIP_2(FILE* fd,const int dummy)	{int dummy2=0; fread_x(&dummy2, sizeof(dummy2), 1, fd); if(dummy!=dummy2) throw_("bad block header");}

inline data_info load_snapshot(const char *fname,const int files,const bool ICmode=false,const bool onlyPos=false)
{
	FILE *fd=0;
	char buf[200];
	int  i,k,dummy,ntot_withmasses;
	int  n,pc,pc_new,pc_sph;

	int NumPart=0;
	particle_data* P=0;
	int* Id=0;
	io_header_1 header1;

	assert( sizeof(dummy)== 4 );
	assert( sizeof(header1)== 256 );

	for(i=0, pc=1; i<files; i++, pc=pc_new)
	{
		if(files>1) sprintf(buf,"%s.%d",fname,i);
		else        sprintf(buf,"%s",fname);

		if(!(fd=fopen(buf,"rb")))
		{
			fprintf(stderr,"can't open file `%s`\n",buf);
			exit(0);
		}

		dummy=SKIP_1(fd);
		if (dummy!=256) throw_("bad snapshot format, header tag <256> not found");
		fread_x(&header1, sizeof(header1), 1, fd);
		SKIP_2(fd,dummy);

		if(files==1){
			for(k=0, NumPart=0, ntot_withmasses=0; k<6; k++)
				NumPart+= header1.npart[k];
				//Ngas= header1.npart[0];
		}
		else
		{
			for(k=0, NumPart=0, ntot_withmasses=0; k<6; k++)
				NumPart+= header1.npartTotal[k];
				//Ngas= header1.npartTotal[0];
		}

		for(k=0, ntot_withmasses=0; k<6; k++)
		{
			if(header1.mass[k]==0) ntot_withmasses+= header1.npart[k];
		}

		//if(ntot_withmasses>0) throw_("Cannot handle individual masses yet");
		if(i==0){
			//pair_data pr=allocate_memory(NumPart);
			//P=pr.first;
			//Id=pr.second;
			P=new particle_data[NumPart+1];
			Id=new int[NumPart+1];
			memset(P ,0,(NumPart+1)*sizeof(particle_data));
			memset(Id,0,(NumPart+1)*sizeof(int));
		}

		dummy=SKIP_1(fd);;
		for(k=0,pc_new=pc;k<6;k++)
		{
			for(n=0;n<header1.npart[k];n++)
			{
				fread_x(&P[pc_new].Pos[0], sizeof(float), 3, fd);
				pc_new++;
			}
		}
		SKIP_2(fd,dummy);;

		if(!onlyPos){
			dummy=SKIP_1(fd);; // velo
			for(k=0,pc_new=pc;k<6;k++)
			{
				for(n=0;n<header1.npart[k];n++)
				{
					fread_x(&P[pc_new].Vel[0], sizeof(float), 3, fd);
					pc_new++;
				}
			}
			SKIP_2(fd,dummy);;

			dummy=SKIP_1(fd);;  // id
			for(k=0,pc_new=pc;k<6;k++)
			{
				for(n=0;n<header1.npart[k];n++)
				{
					fread_x(&Id[pc_new], sizeof(int), 1, fd);
					pc_new++;
				}
			}
			SKIP_2(fd,dummy);;

			if(ntot_withmasses>0) dummy=SKIP_1(fd);;
			for(k=0, pc_new=pc; k<6; k++)
			{
				for(n=0;n<header1.npart[k];n++)
				{
					P[pc_new].Type=k;
					if(header1.mass[k]==0){
						fread_x(&P[pc_new].Mass, sizeof(float), 1, fd);
					}
					else P[pc_new].Mass= static_cast<float>(header1.mass[k]);
					pc_new++;
				}
			}
			if(ntot_withmasses>0) SKIP_2(fd,dummy);;

			if(header1.npart[0]>0 && !ICmode)
			{
				dummy=SKIP_1(fd);; // U
				for(n=0, pc_sph=pc; n<header1.npart[0];n++)
				{
					fread_x(&P[pc_sph].U, sizeof(float), 1, fd);
					pc_sph++;
				}
				SKIP_2(fd,dummy);;

				dummy=SKIP_1(fd);; // RHO
				for(n=0, pc_sph=pc; n<header1.npart[0];n++)
				{
					fread_x(&P[pc_sph].Rho, sizeof(float), 1, fd);
					pc_sph++;
				}
				SKIP_2(fd,dummy);;


				if(header1.flag_cooling)
				{
					dummy=SKIP_1(fd);;
					for(n=0, pc_sph=pc; n<header1.npart[0];n++)
					{
						fread_x(&P[pc_sph].Ne, sizeof(float), 1, fd);
						pc_sph++;
					}
					SKIP_2(fd,dummy);;
				}
				else
				for(n=0, pc_sph=pc; n<header1.npart[0];n++)
				{
					P[pc_sph].Ne= 1.0;
					pc_sph++;
				}

				//dummy=SKIP_1(fd);;
				//for(n=0, pc_sph=pc; n<header1.npart[0];n++)
				//{
				//	fread_x(&P[pc_sph].hsml, sizeof(float), 1, fd);
				//	pc_sph++;
				//}
				//SKIP_2(fd,dummy);;
			}
			else{
				for(n=0, pc_sph=pc; n<header1.npart[0];n++)
				{
					P[pc_sph].Ne= 1.0;
					pc_sph++;
				}
			}
		}
	}

	fclose(fd);
	data_info inf;
	inf.P=P;
	inf.Id=Id;
	inf.NumPart=NumPart;
	inf.header1=header1;
	return inf;
}

inline void reordering(data_info& inf,const bool quiet=true)
{
	int i;
	int idsource, idsave, dest;
	struct particle_data psave, psource;

	particle_data* P=inf.P;
	int* Id=inf.Id;

	if (P ==0) throw_("bad P pointer, is null");
	if (Id==0) throw_("bad id pointer, is null");

	timer tim;
	for(i=1; i<=inf.NumPart; i++){
		if(Id[i] != i)	{
			psource= P[i];
			idsource=Id[i];
			dest=Id[i];

			do{
				//if (idsource==dest) {
				//	cout << "src=" << idsource << " dest=" << dest << endl;
				//	throw_("bad src/dest");
				//}
				psave= P[dest];
				idsave=Id[dest];

				P[dest]= psource;
				Id[dest]= idsource;

				if(dest == i)
					break;

				psource= psave;
				idsource=idsave;

				dest=idsource;
			}
			while(1);
		}
		if (!quiet) tim.ToEta(i,inf.NumPart,cerr);
	}

	//delete[] Id;
	//inf.Id=0;
}

inline data_info FilterParticles(const data_info& inf,const int filter)
{
	if(filter<0 || filter>63) throw_("Bad filter type");
	if(filter==0) return inf;
 	io_header_1 h=inf.header1;

	int NumPart=0;
	for(int i=0;i<6;++i) {
		if (filter==0 || filter>>i & 0x1){
			NumPart +=h.npart[i];
		}
		else  h.npart[i]=h.npartTotal[i]=0;
	}

	particle_data* P=new particle_data[NumPart+1];
	int*          Id=new int[NumPart+1];
	memset(P ,0,(NumPart+1)*sizeof(particle_data));
	memset(Id,0,(NumPart+1)*sizeof(int));

	int n=0,m=0;
	for(int i=0;i<6;++i) {
		if (filter==0 || filter>>i & 0x1){
			for(int j=0;j<h.npart[i];++j){
				assert(n<NumPart);
				assert(m<inf.NumPart);
				P [n]=inf.P[m]; // XXX what about 0/1 indexing ???
				Id[n]=inf.Id[m];
				++n;
				++m;
			}
		}
		else m+=inf.header1.npart[i];
	}

	assert( n==NumPart );

	data_info inf2;
	inf2.P=P;
	inf2.Id=Id;
	inf2.NumPart=NumPart;
	inf2.header1=h;

	return inf2;
}

inline io_header_1 FilterParticles(const data_info& inf,const int filter,vector<particle_data_2>& v)
{
	if(filter<0 || filter>63) throw_("Bad filter type");

	io_header_1 h=inf.header1;
	int NumPart=0;
	for(int i=0;i<6;++i) {
		if (filter==0 || filter>>i & 0x1) NumPart +=h.npart[i];
		else {
			h.npart[i]=h.npartTotal[i]=0;
			h.mass[i]=0.0;
		}
	}

	v.resize(0);
	v.reserve(NumPart);

	int n=0,m=0;
	for(int i=0;i<6;++i) {
		if (filter==0 || filter>>i & 0x1){
			for(int j=0;j<h.npart[i];++j){
				assert(n<NumPart);
				assert(m<inf.NumPart);

				const particle_data& d=inf.P[m];
				const double mass=d.Mass==0 ? h.mass[i] : d.Mass;
				v.push_back(particle_data_2(d,mass));
				++n;
				++m;
			}
		}
		else m+=inf.header1.npart[i];
	}

	assert( n==NumPart );
	return h;
}

inline vector<particle_data_2> ToVectorData(const data_info& inf)
{
	vector<particle_data_2> v;
	v.reserve(inf.NumPart);
	int n=0;
	for(int i=0;i<6;++i) {
		const double m=inf.header1.mass[i];

		for(int j=0;j<inf.header1.npart[i];++j){
			assert(n<inf.NumPart);
			const particle_data& p=inf.P[n+1];
			v.push_back(particle_data_2(p,m));
			++n;
		}
	}
	assert( n==inf.NumPart );
	return v;
}

inline void SortParticletypes(vector<particle_data_2>& x,vector<char>& xt)
{
	if (x.size()!=xt.size()) throw_("bad particle and type vector sizes");
	const size_t n=x.size();
	vector<particle_data_2> y;
	y.reserve(n);

	for(int i=0;i<6;++i)
	for(size_t j=0;j<n;++j){
		if (xt[j]==i) y.push_back(x[j]);
	}
	assert( y.size()==x.size() );
	swap(x,y);
}

inline Gadgetdatainfo CountParticletypes(const vector<char>& xt)
{
	Gadgetdatainfo G;
	for(size_t i=0;i<xt.size();++i) ++G.n[static_cast<int>(xt[i])];
	return G;
}

inline pair<_point_len,_point_len> Minmax(const vector<particle_data_2>& v)
{
	if( v.size()==0 ) throw_("empty data vector");
	_point_len pmin=v[0].pos();
	_point_len pmax=v[0].pos();
	for(size_t n=1;n<v.size();++n) {
		const _point_len& p=v[n].pos();
		pmin.min(p);
		pmax.max(p);
	}
	return make_pair(pmin,pmax);
}

template<class T>
size_t SaveSnapshotblock(ofstream& s,const vector<T>& data)
{
	const size_t t=data.size()*sizeof(T);
	Writetyp(s,t);
	Writebin(s,data,false);
	Writetyp(s,t);
	return 2*sizeof(t)+t;
}

inline void SaveSnapshot(const io_header_1& h,const vector<particle_data_2>& x,const string& file,const bool ICmode)
{
	if(h.hasindividualmasses()) throw_("cannot handle individual masses yet");
	h.CheckOmega();
	assert( h.num_files==1 );

	ofstream s(file.c_str());
	AssertFileExists(file);

	const size_t N=x.size();
	assert( N==h.particlestotal() );
	vector<float> v;

	assert( sizeof(size_t)== 4 );
	assert( sizeof(h)== 256 );

	Writetyp(s,sizeof(h));
	Writetyp(s,h);
	Writetyp(s,sizeof(h));
	size_t fsize = 2*sizeof(size_t) + sizeof (h);
	assert( fsize = 2*4+256 );

	// Positions
	v.reserve(3*N);
	v.resize(0);
	for(size_t i=0;i<N;++i) {
		const Triple<_len>& y=x[i].pos();
		for(size_t j=0;j<3;++j) v.push_back(tonum(y[j]));
	}
	fsize += SaveSnapshotblock(s,v);

	// Velocities
	v.reserve(3*N);
	v.resize(0);
	for(size_t i=0;i<N;++i){
		const _vector_vel& y=x[i].vel();
		for(size_t j=0;j<3;++j) v.push_back(tonum(y[j]));
	}
	fsize += SaveSnapshotblock(s,v);

	// Ids
	vector<size_t> id(N);
	for(size_t i=0;i<N;++i) id[i]=i+1; // XXX setting id to defaut
	fsize += SaveSnapshotblock(s,id);

	// Masses, not supported
	// 	if (h.hasindividualmasses()) {
	// 		v.resize(N);
	// 		for(size_t i=0;i<N;++i) v[i]=tonum(x[i].mass());
	// 		fsize += SaveSnapshotblock(s,v);
	// 	}

	if (!ICmode){
		throw_("non-ic mode not supported yet");
		// 		const size_t Nsph=h.npart[0];
		//
		// 		U
		// 		v.resize(Nsph);
		// 		for(size_t i=0;i<Nsph;++i) v[i]=x[i].u();
		// 		fsize += SaveSnapshotblock(s,v);
		//
		// 		Rho
		// 		v.resize(Nsph);
		// 		for(size_t i=0;i<Nsph;++i) v[i]=tonum(x[i].density());
		// 		fsize += SaveSnapshotblock(s,v);
		//
		// 		Ne
		// 		if (h.flag_cooling) {
		// 			v.resize(Nsph);
		// 			for(size_t i=0;i<Nsph;++i) v[i]=x[i].ne();
		// 			fsize += SaveSnapshotblock(s,v);
		// 		}
	}

	s.close();
	const size_t realfilesize=FileSize(file);
	if (realfilesize!=fsize) throw_(string("filesize mismatch,  filesize=") + realfilesize + " calculated size=" + fsize);
}

inline void SaveSnapshot(const data_info& inf,const string& file,const bool ICmode)
{
	SaveSnapshot(inf.header1,ToVectorData(inf),file,ICmode);
}

inline void CheckFileTag(ifstream& s,const size_t gpos0,const size_t gpos1,const size_t expectedsize,const bool verbose=false)
{
	s.seekg(gpos0,ios::beg);
	const size_t t0=Readtyp<unsigned int>(s);
	if (verbose) cout << "% ** CheckFileTag: pos0=" << gpos0 << " t=" << t0;
	if (t0!=expectedsize) throw_("file begin tag wrong");
	s.seekg(gpos1,ios::beg);
	const size_t t1=Readtyp<unsigned int>(s);
	if (verbose) cout << " pos1=" << gpos1 << " t=" << t1 << endl;
	if( t0!=t1 ) throw_("file end tag wrong");
}

inline Triple<size_t> AssignAndCheckFileTags(ifstream& s,const size_t N)
{
	const size_t szint=sizeof(int);
	const size_t szfloat=sizeof(float);
	const size_t szsizet=sizeof(size_t);
	assert( szint==4 && szfloat==4 && szsizet==4 && sizeof(io_header_1)==256 ); // cannot handle 64 bit yet

	const size_t hpos  = szint + sizeof(io_header_1) + szint;
	const size_t ppos0 = hpos  + szint;
	const size_t ppos1 = ppos0 + 3*N*szfloat + szint;
	const size_t vpos0 = ppos1 + szint;
	const size_t vpos1 = vpos0 + 3*N*szfloat + szint;
	const size_t idpos0= vpos1 + szint;
	const size_t idpos1= idpos0+ N*szsizet + szint;

	CheckFileTag(s, 0          , hpos-szint, 256 );
	CheckFileTag(s, ppos0-szint, ppos1-szint,3*N*szfloat);
	CheckFileTag(s, vpos0-szint, vpos1-szint,3*N*szfloat);
	CheckFileTag(s,idpos0-szint,idpos1-szint,N*szsizet);

	return Triple<size_t>(ppos0,vpos0,idpos0);
}

inline pair<particle_data,pair<size_t,size_t> > PeekParticle(const string& file,const size_t id,const bool lookup=false)
{
	const io_header_1 h(file);
	const size_t N=h.particlestotal();
	const size_t szsizet=sizeof(size_t);
	const size_t szfloat=sizeof(float);

	assert( h.hasindividualmasses()==false );
	if (id>=N) throw_("particle ID out of range, ID=" + tostring(id) + " N=" + tostring(N));
	ifstream s(file.c_str());
	const Triple<size_t> fpos=AssignAndCheckFileTags(s,N);

	size_t n=-1,real_id=id;
	if (lookup){
		s.seekg(fpos[2],ios::beg);
		assert( id>=0 && id<N );
		for(size_t i=0;i<N;++i) {
			if (id+1==Readtyp<size_t>(s)) {n=i; i=N;}
		}
		if (n==static_cast<size_t>(-1)) throw_("bad ID lookup");
	}
	else {
		n=id;
		s.seekg(fpos[2]+n*szsizet,ios::beg);
		real_id=Readtyp<size_t>(s);
		if (real_id<1 || real_id>N) throw_("bad real ID lookup");
		--real_id;
	}
	assert( n>=0 && n<N );

	particle_data p;
	p.Type=h.gettype(n);
	p.Mass=h.mass[p.Type];
	p.Rho=p.U=p.Temp=p.Ne=0.0;
	assert( !h.hasindividualmasses() );

	s.seekg(fpos[0]+3*n*szfloat,ios::beg);
	for(int i=0;i<3;++i) p.Pos[i]=Readtyp<float>(s);
	s.seekg(fpos[1]+3*n*szfloat,ios::beg);
	for(int i=0;i<3;++i) p.Vel[i]=Readtyp<float>(s);

	return make_pair(p,make_pair(n,real_id));
}

/*
class particle_data_attribs
{
	private:
	Triple<flt> m_v;
	float m_rho,m_u,m_ne;

	public:
	particle_data_attribs()
	: m_v(0.0), m_rho(-1), m_u(0), m_ne(0)
	{}
};


// Forward definition
class GadgetSimulation;

class particle_data_3 // mini data set
{
	private:
	_point_len m_p;
	float m_temp;
	size_t m_attr;

	public:
	particle_data_3()
	: m_p(-1), m_temp(0), m_attr(-1)
	{
		assert( sizeof(particle_data_3)==3*8+2*4 || sizeof(void*)==8); // nice structure size does not hold for 64 bit
	}

	particle_data_3(const particle_data& d)
	: m_p(d.Pos[0],d.Pos[1],d.Pos[2]), m_temp(d.Temp), m_attr(-1)
	{}

	particle_data_3(const _point_len& q)
	: m_p(q), m_temp(-1)
	{}

	const _point_len& pos     () const {return m_p;}
	_point_len&       pos     ()       {return m_p;}
	float             temp    () const {return m_temp;}
	float&            temp    ()       {return m_temp;}

	size_t            id      (const GadgetSimulation& g) const;
	_mass             mass    (const GadgetSimulation& g) const;
	unsigned int      type    (const GadgetSimulation& g) const;
	void Assignattrib(GadgetSimulation& g);

	friend ostream& operator<<(ostream& s,const particle_data_3& x)
	{
		s << "particle_data_3: {pos=" << x.pos() << " temp=" << x.temp();
		if (x.m_attr!=static_cast<size_t>(-1)) s << " attrib=true";
		return s << "}";
	}
};

class GadgetSimulation : public vector<particle_data_3>
{
	private:
	UniverseParameters m_u;
	Gadgetdatainfo     m_g;
	io_header_1        m_h;
	vector<particle_data_attribs> m_attr;

	public:
 	GadgetSimulation(const string& filename,const int filter=0,const bool ICmode=false,const bool onlyPos=false,const bool reorder=false)
 	: m_u(m_h)
 	{
		Readbin(filename,filter);
	}

	const UniverseParameters& universeparameters() const {return m_u;}
	const Gadgetdatainfo&     gadgetdatainfo    () const {return m_g;}

	void clear()
	{
		m_h=io_header_1();
		m_u=UniverseParameters(m_h);
		m_g=Gadgetdatainfo(m_h);
		static_cast<vector<particle_data_3>*>(this)->clear();
		m_attr.clear();
	}

	size_t Getid(const particle_data_3* x) const
	{
		assert(x>=&front() && x<=back());
		const size_t id=x-&front();
		assert( id<size() );
		return id;
	}

	unsigned int Gettype(const size_t& id) const
	{
		size_t t=0;
		for(unsigned int i=0;i<6;++i) {
			t+=m_g.n[i];
			if (id<t) return i;
		}
		throw_("out of range particle");
	}

	size_t Assignattrib()
	{
		assert( m_attr.size()<size() );
		m_attr.resize(m_attr.size()+1);
		return size()-1;
	}

	void Writebin(const string& filename,const bool ICmode=false) const
	{
		const vector<particle_data_3>& v=*this;
		SaveSnapshot(m_h,v,filename,ICmode);
	}

	void Readbin (const string& filename,const int filter=0,const bool ICmode=false,const bool onlyPos=false,const bool reorder=false)
	{
		clear();
		data_info inf=load_snapshot(filename.c_str(),1,ICmode,onlyPos);

		if(reorder) reordering(inf);
	    const data_info inf2=FilterParticles(inf,filter);

		m_h=inf2.header1;
		m_u=UniverseParameters(m_h);
		m_g=Gadgetdatainfo(m_h);
	}
};

size_t       particle_data_3::id   (const GadgetSimulation& g) const {return g.Getid(this);}
_mass    particle_data_3::mass (const GadgetSimulation& g) const {return g.gadgetdatainfo().m[g.Gettype(g.Getid(this))];}
unsigned int particle_data_3::type (const GadgetSimulation& g) const {return g.Gettype(g.Getid(this));}
void         particle_data_3::Assignattrib(GadgetSimulation& g) {assert( m_attr==-1 ); m_attr=g.Assignattrib();}

*/

/*

inline void SaveSnapshotFormat1_ICmode(const string& filename,const io_header_1& h,const vector<particle_data_2>& x)
{
	if( hasIndividualMasses(h) ) throw_("cannot handle individual masses yet");
	const flt calc_Omega=CalcOmega(h,h.BoxSize,0.1,Gadget_G);
	if (_proximy(h.Omega0,calc_Omega,1E-4)) warn_("mismatch in Omega matter calculation");

	// save the data
	ofstream s(filename.c_str());

	const size_t t=sizeof(h);
	if (t!=256) throw_("head size mismatch, expected sizeof(h))=256");

	Writetyp(s,t);
	Writetyp(s,h);
	Writetyp(s,t);

	const size_t n=x.size();
	particle_data p;

	// Positions
	Writetyp(s,n+1);
	p.Pos[0]=p.Pos[1]=p.Pos[2]=0;
	Writetyp(s,p.Pos); // write dummy index 0
	for(size_t i=0;i<n;++i){
		const _point_len& pp=x[i].pos();
		for(size_t j=0;j<3;++j) p.Pos[j]=pp[j]/_len(1);
		Writetyp(s,p.Pos);
	}
	Writetyp(s,n+1);

	// Velocities
	Writetyp(s,n+1);
	p.Vel[0]=p.Vel[1]=p.Vel[2]=0;
	Writetyp(s,p.Vel); // write dummy index 0
	for(size_t i=0;i<n;++i){
		const _vector_vel& pv=x[i].pos();
		for(size_t j=0;j<3;++j) p.Vel[j]=pv[j]/_vel(1);
		Writetyp(s,p.Vel);
	}
	Writetyp(s,n+1);

	// Ids
	Writetyp(s,n+1);
	const size_t id=0;
	Writetyp(s,id); // write dummy index 0
	for(size_t i=0;i<n;++i){
		Writetyp(s,i);
	}
	Writetyp(s,n+1);
}

void Readtag(ifstream& s,const int tag)
{
	int t;
	Readtyp(s,t);
	if (t!=tag) throw_("bad tag in file");
	if (!is) throw_("bad stream");
)
*/

#endif // __GADGETFUN_H__
