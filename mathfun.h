#ifndef __MATHFUN_H__
#define __MATHFUN_H__

namespace Math
{
	typedef complex<flt> cmplx;

	///////////////////////////////////////////////////////////////////////
	// math functions
	///////////////////////////////////////////////////////////////////////

	#ifdef USE_UNITS
		template<class T> inline void assert_dimless(const T& x){assert(T(1)==T(1)*T(1));}
	#else
		#define assert_dimless(x)
	#endif

	#ifndef USE_FLOAT_CHECKS
		#define FltValidity(x) true
		#define CheckFltValidity(x) (x)
	#else
		template<class T>
		inline bool Validity(const T& x){
			assert( numeric_limits<T>::is_integer==false && numeric_limits<T>::has_infinity );
			if      (numeric_limits<T>::infinity()==x)         return false;
			else if (numeric_limits<T>::infinity()==-x)        return false;
			// else if (numeric_limits<T>::denorm_min()>=fabs(x)) return false;
			// == operator on NAN always produces false
			else if (x!=x)                                     return false;
			return true;
		}

		template<class T>
		inline T CheckValidity(const T& x)
		{
			assert( numeric_limits<T>::is_integer==false && numeric_limits<T>::has_infinity );
			if      (numeric_limits<T>::infinity()==x)         throw_("math exception, infinity");
			else if (numeric_limits<T>::infinity()==-x)        throw_("math exception, -infinity");
			// else if (numeric_limits<T>::denorm_min()>=fabs(x)) throw_("math exception, denort_min");
			// == operator on NAN always produces false
			else if (x!=x)                                     throw_("math exception, NAN");
			return x;
		}

		inline bool FltValidity     (const flt& x){return      Validity(x);}
		inline flt  CheckFltValidity(const flt& x){return CheckValidity(x);}
		#ifdef USE_UNITS
			template<class T> bool FltValidity     (const T& x){return FltValidity(x/T(1));}
			template<class T> T    CheckFltValidity(const T& x){return T(CheckFltValidity(x/T(1)));}
		#endif
		// specializations
		inline int             CheckFltValidity(const int& x)         {return x;}
		inline size_t          CheckFltValidity(const size_t& x)      {return x;}
		inline long double     CheckFltValidity(const long double& x) {return CheckValidity(x);}
		template<class T> inline complex<T> CheckFltValidity(const complex<T>& x) {return complex<T>(CheckFltValidity(x.real()),CheckFltValidity(x.imag()));}
	#endif

	template<class T> inline T Abs  (const T& x) {return T(CheckFltValidity(abs(x/T(1))));}
	template<class T> inline T Exp  (const T& x) {assert_dimless(x); return CheckFltValidity(exp(x));}
	template<class T> inline T Log10(const T& x) {assert_dimless(x); return CheckFltValidity(log10(x));}
	template<class T> inline T Ln   (const T& x) {assert_dimless(x); return CheckFltValidity(log(x));}
	template<class T> inline T Sin  (const T& x) {assert_dimless(x); return CheckFltValidity(sin(x));}
	template<class T> inline T Cos  (const T& x) {assert_dimless(x); return CheckFltValidity(cos(x));}
	template<class T> inline T Tan  (const T& x) {assert_dimless(x); return CheckFltValidity(tan(x));}
	template<class T> inline T ASin (const T& x) {assert_dimless(x); return CheckFltValidity(asin(x));}
	template<class T> inline T ACos (const T& x) {assert_dimless(x); return CheckFltValidity(acos(x));}
	template<class T> inline T ATan (const T& x) {assert_dimless(x); return CheckFltValidity(atan(x));}
	template<class T> inline T Sinc (const T& x) {assert_dimless(x); return x==T(0) ? T(1) : CheckFltValidity(sin(x)/x);}

	template<class T,class R> inline R Sqrt (const T& x) {assert(T(1)==R(1)*R(1)); return R(CheckFltValidity(sqrt(x/T(1))));}
	template<class T>         inline T Sqrt (const T& x) {return Sqrt<T,T>(x);}

	template<class T> inline flt Fact (const T& x) {
		assert(numeric_limits<T>::is_integer);
		T y=x;
		flt r=1;
		while(y>0) r*=y--;
		return CheckFltValidity(r);
	}
	template<class T> inline T Sum(const T& x) {
		assert(numeric_limits<T>::is_integer);
		T y=x,r=0;
		while(y>0) {
			if (r>=numeric_limits<T>::max()-y-1) throw_("value overflow in Sum function");
			r+=y--;
		}
		return CheckFltValidity(r);
	}

	inline flt Pow (const flt& x,const flt& exponent){return CheckFltValidity(pow(x,exponent));}
	template<class T> inline T Pow(const T& x,const int& exponent)
	{
		T y(1);
		if (exponent==0) return y;
		else if (exponent>0 && exponent<6) {
			for(int i=0;i<exponent;++i) y*=x;
			return CheckFltValidity(y);
		}
		else if (exponent<0 && exponent>-6) {
			for(int i=0;i<-exponent;++i) y/=x;
			return CheckFltValidity(y);
		}
		return Pow(x,static_cast<T>(exponent));
	}

	template<class T> inline T Pow2  (const T& x){return CheckFltValidity(x*x);}
	template<class T> inline T Pow3  (const T& x){return CheckFltValidity(x*x*x);}
	template<class T> inline T Pow15 (const T& x){return CheckFltValidity(x*sqrt(x));}

	// Complex specialization
	template<class T> inline T          Abs (const complex<T>& x){return CheckFltValidity(abs(x));}
	template<class T> inline complex<T> Conj(const complex<T>& x){return complex<T>(x.real(),-x.imag());}

	#define abs   DO_NOT_USE_ABS
	#define exp   DO_NOT_USE_EXP
	#define log   DO_NOT_USE_LOG
	#define sqrt  DO_NOT_USE_SQRT
	#define pow   DO_NOT_USE_POW
	#define powf  DO_NOT_USE_POWF
	#define log10 DO_NOT_USE_LOG10
	#define log   DO_NOT_USE_LOG
	#define sin   DO_NOT_USE_SIN
	#define cos   DO_NOT_USE_COS
	#define tan   DO_NOT_USE_TAN
	#define asin  DO_NOT_USE_ASIN
	#define acos  DO_NOT_USE_ACOS
	#define atan  DO_NOT_USE_ATAN
	#define conj  DO_NOT_USE_CONJ

	///////////////////////////////////////////////////////////////////////
	// proximy
	///////////////////////////////////////////////////////////////////////

	template<class T>
	inline bool _proximy_base(const T& x,const T& x0,const char* expr,const char* file,const int line,const double epsilon,const bool print)
	{
		if (Abs(x0)<Abs(x)) return _proximy_base(x0,x,expr,file,line,epsilon,print);
		const flt d= x0==T(0) ? Abs(x)/T(1) : (x==T(0) ? Abs(x0)/T(1) : Abs((x-x0)/x0));
		const bool b=d<epsilon;
		if (print){
			if (b==false)                  cerr << Exception::FormatCompilerMsg(file,line)      << "proximy of expression '" << expr <<  "' not in range, x="                 << x << " x0=" << x0 << " d=" << d << " epsilon=" << epsilon << "\n";
			else if (d!=0 && epsilon/d>10) cerr << Exception::FormatCompilerMsg(file,line,true) << "proximy of expression '" << expr <<  "' has too far epsilon, x="          << x << " x0=" << x0 << " d=" << d << " epsilon=" << epsilon << "\n";
			else if (epsilon>1E-3)         cerr << Exception::FormatCompilerMsg(file,line,true) << "epsilon of proximy of expression '" << expr <<  "' seems unfair large x=" << x << " x0=" << x0 << " d=" << d << " epsilon=" << epsilon << "\n";
		}
		return b;
	}

	template<class T>
	inline bool _proximy(const T& x  ,const T& x0  ,const char* expr,const char* file,const int line,const flt epsilon,const bool print){return _proximy_base(x,x0,expr,file,line,epsilon,print);}

	template<class T>
	inline bool _proximy(const T& x,const T& x0,const flt epsilon){return _proximy_base(x,x0,"","",0,epsilon,false);}

	// flt specialization
	inline bool _proximy(const flt& x,const flt& x0,const char* expr,const char* file,const int line,const flt epsilon,const bool print){return _proximy_base(x,x0,expr,file,line,epsilon,print);}

	// complex template
	template<class T> inline bool _proximy(const complex<T>& x,const complex<T>& x0,const char* expr,const char* file,const int line,const flt epsilon,const bool print)
	{
		return _proximy(x.real(),x0.real(),expr,file,line,epsilon,print) &&
			   _proximy(x.imag(),x0.imag(),expr,file,line,epsilon,print);
	}

	// triple template
	template<class T> inline bool _proximy(const Triple<T>& a,const Triple<T>& b,const char* expr,const char* file,const int line,const flt epsilon,const bool print)
	{
		return _proximy(a[0],b[0],expr,file,line,epsilon,print) &&
			   _proximy(a[1],b[1],expr,file,line,epsilon,print) &&
			   _proximy(a[2],b[2],expr,file,line,epsilon,print);
	}
	// vector template
	template<class T> inline bool _proximy(const vector<T>& a,const vector<T>& b,const char* expr,const char* file,const int line,const flt epsilon,const bool print)
	{
		if(a.size() != b.size() ) return false;
		for(size_t i=0;i<a.size();++i) {
			if (!_proximy(a[i],b[i],expr,file,line,epsilon,print)) return false;
		}
		return true;
	}

	///////////////////////////////////////////////////////////////////////
	// Random functions
	// (ranc_ctr and cgasdev_ctr from http://www.astro.princeton.edu/~esirko/ic/)
	///////////////////////////////////////////////////////////////////////

	inline  double ranc_ctr(int iseed)
	{
		const int N=64;
		const int MIN=( 2 << 8 );

		const static int P[] = {
			46337, 46327, 46309, 46307, 46301, 46279, 46273, 46271,
			46261, 46237, 46229, 46219, 46199, 46187, 46183, 46181,
			46171, 46153, 46147, 46141, 46133, 46103, 46099, 46093,
			46091, 46073, 46061, 46051, 46049, 46027, 46021, 45989,
			45979, 45971, 45959, 45953, 45949, 45943, 45893, 45887,
			45869, 45863, 45853, 45841, 45833, 45827, 45823, 45821,
			45817, 45779, 45767, 45763, 45757, 45751, 45737, 45707,
			45697, 45691, 45677, 45673, 45667, 45659, 45641, 45631
		} ;

		static int a[N] ;
		static int S[N] ;
		static int n = 0 ;
		static int called={1} ;

		int i;
		// seed the random number generator if first time called or if iseed > 0
		if (called || iseed != 0)  {
			called = 0 ;
			if (iseed==0) iseed = ::time(0) ;
			srand(iseed) ;
			n = 0 ;
			for( i = 0 ; i < N ; i++ ) {
				a[i] = 0 ;
				S[i] = 0 ;
				//#define RND	( 0x7fff & rand() )
				while( ( a[i] = ( 0x7fff & rand() ) ) < MIN || a[i] > P[i] - MIN ) ;
				while( ( S[i] = ( 0x7fff & rand() ) ) <   1 || a[i] > P[i] -   1 ) ;
			}
		}

		n = S[n] & ( N - 1 ) ;
		S[n] = ( S[n] * a[n] ) % P[n];
		return  static_cast<double>(S[n]) / static_cast<double>(P[n]) ;
	}

	//  cgasdev -- return Gaussian deviate with zero mean and unit variance
	//    double cgasdev()
	//
	//    based on Numerical Recipes gasdev.for

	inline double cgasdev_ctr()
	{
		static int iset = 0 ;
		static float gset = 0.0 ;
		double gauss,r,v1,v2,fac;

		if (iset)  {
			gauss = gset ;
			iset = 0 ;
		}
		else  {
			r = 2. ;
			while (r >= 1.)  {
				v1 = 2.*ranc_ctr(0)-1. ;
				v2 = 2.*ranc_ctr(0)-1. ;
				r = v1*v1 + v2*v2 ;
			}
			fac = Sqrt(-2.*Ln(r)/r) ;
			gset = v1*fac ;
			gauss = v2*fac ;
			iset = 1 ;
		}
		return gauss;
	}

	inline double gaussian_distribution(const flt& x,const flt& sigma){return 1/Sqrt(2*_pi*sigma)*Exp(-Pow2(x)/(2*Pow2(sigma)));}
	inline double rayleigh_distribution(const flt& x,const flt& sigma){return x/Pow2(sigma)      *Exp(-Pow2(x)/(2*Pow2(sigma)));}

	inline int RandomSeed()
	{
		// fix problem with slow clock on VMware installations
		const clock_t e=clock() + static_cast<clock_t>(0.02 * CLOCKS_PER_SEC);
		while( clock()<e );

		struct timeval tv;
		gettimeofday(&tv,NULL);
		volatile const int sz=sizeof(tv.tv_usec);
		volatile const int s=clock()%13 + (((tv.tv_usec<<(sz-3))>>(sz-3))*100000 + tv.tv_usec)%15713 + (tv.tv_usec & 3 );
		return s<0 ? -s : s;
	}

	inline double Random(const int seed=-1)
	{
		static bool initialzed=false;
		if(!initialzed || seed!=-1){
			const int s=seed==-1 ? RandomSeed() : seed;
			initialzed=true;
			srand(s);
			//ranc_ctr(s);
		}
		const double r=rand();
		assert( r>=0 && r<RAND_MAX );
		return r/RAND_MAX;
	}

	///////////////////////////////////////////////////////////////////////
	// Interpolation
	///////////////////////////////////////////////////////////////////////

	template<class T,class R>
	R InterpolateLinear(const T& x,const map<T,R>& xy)
	{
		assert( xy.size()>2 );
		if (x<  xy.begin()->first) throw_("value of x less that lowerbound, x="     + tostring(x) + " lowerbound=" + tostring(xy.begin()->first));
		if (x>(--xy.end())->first) throw_("value of x greater that uppperbound, x=" + tostring(x) + " upperbound=" + tostring((--xy.end())->first));

		const typename map<T,R>::const_iterator i1=xy.upper_bound(x);
		if (i1==xy.end() || i1==xy.begin()) throw_(string("x out of range, x=") + tostring(x) + " lowerbound=" + tostring(xy.begin()->first) + " upperbound=" + tostring((--xy.end())->first) );
		typename map<T,R>::const_iterator i0=i1;
		--i0;

		assert( x>=i0->first && x<i1->first && i0->first<i1->first);
		const T dx=i1->first-i0->first;
		const R dy=i1->second-i0->second;
		const R y=i0->second + (x-i0->first)*dy/dx;
		return CheckFltValidity(y);
	}

	template<class T,class R>
	R Interpolate(const T& x1,const T& x2,const R& y1,const R& y2,const T& x)
	{
		if      (y1==y2) return y1; // constant y value
		else if (x==x1)  return y1; // low x limit
		else if (x==x2)  return y2; // high x limit

		assert( y1>=y2 ); // only for monotonic decreasing functions
		assert( x1<x2 && x1<x && x<x2 );
		const R dy=(y2-y1);
		const R a=dy/(x2-x1)*T(1);
		const R b=y1-a*x1/T(1);
		const R y=a*x/T(1)+b;
		#ifndef NDEBUG
			const R eps(y*1E-12);
			if(!(y2-eps<=y && y<=y1+eps )) cout << "\t¤¤ x=" << x << " x1=" << x1 << " x2=" << x2 << " y=" << y << " y1=" << y1 << " y2=" << y2 << endl;
			assert( min(y1,y2)-eps<=y && y<=max(y1,y2)+eps );
		#endif
		assert( FltValidity(y/R(1)) );
		return y;
	}

	///////////////////////////////////////////////////////////////////////
	// Integration (from http://www.astro.princeton.edu/~esirko/ic/)
	///////////////////////////////////////////////////////////////////////

	// 	2005-1-9. New Romberg integrator.  Supply limits (a,b) and two flags,
	// 	normally (1,1) meaning the integral is closed; the function can be
	// 	evaluated right on the limits (a,b).  If the flags are set to zero then
	// 	the function cannot be evaluated right on that limit and this routine
	// 	performs an open (or semi-open) interval integration.
	//
	// 	More details: The code can be divided into two parts, the trapezoidal
	// 	integrator and "Richardson's deferred approach to limit." See NR Fig 4.2.1:
	// 	the row labelled "N=2" is where we start with k=0.  Then the next row
	// 	(labelled N=3) is k=1, and so on, so that the number of function
	// 	evaluations per k step is n = 2^k.  dx is the spacing between the points.
	// 	fxna,fxnb,fxn0, and fxn1 are basically temporary variables which are
	// 	shuffled around depending on the closed flags.
	//
	// 	2005-1-10 adding a new criterion for convergence, because occasionally
	// 	I get convergent wrong answers.  Require the last two answer[k]'s to
	// 	agree to a certain fraction.  This doesn't have be a very small number.
	// 	Hm, actually it does.  We are dealing with an function that can be very
	// 	tricky if you sample at even intervals.  In order to improve, ideally
	// 	you would couple this integrator with a monte carlo abscissa integrator,
	// 	and make sure the results match.

	template<class T,class R,class S,class Q>
	S Integrate(const Q& funcobj,const R& a,const R& b,const int closed_a,const int closed_b)
	{
		const int kmax=21;
		const int nfit=8;

		int i,j,k,n;
		flt fxna=0,fxnb=0,dx,halfdx,fxn0,fxn1,sum,totsum,tentative,cdterm;
		flt answer[kmax],h[kmax],c[nfit],d[nfit];
		flt tolerance = 1.e-7, tolerance2=1.e-4, answer_discrep;

		n = 1;
		totsum = 0.;
		h[0] = 1.;
		if (closed_a) fxna = funcobj(a)/T(1);
		if (closed_b) fxnb = funcobj(b)/T(1);
		for (k=0;k<kmax;k++) {
			if (k) h[k] = .25 * h[k-1];
			dx = (b-a)/R(1) / static_cast<flt>(n);
			halfdx = dx/2.;
			fxn0 = funcobj(a + R(halfdx))/T(1);
			fxn1 = funcobj(b - R(halfdx))/T(1);
			if (n==1) sum = fxn0; else sum = fxn0 + fxn1;
			for (i=1;i<n-1;i++) {
				sum += funcobj(a + R(halfdx) + R(i*dx)) / T(1);
			}
			totsum += sum;
			if (!closed_a) fxna = fxn0;
			if (!closed_b) fxnb = fxn1;
			answer[k] = (dx/2.) * (totsum + .5*(fxna+fxnb)); // the tentative answer
			tentative = answer[k];
   			//printf("%d\t%g\t%g\t%f\n",k,answer[k],a,b);
			if (k >= nfit-1) { // This is the Neville part (NR 3.1)
				answer_discrep = fabs(answer[k]-answer[k-1]);
				if (answer_discrep <= tolerance2*fabs(tentative)) {// passes my new test
					for (i=0;i<nfit;i++) {
						c[i] = answer[k-nfit+1 + i];
						d[i] = answer[k-nfit+1 + i];
					}
					for (i=1;i<nfit;i++) {
						for (j=0;j<nfit-i;j++) {
							cdterm = (c[j+1]-d[j]) / (h[j]-h[j+i]);
							d[j] = h[j+i]*cdterm;
							c[j] = h[j]  *cdterm;
						}
						tentative += d[j-1];
						if (fabs(d[j]) < tolerance*fabs(tentative))	return R(tentative)*T(1);
					}
				}
			}
			n *= 2;
		}
		warn_("too many romberg steps");
		return S(0);
	}

	///////////////////////////////////////////////////////////////////////
	// min/max
	///////////////////////////////////////////////////////////////////////

	template<class T>
	inline T Max(const vector<T>& v,const unsigned int column,const unsigned int ncolumns)
	{
		if (v.size()==0) throw_("zero array in Max");
		T m=v[0];
		assert(column>=1);
		assert(v.size()%ncolumns==0);
		for(unsigned int i=column-1;i<v.size();i+=ncolumns)	if (v[i]>m) m=v[i];
		return m;
	}

	template<class T>
	inline T Min(const vector<T>& v,const unsigned int column,const unsigned int ncolumns)
	{
		if (v.size()==0) throw_("zero array in Min");
		T m=v[0];
		assert(column>=1);
		assert(v.size()%ncolumns==0);
		for(unsigned int i=column-1;i<v.size();i+=ncolumns)	if (v[i]<m) m=v[i];
		return m;
	}

	template<class T> T Mean(const vector<T>& x)
	{
		if (x.size()==0) throw_("mean of zero array");
		T mean=x[0];
		for(size_t n=1;n<x.size();++n) mean += x[n];
		return mean/x.size();
	}

	template<class T,class R>
	T MeanVar(const vector<T>& x,R& rms,R& sigma2)
	{
		if (x.size()==0) throw_("mean/var of zero array");

		T mean=x[0];
		rms=x[0]*x[0];
		sigma2=rms;

		for(size_t n=1;n<x.size();++n) {
			mean += x[n];
			rms  += x[n]*x[n];
		}

		mean /= x.size();
		rms  /= x.size();
		sigma2  = rms - mean*mean; // = <x^2> - <x>^2, or should var be=rms/(N-1) - mean*mean?

		return mean;
	}

}; // namespace Math

#endif // __MATHFUN_H__
