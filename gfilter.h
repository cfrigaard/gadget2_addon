void Reordering(data_info& inf)
{
	const size_t N=inf.NumPart;
	assert( N==inf.header1.particlestotal() );
	if (N==0) return;

	map<size_t,size_t> m;
	for(size_t i=0;i<N;++i) {
		const size_t id=inf.Id[i+1];

		map<size_t,size_t>::iterator itt=m.find(id);
		if (itt!=m.end()) cerr << "i=" << i << "  id=" << id << " already there  i=" << itt->second << "\n";
		assert(m.find(id)==m.end());
		m[id]=i;
	}

	particle_data* d=new particle_data[N];
	size_t i=0;
	for(map<size_t,size_t>::const_iterator itt=m.begin();itt!=m.end();++itt,++i) {
		const size_t id=itt->first;
		d[i]=inf.P[id];
	}
	delete[] inf.P;
	inf.P=d;
}

void Sortparticles(const string& ifile,const string& ofile,data_info& inf,const int filter,const bool sml,const bool nosort,const size_t gridsize,const size_t nearest)
{
	//cout << "% gfilter: ofile=" << ofile << " sml=" << sml << " nosort=" << nosort << " gridsize=" << gridsize << " nearest=" << nearest << endl;
	data_info inf2=FilterParticles(inf,filter);
	if (!nosort) {
		cerr << "%   reordering..." << endl;
		Reordering(inf2);
	}

	if(sml){
		cerr << "%   finding smoothing lenghts..." << endl;
		const vector<particle_data_2> v=ToVectorData(inf2);
		const searchgrid s(v,_len(0),inf.header1.bbox()(),gridsize,true);
		FindSmoothingLengths(v,s,nearest,false);

		vector<_len> l;
		l.reserve(v.size());
		for(size_t i=0;i<v.size();++i) l.push_back(v[i].h());
		Writeascii(ofile + ".sml",l,"smoothinglenghts");
	}

	if (!(inf.NumPart==inf2.NumPart && nosort) || ifile!=ofile) SaveSnapshot(inf2,ofile,true);
}

int Usage_Gsort(const args& a)
{
	cerr << "Usage: " << a[0] << " <inputfile> [-o <outputfile>] [-f <filter>] [-t] [-g gridsize] [-n neighbours] [-nf]\n";
	cerr << "   " << Version() <<  " " << Config() <<  " \n";
	cerr << "   inputfile: snapshot file\n";
	cerr << "Options:\n";
	cerr << "  -o <outputfile>: write data in this file instead of overwriting\n";
	cerr << "  -f <int>: 1=gas, 2=halo, 4=disk, 8=bulge, 16=stars, 32=bndry\n";
	cerr << "  -s: find smoothing lenghts in and write to file .sml\n";
	cerr << "  -g <int>: gridsize in search cube, default 20\n";
	cerr << "  -n <int>: nearest neighbours in calculationg smoothing lenghts, default 33\n";
	cerr << "  -ns: do no sort, for tagging purpose only\n";
	return -1;
}

int main_gfilter(int argc,char** argv)
{
	try{
		args a(argc,argv,false,false);
		if (a.size()<=1) return Usage_Gsort(a);

		const int filter=a.parseval<int>("-f",0);
		const bool sml=a.parseopt("-s");
		const bool nosort=a.parseopt("-ns");
		const size_t gridsize=a.parseval<size_t>("-g",20);
		const size_t nearest =a.parseval<size_t>("-n",33);

		if (nosort && !sml) warn_("nosort normally implies finding the smoothinglenghts, please use -s option");
		if(a.size()<2) return Usage_Gsort(a);
		const string ifile=a[1];
		const string ofile=a.parseval<string>("-o",ifile);
		if(a.size()!=2) return Usage_Gsort(a);

 		data_info inf=load_snapshot(ifile.c_str(),1);
		Sortparticles(ifile,ofile,inf,filter,sml,nosort,gridsize,nearest);

		return 0;
	}
	CATCH_ALL;
	return -1;
}
