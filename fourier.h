#ifndef __FOURIER_H__
#define __FOURIER_H__

namespace Math
{
	///////////////////////////////////////////////////////////////////////
	// inline conversions
	///////////////////////////////////////////////////////////////////////

	template<typename T,typename R> T totype_(const R& x,const flt& eps=1E-6)          {return static_cast<T>(x);} // eps not used
	template<typename T,typename R> T totype_(const complex<T>& x,const flt& eps=1E-6) {assert(eps==0 || Abs(x.imag())<T(eps)); return x.real();}

	template<class T,class R>
	vector<T> totype(const vector<R>& x,const flt eps=1E-6)
	{
		vector<T> y;
		y.resize(x.size());
		for(size_t i=0;i<x.size();++i) y[i]=totype_<T,R>(x[i],eps);
		return y;
	}

	template<class T,class R>
	vector<vector<T> > totype(const vector<vector<R> >& x,const flt eps=1E-6)
	{
		vector<vector<T> > y(x.size());
		for(size_t i=0;i<x.size();++i) y[i]=totype<T,R>(x[i],eps);
		return y;
	}

	template<class T,class R>
	array3d<T> totype(const array3d<R>& x,const flt eps=1E-6)
	{
		array3d<T> y(x.sizet());
		for(size_t i=0;i<x.size();++i) y[i]=totype_<T,R>(x[i],eps);
		return y;
	}

	///////////////////////////////////////////////////////////////////////
	// indexing functions
	///////////////////////////////////////////////////////////////////////

	inline int idx2k(const size_t idx,const size_t N)
	{
		assert( idx<N );
		if (idx<=N/2) return idx; // XXX is it <= or < ???
		else return -(N-idx);
	}

	inline Triple<int> idx2kidx(const t_idx& idx,const size_t N)
	{
		const Triple<int> kidx(idx2k(idx[0],N),idx2k(idx[1],N),idx2k(idx[2],N));
		assert( kidx[0]<(1<<16) && kidx[1]<(1<<16) && kidx[2]<(1<<16) );
		return kidx;
	}

	inline flt idx2klen(const t_idx& idx,const size_t N)
	{
		const Triple<int> kidx=idx2kidx(idx,N);
		return Sqrt(1.0*kidx[0]*kidx[0]+kidx[1]*kidx[1]+kidx[2]*kidx[2]);
	}

	inline Triple<_per_len> idx2kvec(const t_idx& idx,const size_t N,const _per_len dk)
	{
		const Triple<int> kidx=idx2kidx(idx,N);
		return Triple<_per_len>(kidx[0]*dk,kidx[1]*dk,kidx[2]*dk);
	}

	inline bool hasinv_idx(const size_t& idx,const size_t N)
	{
		return !(N%2==0 && idx==N/2);
	}

	inline bool hasinv_idx(const t_idx& idx,const size_t N)
	{
		return hasinv_idx(idx[0],N) && hasinv_idx(idx[1],N) && hasinv_idx(idx[2],N);
	}

	inline int idx2inv_idx(const size_t& idx,const size_t N)
	{
		// do not use inv indexes for N%2==0 and idx==N/2!
		//if (!hasinv_idx(idx,N)) warn_("index N/2 value with N%2==0 has no inv index");
		assert( idx<N );
		return idx==0 ? 0 : N-idx;
	}

	inline t_idx idx2inv_idx(const t_idx& idx,const size_t N)
	{
		return t_idx(idx2inv_idx(idx[0],N),idx2inv_idx(idx[1],N),idx2inv_idx(idx[2],N));
	}

	inline bool isidx_corner(const t_idx& idx,const size_t& N)
	{
		if (N%2!=0) return idx==t_idx(0,0,0);
		else{
			const size_t Nhalf=N/2;
			for(size_t i=0;i<3;++i) if (idx[i]!=0 && idx[i]!=Nhalf) return false;
			return true;
		}
	}

	///////////////////////////////////////////////////////////////////////
	// Fourier/Hermitian form functions
	///////////////////////////////////////////////////////////////////////

	// N=4, N%2==0
	//   idx: k=0-N-1, j=0-N-1, i=0-N-1
	//        0=DC, 1=1 pos k, 2=-2 k, 3=-1 k, kvec= {-2.-1,0,1}
	//   DC: (0,0,0), max -k (-2,-2,-2) -> no -(-k)
	//   corners: (0,0,0) (2,0,0) (0,2,0) ... (2,2,0) (2,2,2)
	// N=5, N%2==1
	//   idx: k=0-N-1, j=0-N-1, i=0-N-1
	//        0=DC, 1=1 pos k, 2=2 pos, 3=-2 k, 4=-1 k, kvec= {-2.-1,0,1,2}
	//   DC: (0,0,0), max -k=(-2,-2,-2) -> -(-k)=(2,2,2)
	//   corners: (0,0,0) only

	template<class T> bool isFourierFormOK  (const array3d<complex<T> >& a) {return (a.sizey()==a.sizez() && (a.sizez()/2+1==a.sizex() || a.sizez()==a.sizex()));}
	template<class T> bool isFourierSquare  (const array3d<complex<T> >& a) {if(!isFourierFormOK(a)) throw_("Fourier data not in redundant square (n,n,n) or in non-redundant (n,n,n/2+1) form"); return a.isSquare();}
	template<class T> bool hasImaginaryparts(const array3d<complex<T> >& a,const flt threshold=1E-9)
	{
		for(size_t i=0;i<a.size();++i) if (Abs(a[i].imag())>threshold) return true;
		return false;
	}

	template<class T> void DublicateFourierNyquistData(array3d<complex<T> >& a)
	{
		if (!isFourierSquare(a)) return;

		const size_t nby2p1=a.sizez()/2+1;
		for(size_t k=nby2p1+1;k<a.sizez();++k)
		for(size_t j=0;j<a.sizey();j++)
		for(size_t i=0;i<a.sizex();i++){
			a(i,j,k)=Conj(a(i,j,a.sizez()-k));
		}
	}

	template<class T>
	void EnforceHermitian(array3d<complex<T> >& a)
	{
		assert( isFourierFormOK(a) );

		const int n=a.sizez();
		const int nby2 = n/2;
		const int nby2p1 = nby2 + 1;
		const int nby2p1xn = nby2p1 * n;
		const flt sqrt2=Sqrt(2.);

		#define gen_index(i,j,k) (i)*nby2p1xn + (j)*nby2p1 + (k)
		for (int k=0;k<nby2+1;k+=nby2) { // do k=0 and k=Nyquist planes
			a[gen_index(0   ,0   ,k)] = sqrt2*a[gen_index(0   ,0   ,k)].real();
			a[gen_index(0   ,nby2,k)] = sqrt2*a[gen_index(0   ,nby2,k)].real();
			a[gen_index(nby2,0   ,k)] = sqrt2*a[gen_index(nby2,0   ,k)].real();
			a[gen_index(nby2,nby2,k)] = sqrt2*a[gen_index(nby2,nby2,k)].real();
			for (int i=1;i<nby2;i++) {
				a[gen_index(0   ,n-i ,k)] = Conj(a[gen_index(0,i,k)]);
				a[gen_index(nby2,n-i ,k)] = Conj(a[gen_index(nby2,i,k)]);
				a[gen_index(n-i ,0   ,k)] = Conj(a[gen_index(i,0,k)]);
				a[gen_index(n-i ,nby2,k)] = Conj(a[gen_index(i,nby2,k)]);
			}
			for (int j=1;j<nby2;j++) {
				for (int i=1;i<nby2;i++)   a[gen_index(n-j,n-i,k)] = Conj(a[gen_index(j,i,k)]);
				for (int i=nby2+1;i<n;i++) a[gen_index(n-j,n-i,k)] = Conj(a[gen_index(j,i,k)]);
			}
		}
		#undef gen_index
		DublicateFourierNyquistData(a);
	}

	template<class T>
	bool areCornersReal(const array3d<complex<T> >& X,const flt eps=1E-12,const bool verbose=false)
	{
		const size_t N=X.sizex();
		const size_t Nhalf=N/2;
		if(!X.isSquare()) throw_("cannot handle non-square data");

		if (N%2!=0) return Abs(X[t_idx(0,0,0)].imag())<eps; // bad handling of N%2!=0

		int r=0;
		for(size_t k=0;k<N;k+=Nhalf){
			for(size_t j=0;j<N;j+=Nhalf){
				for(size_t i=0;i<N;i+=Nhalf){
					const t_idx idx(i,j,k);
					if (Abs(X[idx].imag())<eps) ++r;
					else if (verbose) cout << "** non-real corner: " << idx << " imag=" << X[idx].imag() << " N=" << N << endl;
				}
			}
		}
		return r==8;
	}

	template<class T>
	bool isHermitian(const array3d<complex<T> >& X,const flt eps=1E-12,const bool verbose=false)
	{
		const size_t N=X.sizex();
		if(!X.isSquare()) throw_("cannot handle non-square data");

		bool herm=true;
		// test all FFT data below N/2 and aliased data above
		for(size_t k=0;k<N;++k){
			for(size_t j=0;j<N;++j){
				for(size_t i=0;i<N;++i){
					const t_idx idx=t_idx(i,j,k);
					if (hasinv_idx(idx,N)) { // avoid -k-values, that have no -(k) due to N being even
						const t_idx inv_idx=idx2inv_idx(idx,N);

						const cmplx c1=X[idx];
						const cmplx c2=Conj(X[inv_idx]);

						const bool e1=_proximy(c1.real(),c2.real(),eps);
						const bool e2=_proximy(c1.imag(),c2.imag(),eps);

						if (!(e1 && e2)) {
							herm=false;
							if (verbose) cerr << "% idx=" << idx << " inv_idx" << inv_idx << " c1=" << c1 << " c2=" << c2 << " e1=" << e1 << " e2=" << e2 << endl;
							else return false;
						}
					}
				}
			}
		}
		return herm; //&& areCornersReal(X,eps,verbose);
	}

	template<class T>
	void HermitianConstraint(array3d<complex<T> >& X)
	{
		const size_t N=X.sizex();
		const flt sqrt2=Sqrt(2.0);
		if(!X.isSquare()) throw_("cannot handle non-square data");
		int r=0;

		// FFT data below N/2, and above, some redunancy in calc
		for(size_t k=0;k<N;++k){
			for(size_t j=0;j<N;++j){
				for(size_t i=0;i<N;++i){
					const t_idx idx=t_idx(i,j,k);
					if (hasinv_idx(idx,N)){
						const cmplx c1=X[idx];
						const t_idx inv_idx=idx2inv_idx(idx,N);
						X[inv_idx]=Conj(c1);
					}
					if (isidx_corner(idx,N)){
						// cout << "** idx=" << idx << " N=" << N << " r=" << r << endl;
						X[idx]=sqrt2*X[idx].real();
						++r;
					}
				}
			}
		}
		assert( areCornersReal(X) );
		assert( (r==8 || (N%2==1 && r==1)) && isHermitian(X) );
	}

	///////////////////////////////////////////////////////////////////////
	// Fourier functions
	///////////////////////////////////////////////////////////////////////

	// Fourier 1D

	//  For length N input vector x, the DFT is a length N vector X,
	//     with elements
	//                      N
	//        X(k) =       sum  x(n)*exp(-j*2*pi*(k-1)*(n-1)/N), 1 <= k <= N.
	//                     n=1
	//     The inverse DFT (computed by IFFT) is given by
	//                      N
	//        x(n) = (1/N) sum  X(k)*exp( j*2*pi*(k-1)*(n-1)/N), 1 <= n <= N.
	//                     k=1
	//

	inline vector<cmplx> Ft(const vector<cmplx>& x)
	{
		const size_t N=x.size();
		vector<cmplx> F(N);
		for(size_t k=0;k<N;++k){
			for(size_t i=0;i<N;++i) {
				const cmplx a(0,-2.0*Constants::_pi*i*k/N);
				F[k] += x[i] * Exp(a);
			}
		}
		return F;
	}

	inline vector<cmplx> iFt(const vector<cmplx>& F)
	{
		const size_t N=F.size();
		vector<cmplx> x(N);
		for(size_t i=0;i<N;++i) {
			for(size_t k=0;k<N;++k){
				const cmplx a(0,2.0*Constants::_pi*i*k/N);
				const cmplx c=F[k] * Exp(a) / cmplx(N,0);
				//assert( c.imag()<1E-3 );
				x[i] += c;
			}
		}
		return x;
	}

	inline vector<cmplx> Ft_base(const vector<cmplx>& x,const bool dir)
	{
		if (dir) return  Ft(x);
		else     return iFt(x);
	}


	#ifndef USE_FFTW

	// Fast Fourier 1D

	// This computes an in-place complex-to-complex FFT
	// x and y are the real and imaginary arrays of 2^m points.
	// dir =  1 gives forward transform
	// dir = -1 gives reverse transform
	// Source: http://local.wasp.uwa.edu.au/~pbourke/other/dft/
	inline void FFt_raw(const short int dir,const long m,double *x,double *y)
	{
		assert( dir==1 || dir==-1 );
		assert( m>=0 );
		assert( x!=0 && y!=0 );

		long n,i,i1,j,k,i2,l,l1,l2;
		double c1,c2,tx,ty,t1,t2,u1,u2,z;

		// Calculate the number of points
		n = 1;
		for (i=0;i<m;i++) n *= 2;

		// Do the bit reversal
		i2 = n >> 1;
		j = 0;
		for (i=0;i<n-1;i++) {
			if (i < j) {
				tx = x[i];
				ty = y[i];
				x[i] = x[j];
				y[i] = y[j];
				x[j] = tx;
				y[j] = ty;
			}
			k = i2;
			while (k <= j) {
				j -= k;
				k >>= 1;
			}
			j += k;
		}

		// Compute the FFT
		c1 = -1.0;
		c2 = 0.0;
		l2 = 1;
		for (l=0;l<m;l++) {
			l1 = l2;
			l2 <<= 1;
			u1 = 1.0;
			u2 = 0.0;
			for (j=0;j<l1;j++) {
				for (i=j;i<n;i+=l2) {
					i1 = i + l1;
					t1 = u1 * x[i1] - u2 * y[i1];
					t2 = u1 * y[i1] + u2 * x[i1];
					x[i1] = x[i] - t1;
					y[i1] = y[i] - t2;
					x[i] += t1;
					y[i] += t2;
				}
				z =  u1 * c1 - u2 * c2;
				u2 = u1 * c2 + u2 * c1;
				u1 = z;
			}
			c2 = Sqrt((1.0 - c1) / 2.0);
			if (dir == 1) c2 = -c2;
			c1 = Sqrt((1.0 + c1) / 2.0);
		}

		// Scaling for forward transform, XXX forward here means backward in this file (dir=false=> dir==1)
		if (dir == 1) {
			for (i=0;i<n;i++) {
				x[i] /= n;
				y[i] /= n;
			}
		}
	}

	template<class T>
	inline T PowersOf2(const T& x)
	{
		assert( numeric_limits<T>::is_signed==false && numeric_limits<T>::is_integer==true &&  numeric_limits<T>::is_exact==true);
		assert( x<numeric_limits<T>::max()/2);
		T n=1,m=0;
		if      (x==0) return 0;
		else if (x==1) return 1;
		else {
			while(n<x) {n*=2; ++m;}
			assert( n>0 &&  n>=x && T(1)<<m==n);
			return m;
		}
	}

	template<class T>
	inline T isPowersOf2(const T& x){return x==(T(1)<<PowersOf2(x));}

	inline vector<cmplx> FFt_base(const vector<cmplx>& f,const bool dir)
	{
		const size_t n_org=f.size();
		if (n_org==0) return vector<cmplx>();

		const size_t m=PowersOf2(f.size());
		const size_t n=1<<m;

		if (n_org!=n) throw_("FFt cannot handle non powers-of-two yet");

		assert( n>=n_org && m>0 );
		vector<flt> x(n),y(n);
		for(size_t i=n_org;i<n;++i) x[i]=y[i]=0; // pad zero

		if (dir==1){
			for(size_t i=0;i<n_org;++i) x[i]=f[i].real(), y[i]=f[i].imag();
		} else {
			x[0]=f[0].real(), y[0]=f[0].imag(); // copy DC
			for(size_t i=1;i<n_org;++i) x[n_org-i]=f[i].real(), y[n_org-i]=f[i].imag(); // Copy all but DC, reverse order
		}

		assert( n==x.size() && x.size()==y.size() );
		FFt_raw(dir ? -1 : 1,m,&x[0],&y[0]);

	    assert(n_org>0);
		vector<cmplx> F(n_org);
		if (dir==1){
			F[0]=cmplx(x[0],y[0]); // copy DC
			for(size_t i=1;i<n_org;++i) F[n_org-i]=cmplx(x[i],y[i]); // Copy all but DC, reverse order
		} else {
			for(size_t i=0;i<n_org;++i) F[i]=cmplx(x[i],y[i]);
		}
		return F;
	}

	inline vector<cmplx>  FFt(const vector<cmplx>& f){return FFt_base(f,true);}
	inline vector<cmplx> iFFt(const vector<cmplx>& F){return FFt_base(F,false);}

	#else // external fft algorithms

	inline array3d<cmplx> fromarray3Complex(const array3<Complex>& x)
	{
		const size_t nx=x.Nx(),ny=x.Ny(),nz=x.Nz();
		array3d<cmplx> f(nx,ny,nz);
		for(size_t k=0;k<nz;++k){
			for(size_t j=0;j<ny;++j){
				for(size_t i=0;i<nx;++i){
					const Complex& c=x(i,j,k);
					f(i,j,k)=cmplx(c.real(),c.imag());
				}
			}
		}
		return f;
	}

	inline array3<Complex> toarray3Complex(const array3d<cmplx>& x)
	{
		const size_t nx=x.sizex(),ny=x.sizey(),nz=x.sizez();
		array3<Complex> f(nx,ny,nz,sizeof(Complex));
		for(size_t k=0;k<nz;++k){
			for(size_t j=0;j<ny;++j){
				for(size_t i=0;i<nx;++i){
					const cmplx& c=x(i,j,k);
					f(i,j,k)=Complex(c.real(),c.imag());
				}
			}
		}
		return f;
	}

	inline vector<cmplx> FFt(const vector<flt>& x)
	{
		const size_t N=x.size();
		array1<Complex> f(N);
		for(size_t i=0;i<N;++i) f[i]=Complex(x[i],0);

		fft1d Forward1(-1,f);
		Forward1.fft(f);

		vector<cmplx> y(N);
		for(size_t i=0;i<N;++i) y[i]=cmplx(f[i].real(),f[i].imag());
		return y;
	}

	inline vector<flt> iFFt(const vector<cmplx>& x)
	{
		const size_t N=x.size();
		array1<Complex> f(N);
		for(size_t i=0;i<N;++i) f[i]=Complex(x[i].real(),x[i].imag());

		fft1d Backward1(1,f);
		Backward1.fftNormalized(f);

		vector<flt> y(N);
		for(size_t i=0;i<N;++i) y[i]=f[i].real();
		return y;
	}

	#endif // USE_FFTW

	// Fourier 2D

	inline vector<vector<cmplx> > FourierTrans2_base(const vector<vector<cmplx> >& x,const bool dir,const bool fast)
	{
		const size_t N=x.size();
		if (N<2) throw_("to small datasize in Fourier transform");

		vector<vector<cmplx> > F;
		for(size_t j=0;j<N;++j)	{
			assert( x[j].size() == x[0].size() );
			const vector<cmplx>& y=x[j];
			// F.push_back(dir ? (fast ? FFt(y) : Ft(y) : (fast ? iFFt(y) : iFt(y)) );
			F.push_back(fast ? FFt_base(y,dir) : Ft_base(y,dir) );
		}

		if (F.size()<1) throw_("to small datasize in Fourier transform");
		const size_t M=F[1].size();

		for(size_t i=0;i<M;++i)	{
		vector<cmplx> c,f;
			for(size_t j=0;j<N;++j)	c.push_back(F[j][i]);
			//f=dir ? (fast ? FFt(c) : Ft(c)) : (fast ? iFFt(c) : iFt(c));
			f=fast ? FFt_base(c,dir) : Ft_base(c,dir);
			for(size_t j=0;j<N;++j)	F[j][i]=f[j];
		}
		return F;
	}

	inline vector<vector<cmplx> >  Ft2 (const vector<vector<cmplx> >& x) {return FourierTrans2_base(x,true ,false);}
	inline vector<vector<cmplx> > iFt2 (const vector<vector<cmplx> >& x) {return FourierTrans2_base(x,false,false);}

	inline vector<vector<cmplx> >  FFt2(const vector<vector<cmplx> >& x) {return FourierTrans2_base(x,true ,true);}
	inline vector<vector<cmplx> > iFFt2(const vector<vector<cmplx> >& x) {return FourierTrans2_base(x,false,true);}

	// Fourier 3D

	inline array3d<cmplx> FourierTrans3_base(const array3d<cmplx>& x,const bool dir,const bool fast)
	{
		if (x.sizex()<2 || x.sizey()<2 || x.sizez()<2) throw_("to small datasize in Fourier transform");
		array3d<cmplx> c(x.sizex(),x.sizey(),x.sizez());

		for(size_t k=0;k<x.sizez();++k)	{
			vector<vector<cmplx> > t;
			for(size_t j=0;j<x.sizey();++j)	{
				vector<cmplx> t2;
				for(size_t i=0;i<x.sizex();++i)	{
					t2.push_back(x(i,j,k));
				}
				t.push_back(t2);
			}
			const vector<vector<cmplx> > t3=FourierTrans2_base(t,dir,fast);
			for(size_t j=0;j<x.sizey();++j)	{
				for(size_t i=0;i<x.sizex();++i)	{
					assert( j<t3.size() && i<t3[j].size() );
					c(i,j,k)=t3[j][i];
				}
			}
		}

		for(size_t j=0;j<x.sizey();++j)	{
			for(size_t i=0;i<x.sizex();++i)	{
				vector<cmplx> t;
				for(size_t k=0;k<x.sizez();++k)	{
					t.push_back(c(i,j,k));
				}
				const vector<cmplx> t2=fast ? FFt_base(t,dir) : Ft_base(t,dir);
				for(size_t k=0;k<x.sizez();++k)	{
					c(i,j,k)=t2[k];
				}
			}
		}
		return c;
	}

	inline array3d<cmplx>  Ft3(const array3d<cmplx>& x) {return FourierTrans3_base(x,true ,false);}
	inline array3d<cmplx> iFt3(const array3d<cmplx>& x) {return FourierTrans3_base(x,false,false);}

	#ifndef USE_FFTW

	inline array3d<cmplx>  FFt3(const array3d<cmplx>& x) {return FourierTrans3_base(x,true ,true);}
	inline array3d<cmplx> iFFt3(const array3d<cmplx>& x) {return FourierTrans3_base(x,false,true);}

	#else // USE_FFTW

	inline array3d<cmplx> FFt3(const array3d<cmplx>& x)
	{
		array3<Complex> f=toarray3Complex(x);
		fft3d Forward3(-1,f);
		Forward3.fft(f);

		const array3d<cmplx> y=fromarray3Complex(f);
		if( y.sizex()!=x.sizex() ) throw_ ("bad x");
		if( y.sizey()!=x.sizey() ) throw_ ("bad y");
		if( y.sizez()!=x.sizez() ) throw_ ("bad z");
		return y;
	}

	inline array3d<cmplx> iFFt3(const array3d<cmplx>& x)
	{
		array3<Complex> f=toarray3Complex(x);
		fft3d Backward3(1,f);
		Backward3.fftNormalized(f);
		return fromarray3Complex(f);
	}
	#endif // USE_FFTW
}; // namespace Math

#endif // __FOURIER_H__
