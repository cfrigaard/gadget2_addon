#ifndef __INC_H__
#define __INC_H__

// Include strategy: all headers included here,
//                   all files compiled i one blob
// reason: STL and other templates takes the longest to compile,
// only done once in this non-c object file mode

#ifdef WIN32
	// Will probaly not work
	#define _ARCH_WIN32_
	#pragma warning( disable : 4244)
	#pragma warning( disable : 4018)
	#pragma warning( disable : 4267)
	#pragma warning( disable : 4305)
#endif
#ifdef __GNUC__
	#if __GNUC__ > 3
		#define __GCC_V4__
	#endif
#endif

#ifdef USE_MPI
	#include <mpi.h>
#endif

#include <cassert>
#include <cmath>
#include <complex>
#include <ctime>
#include <time.h>
#include <sys/time.h>
#include <vector>
#include <map>
#include <list>
#include <string>
#include <iostream>
#include <sstream>
#include <fstream>

#include <cstring>
#include <cstdlib>
#include <limits>
#include <algorithm>

#ifdef USE_FFTW
	#include "Addon/Array.h"
	#include "Addon/fftw++.h"
	#include "Addon/convolution.h"
#endif

using namespace std;
typedef double flt;

#include "ver.h"
#include "constants.h"
#include "exception.h"
#include "stringfun.h"
#include "funs.h"
#include "triple.h"
#include "configfile.h"
#include "units.h"

using namespace GadgetUnitSystem;

#include "file.h"
#include "array.h"
#include "mathfun.h"
#include "fourier.h"

using namespace Math;

#include "args.h"
#include "bbox.h"
#include "range.h"
#include "bins.h"
#include "compressedarray.h"
#include "configfile.h"
#include "structs.h"
#include "gadgetfun.h"
#include "graficheader.h"
#include "search.h"
#include "sigma.h"
#include "sph.h"
#include "mpi.h"

#endif // __INC_H__
