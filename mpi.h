#ifndef __MPI_H__
#define __MPI_H__

#ifndef USE_MPI
class Mpi // dummy mpi class
{
	public:
	Mpi(int argc=0,char** argv=0){}
	~Mpi() {}

	size_t Rank    () const {return 0;}
	size_t Size    () const {return 1;}
	string Core    () const {return "unknown";}
	bool   isMaster() const {return true;}

	size_t Lower(const size_t n) const {return 0;}
	size_t Upper(const size_t n) const {return n;}

	template<class T>
 	void CollectVector(vector<T>& x,const size_t N,const bool binary=true) const {}

	template<class T,class R>
 	void CollectMap(map<T,R>& x,const bool binary=true,const int mode=0) const {}

	template<class T>
	const Mpi& operator<<(const T& x) const {cerr << x;return *this;}

	// output endl and flush operator
	struct Mpiendl{int flush; Mpiendl(int f) : flush(f) {}; };
	const Mpi& operator<<(const Mpiendl& e) const
	{
		cout.flush();
		if (e.flush==0) cerr << endl;
		cerr.flush();
		return *this;
	}
	#define mpiendl  Mpi::Mpiendl(0)
	#define mpiflush Mpi::Mpiendl(1)

	friend ostream& operator<<(ostream& s,const Mpi& x)     {return s << "Mpi:{rank=" << x.Rank() << " size=" << x.Size() << " core=" << x.Core() << "}";}
};
#else

#define mpi_throw_(msg) {warn_(msg); MPI_Abort(MPI_COMM_WORLD,99);}

class Mpi
{
	private:
	mutable MPI_Status m_status;
	mutable string m_outputbuffer;
	size_t m_rank,m_size;
	bool m_asymetric;
	int m_bogmipsototal;
	vector<int> m_bogomips;
	bool m_outputenabled;

	enum {TAG_COLLECT_VEC=2,TAG_COLLECT_MAP=3} t_tags;

	static void CheckMPIReturnCode(const int e,const string& msg,const char* file,const int line){
		if (e!=MPI_SUCCESS) {
			const string msg2=Exception::FormatCompilerMsg(file,line) + msg;
			mpi_throw_(string("Mpi return error, e=") + e + " in " + msg2);
		}
	}
	#define CHECH_MPI_RETURN(f,msg) CheckMPIReturnCode(f,msg, __FILE__, __LINE__)

	size_t GetCount(MPI_Datatype datatype) const
	{
		int count=0;
		MPI_Get_count(&m_status,datatype,&count);
		assert(count>=0);
		return static_cast<size_t>(count);
	}

	void flush(const int f) const
	{
		cout.flush();
		cerr.flush();
		if (m_outputbuffer.size()>0) {
			if (Size()>1 && m_outputenabled) cerr << "% [" << Rank() << "]: ";
			cerr << m_outputbuffer;
			if (f==0) cerr << endl;
			else cerr.flush();
		}
		m_outputbuffer="";
	}

	static int BogoMips(const int seconds=1)
	{
		volatile flt x=0;
		volatile size_t N=0;
		const clock_t endwait = clock () + seconds * CLOCKS_PER_SEC;
		while (clock() < endwait) {
			// do some dummy float/integer ops for some cpu time
			for(size_t i=0;i<10000;++i) {
				x += i*i;
				x += Sin(0.1*i);
				x += Exp(Cos(0.1*i));
			}
			++N;
		}
		return static_cast<int>(1.0*N/seconds+.5);
	}

	size_t Lower_asymetric(const size_t r,const size_t n) const
 	{
		assert( m_asymetric );
		const size_t s=Size();
		assert(s>=1 && r<s);
  	    if (r>n && s>1) throw_(string("Mpi range lower, r=") + r +" greater than n=" + n );
  	    if (r==0) return 0;
  	    assert( r>0 && r+1<m_bogomips.size() );
  	    const unsigned long long x=static_cast<unsigned long long>(m_bogomips[r])*(n/s)/1000;
  	    return static_cast<size_t>(x);
	}

 	size_t Upper_asymetric(const size_t r,const size_t n) const
 	{
		assert( m_asymetric );
		const size_t s=Size();
		assert(s>=1 && r<s);
  	    if (r>=n && s>1) throw_(string("Mpi range upper, r=") + r + " greater than n=" + n);
  	    if (r+1==s) return n;
  	    assert( r>0 && r+1<m_bogomips.size() );
   	    const unsigned long long x=static_cast<unsigned long long>(m_bogomips[r+1])*(n/s)/1000;
  	    assert( Lower(r,n)<x );
  	    return static_cast<size_t>(x);
	}

	public:
	Mpi(int argc=0,char** argv=0,const bool asymetric=false) : m_rank(0), m_size(0), m_asymetric(asymetric), m_outputenabled(false)
	{
	    
		static volatile bool initialized=false;
		if (!initialized){
			char dummyarg1[]="executable";
			char* dummyargs[2];
			dummyargs[0]=dummyarg1;
			dummyargs[1]=NULL;
			if (argc==0){
				argc=1;
				argv=dummyargs;
			}

			initialized=true; // XXX, thread race condition
			CHECH_MPI_RETURN(MPI_Init(&argc,&argv),"multiple Mpi class initialized, in PeekRank()");

			int r,s;
			CHECH_MPI_RETURN(MPI_Comm_rank(MPI_COMM_WORLD,&r),"MPI_Comm_rank, CTOR");
			CHECH_MPI_RETURN(MPI_Comm_size(MPI_COMM_WORLD,&s),"MPI_Comm_size, CTOR");
			assert( r>=0 && s>=0 );
			m_rank=r;
			m_size=s;

			if (m_size>1){
				vector<string> cores(m_size);
				m_bogomips.resize(m_size);
				const string ver="MPI sync:" + Version();

				m_bogomips[0]=(m_asymetric ? BogoMips() : 1);
				cores[0]=Core();
				if(isMaster()){
					for(size_t i=1;i<Size();++i) {
						const string c=RecvString(i);
						const string v=RecvString(i);
						const int    b=RecvInt(i);
						if (v!=ver) mpi_throw_("not same binary versions, master core=" + Core() + " version=" + ver + " slave core=" + c + " version=" + v);

						assert( i<m_bogomips.size() );
						m_bogomips[i]=b;
						cores[i]=c;
					}
					for(size_t i=1;i<Size();++i) Send(m_bogomips,i);

				} else {
					SendString(Core(),0);
					SendString(ver,0);
					SendInt(BogoMips(),0);
					Recv(m_bogomips,0); // get total bogomips
				}

				if (m_asymetric) {
					assert( m_bogomips.size()==m_size );
					m_bogmipsototal=0;
					for(unsigned int i=0;i<m_bogomips.size();++i) m_bogmipsototal+=m_bogomips[i];
					assert( m_bogmipsototal>0  );
					int ftot=0;
					for(unsigned int i=0;i<m_bogomips.size();++i) {
						const int f=(1000*m_bogomips[i])/m_bogmipsototal;
						if (isMaster()) cout << "%  Mpi sync: core=" << cores[i] << " bogomips=" << m_bogomips[i] << " fraction=" << f/1000.0 << endl;
						m_bogomips[i] = f + (i>0 ? m_bogomips[i-1] : 0 ); // accumulated
						assert( i==0 || m_bogomips[i]>m_bogomips[i-1] );
						ftot += m_bogomips[i];
					}
					assert( ftot<=1000 && ftot>950 );
					m_bogomips.back()=1000;
				}
			}
		}
		else mpi_throw_("multiple Mpi class initializations");

		assert( MPI_ANY_TAG!=TAG_COLLECT_VEC );
		assert( MPI_ANY_TAG!=TAG_COLLECT_MAP );
	}
	~Mpi() {flush(0); MPI_Finalize();}

	bool   isMaster() const {return Rank()==0;}
	size_t Rank    () const {return m_rank;}
	size_t Size    () const {return m_size;}
	void   Barrier () const {CHECH_MPI_RETURN(MPI_Barrier(MPI_COMM_WORLD),"Barrier");}
	string Core    () const {int length; char name[256]; CHECH_MPI_RETURN(MPI_Get_processor_name(name,&length),"Core"); return name; }
	size_t RecvNode() const {assert(m_status.MPI_SOURCE>=0); return m_status.MPI_SOURCE;}
	void   EnableOutput()   {m_outputenabled=true;}
	static int PeekRank()   {return 0;}

	size_t Lower(const size_t n) const {return Lower(Rank(),n);}
	size_t Upper(const size_t n) const {return Upper(Rank(),n);}

// 	size_t Lower(const size_t r,const size_t n) const {assert(Size()>=1); return r*(n/Size());}
// 	size_t Upper(const size_t r,const size_t n) const {assert(Size()>=1); return r+1==Size() ? n : Lower(r,n)+n/Size();}
//
// 	size_t Lower(const size_t r,const size_t n) const
// 	{
// 		assert(Size()>=1);
// 		if (r>n && Size()>1) warn_(string("Mpi range lower, r=") + r +" greater than n=" + n );
// 		return r*(n/Size());
// 	}
// 	size_t Upper(const size_t r,const size_t n) const
// 	{
// 		assert(Size()>=1);
// 		if (r>=n && Size()>1) warn_(string("Mpi range upper, r=") + r + " greater than n=" + n);
// 		return r+1==Size() ? n : Lower(r,n)+n/Size();
// 	}

 	size_t Lower(const size_t r,const size_t n) const
 	{
		if (m_asymetric) return Lower_asymetric(r,n);
		const size_t s=Size();
		assert(s>=1 && r<s);
		if (r>n && s>1) throw_(string("Mpi range lower, r=") + r +" greater than n=" + n );
		return r*(n/s);
	}

 	size_t Upper(const size_t r,const size_t n) const
 	{
		if (m_asymetric) return Upper_asymetric(r,n);
		const size_t s=Size();
		assert(s>=1 && r<s);
		if (r>=n && s>1) throw_(string("Mpi range upper, r=") + r + " greater than n=" + n);
		return r+1==s ? n : Lower(r,n)+n/s;
	}

	template<class T>
	const Mpi& operator<<(const T& x) const
	{
		if (m_outputenabled || isMaster()) m_outputbuffer += tostring(x);
		return *this;
	}

	// output endl and flush operator
	struct Mpiendl{int flush; Mpiendl(int f) : flush(f) {}; };
	const Mpi& operator<<(const Mpiendl& e) const {flush(e.flush); return *this;}
	#define mpiendl  Mpi::Mpiendl(0)
	#define mpiflush Mpi::Mpiendl(1)

	friend ostream& operator<<(ostream& s,const Mpi& x) {return s << "Mpi:{rank=" << x.Rank() << " size=" << x.Size() << " core=" << x.Core() << "}";}

 	void SendInt(const int n,const int node,const int tag=0) const
 	{
		int m=n;
		if(0!=MPI_Send(&m, 1, MPI_INT, node, tag, MPI_COMM_WORLD)) mpi_throw_("SendInt() failed");
	}

 	void SendString(const string& s,const int node,const int tag=0) const {SendBinary(s.c_str(),s.size(),node,tag);}

 	void SendBinary(const char* c,const size_t n,const int node,const int tag=0) const
	{
		SendInt(n,node,tag);
		assert( sizeof(char)==1 ); // fit in 8 bit MPI_BYTE
		char* pc=const_cast<char*>(c);
		if (0!=MPI_Send (pc, n, MPI_BYTE, node, tag, MPI_COMM_WORLD)) mpi_throw_("SendBinary() failed");
	}

 	int RecvInt(const int node=MPI_ANY_SOURCE,const int tag=MPI_ANY_TAG) const
 	{
		int n=0;
		if(0!=MPI_Recv(&n, 1, MPI_INT, node, tag, MPI_COMM_WORLD, &m_status)) mpi_throw_("RecvInt() failed");
		if(GetCount(MPI_INT)!=1) throw("truncated mpi channel");
		return n;
 	}

	string RecvString(const int node=MPI_ANY_SOURCE,const int tag=MPI_ANY_TAG) const
	{
		const vector<char> v=RecvBinary(node,tag);
		string s;
		s.resize(v.size());
		for(size_t i=0;i<v.size();++i) s[i]=v[i];
		return s;
 	}

	vector<char> RecvBinary(const int node=MPI_ANY_SOURCE,const int tag=MPI_ANY_TAG) const
	{
		int n=RecvInt(node,tag);
		vector<char> v;
		v.resize(n);
		assert( sizeof(char)==1 ); // fit in 8 bit MPI_BYTE
		char* pc=reinterpret_cast<char*>(&v[0]);
		if(0!=MPI_Recv(pc, n, MPI_BYTE, node, tag, MPI_COMM_WORLD, &m_status)) mpi_throw_("RecvBinary() failed");
		if(GetCount(MPI_BYTE)!=static_cast<size_t>(n)) throw("truncated mpi channel");
		return v;
 	}

	template<class T> void SendBin(const T* x,const size_t n,const int node=MPI_ANY_SOURCE,const int tag=0) const {SendBinary(reinterpret_cast<const char*>(x),n*sizeof(T),node,tag);}
 	template<class T> void RecvBin(      T* x,const size_t n,const int node=MPI_ANY_SOURCE,const int tag=0) const
 	{
		const vector<char> v=RecvBinary(node,tag);
		if (v.size()!=n*sizeof(T)) mpi_throw_("bad size in RecvBinary");
 		memcpy(x,&v[0],v.size());
	}

	template<class T> void SendBin(const vector<T>& x,const int node=MPI_ANY_SOURCE,const int tag=0) const {SendBin(&x[0],x.size(),node,tag);}
 	template<class T> void RecvBin(      vector<T>& x,const int node=MPI_ANY_SOURCE,const int tag=0) const {RecvBin(&x[0],x.size(),node,tag);}

	template<class T,class R> void SendBin(const map<T,R>& x,const int node=MPI_ANY_SOURCE,const int tag=0) const
	{
 		SendInt(x.size(),node,tag);
		vector<pair<T,R> > v(x.size());
		size_t i=0;
 		for(typename map<T,R>::const_iterator itt=x.begin();itt!=x.end();++itt,++i) v[i]=*itt;
		SendBin(v,node,tag);
	}

 	template<class T,class R> void RecvBin(map<T,R>& x,const int node=MPI_ANY_SOURCE,const int tag=0) const
 	{
		const size_t n=RecvInt(node,tag);
		vector<pair<T,R> > v(n);
		RecvBin(v,node,tag);
 		for(size_t i=0;i<n;++i) {
			const T& t=v[i].first;
			const R& r=v[i].second;
			if (x.find(t)!=x.end()) mpi_throw_("map value not uniqe in RecvBin (map)");
			x[t]=r;
		}
	}

	template<class T> void Send   (const T& x,const int node=MPI_ANY_SOURCE,const int tag=0) const {SendString(tostring(x),node,tag);}
 	template<class T> void Recv   (      T& x,const int node=MPI_ANY_SOURCE,const int tag=0) const {x=totype<T>(RecvString(node,tag));}

	template<class T>
 	void CollectVector(vector<T>& x,const size_t N,const bool binary=true)
	{
		timer tim;
		if (Size()==1) return;
		const size_t r=Upper(N)-Lower(N);
		if( x.size()!=r ) mpi_throw_("bad size of vector, x.size!=Upper-Lower");

		if (!isMaster()) {
			SendInt(Rank(),0,TAG_COLLECT_VEC);
			if (binary) SendBin(x,0);
			else        Send   (x,0);
		}
		else {
			vector<bool> v(N),r(Size());
			for(size_t i=0;i<v.size();++i) v[i]=i<N;
			for(size_t i=0;i<r.size();++i) r[i]=(i==0);

			x.resize(N);
			//size_t count=0;
			//while(++count<Size())
			for(size_t count=1;count<Size();++count)
			{
				//const size_t i=RecvInt(MPI_ANY_SOURCE,TAG_COLLECT_VEC);
				const size_t i=RecvInt(count,TAG_COLLECT_VEC);
				if (r[i] || i==0 || i>Size() || i!=count) mpi_throw_("Bad receive node in CollectVector, i=" + tostring(i));
				r[i]=true;

				const size_t l=Lower(i,N);
				const size_t u=Upper(i,N);
				vector<T> y(u-l);

				// cout << "** c=" << count <<" l=" << l << " u=" << u << endl;
				if (binary) RecvBin(y,i);
				else 		Recv   (y,i);

				if (y.size()!=u-l) mpi_throw_("bad size of received vector, y.size!=Upper-Lower");
				if (x.size()<l) mpi_throw_("bad lower value");
				if (x.size()<u) mpi_throw_("bad upper value");

				for(size_t j=l,k=0;j<u;++j,++k) {
					x[j]=y[k]; // splice data
					v[j]=true;
				}
			}
			for(size_t i=0;i<v.size();++i) if (!v[i]) mpi_throw_("hull vector, missing some data in CollectVector");
			for(size_t i=0;i<r.size();++i) if (!r[i]) mpi_throw_("missing some nodes in CollectVector");

			if (tim.elapsed()>60*60) *this << "% collect vector: " << tim << mpiendl;
		}
	}

	template<class T,class R>
 	void CollectMap(map<T,R>& x,const bool binary=true,const int mode=0) const
	{
		timer tim;
		if (Size()==1) return;
		if (mode<0 && mode>2) mpi_throw_("bad mode in CollectMap");

		if (!isMaster()) {
			SendInt(Rank(),0,TAG_COLLECT_MAP);
			if (binary) SendBin(x,0);
			else        Send   (x,0);
		} else {
			vector<bool> r(Size());
			for(size_t i=0;i<r.size();++i) r[i]=(i==0);

			//size_t count=0;
			//while(++count<Size())
			for(size_t count=1;count<Size();++count)
			{
				//const size_t i=RecvInt(MPI_ANY_SOURCE,TAG_COLLECT_MAP);
				const size_t i=RecvInt(count,TAG_COLLECT_MAP);
				if (r[i] || i==0 || i>Size() || i!=count) mpi_throw_("Bad receive node in CollectMap, i=" + tostring(i));
				r[i]=true;

				map<T,R> y;
				if (binary) RecvBin(y,i);
				else 		Recv   (y,i);

				for(typename map<T,R>::const_iterator itt=y.begin();itt!=y.end();++itt) {
					const T& t=itt->first;
					const R& r=itt->second;
					if      (mode==0 && x.find(t)!=x.end()) {mpi_throw_("map not uniqe in CollectMap");}
					else if (mode==2 && x.find(t)!=x.end()) x[t] += r;
					else x[t]=r; // splice data
				}
			}
			for(size_t i=0;i<r.size();++i) if (!r[i]) mpi_throw_("missing some nodes in CollectMap");
			if (tim.elapsed()>60*60) *this << "% collect vector: " << tim << mpiendl;
		}
	}

};

/*
	class partition
	{
		private:
			const size_t m_rank,m_size,m_N;

			static size_t lower(const size_t r,const size_t s,const size_t n) {assert(r<s && s!=0); const size_t x=r*(n/s); assert(x<n); return x;}
			static size_t upper(const size_t r,const size_t s,const size_t n) {assert(r<s && s!=0); const size_t x=r+1==s ? n : lower(r,s,n)+n/s; assert(x<n && x>=lower(r,s,n) ); return x;}

		public:
			partition(const size_t rank,const size_t size,const size_t N)
			: m_rank(rank), m_size(size), m_N(N) {assert(m_size>0);}

			partition(const Mpi& m,const size_t N)
			: m_rank(m.Rank()), m_size(m.Size()), m_N(N) {assert(m_size>0);}

			size_t rank() const {return m_rank;}
			size_t size() const {return m_size;}
			size_t N   () const {return m_N;}
			size_t partitionsize ()               const {return partitionsize(m_rank);}
			size_t partitionsize (const size_t r) const {return upper(r)-lower(r);}

			size_t lower()               const {return lower(m_rank);}
			size_t upper()               const {return upper(m_rank);}
			size_t lower(const size_t r) const {return lower(r,m_size,m_N);}
			size_t upper(const size_t r) const {return upper(r,m_size,m_N);}

			friend ostream& operator<<(ostream& s,const partition& x) {return s << "partition: rank=" << x.rank() << " size=" << x.size() << " N=" << x.N() << " lower=" + x.lower() << " upper=" << x.upper();}
	};
	//	size_t Lower(const size_t n) const {return partition(*this,n).lower();}
	//	size_t Upper(const size_t n) const {return partition(*this,n).upper();}

 	void BroadcastInt(int& n,const int node) const
 	{
		if (0!=MPI_Bcast(&n, 1, MPI_INT, node, MPI_COMM_WORLD)) mpi_throw_("BroadcastInt() failed");
	}

	void BroadcastString(string& s,const int node) const
 	{
		int n=s.size();
		BroadcastInt(n,node);
		s.resize(n);
		assert( s.size()==size_t(n) );
		char* pc=const_cast<char*>(s.c_str());
		if (0!=MPI_Bcast(pc, s.size(), MPI_CHAR, node, MPI_COMM_WORLD)) mpi_throw_("BroadcastString() failed");
	}

  	template<class T> void Broadcast (T& x,const int node) const
  	{
		string s=tostring(x);
		BroadcastString(s,node);
		if (static_cast<const size_t>(node)!=Rank()) x=totype<T>(s);
  	}

*/

#endif // USE_MPI
#endif // __MPI_H__
