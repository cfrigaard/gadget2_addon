# Makefile for Gadget-add-ons

CC =mpicc # gcc
CPP=mpicxx # g++

# possible code options:
#   NDEBUG     : enable release version (no asserts)
#   USE_MPI    : enable mpi parallel processing, sync with g++/mpicxx above, and -fPic below
#   USE_UNITS  : use quan unit system, only needed for debug check compilation, do not run binaries with this option
#   USE_FFTW_IC: use external fftw2 or fftw3 lib for IC generation only
#                also enable link -ldrfftw -ldfftw or -lsrfftw -lsfftw below
#   USE_FFTW   : use exeternal fftw3 or internal FFT lib, also enable link -lfftw3 below
#   USE_FLOAT_CHECKS : check float values, for debug mostly

VERSION  = 0.8

OPTIONS  = -DVERSION=$(VERSION) -DARC_$(shell uname -m) -DOS_$(shell uname -s)
# 64 bit athlon64: OPTIONS +=  -m64 -march=athlon64 -mtune=athlon64 # -mfpmath=sse -mmmx -msse -msse2 -msse3 # -march=$(shell uname -m)
# 32 bit :
OPTIONS += -mfpmath=sse -mmmx -msse -msse2 -msse3 -march=$(shell uname -m) # -m32 -march=athlon64
OPTIONS += -DUSE_MPI -DNDEBUG # -DUSE_UNITS -DUSE_FLOAT_CHECKS -DUSE_FFTW_IC -DUSE_FFTW -g
OPTIONS += -O3

SRCHOME_DIR = ~/Spec#        # only needed for tar (backup) and pub making
INSTALL_DIR = ~/Bin/cosmo_local#   # where to put binaries when installing
BACKUP_DIR  = $(SRCHOME_DIR)/Backup/BackupSpeciale# backupdir for make tar

INC_BOOST=-I/usr/include/boost -I/usr/local/include/boost
LIB_BOOST=
INC_FFTW=
LIB_FFTW= #-lfftw3 # -ldrfftw -ldfftw

CFLAGS  =$(INC_FFTW)              -fPIC -Wall
CPPFLAGS=$(INC_FFTW) $(INC_BOOST) -fPIC -Wall -Winit-self # to pedantic warns: -pedantic -Wshadow -Wold-style-cast -Weffc++ -Wunreachable-code

CLINK_OPTIONS  =
CPPLINK_OPTIONS=$(LIB_BOOST) $(LIB_FFTW) $(LIB_SPEC)

CFILES  =$(shell ls *.c   */*.c   2>/dev/null)
CPPFILES=$(shell ls *.cpp */*.cpp 2>/dev/null)
BINS    =$(sort convert density gdiff gendata ghead gfilter grafic2gadget massinsphere nbodysolve power testsuite toascii twopointcorr)
OO_DEP  =mainstubs.oo

.PHONY: all
all: $(LIB_SPEC) $(BINS)
	@ echo ; echo "** build ok"
	@ ./testsuite
	@ echo "** test ok" ; echo

convert:   convert.oo $(OO_DEP)
	$(CPP) $+ $(LIB_SPEC) -o $@ $(CPPLINK_OPTIONS)

density:   density.oo $(OO_DEP)
	$(CPP) $+ $(LIB_SPEC) -o $@ $(CPPLINK_OPTIONS)

gdiff:     gdiff.oo $(OO_DEP)
	$(CPP) $+ $(LIB_SPEC) -o $@ $(CPPLINK_OPTIONS)

gendata:   gendata.oo $(OO_DEP)
	$(CPP) $+ $(LIB_SPEC) -o $@ $(CPPLINK_OPTIONS)

ghead:     ghead.oo $(OO_DEP)
	$(CPP) $+ $(LIB_SPEC) -o $@ $(CPPLINK_OPTIONS)

gfilter:   gfilter.oo $(OO_DEP)
	$(CPP) $+ $(LIB_SPEC) -o $@ $(CPPLINK_OPTIONS)

grafic2gadget:   grafic2gadget.oo $(OO_DEP)
	$(CPP) $+ $(LIB_SPEC) -o $@ $(CPPLINK_OPTIONS)

massinsphere: massinsphere.oo $(OO_DEP)
	$(CPP) $+ $(LIB_SPEC) -o $@ $(CPPLINK_OPTIONS)

nbodysolve: nbodysolve.oo $(OO_DEP)
	$(CPP) $+ $(LIB_SPEC) -o $@ $(CPPLINK_OPTIONS)

power:     power.oo $(OO_DEP)
	$(CPP) $+ $(LIB_SPEC) -o $@ $(CPPLINK_OPTIONS)

testsuite: testsuite.oo
	$(CPP) $+ $(LIB_SPEC) -o $@ $(CPPLINK_OPTIONS)

toascii:   toascii.oo $(OO_DEP)
	$(CPP) $+ $(LIB_SPEC) -o $@ $(CPPLINK_OPTIONS)

twopointcorr: twopointcorr.oo $(OO_DEP)
	$(CPP) $+ $(LIB_SPEC) -o $@ $(CPPLINK_OPTIONS)

# for building shared library
#LIB_SPEC=libspec.so # use, when building a shared lib, diabled in public makefile
#$(LIB_SPEC): $(OO_DEP)
#	$(CPP) -shared -o $@ -Wl,-soname,$@ $+

include Makefile_base

.PHONY: install
install: $(LIB_SPEC) $(BINS)
	@ echo copying $+ to install dirs
	@ cp -u $+ $(INSTALL_DIR) # --attribute=-

.PHONY: countlines
countlines:
	@ cat *.c *.cpp *.h | wc ; wc testsuite.cpp

.PHONY: pub
PUBVERSION=gadget_addon-$(VERSION)
PUBCD=$(SRCHOME_DIR)/Src
pub: # will only work for me
	@ mkdir -p man ; cp -u $(SRCHOME_DIR)/LaTeX/Extra/*.1 man/
	@ - pushd $(PUBCD)/Speciale ; rm -f *~ ;  touch * ; popd
	@ pushd $(PUBCD) ; tar czhf t.tgz Speciale/ --exclude=Speciale/Test --anchored; popd
	@ pushd $(PUBCD) ; mkdir -p temp ; cd temp ; tar xzf ../t.tgz ; rm -f ../t.tgz ; rm -rf $(PUBVERSION) ; mv Speciale $(PUBVERSION) ; cd $(PUBVERSION); make clean; rm -f *~ ; cd .. ; tar czf $(PUBVERSION).tgz $(PUBVERSION) ; mv $(PUBVERSION).tgz .. ; rm $(PUBVERSION) -r ; popd
	@ pushd $(PUBCD)/Speciale ; tar czf $(PUBCD)/$(PUBVERSION)-testfiles.tgz Test ; popd

.PHONY: clean
clean: clean_base
	@ rm -rf *.dep* testsuite.txt testsuite_slave*.txt $(BINS) $(LIB_SPEC)